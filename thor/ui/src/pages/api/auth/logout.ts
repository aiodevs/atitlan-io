import type { APIContext, APIRoute } from "astro";
import { Config } from "@/utils/config";

export const POST: APIRoute = async (astro: APIContext) => {
  const path = Config.API_BASE_URL + "auth/logout";
  const cookie = astro.request.headers.get('cookie');
  if(!cookie){
    return new Response(
      JSON.stringify({
        message: "No session",
      }),
      { status: 400 }
    );
  }
  try{
  const response: Response = await fetch(path, {
    method: "POST",
    headers: { "cookie": cookie },
  });

  const body = await response.json();
  return new Response(
    JSON.stringify({
      message: body.message,
    }),
    { status: response.status }
  );}
  catch(error){
    console.log(error);
    return new Response(
      JSON.stringify({
        message: "Internal Server Error",
      }),
      { status: 500 }
    );
  }
};
