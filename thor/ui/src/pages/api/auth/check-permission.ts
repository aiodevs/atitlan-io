import type { APIContext, APIRoute } from "astro";
import { Config } from "@/utils/config";

export const GET: APIRoute = async (astro: APIContext) => {
    const cookie = astro.request.headers.get('cookie');
    if (!cookie) {
        return new Response(
            JSON.stringify({
                message: "No session",
            }),
            { status: 400 }
        );
    }

    const action = astro.request.headers.get('X-Action');
    const resource = astro.request.headers.get('X-Resource');
    if (!action || !resource) {
        return new Response(
            JSON.stringify({
                message: "Missing required fields",
            }),
            { status: 400 }
        );
    }

    // console.log("Checking permission for action: " + action + " and resource: " + resource);
    const path = Config.API_BASE_URL + "auth/check-permission";
    try {
        const response: Response = await fetch(path, {
            method: "POST",
            headers: { "Content-Type": "application/json", "Cookie": cookie },
            body: JSON.stringify({ action: action, resource: resource }),
        });
        console.log("Checking permission for resource: " + resource + " and action: " + action + "")
        const body = await response.json();
        console.log("Response from API: " + JSON.stringify(body) + " with status: " + response.status);
        if (response.status == 401) {
            return astro.redirect("/login");
        }

        return new Response(
            JSON.stringify({
                message: body.message,
            }),
            { status: response.status }
        );
    }
    catch (error) {
        console.log(error);
        return new Response(
            JSON.stringify({
                message: "Internal Server Error",
            }),
            { status: 500 }
        );
    }
};
