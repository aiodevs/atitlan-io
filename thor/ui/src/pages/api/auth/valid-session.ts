import type { APIContext, APIRoute } from "astro";
import { Config } from "@/utils/config";

export const GET: APIRoute = async (astro: APIContext) => {
  const path = Config.API_BASE_URL + "auth/valid-session";
  const cookie = astro.request.headers.get('cookie');
  if (!cookie) {
    return new Response(
      JSON.stringify({
        message: "No session",
      }),
      { status: 400 }
    );
  }
  try {
    const response: Response = await fetch(path, {
      method: "GET",
      headers: { "cookie": cookie },
    });

    const body = await response.json();
    return new Response(
      JSON.stringify({
        message: body.message,
        user: body.user,
      }),
      { status: response.status }
    );
  }
  catch (error) {
    console.log(error);
    return new Response(
      JSON.stringify({
        message: "Internal Server Error",
      }),
      { status: 500 }
    );
  }
};
