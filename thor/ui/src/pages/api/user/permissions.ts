import type { APIContext, APIRoute } from "astro";
import { Config } from "@/utils/config";

export const GET: APIRoute = async (astro: APIContext) => {
    console.log("GET /api/user/permissions");
    const cookie = astro.request.headers.get('cookie');
    if (!cookie) {
        return new Response(
            JSON.stringify({
                message: "No session",
            }),
            { status: 400 }
        );
    }

    const path = Config.API_BASE_URL + "user/permissions";

    try {
        const response: Response = await fetch(path, {
            method: "GET",
            headers: { "Content-Type": "application/json", "Cookie": cookie },
        });

        const body = await response.json();
        if (response.status == 401) {
            return astro.redirect("/login");
        }

        return new Response(
            JSON.stringify({
                message: body.message,
                permissions: body.permissions
            }),
            { status: response.status }
        );
    }
    catch (error) {
        console.log(error);
        return new Response(
            JSON.stringify({
                message: "Internal Server Error",
            }),
            { status: 500 }
        );
    }
};
