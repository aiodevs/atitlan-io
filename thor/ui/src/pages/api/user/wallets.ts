import type { APIContext, APIRoute } from "astro";
import { Config } from "@/utils/config";
import { ApiCurrencyBalance, ApiWalletGeneralInfo, DisplayWalletGeneralInfo } from "@/types/types";

export const GET: APIRoute = async (astro: APIContext) => {
    const cookie = astro.request.headers.get('cookie');
    if (!cookie) {
        return new Response(
            JSON.stringify({
                message: "No session",
            }),
            { status: 400 }
        );
    }

    const path = Config.API_BASE_URL + "user/wallets?balance=true";

    try {
        const response: Response = await fetch(path, {
            method: "GET",
            headers: { "Content-Type": "application/json", "Cookie": cookie },
        });

        if (response.status == 401) {
            return astro.redirect("/login");
        }
        
        const body = await response.json();
        const apiWallets: ApiWalletGeneralInfo[] = body.wallets;

        return new Response(
            JSON.stringify({
                message: body.message,
                wallets: apiWallets,  
            }),
            { status: response.status }
        );
    }
    catch (error) {
        console.log(error);
        return new Response(
            JSON.stringify({
                message: "Internal Server Error",
            }),
            { status: 500 }
        );
    }
};
