import { env } from "process";
import dotenv from "dotenv";

dotenv.config();

export class Config {
    public static API_HOST_URL = env.THOR_API_ROOT_URL || "http://localhost:3000";
    public static API_API_PATH = env.THOR_API_PATH_URL || "/api/v1/";
    public static API_BASE_URL = this.API_HOST_URL + this.API_API_PATH;
    public static UI_BASE_URL = env.THOR_UI_ROOT_URL || "http://localhost:4321";
    public static UI_PORT: number = env.THOR_UI_PORT ? parseInt(env.THOR_UI_PORT) : 4321;
}
