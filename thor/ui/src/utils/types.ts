
export interface Permission {
    id: string;
    userId: string;
    action: string;
    resource: string;
    expiresAt: string;
    lastUsed: string;
    createdAt: string;
}