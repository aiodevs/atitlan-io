import { AstroBoss } from "../types/types";

export async function delay() {
    return new Promise(resolve => setTimeout(resolve, 500));
}

export function getSessionCookie(Astro: AstroBoss) : string | null {
    const cookie = Astro.request.headers.get("cookie");
    if (!cookie || !cookie.includes("aio-userSession")) {
        console.log("No session");
        return null;
    }
    return cookie;
}