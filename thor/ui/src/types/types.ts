import { AstroGlobal } from "astro";
import { AstroComponentFactory } from "astro/runtime/server/index.js";
import Decimal from "decimal.js";

export type AstroBoss = Readonly<AstroGlobal<Record<string, any>, AstroComponentFactory, Record<string, string | undefined>>>;

export interface Permission {
    id: string;
    userId: string;
    action: string;
    resource: string;
    expiresAt: string;
    lastUsed: string;
    createdAt: string;
}

export interface User {
    id: string;
    email: string;
    displayName: string;
}

export interface ApiCurrencyBalance {
    incoming: string;
    outgoing: string;
    net: string;
}

export interface ApiWalletGeneralInfo {
  id: string;
  name: string;
  createdAt: string;
  updatedAt: string;
  organizationName: string;
  balance: Record<string, ApiCurrencyBalance>;
  paygmentGateway: {
      id: string;
      createdAt: string;
      updatedAt: string;
      disabledAt: string | null;
      name: string;
      paymentIntegrationChannel: string;
  } | null;
}

export interface DisplayWalletGeneralInfo {
  id: string;
  name: string;
  createdAt: string;
  updatedAt: string;
  organizationName: string;
  displayBalance: {
    currency: string;
    amount: string;
  };
  balanceSheet: {
    currency: string;
    incoming: Decimal;
    outgoing: Decimal;
    net: Decimal;
  }[];
  paygmentGateway: {
    id: string;
    createdAt: string;
    updatedAt: string;
    disabledAt: Date | null;
    name: string;
    paymentIntegrationChannel: string;
  } | null;
  walletIndex: number;
}