export enum ButtonState {
  ENABLED = "ENABLED",
  LOADING = "LOADING",
  SUCCESS = "SUCCESS",
  DISABLED = "DISABLED"
}

export function getButtonText(state: ButtonState, enabledText: string, successText: string, disabledText: string): string {
  switch (state) {
    case ButtonState.ENABLED: return enabledText;
    case ButtonState.SUCCESS: return successText;
    case ButtonState.DISABLED: return disabledText;
    default: return "";
  }
}

export async function handleClick(onClick: () => Promise<void>, internalState: any) {
  if (internalState.value !== ButtonState.ENABLED || !onClick) {
    return;
  }
  internalState.value = ButtonState.LOADING;
  await onClick();
  if (internalState.value === ButtonState.LOADING) {
    internalState.value = ButtonState.ENABLED;
  }
}

export function getButtonStateClass(state: ButtonState): string{
  switch (state) {
    case ButtonState.ENABLED: return "enabled cursor-pointer";
    case ButtonState.LOADING: return "loading cursor-progress";
    case ButtonState.SUCCESS: return "success cursor-not-allowed";
    case ButtonState.DISABLED: return "disabled cursor-not-allowed";
    default: return "";
  }
}
