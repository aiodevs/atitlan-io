import { Config } from "@/utils/config";
import type { AstroBoss, Permission, User } from "../types/types";
import { getSessionCookie } from "../utils/helper";

export const auth = async (Astro: AstroBoss)
  : Promise<{
    response: Response;
    user: User | null;
  }> => {
  const url = Config.UI_BASE_URL + "/api/auth/valid-session";
  const cookie = getSessionCookie(Astro);
  if (!cookie) {
    console.log("No session auth");
    return {
      response: Astro.redirect("/login"),
      user: null,
    }
  }
  const result = await fetch(url, {
    method: "GET",
    headers: { cookie: cookie },
  });
  if (!result.ok) {
    return {
      response: Astro.redirect("/login"),
      user: null,
    }
  }

  const body = await result.json();
  return {
    response: result,
    user: {
      id: body.user.id,
      email: body.user.email,
      displayName: body.user.email.split("@")[0].toString(),
    }
  };
};

export const permissions = async (Astro: AstroBoss): Promise<Permission[]> => {
  const cookie = getSessionCookie(Astro);
  if (!cookie) {
    console.log("No session");
    return [];
  }
  const url = Config.UI_BASE_URL + "/api/user/permissions";

  const response = await fetch(url, {
    method: "GET",
    headers: { cookie: cookie },
  });

  if (!response.ok) {
    console.log("No permissions permissions");
    return [];
  }
  const permissionsJson = await response.json();
  return permissionsJson.permissions;
}

export const checkPermission = async (
  Astro: AstroBoss,
  resource: string,
  action: string)
  : Promise<boolean> => {

  const cookie = getSessionCookie(Astro);
  if (!cookie) {
    return false;
  }
  const url = Config.API_HOST_URL + "/api/v1/auth/check-permission";
  const response = await fetch(url, {
    method: "POST",
    headers: { "Content-Type": "application/json", cookie: cookie },
    body: JSON.stringify({ resource: resource, action: action }),
  });

  if (!response.ok) {
    console.log("No permissions checkpermission");
    return false;
  }

  return true;
}
