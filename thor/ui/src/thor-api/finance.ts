import { Config } from "../utils/config";
import { getSessionCookie } from "../utils/helper";
import { AstroBoss, DisplayWalletGeneralInfo, ApiCurrencyBalance } from "../types/types";
import Decimal from "decimal.js";

export const getUserWallets = async (Astro: AstroBoss): Promise<DisplayWalletGeneralInfo[]> => {
    const cookie = getSessionCookie(Astro);
    if (!cookie) {
        console.log("No session");
        return [];
    }
    const url = Config.UI_BASE_URL + "/api/user/wallets";

    const response = await fetch(url, {
        method: "GET",
        headers: { cookie: cookie },
    });
    console.log("Wallets response", response.status, response.statusText, response.ok);

    if (!response.ok) {
        console.log("No wallets");
        return [];
    }
    const bodyJson = await response.json();
    const wallets: {
        id: string;
        name: string;
        createdAt: string;
        updatedAt: string;
        organizationName: string;
        balance: Record<string, ApiCurrencyBalance>;
        paymentGateway: any | null;
    }[] = bodyJson.wallets;

    const remapped: DisplayWalletGeneralInfo[] = [];
    for (const [i, wallet] of wallets.entries()) {
      if (!!wallet.organizationName && wallet.organizationName !== "") {
        continue;
      }

      let displaySum: Decimal = new Decimal(0);
      let displayCurrency: string = "USD";

      const balanceSheet = Object.entries(wallet.balance).map(
        ([currency, balanceData]) => {
          const { incoming, outgoing, net } = balanceData as ApiCurrencyBalance;
          // TODO: Add currency conversion, this is bs code
          if (currency !== displayCurrency) {
            displaySum = new Decimal(net);
            displayCurrency = currency;
          } else {
            displaySum = displaySum.plus(net);
          }

          const row = {
            currency,
            incoming: new Decimal(incoming),
            outgoing: new Decimal(outgoing),
            net: new Decimal(net),
          };
          return row;
        }
      );

      const remappedData: DisplayWalletGeneralInfo = {
        id: wallet.id,
        name: wallet.name,
        createdAt: wallet.createdAt,
        updatedAt: wallet.updatedAt,
        organizationName: wallet.organizationName,
        displayBalance: {
          currency: displayCurrency,
          amount: displaySum.toString(),
        },
        balanceSheet: balanceSheet,
        paygmentGateway: wallet.paymentGateway || null,
        walletIndex: i,
      };
      remapped.push(remappedData);
    }
    remapped.sort((a, b) =>
      a.displayBalance.amount < b.displayBalance.amount ? 1 : -1
    );

    for (const [i, wallet] of remapped.entries()) {
      wallet.walletIndex = i;
    }

    return remapped;
}