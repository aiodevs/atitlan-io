export interface Theme {
  background: string;
  backgroundAlternate: string;
  foreground: string;

  font: string;
  fontSize: string;
  textColorAlternate: string;

  buttonColor: string;
  buttonTextColor: string;
  AppButtonColor: string;
}

export const defaultTheme: Theme = {
  background: 'hsl(0, 0%, 98%)',
  backgroundAlternate: 'hsl(0, 0%, 95%)',
  foreground: '#252627',

  font: 'montserrat, Arial, sans-serif',
  fontSize: '16px',

  textColorAlternate: '#22031F',

  buttonColor: '#FFFF78',
  buttonTextColor: '#22031F',
  AppButtonColor: '#FFFFFF'
  
};