module.exports = {
    plugins: [
        require('cssnano')(),
        require('autoprefixer')(),
        require('postcss-property-lookup')(),
        require('postcss-advanced-variables')(),
        require("tailwindcss/nesting"),
    ],
};