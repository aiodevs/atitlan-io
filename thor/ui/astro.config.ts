import { defineConfig } from 'astro/config';
import vue from '@astrojs/vue';
import node from "@astrojs/node";
import tailwind from "@astrojs/tailwind";
import { Config } from './src/utils/config';

// https://astro.build/config
export default defineConfig({
  // Enable Vue to support Vue components.
  integrations: [vue(), tailwind({applyBaseStyles: false,})],
  output: "server",
  adapter: node({
    mode: "standalone"
  }),
  server: {
    port: Config.UI_PORT as number
  },
});