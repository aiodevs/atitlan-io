#!/bin/bash
THOR_NETWORK=${1:-$THOR_NETWORK}

echo "Checking creation of networks needed swarm:\"$THOR_NETWORK\""

if [ -n "$THOR_NETWORK" ]; then
  if ! docker network ls | grep -q $THOR_NETWORK; then
    echo "Creating network $THOR_NETWORK"
    docker network create --driver=overlay --attachable $THOR_NETWORK
  else
    echo "Network already exists $THOR_NETWORK"
  fi
else
  echo "No swarm network name provided, skipping creation."
fi
