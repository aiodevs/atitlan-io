#!/bin/bash

# Define your parameters
CUSTOMER_EMAIL="customer@example.com"
CUSTOMER_NAME="A VD ZALM"
AMOUNT="10000"
PAYMENT_GATEWAY_ID="2"
TRANSACTION_INTENT_ID="1"

# Read the fixture file and replace placeholders
sed -e "s/{{CUSTOMER_EMAIL}}/$CUSTOMER_EMAIL/g" \
    -e "s/{{CUSTOMER_NAME}}/$CUSTOMER_NAME/g" \
    -e "s/{{AMOUNT}}/$AMOUNT/g" \
    -e "s/{{PAYMENT_GATEWAY_ID}}/$PAYMENT_GATEWAY_ID/g" \
    -e "s/{{TRANSACTION_INTENT_ID}}/$TRANSACTION_INTENT_ID/g" \
    checkout-session-completed-fixtures.template.json > checkout-completed.json

# Run the fixtures with the Stripe CLI
stripe fixtures checkout-completed.json
