import fs from 'fs/promises';
import path from 'path';
import clipboardy from 'clipboardy';

async function main() {
    const args = process.argv.slice(2); // Get arguments, excluding the first two elements
    if (args.length < 2) {
        console.error('Please provide a component name and the path to components.');
        process.exit(1);
    }

    const componentName = args[1];
    const componentsDirectory = args[0] + "/" + componentName; // The path to your components directory from arguments

    try {
        const prompts = await generateTestPrompts(componentName, componentsDirectory);
        const combinedPrompts = prompts.join('\n\n');
        clipboardy.writeSync(combinedPrompts);
        console.log('Prompts copied to clipboard!');
    } catch (error: any) {
        console.error(error.message);
    }
}

main();

async function readFileContent(filePath: string): Promise<string> {
    return fs.readFile(filePath, 'utf8');
}

export type Endpoint = {
    method: string;
    url: string;
    schema: string;
};

async function extractEndpoints(routeContent: string): Promise<Endpoint[]> {
    const endpointRegex = /fastify\.route\(\{\s*method:\s*"(.*?)",\s*url:\s*`(.*?)`,\s*schema:\s*(.*?),/gs;
    const endpoints: Endpoint[] = [];
    console.log(`Route content: ${routeContent}`);
    let match;
    while ((match = endpointRegex.exec(routeContent)) !== null) {
        const method = match[1].toUpperCase();
        const url = match[2];
        const schema = match[3].trim();
        console.log(`Match: ${method} ${url} ${schema}`);
        endpoints.push({ method, url, schema });
    }

    return endpoints;
}

function generatePrompts(componentName: string, source: string[], endpoints: any[]): string[] {
    const prompts: string[] = [];

    // First prompt including all component source material
    prompts.push(`Source code for component "${componentName}":\n${source.join('\n\n')}`);
    console.log(`Endpoints: ${JSON.stringify(endpoints, null, 2)}`)
    // Subsequent prompts for each endpoint
    endpoints.forEach(endpoint => {
        prompts.push(`Based on the provided source code for "${componentName}", write an integration test for the "${endpoint.method} ${endpoint.url}" endpoint using schema ${endpoint.schema}. Ensure to cover various scenarios including success cases and possible error conditions. Please check that all the fields of an expected output are teFACTORYsted, use schema knowledge for that. Please suggest the creation of new test-data-factory types to improve dry on the tests.`);
    });

    return prompts;
}

async function retrieveComponentsAndSchema(componentName: string, componentsDirectory: string): Promise<string[]> {
    let content: string[] = [];
    const fileTypes = ['controller', 'service', 'route', 'schema', 'type', 'util'];

    // Retrieve content of each component file
    for (const type of fileTypes) {
        const fileName = `${componentName}.${type}.ts`;
        const filePath = path.join(componentsDirectory, fileName);
        try {
            const fileContent = await readFileContent(filePath);
            content.push(`// ${type.toUpperCase()} \n// ${fileName} \n${fileContent}`);
        } catch (error) {
            console.warn(`Warning: ${type} file for component "${componentName}" not found.`);
        }
    }

    try {
        const schema = await readFileContent("src/prisma/schema.prisma");
        content.push(`// SCHEMA \n// schema.prisma \n${schema}`);

        const testData = await readFileContent("test/integration/util/test-data-factory.ts");
        content.push(`// TEST DATA FACTORY \n// test-data-factory.ts \n${testData}`);

        const integrationTestSuiteUtil = await readFileContent("test/integration/util/integration-test.ts");
        content.push(`// TEST UTIL \n// integration-test.ts \n${integrationTestSuiteUtil}`);

        const exampleTest = await readFileContent("test/integration/components/finance/recurrente.test.ts");
        content.push(`// EXAMPLE TEST, please pay attention to how this class is set up and what kind of overloaded test it is using and do the same for your new tests. \n// wallet.test.ts \n${exampleTest}`);

        const packageJson = await readFileContent("package.json");
        content.push(`// PACKAGE.JSON \n// package.json \n${packageJson}`);


    } catch (error) {
        console.warn(`Warning: prisma schema not found.`);
    }

    return content;
}

async function generateTestPrompts(componentName: string, componentsDirectory: string): Promise<string[]> {
    let source: string[] = await retrieveComponentsAndSchema(componentName, componentsDirectory);

    const routeContent = source.find(content => content.includes('// ROUTE'));
    if (!routeContent) {
        throw new Error(`Route content for component "${componentName}" not found.`);
    }

    const endpoints = await extractEndpoints(routeContent);
    return generatePrompts(componentName, source, endpoints);
}