import { exec as node_exec } from "node:child_process";
import { promisify } from "node:util";
import { UUID, randomUUID } from "node:crypto";
import { getPrisma } from "../../../src/util/prisma";
import fs from "fs/promises";

const exec = promisify(node_exec);

export interface schemaUrl {
  url: string;
  schema: UUID;
}

export interface envProps {
  user: string;
  password: string;
  db: string;
  port: string;
  host: string;
}

export default class TestUtils {
  public static async resetDBviaPrisma(): Promise<void> {
    // Delete child table records first
    await getPrisma().completedTransactionLink.deleteMany();
    await getPrisma().paymentGatewayApiKeyLink.deleteMany();
    await getPrisma().paymentGatewaySettingLink.deleteMany();
    await getPrisma().transactionIntent.deleteMany();
    await getPrisma().userOrganizationRoleAccessLink.deleteMany();
    await getPrisma().userWalletRoleAccessLink.deleteMany();
    await getPrisma().organizationRoleLink.deleteMany();
    await getPrisma().organizationUserWalletRoleAccessLink.deleteMany();
    await getPrisma().organizationWalletRoleAccessLink.deleteMany();
    await getPrisma().permissionRoleLink.deleteMany();

    await getPrisma().invoice.deleteMany();
    await getPrisma().apiKey.deleteMany();

    // The order of deletion matters
    await getPrisma().transaction.deleteMany();
    await getPrisma().paymentGateway.deleteMany();
    await getPrisma().wallet.deleteMany();
    await getPrisma().organization.deleteMany();
    await getPrisma().role.deleteMany();

    // Finally, delete the remaining tables
    await getPrisma().userSession.deleteMany();
    await getPrisma().key.deleteMany();
    await getPrisma().permission.deleteMany();
    await getPrisma().user.deleteMany();
  }

  public static getEnvProperties(): envProps {
    const user = process.env.THOR_API_DB_USER;
    const password = process.env.THOR_API_DB_PASSWORD;
    const db = process.env.THOR_API_DB_SCHEMA;
    const port = process.env.THOR_API_DB_PORT;
    const host = process.env.THOR_API_DB_HOST;
    if (!user || !password || !db || !port || !host)
      throw new Error("Missing env vars");
    return { user, password, db, port, host };
  }

  public static async initDB(databaseURL: string): Promise<void> {
    try {
      const command = `DATABASE_URL=${databaseURL} pnpm prisma migrate deploy --schema ./src/prisma/schema.prisma`;
      const result = await exec(command);
    } catch (error) {
      console.error("Exec error:", error);
    }
  }

  public static databaseURL({ user, password, host, port, db }: envProps) {
    return `postgresql://${user}:${password}@${host}:${port}/${db}?schema=public`;
  }

  public static setRandomDatabaseUrlSchema(databaseUrl?: string): schemaUrl {
    const schema = randomUUID();
    const dbUrl = databaseUrl ? databaseUrl : process.env.DATABASE_URL;
    if (!dbUrl) {
      throw new Error("Please provide a DATABASE_URL environment variable.");
    }

    const url = new URL(dbUrl);
    url.searchParams.set("schema", schema);

    return { url: url.toString(), schema: schema };
  }

  public static async readFileAndReplaceTextToJson(
    filePath: string,
    replacements: Record<string, string | number | boolean>
  ) {
    const fileContent = await fs.readFile(filePath, "utf8");
    let modifiedContent = fileContent;

    for (const [placeholder, value] of Object.entries(replacements)) {
      const regex = new RegExp(`\\$\{${placeholder}\}`, "g");
      modifiedContent = modifiedContent.replace(regex, String(value));
    }

    return JSON.parse(modifiedContent);
  }

  public static async readFileToJson(filePath: string) {
    const fileContent = await fs.readFile(filePath, "utf8");
    return JSON.parse(fileContent);
  }
}
