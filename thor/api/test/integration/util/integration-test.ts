import Api from "../../../src/api";
import supertest from "supertest";
import { SpyInstance, TestContext, vi } from "vitest";
import TestUtils from "./test-util";
import { StripeFactory, createStripe } from "../../../src/util/stripe-factory";
import { AdminWithWalletAndPermissions } from "../factories/test-data-factory";

// TODO speedup:
// - migrate reset without any fancy schema switching
// or
// - parrelize everything including the migrate with a env bind
// TODO move before... to api-integration-setup.ts
// Clone schema
// Set up one container for each test suite
// Create a db reset function that manually deletes all the data using  prisma.deleteMany

export interface ApiIntegrationTest extends TestContext {
  api: Api;
  server: supertest.SuperTest<supertest.Test>;
  beforeTime: number;
  startTime: number;
  logSpy: SpyInstance<[message?: any, ...optionalParams: any[]], void>;
  fetchSpy: SpyInstance<[input: string | URL | Request, init?: RequestInit | undefined], Promise<Response>>;
  admin: AdminWithWalletAndPermissions | null;
}

export interface testSuite {
  server: supertest.SuperTest<supertest.Test>;
}

export class IntegrationTestFastResetDB {
  public static async beforeAll(
    port?: string,
    stripeFactory: StripeFactory = createStripe
  ): Promise<supertest.SuperTest<supertest.Test>> {
    const start = Date.now();

    const props = TestUtils.getEnvProperties();
    props.port = port ?? process.env.THOR_API_DB_PORT ?? "5432";
    const url = TestUtils.databaseURL(props);

    await TestUtils.initDB(url);

    const api = new Api(url, stripeFactory);
    await api.app.ready();
    const server = supertest(api.app.server);

    console.log("beforeAll: ", Date.now() - start);

    return server;
  }

  public static async beforeEach(context: ApiIntegrationTest, server) {
    context.startTime = Date.now();
    context.server = server;
    context.fetchSpy = vi.spyOn(global, 'fetch');
    context.logSpy = vi.spyOn(console, 'log');
    await TestUtils.resetDBviaPrisma();

    context.admin = null;

    context.beforeTime = Date.now() - context.startTime;
  };

  public static afterEach = async (context: ApiIntegrationTest) => {
    const start = Date.now();

    const after = Date.now() - start;
    const before = context.beforeTime;
    const totalTime = Date.now() - context.startTime;
    const totalTest = totalTime - after - before;
    console.log(
      `Test took: ${totalTest}ms before ${before}ms after ${after}ms total: ${totalTime}ms | name: ${context.task.name} | mode: ${context.task.mode} | concurrent: ${context.task.concurrent}`
    );
  };
}
