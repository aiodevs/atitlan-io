import path from 'path';
import { defineConfig } from 'vitest/config';

export default defineConfig({
  resolve: {
    alias: {
      '@': path.join(process.cwd(), 'src'),
    },
  },
  build: {
    outDir: './dist',
  },
  test: {
    testTimeout: 100000,
    hookTimeout: 100000,
    threads: false, //https://github.com/vitest-dev/vitest/issues/3143
    coverage: {
      enabled: true,
      provider: 'istanbul',
      reporter: [
        ['cobertura'],
        ['text'],
        ['text-summary'],
        ['html'],
      ],
      cleanOnRerun: false,
      reportOnFailure: true,
      reportsDirectory: './test/reports/',
    },
    reporters: ['junit', 'html'],
    outputFile: {
      junit: './test/reports/e2e-report.xml',
      html: './test/reports/e2e-report.html'
    }
  },
});
