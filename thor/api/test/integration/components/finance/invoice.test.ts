import { afterEach, beforeAll, beforeEach, expect, test } from "vitest";
import supertest from "supertest";
import { createInvoice, createUser } from "../../factories/test-data-factory";
import { IntegrationTestFastResetDB, ApiIntegrationTest } from "../../util/integration-test";
import AtitlanApiRoutes from "../../../../src/components/shared/routes";
import { createUserOwnedWallet } from "../../factories/wallets.factory";

let server: supertest.SuperTest<supertest.Test>;

beforeAll(async () => {
    server = await IntegrationTestFastResetDB.beforeAll();
});

beforeEach<ApiIntegrationTest>(async (context: ApiIntegrationTest) => {
    await IntegrationTestFastResetDB.beforeEach(context, server);
});

afterEach<ApiIntegrationTest>(IntegrationTestFastResetDB.afterEach);

test<ApiIntegrationTest>("GET /invoices/get/:id - Success", async (c) => {
    // Create a user and an invoice for testing
    const user = await createUser(c.server, "test@example.com", "password");
    const wallet = await createUserOwnedWallet(c.server, "Test Wallet", user.cookie);
    const invoice = await createInvoice(c.server, {
        amount: "10",
        currency: "BTC",
        description: "Test Invoice",
        targetWalletId: wallet.id,
    }, user.cookie);

    // Test successful retrieval
    const response = await c.server
        .get(AtitlanApiRoutes.INVOICE.GET(invoice.id.toString()))
        .set("Cookie", user.cookie);
    expect(response.status).toBe(200);
    expect(response.body.invoice).toBeDefined();
    expect(response.body.invoice.amount.toString()).toBe("10");
    expect(response.body.invoice.currency).toBe("BTC");
    expect(response.body.invoice.description).toBe("Test Invoice");
    expect(response.body.invoice.targetWalletId.toString()).toBe(wallet.id);
    expect(response.body.invoice.id).toBe(invoice.id);
});

test<ApiIntegrationTest>("GET /invoices/get/:id - Invoice Not Found", async (c) => {
    // Create a user for authentication
    const user = await createUser(c.server, "test@example.com", "password");

    // Test retrieval of a non-existent invoice
    const response = await c.server.get(AtitlanApiRoutes.INVOICE.GET('9999999999')).set("Cookie", user.cookie);
    expect(response.status).toBe(404);
});

test<ApiIntegrationTest>("GET /invoices/get/:id - Unauthorized", async (c) => {
    // Test unauthorized access
    const response = await c.server.get(AtitlanApiRoutes.INVOICE.GET("1"));
    expect(response.status).toBe(401);
});

const createInvoiceUrl = AtitlanApiRoutes.INVOICE.CREATE;

test<ApiIntegrationTest>("POST /invoices/create - Success", async (c) => {
    // Create a user and wallet for testing
    const user = await createUser(c.server, "create-success@example.com", "password");
    const wallet = await createUserOwnedWallet(c.server, "Create Success Wallet", user.cookie);

    const invoiceData = {
        amount: "150",
        currency: "USD",
        description: "Test Invoice Creation",
        targetWalletId: wallet.id,
    };

    // Test successful invoice creation
    const response = await c.server
        .post(createInvoiceUrl)
        .set("Cookie", user.cookie)
        .send(invoiceData);
    console.log(response.status);
    console.log(response.body);
    expect(response.status).toBe(200);
    expect(response.body.invoice).toBeDefined();
    expect(response.body.invoice.amount.toString()).toBe(invoiceData.amount);
    expect(response.body.invoice.currency).toBe(invoiceData.currency);
    expect(response.body.invoice.description).toBe(invoiceData.description);
    expect(response.body.invoice.targetWalletId.toString()).toBe(wallet.id);
});

test<ApiIntegrationTest>("POST /invoices/create - Invalid Data", async (c) => {
    const user = await createUser(c.server, "create-invalid@example.com", "password");
    const wallet = await createUserOwnedWallet(c.server, "Wallet", user.cookie);

    // Invoice creation data with invalid fields
    const invalidInvoiceData = {
        amount: -100,  // Invalid amount
        currency: "DOLLORAS",  // Invalid currency format
        description: "Invalid Invoice",
        targetWalletId: wallet.id + 1,
    };

    const validInvoiceData = {
        amount: 150,
        currency: "USD",
        description: "Test Invoice Creation",
        targetWalletId: wallet.id,  // Example wallet ID
    }

    let response = await c.server
        .post(createInvoiceUrl)
        .set("Cookie", user.cookie)
        .send({
            amount: invalidInvoiceData.amount,
            currency: validInvoiceData.currency,
            description: validInvoiceData.description,
            targetWalletId: validInvoiceData.targetWalletId,
        });
    expect(response.status).toBe(400);

    response = await c.server
        .post(createInvoiceUrl)
        .set("Cookie", user.cookie)
        .send({
            amount: validInvoiceData.amount,
            currency: invalidInvoiceData.currency,
            description: validInvoiceData.description,
            targetWalletId: validInvoiceData.targetWalletId,
        });
    expect(response.status).toBe(400);

    response = await c.server
        .post(createInvoiceUrl)
        .set("Cookie", user.cookie)
        .send({
            amount: validInvoiceData.amount,
            currency: validInvoiceData.currency,
            description: validInvoiceData.description,
            targetWalletId: invalidInvoiceData.targetWalletId,
        });
    expect(response.status).toBe(400);
});

test<ApiIntegrationTest>("POST /invoices/create - Unauthorized", async (c) => {
    const invoiceData = {
        amount: "200",
        currency: "EUR",
        description: "Unauthorized Invoice Creation",
        targetWalletId: "1",
    };

    // Test unauthorized invoice creation attempt
    const response = await c.server
        .post(createInvoiceUrl)
        .send(invoiceData);

    expect(response.status).toBe(401);
});
