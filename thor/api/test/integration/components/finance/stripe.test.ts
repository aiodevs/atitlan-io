import {
    beforeAll,
    expect,
    test,
    afterEach,
    beforeEach,
    vi,
} from "vitest"
import { ApiIntegrationTest, IntegrationTestFastResetDB } from "../../util/integration-test";
import supertest from "supertest";
import { createUser, createAdminWithWalletAndPermissions, createStripePaymentGateway, createInvoice, createStripeHostedCheckout } from "../../factories/test-data-factory";
import { StripeFactory } from "../../../../src/util/stripe-factory";
import Stripe from "stripe";
import TestUtils from "../../util/test-util";
import AtitlanApiRoutes from "../../../../src/components/shared/routes";
import { createUserOwnedWallet, getWalletDetails } from "../../factories/wallets.factory";
import { createTransactionIntent, getTransactionIntent } from "../../factories/transaction-intent";

let server: supertest.SuperTest<supertest.Test>;
let stripeFactory: StripeFactory = (apiKey: string) => stripeMock as Stripe;
const checkoutCompletedTemplatePath = "test/stripe/checkout-session-completed.template.json";
const stripeLoggingWebhookUrl = AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.STRIPE.WEBHOOK_LOGGING;
const stripeCheckoutWebhookUrl = AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.STRIPE.WEBHOOK_CHECKOUT_SESSION;

const stripeCheckoutSessionsMock = {
    create: vi.fn().mockResolvedValue({
        id: 'cs_test_example',
        url: 'https://mock-stripe-checkout-url.com',
    }),
    retrieve: vi.fn(),
    list: vi.fn(),
    expire: vi.fn(),
    listLineItems: vi.fn(),
};

type PartialStripe = {
    [P in keyof Stripe]?: Partial<Stripe[P]>;
};

const constructEventMock = vi.fn((rawBody, sig, endpointSecret) => {
    return JSON.parse(rawBody);
});

export const stripeMock: PartialStripe = {
    checkout: {
        sessions: stripeCheckoutSessionsMock
    },
    webhooks: {
        constructEvent: constructEventMock
    }
};

beforeAll(async () => {
    server = await IntegrationTestFastResetDB.beforeAll(undefined, stripeFactory);
});

beforeEach<ApiIntegrationTest>(async (context: ApiIntegrationTest) => {
    await IntegrationTestFastResetDB.beforeEach(context, server);
});

afterEach<ApiIntegrationTest>(IntegrationTestFastResetDB.afterEach);

test<ApiIntegrationTest>("Stripe - full flow - add funds - 200 ok", async (c) => {
    const publisheableApiKey = "pk_test_4e";
    const endpointSecret = "whsec_4e";
    // const stripeMock = vi.fn().mockImplementation(stripe)
    const user = await createUser(c.server, "user1@atitlan.io", "password");
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@atitlan.io", "password");
    const userWallet = await createUserOwnedWallet(c.server, "My stacks of auro", user.cookie);
    const stripePaymentGateway = await createStripePaymentGateway(
        c.server,
        admin.cookie,
        "AIO-Stripe-PaymentGateway",
        "AIO-Stripe-Wallet",
        "sk_test_4e",
        publisheableApiKey,
        endpointSecret);
    const stripeWallet = await getWalletDetails(c.server, admin.cookie, stripePaymentGateway.sourceWalletId.toString());
    expect(stripeWallet.name).toBe("AIO-Stripe-Wallet");

    const addFundsInvoice = await createInvoice(c.server, {
        amount: "1000",
        currency: "EUR",
        description: "Add funds to my wallet",
        targetWalletId: userWallet.id
    }, user.cookie);
    expect(addFundsInvoice.amount.toString()).toBe("1000");

    const validSession = await c.server
        .get(AtitlanApiRoutes.AUTH.VALID_SESSION)
        .set("Cookie", user.cookie);
    expect(validSession.status).toBe(200);

    const transactionIntent = await createTransactionIntent(
        c.server,
        user.cookie,
        addFundsInvoice.id.toString(),
        userWallet.id.toString());
    expect(transactionIntent.status).toBe("created");

    const stripeHostedCheckout = await createStripeHostedCheckout(
        c.server,
        user.cookie,
        transactionIntent.id.toString(),
        stripePaymentGateway.id.toString());
    expect(stripeHostedCheckout.publisheableApiKey).toBe(publisheableApiKey);
    expect(stripeHostedCheckout.stripeUrl).toBe("https://mock-stripe-checkout-url.com");
    const checkoutReplacements = stripeEventTemplateReplacements(
        "evt_1J9J1f2eZvKYlo2C7eRk3l2z",
        "evt_2J9J1f2eZvKYlo2C7eRk3l2z",
        "evt_3J9J1f2eZvKYlo2C7eRk3l2z",
        "evt_4J9J1f2eZvKYlo2C7eRk3l2z",
        "2020-08-27",
        1598524915,
        1598524914,
        1598524915,
        1598524915,
        "ch_1J9J1f2eZvKYlo2C7eRk3l2z",
        "pi_1J9J1f2eZvKYlo2C7eRk3l2z",
        "cs_1J9J1f2eZvKYlo2C7eRk3l2z",
        1000,
        1000,
        1000,
        "eur",
        user.id.toString(),
        "Add funds to my wallet",
        false,
        transactionIntent.id.toString(),
        stripePaymentGateway.id.toString(),
        "https://thor.atitlan.io/cancel",
        "https://thor.atitlan.io/success"
    )
    const stripeEvents: [any] = await TestUtils.readFileAndReplaceTextToJson(
        checkoutCompletedTemplatePath,
        checkoutReplacements);
    for (const stripeEvent of stripeEvents) {
        const headerStripeSignature = "headerStripeSignature";
        const stripeLoggingEvent = await c.server
            .post(stripeLoggingWebhookUrl)
            .set("Stripe-Signature", "test")
            .send(stripeEvent);
        expect(stripeLoggingEvent.status).toBe(200);
        if (stripeEvent.type === "checkout.session.completed") {
            console.log("checkout_session fixture: ", stripeEvent);
            const stripeCheckoutEvent = await c.server
                .post(stripeCheckoutWebhookUrl)
                .set("Stripe-Signature", headerStripeSignature)
                .send(stripeEvent);
            console.log("stripeCheckoutEvent: ", stripeCheckoutEvent.body);
            console.log("stripeCheckoutEvent: ", stripeCheckoutEvent.status);
            expect(stripeCheckoutEvent.status).toBe(200);
            expect(stripeMock.webhooks?.constructEvent)
                .toHaveBeenCalledWith(
                    expect.anything(),
                    headerStripeSignature,
                    endpointSecret);
        }
    }
    const completedTransactionIntent = await getTransactionIntent(
        c.server,
        user.cookie,
        transactionIntent.id.toString());
    // console.log(JSON.stringify(completedTransactionIntent, null, 2))
    if (!completedTransactionIntent.completedTransactions) {
        throw new Error("Transaction not found");
    }
    const completedTransaction = completedTransactionIntent.completedTransactions[0];

    expect(completedTransactionIntent.status).toBe("success");
    expect(completedTransactionIntent.completedTransactions?.length).toBe(1);
    expect(completedTransaction?.transaction?.id).toBeDefined();
    expect(completedTransaction?.transaction?.sourceCurrency).toBe("EUR");
    expect(completedTransaction?.transaction?.sourceAmount).toBe("1000");
    expect(completedTransaction?.transaction?.targetCurrency).toBe("EUR");
    expect(completedTransaction?.transaction?.targetAmount).toBe("1000");
    expect(completedTransaction?.transaction?.sourceWalletId).toBe(stripeWallet.id);
    expect(completedTransaction?.transaction?.targetWalletId).toBe(userWallet.id);
});

test<ApiIntegrationTest>("Stripe - full flow - pay business - 200 ok", async (c) => {
    // Setup
    const publisheableApiKey = "pk_test_4e";
    const endpointSecret = "whsec_4e";
    const user = await createUser(c.server, "user1@atitlan.io", "password");
    const businessUser = await createUser(c.server, "user2@atitlan.io", "password");
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@atitlan.io", "password");
    const userWallet = await createUserOwnedWallet(c.server, "My stacks of auro", user.cookie);
    const businessWallet = await createUserOwnedWallet(c.server, "Restaurant", businessUser.cookie);
    const stripePaymentGateway = await createStripePaymentGateway(
        c.server,
        admin.cookie,
        "AIO-Stripe-PaymentGateway",
        "AIO-Stripe-Wallet",
        "sk_test_4e",
        publisheableApiKey,
        endpointSecret);
    const stripeWallet = await getWalletDetails(c.server, admin.cookie, stripePaymentGateway.sourceWalletId.toString());
    expect(stripeWallet.name).toBe("AIO-Stripe-Wallet");

    const restaurantInvoice = await createInvoice(c.server, {
        amount: "1000",
        currency: "EUR",
        description: "So much cacao",
        targetWalletId: businessWallet.id
    }, user.cookie);

    const transactionIntent = await createTransactionIntent(
        c.server,
        user.cookie,
        restaurantInvoice.id.toString(),
        userWallet.id.toString());
    expect(transactionIntent.status).toBe("created");

    const stripeHostedCheckout = await createStripeHostedCheckout(
        c.server,
        user.cookie,
        transactionIntent.id.toString(),
        stripePaymentGateway.id.toString());
    expect(stripeHostedCheckout.publisheableApiKey).toBe(publisheableApiKey);
    expect(stripeHostedCheckout.stripeUrl).toBe("https://mock-stripe-checkout-url.com");
    const checkoutReplacements = stripeEventTemplateReplacements(
        "evt_1J9J1f2eZvKYlo2C7eRk3l2z",
        "evt_2J9J1f2eZvKYlo2C7eRk3l2z",
        "evt_3J9J1f2eZvKYlo2C7eRk3l2z",
        "evt_4J9J1f2eZvKYlo2C7eRk3l2z",
        "2020-08-27",
        1598524915,
        1598524914,
        1598524915,
        1598524915,
        "ch_1J9J1f2eZvKYlo2C7eRk3l2z",
        "pi_1J9J1f2eZvKYlo2C7eRk3l2z",
        "cs_1J9J1f2eZvKYlo2C7eRk3l2z",
        1000,
        1000,
        1000,
        "eur",
        user.id.toString(),
        "Add funds to my wallet",
        false,
        transactionIntent.id.toString(),
        stripePaymentGateway.id.toString(),
        "https://thor.atitlan.io/cancel",
        "https://thor.atitlan.io/success"
    )
    const stripeEvents: [any] = await TestUtils.readFileAndReplaceTextToJson(
        checkoutCompletedTemplatePath,
        checkoutReplacements);
    for (const stripeEvent of stripeEvents) {
        const headerStripeSignature = "headerStripeSignature";
        const stripeLoggingEvent = await c.server
            .post(stripeLoggingWebhookUrl)
            .set("Stripe-Signature", "test")
            .send(stripeEvent);
        expect(stripeLoggingEvent.status).toBe(200);
        if (stripeEvent.type === "checkout.session.completed") {
            console.log("checkout_session fixture: ", stripeEvent);
            const stripeCheckoutEvent = await c.server
                .post(stripeCheckoutWebhookUrl)
                .set("Stripe-Signature", headerStripeSignature)
                .send(stripeEvent);
            expect(stripeCheckoutEvent.status).toBe(200);
            expect(stripeMock.webhooks?.constructEvent)
                .toHaveBeenCalledWith(
                    expect.anything(),
                    headerStripeSignature,
                    endpointSecret);
        }
    }
    const completedTransactionIntent = await getTransactionIntent(
        c.server,
        user.cookie,
        transactionIntent.id.toString());
    // console.log(JSON.stringify(completedTransactionIntent, null, 2))
    if (!completedTransactionIntent.completedTransactions) {
        throw new Error("Transaction not found");
    }
    const addFundsCompletedTransaction = completedTransactionIntent.completedTransactions[0];
    const completedInvoiceTransaction = completedTransactionIntent.completedTransactions[1];

    expect(completedTransactionIntent.status).toBe("success");
    expect(completedTransactionIntent.completedTransactions?.length).toBe(2);
    expect(addFundsCompletedTransaction?.transaction?.id).toBeDefined();
    expect(addFundsCompletedTransaction?.transaction?.sourceCurrency).toBe("EUR");
    expect(addFundsCompletedTransaction?.transaction?.sourceAmount).toBe("1000");
    expect(addFundsCompletedTransaction?.transaction?.targetCurrency).toBe("EUR");
    expect(addFundsCompletedTransaction?.transaction?.targetAmount).toBe("1000");
    expect(addFundsCompletedTransaction?.transaction?.sourceWalletId).toBe(stripeWallet.id);
    expect(addFundsCompletedTransaction?.transaction?.targetWalletId).toBe(userWallet.id);

    expect(completedInvoiceTransaction?.transaction?.id).toBeDefined();
    expect(completedInvoiceTransaction?.transaction?.sourceCurrency).toBe("EUR");
    expect(completedInvoiceTransaction?.transaction?.sourceAmount).toBe("1000");
    expect(completedInvoiceTransaction?.transaction?.targetCurrency).toBe("EUR");
    expect(completedInvoiceTransaction?.transaction?.targetAmount).toBe("1000");
    expect(completedInvoiceTransaction?.transaction?.sourceWalletId).toBe(userWallet.id);
    expect(completedInvoiceTransaction?.transaction?.targetWalletId).toBe(businessWallet.id);
});

function stripeEventTemplateReplacements(
    eventId1: string,
    eventId2: string,
    eventId3: string,
    eventId4: string,
    apiVersion: string,
    created1: number,
    created2: number,
    created3: number,
    created4: number,
    chargeId: string,
    paymentIntentId: string,
    checkoutSessionId: string,
    amount: number,
    amountSubtotal: number,
    amountTotal: number,
    currency: string,
    customerId: string,
    description: string,
    livemode: boolean,
    transactionIntentId: string,
    paymentGatewayId: string,
    cancelUrl: string,
    successUrl: string
): Record<string, string | number | boolean> {
    return {
        "eventId1": eventId1,
        "eventId2": eventId2,
        "eventId3": eventId3,
        "eventId4": eventId4,
        "apiVersion": apiVersion,
        "created1": created1,
        "created2": created2,
        "created3": created3,
        "created4": created4,
        "chargeId": chargeId,
        "paymentIntentId": paymentIntentId,
        "checkoutSessionId": checkoutSessionId,
        "amount": amount,
        "amountSubtotal": amountSubtotal,
        "amountTotal": amountTotal,
        "currency": currency,
        "customerId": customerId,
        "description": description,
        "livemode": livemode,
        "transactionIntentId": transactionIntentId,
        "paymentGatewayId": paymentGatewayId,
        "cancelUrl": cancelUrl,
        "successUrl": successUrl
    };
}


function stripeCheckoutFixtureReplacements(
    transactionIntentId: string,
    stripePaymentGatewayId: string,
    customerEmail: string,
    customerName: string,
    invoiceTotal: string,
    invoiceCurrency: string,
    paymentIntentId: string,
    thorSuccessUrl: string,
    thorCancelUrl: string,
    customerId: string,
    invoiceQuantity: string
) {
    return {
        "customer:email": customerEmail,
        "customer:name": customerName,
        "invoice:total": invoiceTotal,
        "invoice:currency": invoiceCurrency,
        "payment_intent:id": paymentIntentId,
        "thor:success_url": thorSuccessUrl,
        "thor:cancel_url": thorCancelUrl,
        "paymentGateway:id": stripePaymentGatewayId,
        "transactionIntent:id": transactionIntentId,
        "customer:id": customerId,
        "invoice:quantity": invoiceQuantity
    };
}
