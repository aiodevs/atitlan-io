import supertest from "supertest";
import { beforeAll, beforeEach, vi, afterEach, test, expect } from "vitest";
import { IntegrationTestFastResetDB, ApiIntegrationTest } from "../../util/integration-test";
import { getPaymentGateway, getPaymentGatewayDetails, updatePaymentGateway } from "../../factories/payment-gateway.factory";
import { createUserOwnedWallet } from "../../factories/wallets.factory";
import { createRecurrentePaymentGateway, updateRecurrenteSettings } from "../../factories/recurrente.factory";
import { createAdminWithWalletAndPermissions, createInvoice, createUser } from "../../factories/test-data-factory";
import { RecurrenteSettings } from "../../../../src/components/finance/integration-channels/recurrente/recurrente.type";
import AtitlanApiRoutes from "../../../../src/components/shared/routes";
import { createTransactionIntent } from "../../factories/transaction-intent";

let server: supertest.SuperTest<supertest.Test>;

beforeAll(async () => {
    server = await IntegrationTestFastResetDB.beforeAll();
});

beforeEach<ApiIntegrationTest>(async (context: ApiIntegrationTest) => {
    await IntegrationTestFastResetDB.beforeEach(context, server);
    context.admin = await createAdminWithWalletAndPermissions(
        server,
        "admin@atitlan.io",
        "password"
    );
});

afterEach<ApiIntegrationTest>(async (context: ApiIntegrationTest) => {
    await IntegrationTestFastResetDB.afterEach(context);
    vi.restoreAllMocks();
});

test<ApiIntegrationTest>("Payment Gateway - update & get - 200 ok", async (c) => {

    const recurrentePaymentGateway = await createRecurrentePaymentGateway(
        c.server,
        c.admin?.cookie,
        "Recurrente Payment Gateway",
        "Recurrente wallet",
        "Recurrente API Key",
        "Recurrente API Secret",
        "Recurrente Endpoint Secret");

    const pg = await getPaymentGateway(c.server, c.admin?.cookie, recurrentePaymentGateway.id.toString());

    expect(pg.name).toBe("Recurrente Payment Gateway");
    expect(pg.sourceWalletId).toBe(recurrentePaymentGateway.sourceWalletId);

    const user = await createUser(c.server, "user@user.nl", "password");
    const wallet = await createUserOwnedWallet(c.server, "User Wallet", user.cookie);

    const updatedPG = await updatePaymentGateway(
        c.server,
        c.admin?.cookie,
        recurrentePaymentGateway.id.toString(),
        "Updated Recurrente Payment Gateway");

    expect(updatedPG.name).toBe("Updated Recurrente Payment Gateway");
});

test<ApiIntegrationTest>("Payment Gateway - disable and enable paymentGateway", async (c) => {
    if (!c.admin) throw new Error("Admin should be created");
    const recurrentePaymentGateway = await createRecurrentePaymentGateway(
        c.server,
        c.admin.cookie,
        "Recurrente Payment Gateway",
        "Recurrente wallet",
        "Recurrente API Key",
        "Recurrente API Secret",
        "Recurrente Endpoint Secret");

    const recurrentePGDeets = await getPaymentGatewayDetails(c.server, c.admin.cookie, recurrentePaymentGateway.id.toString());

    expect(recurrentePGDeets.name).toBe("Recurrente Payment Gateway");
    expect(recurrentePGDeets.sourceWalletId).toBe(recurrentePaymentGateway.sourceWalletId);
    expect(recurrentePGDeets.disabledAt).toBe('');

    const date = new Date();
    const updatedPG = await updateRecurrenteSettings(
        c.server,
        c.admin?.cookie,
        recurrentePaymentGateway.id.toString(),
        "Updated Recurrente Payment Gateway",
        "Recurrente API Secret 2",
        "Recurrente API Key 2",
        "Recurrente Endpoint Secret 2",
        date);

    expect(updatedPG.name).toBe("Updated Recurrente Payment Gateway");
    expect(updatedPG.disabledAt).toBe(date.toISOString());
    expect(updatedPG.sourceWalletId).toBe(recurrentePaymentGateway.sourceWalletId);
    const publicKey = updatedPG.apiKeys.find(k => k.apiKey === "Recurrente API Key 2" && k.type === "Public");
    const secretKey = updatedPG.apiKeys.find(k => k.apiKey === "Recurrente API Secret 2" && k.type === "Secret");
    const endpoint = updatedPG.settings.find(k => k.value === "Recurrente Endpoint Secret 2" && k.encrypted && k.name === RecurrenteSettings.endpointSecret);
    if (!publicKey || !secretKey || !endpoint) {
        throw new Error("Keys not found");
    }

    const invoice = await createInvoice(c.server, {
        amount: "100",
        currency: "USD",
        description: "test",
        targetWalletId: c.admin.wallets.fundsWallet.id.toString(),
    }, c.admin.cookie);
    const intent = await createTransactionIntent(c.server, c.admin.cookie, invoice.id.toString(), c.admin.wallets.fundsWallet.id.toString());
    c.logSpy.mockReset();
    const confirmCheckoutResponse = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.HOSTED_CHECKOUT)
        .send({
            transactionIntentId: intent.id,
            paymentGatewayId: updatedPG.id
        });
    expect(confirmCheckoutResponse.status).toBe(400);
    expect(c.logSpy).toHaveBeenCalledWith(`PaymentGatewayService.validPaymentGateway - payment gateway is disabled id:${updatedPG.id}`);


    const reEnabledPG = await updateRecurrenteSettings(
        c.server,
        c.admin?.cookie,
        recurrentePaymentGateway.id.toString(),
        "ReEnabled Recurrente Payment Gateway",
        "Recurrente API Secret 3",
        "Recurrente API Key 3",
        "Recurrente Endpoint Secret 3",
        null);

    expect(reEnabledPG.name).toBe("ReEnabled Recurrente Payment Gateway");
    expect(reEnabledPG.disabledAt).toBe('');
    expect(reEnabledPG.sourceWalletId).toBe(recurrentePaymentGateway.sourceWalletId);
    const publicKeyReEnabled = reEnabledPG.apiKeys.find(k => k.apiKey === "Recurrente API Key 3" && k.type === "Public");
    const secretKeyReEnabled = reEnabledPG.apiKeys.find(k => k.apiKey === "Recurrente API Secret 3" && k.type === "Secret");
    const endpointReEnabled = reEnabledPG.settings.find(k => k.value === "Recurrente Endpoint Secret 3" && k.encrypted && k.name === RecurrenteSettings.endpointSecret);
    if (!publicKeyReEnabled || !secretKeyReEnabled || !endpointReEnabled) {
        throw new Error("Keys not found");
    }
});

test<ApiIntegrationTest>("Payment Gateway - disable and enable paymentGateway", async (c) => {
    if (!c.admin) throw new Error("Admin should be created");
    const recurrentePaymentGateway = await createRecurrentePaymentGateway(
        c.server,
        c.admin.cookie,
        "Recurrente Payment Gateway",
        "Recurrente wallet",
        "Recurrente API Key",
        "Recurrente API Secret",
        "Recurrente Endpoint Secret");

    const recurrentePGDeets = await getPaymentGatewayDetails(c.server, c.admin.cookie, recurrentePaymentGateway.id.toString());

    expect(recurrentePGDeets.name).toBe("Recurrente Payment Gateway");
    expect(recurrentePGDeets.sourceWalletId).toBe(recurrentePaymentGateway.sourceWalletId);
    expect(recurrentePGDeets.disabledAt).toBe('');

    const date = new Date();
    const updatedPG = await updateRecurrenteSettings(
        c.server,
        c.admin?.cookie,
        recurrentePaymentGateway.id.toString(),
        "Updated Recurrente Payment Gateway",
        "Recurrente API Secret 2",
        "Recurrente API Key 2",
        "Recurrente Endpoint Secret 2",
        date);

    expect(updatedPG.name).toBe("Updated Recurrente Payment Gateway");
    expect(updatedPG.disabledAt).toBe(date.toISOString());
    expect(updatedPG.sourceWalletId).toBe(recurrentePaymentGateway.sourceWalletId);
    const publicKey = updatedPG.apiKeys.find(k => k.apiKey === "Recurrente API Key 2" && k.type === "Public");
    const secretKey = updatedPG.apiKeys.find(k => k.apiKey === "Recurrente API Secret 2" && k.type === "Secret");
    const endpoint = updatedPG.settings.find(k => k.value === "Recurrente Endpoint Secret 2" && k.encrypted && k.name === RecurrenteSettings.endpointSecret);
    if (!publicKey || !secretKey || !endpoint) {
        throw new Error("Keys not found");
    }

    // TODO test stripe pg and check that it fails on trying to use stripe with a rec pg and rec with stripe

    const invoice = await createInvoice(c.server, {
        amount: "100",
        currency: "USD",
        description: "test",
        targetWalletId: c.admin.wallets.fundsWallet.id.toString(),
    }, c.admin.cookie);
    const intent = await createTransactionIntent(c.server, c.admin.cookie, invoice.id.toString(), c.admin.wallets.fundsWallet.id.toString());
    c.logSpy.mockReset();
    const confirmCheckoutResponse = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.HOSTED_CHECKOUT)
        .send({
            transactionIntentId: intent.id,
            paymentGatewayId: updatedPG.id
        });
    expect(confirmCheckoutResponse.status).toBe(400);
    expect(c.logSpy).toHaveBeenCalledWith(`PaymentGatewayService.validPaymentGateway - payment gateway is disabled id:${updatedPG.id}`);


    const reEnabledPG = await updateRecurrenteSettings(
        c.server,
        c.admin?.cookie,
        recurrentePaymentGateway.id.toString(),
        "ReEnabled Recurrente Payment Gateway",
        "Recurrente API Secret 3",
        "Recurrente API Key 3",
        "Recurrente Endpoint Secret 3",
        null);

    expect(reEnabledPG.name).toBe("ReEnabled Recurrente Payment Gateway");
    expect(reEnabledPG.disabledAt).toBe('');
    expect(reEnabledPG.sourceWalletId).toBe(recurrentePaymentGateway.sourceWalletId);
    const publicKeyReEnabled = reEnabledPG.apiKeys.find(k => k.apiKey === "Recurrente API Key 3" && k.type === "Public");
    const secretKeyReEnabled = reEnabledPG.apiKeys.find(k => k.apiKey === "Recurrente API Secret 3" && k.type === "Secret");
    const endpointReEnabled = reEnabledPG.settings.find(k => k.value === "Recurrente Endpoint Secret 3" && k.encrypted && k.name === RecurrenteSettings.endpointSecret);
    if (!publicKeyReEnabled || !secretKeyReEnabled || !endpointReEnabled) {
        throw new Error("Keys not found");
    }
});

