import {
    beforeAll,
    expect,
    test,
    afterEach,
    beforeEach,
} from "vitest"
import { ApiIntegrationTest, IntegrationTestFastResetDB } from "../../util/integration-test";
import supertest from "supertest";
import { createAdminWithWalletAndPermissions, createFundsTransaction, createFundsTransactionResponse, createInternalTransactionResponse, createUser } from "../../factories/test-data-factory";
import { BalanceSheet } from "../../../../src/components/finance/wallet/wallet.types";
import AtitlanApiRoutes from "../../../../src/components/shared/routes";
import { createUserOwnedWallet, getWalletDetails, getWalletDetailsFromUserId, updateWallets } from "../../factories/wallets.factory";
import { createOrganization, createOrganizationOwnedWallet } from "../../factories/organization.factory";
import { DefaultWalletRoleNames } from "../../../../src/components/heimdall/authorization/authorization.type";

let server: supertest.SuperTest<supertest.Test>;

beforeAll(async () => {
    server = await IntegrationTestFastResetDB.beforeAll();
});

beforeEach<ApiIntegrationTest>(async (context: ApiIntegrationTest) => {
    await IntegrationTestFastResetDB.beforeEach(context, server);
});

afterEach<ApiIntegrationTest>(IntegrationTestFastResetDB.afterEach);

test<ApiIntegrationTest>("Wallet - seed & initial funds - 200 ok", async (c) => {
    const email = "test@atitlanio.io";
    const password = "password";
    let loginResponse = await c.server.post(AtitlanApiRoutes.AUTH.LOGIN).send({
        email: email,
        password: password,
    });
    expect(loginResponse.status).toBe(401);

    const adminUser = await createAdminWithWalletAndPermissions(c.server, email, password);
    console.log(adminUser);
    loginResponse = await c.server.post(AtitlanApiRoutes.AUTH.LOGIN).send({
        email: email,
        password: password,
    });
    expect(loginResponse.status).toBe(200);
    expect(loginResponse.body.user.email).toBe(email);

    const adminId: string = loginResponse.body.user.id;
    const adminCookies = loginResponse.headers["set-cookie"];

    const getPermissionResponse = await c.server
        .get(AtitlanApiRoutes.USER.PERMISSIONS)
        .set("Cookie", adminCookies);
    expect(getPermissionResponse.status).toBe(200);
    const permissionsResult = getPermissionResponse.body;
    const permissions: { userId: string, action: string, resource: string, expiresAt: string }[] = permissionsResult.permissions;
    expect(permissions.length).toBe(4);

    const walletsResponse = await c.server
        .get(AtitlanApiRoutes.USER.WALLETS())
        .set("Cookie", adminCookies);
    const wallets: {
        id: string;
        createdAt: Date;
        updatedAt: Date;
        lastUsed: Date | null;
        name: string;
        userId: string;
    }[] = walletsResponse.body.wallets;
    expect(walletsResponse.status).toBe(200);
    console.log(walletsResponse.body)
    expect(wallets.length).toBe(2);
    const walletNames = wallets.map(wallet => wallet.name);
    expect(walletNames).toContain("aio-minter");
    expect(walletNames).toContain("aio-funds");

    const user1BuysFromAIO = await createFundsTransactionResponse(
        c.server,
        {
            walletId: adminUser.wallets.sourceWallet.id,
            amount: "1",
            currency: "BTC",

        },
        {
            walletId: adminUser.wallets.fundsWallet.id,
            amount: "1",
            currency: "BTC",
        },
        "Transaction from admin funds wallet to user1",
        adminUser.cookie);

    // Check wallet balance
    const minterWalletId = adminUser.wallets.sourceWallet.id;
    const fundsWalletId = adminUser.wallets.fundsWallet.id;
    let balance = await getWalletDetails(c.server, adminCookies, fundsWalletId.toString());

    if (!balance.balance) throw new Error();
    const fundsBalance: BalanceSheet = balance.balance;
    expect(fundsBalance.BTC.net).toBe("1");
    expect(fundsBalance.BTC.incoming).toBe("1");
    expect(fundsBalance.BTC.outgoing).toBe("0");
    expect(Object.keys(fundsBalance).length).toBe(1);

    balance = await getWalletDetails(
        c.server,
        adminCookies,
        minterWalletId.toString(),
        true);
    if (!balance.balance) throw new Error();

    const minterBalance: BalanceSheet = balance.balance;
    expect(minterBalance.BTC.net).toBe("-1");
    expect(minterBalance.BTC.incoming).toBe("0");
    expect(minterBalance.BTC.outgoing).toBe("1");
    expect(Object.keys(minterBalance).length).toBe(1);

    // Check wallet transactions
    const fundsTransactions = await getWalletDetails(c.server, adminCookies, fundsWalletId.toString());
    if (!fundsTransactions || !fundsTransactions.incomingTransactions) throw new Error();

    expect(fundsTransactions.id).toBe(fundsWalletId.toString());
    expect(fundsTransactions.name).toBe("aio-funds");
    expect(fundsTransactions.incomingTransactions[0].sourceCurrency).toBe("BTC");
    expect(fundsTransactions.incomingTransactions[0].sourceAmount).toBe("1");
    expect(fundsTransactions.incomingTransactions[0].targetCurrency).toBe("BTC");
    expect(fundsTransactions.incomingTransactions[0].targetAmount).toBe("1");
    expect(fundsTransactions.incomingTransactions[0].description).toBe(`Transaction from admin funds wallet to user1`);
    expect(fundsTransactions.incomingTransactions[0].sourceWalletId).toBe(minterWalletId.toString());
    expect(fundsTransactions.incomingTransactions[0].targetWalletId).toBe(fundsWalletId.toString());

    const mintTransactions = await getWalletDetails(c.server, adminCookies, minterWalletId.toString());
    if (!mintTransactions || !mintTransactions.outgoingTransactions) throw new Error();

    expect(mintTransactions.id).toBe(minterWalletId.toString());
    expect(mintTransactions.name).toBe("aio-minter");
    expect(mintTransactions.outgoingTransactions[0].sourceCurrency).toBe("BTC");
    expect(mintTransactions.outgoingTransactions[0].sourceAmount).toBe("1");
    expect(mintTransactions.outgoingTransactions[0].targetCurrency).toBe("BTC");
    expect(mintTransactions.outgoingTransactions[0].description).toBe(`Transaction from admin funds wallet to user1`);
    expect(mintTransactions.outgoingTransactions[0].sourceWalletId).toBe(minterWalletId.toString());
    expect(mintTransactions.outgoingTransactions[0].targetWalletId).toBe(fundsWalletId.toString());
    expect(mintTransactions.outgoingTransactions[0].targetAmount).toBe("1");
});

test<ApiIntegrationTest>("Wallet - create and rename wallet - 200 ok", async (c) => {
    const email = "user@hotmail.com"
    const password = "password";
    const signupResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.SIGNUP)
        .send({
            email: email,
            password: password,
        });
    expect(signupResponse.status).toBe(200);
    const userId = signupResponse.body.user.id;
    const userCookies = signupResponse.headers["set-cookie"];

    const intialWalletsResponse = await c.server
        .get(AtitlanApiRoutes.USER.WALLETS())
        .set("Cookie", userCookies);
    const intialWallets: {
        id: string;
        createdAt: Date;
        updatedAt: Date;
        lastUsed: Date | null;
        name: string;
        userId: string;
    }[] = intialWalletsResponse.body.wallets;
    expect(intialWalletsResponse.status).toBe(200);
    expect(intialWallets.length).toBe(0);

    const createWalletResponse = await c.server
        .post(AtitlanApiRoutes.WALLET.CREATE)
        .set("Cookie", userCookies)
        .send({
            name: "groceries",
        });
    expect(createWalletResponse.status).toBe(200);
    const newWallet = createWalletResponse.body.wallet;
    const createWalletId = createWalletResponse.body.wallet.id;
    expect(createWalletId).toBeDefined();
    expect(newWallet.name).toBe("groceries");

    const secondWalletsResponse = await c.server
        .get(AtitlanApiRoutes.USER.WALLETS())
        .set("Cookie", userCookies);
    const secondWallets: {
        id: string;
        createdAt: Date;
        updatedAt: Date;
        lastUsed: Date | null;
        name: string;
        userId: string;
    }[] = secondWalletsResponse.body.wallets;
    expect(secondWalletsResponse.status).toBe(200);
    expect(secondWallets.length).toBe(1);
    expect(secondWallets[0].name).toBe("groceries");

    const walletId = secondWallets[0].id;
    const renamedWalletsUpdateRequest = await updateWallets(
        c.server,
        userCookies,
        [{
            walletId: walletId,
            name: "stocks",
        }]);

    const renamedWalletNames = renamedWalletsUpdateRequest.map(wallet => wallet.name);
    expect(renamedWalletNames).toContain("stocks");

    const afterRenameWalletsResponse = await c.server
        .get(AtitlanApiRoutes.USER.WALLETS())
        .set("Cookie", userCookies);
    const renamedWallets: {
        id: string;
        createdAt: Date;
        updatedAt: Date;
        lastUsed: Date | null;
        name: string;
        userId: string;
    }[] = afterRenameWalletsResponse.body.wallets;
    expect(afterRenameWalletsResponse.status).toBe(200);
    expect(renamedWallets.length).toBe(1);
    const afterRenamedWalletNames = renamedWallets.map(wallet => wallet.name);
    expect(afterRenamedWalletNames).toContain("stocks");
});

test<ApiIntegrationTest>("Wallet - create and update wallet permissions - 200 ok", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@atitlan.io", "password");
    const user = await createUser(c.server, "user@gmail.com", "password");
    const userReadOnly = await createUser(c.server, "user2@gmail.com", "password");
    const userWallet = await createUserOwnedWallet(c.server, "mywallet", user.cookie);
    const userReadOnlyWallet = await createUserOwnedWallet(c.server, "mywallet2", userReadOnly.cookie);

    expect(userWallet.name).toBe("mywallet");
    expect(userWallet.authorizedOrganizationUsers.length).toBe(0);
    expect(userWallet.authorizedUsers.length).toBe(1);
    expect(userWallet.authorizedUsers[0].userId).toBe(user.id);
    expect(userWallet.authorizedOrganizations.length).toBe(0);
    expect(userReadOnlyWallet.name).toBe("mywallet2");
    expect(userReadOnlyWallet.authorizedOrganizationUsers.length).toBe(0);
    expect(userReadOnlyWallet.authorizedUsers.length).toBe(1);
    expect(userReadOnlyWallet.authorizedUsers[0].userId).toBe(userReadOnly.id);
    expect(userReadOnlyWallet.authorizedOrganizations.length).toBe(0);

    const readOnlyRole = userReadOnlyWallet.defaultWalletRoles.find(role => role.name === DefaultWalletRoleNames.ReadOnly);
    const ownerRole = userReadOnlyWallet.defaultWalletRoles.find(role => role.name === DefaultWalletRoleNames.Owner);
    if (!readOnlyRole || !ownerRole) throw new Error("Role not found");
    const updatedUserWallet = await updateWallets(
        c.server,
        user.cookie,
        [{
            walletId: userWallet.id,
            authorizedUsers: [
                {
                    userId: userReadOnly.id,
                    roleId: readOnlyRole.id,
                },
            ],
        }]);
    expect(updatedUserWallet.length).toBe(1);
    expect(updatedUserWallet[0].authorizedUsers.length).toBe(2);
    const user1Auth = updatedUserWallet[0].authorizedUsers.find(auth => auth.userId === user.id);
    const user2Auth = updatedUserWallet[0].authorizedUsers.find(auth => auth.userId === userReadOnly.id);
    expect(user1Auth).toBeDefined();
    expect(user2Auth).toBeDefined();
    if (!user1Auth || !user2Auth) throw new Error("Auth not found");
    expect(user1Auth.roleId).toBe(ownerRole.id);
    expect(user2Auth.roleId).toBe(readOnlyRole.id);

    const getUser1WalletAsUser2 = await getWalletDetails(c.server, userReadOnly.cookie, userWallet.id);
    const getUser1WalletAsUser1 = await getWalletDetails(c.server, user.cookie, userWallet.id);
    const getUser1WalletAsAdmin = await getWalletDetailsFromUserId(c.server, admin.cookie, user.id, userWallet.id);

    expect(getUser1WalletAsUser2.authorizedUsers.length).toBe(2);
    expect(getUser1WalletAsUser1.authorizedUsers.length).toBe(2);
    expect(getUser1WalletAsAdmin.authorizedUsers.length).toBe(2);

    const addFunds = await createFundsTransaction(c.server,
        { walletId: admin.wallets.sourceWallet.id, amount: "1", currency: "BTC" },
        { walletId: userWallet.id, amount: "1", currency: "BTC" },
        "Addfunds for honey pot.",
        admin.cookie);
    const userWalletBalance = await getWalletDetails(c.server, user.cookie, userWallet.id);
    if (!userWalletBalance.balance) throw new Error();
    expect(userWalletBalance.balance.BTC.net).toBe("1");

    const illegalTransactionFromReadOnlyRole = await c.server.post(AtitlanApiRoutes.TRANSACTIONS.CREATE_INTERNAL)
        .set("Cookie", userReadOnly.cookie)
        .send({
            sourceWalletId: userWallet.id,
            targetWalletId: userReadOnlyWallet.id,
            sourceAmount: "1",
            targetAmount: "1",
            sourceCurrency: "BTC",
            targetCurrency: "BTC",
            description: "Thanks for your btc!",
        });
    console.log(illegalTransactionFromReadOnlyRole.body);
    expect(illegalTransactionFromReadOnlyRole.status).toBe(401);

    const illegalTransactionFromNoConnection = await createInternalTransactionResponse(c.server,
        { walletId: userWallet.id, amount: "1", currency: "BTC" },
        { walletId: admin.wallets.sourceWallet.id, amount: "1", currency: "BTC" },
        "Thanks for your btc, no consent bam!",
        admin.cookie);
    expect(illegalTransactionFromNoConnection.status).toBe(401);

    const adminRole = userReadOnlyWallet.defaultWalletRoles.find(role => role.name === DefaultWalletRoleNames.Admin);
    if (!adminRole) throw new Error("Role not found");
    const updatedUserWalletAdmin = await updateWallets(
        c.server,
        user.cookie,
        [{
            walletId: userWallet.id,
            authorizedUsers: [
                {
                    userId: admin.id,
                    roleId: adminRole.id,
                },
            ],
        }]);

    const legalTransactionFromOwner = await createInternalTransactionResponse(c.server,
        { walletId: userWallet.id, amount: "1", currency: "BTC" },
        { walletId: admin.wallets.sourceWallet.id, amount: "1", currency: "BTC" },
        "Thanks for your btc, its back baby!",
        admin.cookie);
    expect(legalTransactionFromOwner.status).toBe(200);
});

test<ApiIntegrationTest>("Wallet - test valid permissions - 200 ok", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@test.com", "password");
    const user = await createUser(c.server, "user@supermail.com", "password");
    const userWallet = await createUserOwnedWallet(c.server, "mywallet", user.cookie);
    const userEvil = await createUser(c.server, "evul@prohacker.io", "password");
    const orgUser = await createUser(c.server, "i@love.org", "password");
    const org = await createOrganization(c.server, orgUser.cookie, "love org",);
    const seedUser = await createFundsTransactionResponse(c.server, {
        walletId: admin.wallets.sourceWallet.id,
        amount: "1",
        currency: "BTC",
    }, {
        walletId: userWallet.id,
        amount: "1",
        currency: "BTC",
    }, "Transaction from admin funds wallet to user1", admin.cookie);
    const userWalletAfterTransaction = await getWalletDetails(c.server, user.cookie, userWallet.id);
    console.log(userWalletAfterTransaction);
    const userWalletDetails = await c.server.get(AtitlanApiRoutes.WALLET
        .GET_USER_WALLET_DETAILS(user.id, userWallet.id))
        .set("Cookie", userEvil.cookie);
    console.log(userWalletDetails.body);
    expect(userWalletDetails.status).toBe(401);
    // expect(c.logSpy)
});

test<ApiIntegrationTest>("Wallet - test unauthorized - 401 Unauthorized", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "aio@atitlan.io", "password");
    const user = await createUser(c.server, "user@gangster.com", "password");
    const userWallet = await createUserOwnedWallet(c.server, "mywallet", user.cookie);
    const evilUser = await createUser(c.server, "hack@the.world", "password");
    const evilAttemptUserWalletDetails = await c.server.get(AtitlanApiRoutes.WALLET.GET_USER_WALLET_DETAILS(user.id, userWallet.id))
        .set("Cookie", evilUser.cookie)
        .expect(401);
    const evilAttempt2UserWalletDetails = await c.server.get(AtitlanApiRoutes.WALLET.GET_USER_WALLET_DETAILS(evilUser.id, userWallet.id))
        .set("Cookie", evilUser.cookie)
        .expect(401);

    const evilAttemptWalletDetails = await c.server.get(AtitlanApiRoutes.USER.WALLET_DETAILS(userWallet.id))
        .set("Cookie", evilUser.cookie)
        .expect(401);
    const adminAttemptWalletDetails = await c.server.get(AtitlanApiRoutes.USER.WALLET_DETAILS(userWallet.id))
        .set("Cookie", admin.cookie)
        .expect(401);

    const org = await createOrganization(c.server, user.cookie, "org");
    const orgWallet = await createOrganizationOwnedWallet(c.server, user.cookie, org.id, "orgwallet");
    const evilAttemptOrgWalletDetails = await c.server.get(AtitlanApiRoutes.ORGANIZATION.GET_WALLET_DETAILS(org.id, orgWallet.id))
        .set("Cookie", evilUser.cookie)
        .expect(401);
    expect(c.logSpy).toBeCalledWith(`User ${evilUser.id} does not have read access to wallet ${orgWallet.id} ` +
        `in organization ${org.id}`);

    const evilAttemptAllUserIdWallets = await c.server.get(AtitlanApiRoutes.WALLET.GET_USER_WALLETS(user.id))
        .set("Cookie", evilUser.cookie)
        .expect(401);
    expect(c.logSpy).toBeCalledWith(`User ${evilUser.id} does not have admin permission to see any users wallets`);

});

test<ApiIntegrationTest>("Wallet - test invalid params - 400 Bad Request", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "aio@atitlan.io", "password");
    const user = await createUser(c.server, "user@gangster.com", "password");

    const orgOwnedWalletBadRequest = await c.server.post(AtitlanApiRoutes.ORGANIZATION.CREATE_WALLET)
        .set("Cookie", user.cookie)
        .send({
            name: "org-wallet",
            organizationId: "fakeOrgId",
        }).expect(400);

    expect(c.logSpy).toBeCalledWith(`createOrganizationOwnedWallet - Organization fakeOrgId not found`);

    const walletDetailBadRequest = await c.server.get(AtitlanApiRoutes.USER.WALLET_DETAILS("fakeWalletId"))
        .set("Cookie", user.cookie)
        .expect(401);
    // expect(c.logSpy).toBeCalledWith(`getWalletDetails - Wallet fakeWalletId not found`);

    const org = await createOrganization(c.server, user.cookie, "org");
    const orgWallet = await createOrganizationOwnedWallet(c.server, user.cookie, org.id, "orgwallet");

    const walletOrgDetailUnauthorized = await c.server.get(AtitlanApiRoutes.ORGANIZATION.GET_WALLET_DETAILS("fakeOrgId", "fakeWalletId"))
        .set("Cookie", admin.cookie)
        .expect(401);

    const walletOrgDetail2Unauthorized = await c.server.get(AtitlanApiRoutes.ORGANIZATION.GET_WALLET_DETAILS("fakeOrgId", orgWallet.id))
        .set("Cookie", admin.cookie)
        .expect(401);

    const walletOrgDetail3Unauthorized = await c.server.get(AtitlanApiRoutes.ORGANIZATION.GET_WALLET_DETAILS(org.id, "fakeWalletId"))
        .set("Cookie", admin.cookie)
        .expect(401);

    const walletOrgDetailBadRequest = await c.server.get(AtitlanApiRoutes.ORGANIZATION.GET_WALLET_DETAILS("fakeOrgId", "fakeWalletId"))
        .set("Cookie", admin.cookie)
        .expect(401);

    const walletOrgDetail2BadRequest = await c.server.get(AtitlanApiRoutes.ORGANIZATION.GET_WALLET_DETAILS("fakeOrgId", orgWallet.id))
        .set("Cookie", admin.cookie)
        .expect(401);

    const walletOrgDetail3BadRequest = await c.server.get(AtitlanApiRoutes.ORGANIZATION.GET_WALLET_DETAILS(org.id, "fakeWalletId"))
        .set("Cookie", admin.cookie)
        .expect(401);
});

