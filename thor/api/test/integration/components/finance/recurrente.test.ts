import { beforeAll, expect, test, afterEach, beforeEach, vi } from "vitest";
import { ApiIntegrationTest, IntegrationTestFastResetDB, } from "../../util/integration-test";
import TestUtils from "../../util/test-util";

import AtitlanApiRoutes from "../../../../src/components/shared/routes";
import { createUserOwnedWallet, getWalletDetails } from "../../factories/wallets.factory";
import { createOrganization, getOrganizationDetails, getOrganizationWalletDetails } from "../../factories/organization.factory";
import { DefaultWalletRoleNames } from "../../../../src/components/heimdall/authorization/authorization.type";
import { RecurrenteSettings } from "../../../../src/components/finance/integration-channels/recurrente/recurrente.type";
import { getPaymentGatewayDetails } from "../../factories/payment-gateway.factory";
import supertest from "supertest";
import { createRecurrentePaymentGateway, RecurrenteEvent, startRecurrenteHostedCheckout } from "../../factories/recurrente.factory";
import { createAdminWithWalletAndPermissions, createUser, createInvoice } from "../../factories/test-data-factory";
import { createAnonymousTransactionIntent, createTransactionIntent, getTransactionIntent, updateAnonymousIntentWithUserWallet } from "../../factories/transaction-intent";
import { TransactionIntentStatus } from "../../../../src/components/finance/transaction-intent/transaction-intent.type";
import { TransactionIntentCompletionType } from "@prisma/client";
import { getPrisma } from "../../../../src/util/prisma";

let server: supertest.SuperTest<supertest.Test>;
const checkoutCompletedJSONPath = "test/recurrente/checkout-session-succeeded.json";
const checkoutFailedJSONPath = "test/recurrente/checkout-session-failed.json";
const checkoutUnknownJSONPath = "test/recurrente/checkout-session-unknown.json";
const mockCheckoutId = "ch_act3edhg85ruj72e";

beforeAll(async () => {
    server = await IntegrationTestFastResetDB.beforeAll();
});

export const checkoutMock200 = () => Promise.resolve({
    ok: true,
    status: 200,
    json: async () => ({
        checkout_url: "https://mock-checkout-url.com/checkout_session_id",
        id: mockCheckoutId,
    })
} as Response);

export const checkoutMock500 = () => Promise.resolve({
    ok: false,
    status: 500,
    statusText: "Internal Server Error",
    json: async () => ({ error: "Server error" })
} as Response);

beforeEach<ApiIntegrationTest>(async (context: ApiIntegrationTest) => {
    await IntegrationTestFastResetDB.beforeEach(context, server);
    context.fetchSpy = vi.spyOn(global, "fetch").mockImplementation(checkoutMock200);
    context.admin = await createAdminWithWalletAndPermissions(
        server,
        "admin@atitlan.io",
        "password"
    );
});

afterEach<ApiIntegrationTest>(async (context: ApiIntegrationTest) => {
    await IntegrationTestFastResetDB.afterEach(context);
    vi.restoreAllMocks();
});

test<ApiIntegrationTest>("Recurrente - full flow - add funds - 200 ok", async (c) => {
    const publisheableApiKey = "pk_test_4e";
    const endpointSecret = "whsec_4e";
    const user = await createUser(c.server, "user1@atitlan.io", "password");
    const admin = await createAdminWithWalletAndPermissions(
        c.server,
        "admin@atitlan.io",
        "password"
    );
    const userWallet = await createUserOwnedWallet(
        c.server,
        "My stacks of auro",
        user.cookie,
    );
    const recurrentePaymentGateway = await createRecurrentePaymentGateway(
        c.server,
        admin.cookie,
        "AIO-Recurrente-PaymentGateway",
        "AIO-Recurrente-Wallet",
        "sk_test_4e",
        publisheableApiKey,
        endpointSecret
    );
    const recurrenteWallet = await getWalletDetails(
        c.server,
        admin.cookie,
        recurrentePaymentGateway.sourceWalletId.toString()
    );
    expect(recurrenteWallet.name).toBe("AIO-Recurrente-Wallet");

    const addFundsInvoice = await createInvoice(
        c.server,
        {
            amount: "1000",
            currency: "EUR",
            description: "Add funds to my wallet",
            targetWalletId: userWallet.id,
        },
        user.cookie
    );
    expect(addFundsInvoice.amount.toString()).toBe("1000");

    const transactionIntent = await createTransactionIntent(
        c.server,
        user.cookie,
        addFundsInvoice.id.toString(),
        userWallet.id.toString()
    );
    expect(transactionIntent.status).toBe("created");

    const recurrenteHostedCheckout = await startRecurrenteHostedCheckout(
        c.server,
        transactionIntent.id.toString(),
        recurrentePaymentGateway.id.toString()
    );
    expect(recurrenteHostedCheckout.recurrenteUrl).toBe(
        "https://mock-checkout-url.com/checkout_session_id"
    );
    expect(c.fetchSpy).toHaveBeenLastCalledWith(
        "https://app.recurrente.com/api/checkouts/",
        {
            method: "POST",
            headers: {
                "X-PUBLIC-KEY": "pk_test_4e",
                "X-SECRET-KEY": "sk_test_4e",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                items: [
                    {
                        name: "Thor AtitlanIO",
                        currency: "EUR",
                        amount_in_cents: "100000",
                    },
                ],
                success_url: `https://thor.atitlan.io/success?transactionIntentId=${transactionIntent.id}`,
                cancel_url: `https://thor.atitlan.io/cancel?transactionIntentId=${transactionIntent.id}`,
            }),
        }
    );

    const transactionIntentAfterCheckout = await getTransactionIntent(
        c.server,
        user.cookie,
        transactionIntent.id.toString()
    );
    console.log("transactionIntentAfterCheckout: ", transactionIntentAfterCheckout);

    expect(transactionIntentAfterCheckout.status).toBe("processing");
    expect(transactionIntentAfterCheckout.paymentGatewayIdentifier).toBe(mockCheckoutId);

    c.logSpy.mockClear();
    const recurrenteEvent: RecurrenteEvent = await TestUtils.readFileToJson(checkoutCompletedJSONPath);
    const recurrenteLoggingEvent = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_LOGGING)
        .send(recurrenteEvent);
    expect(recurrenteLoggingEvent.status).toBe(200);
    expect(c.logSpy).toHaveBeenLastCalledWith(`Recurrente logging webhook - body: ${JSON.stringify(recurrenteEvent, null, 2)}`);

    console.log("checkout_session fixture: ", recurrenteEvent);
    const recurrenteCheckoutEvent = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_CHECKOUT_SESSION)
        .send(recurrenteEvent);
    expect(recurrenteCheckoutEvent.status).toBe(200);
    const completedTransactionIntent = await getTransactionIntent(
        c.server,
        user.cookie,
        transactionIntent.id.toString()
    );
    if (!completedTransactionIntent.completedTransactions) {
        throw new Error("Transaction not found");
    }
    const completedTransaction =
        completedTransactionIntent.completedTransactions[0];

    expect(completedTransactionIntent.status).toBe("success");
    expect(completedTransactionIntent.completedTransactions?.length).toBe(1);
    expect(completedTransaction?.transaction?.id).toBeDefined();
    expect(completedTransaction?.transaction?.sourceCurrency).toBe("EUR");
    expect(completedTransaction?.transaction?.sourceAmount).toBe("1000");
    expect(completedTransaction?.transaction?.targetCurrency).toBe("EUR");
    expect(completedTransaction?.transaction?.targetAmount).toBe("1000");
    expect(completedTransaction?.transaction?.sourceWalletId).toBe(
        recurrenteWallet.id
    );
    expect(completedTransaction?.transaction?.targetWalletId).toBe(userWallet.id);
});

test<ApiIntegrationTest>("Recurrente - createPaymentGateway - 401 no permission", async (c) => {
    const response = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.CREATE_PAYMENT_GATEWAY)
        .send({
            paymentGatewayName: "TestGateway",
            paymentGatewayWalletName: "TestWallet",
            secretApiKey: "secretKey",
            publicApiKey: "publicKey",
            endpointSecret: "endpointSecret"
        });

    expect(response.status).toBe(401);

    const userWithNoPermissions = await createUser(c.server, "user@web.com", "password");
    const response2 = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.CREATE_PAYMENT_GATEWAY)
        .set("Cookie", userWithNoPermissions.cookie)
        .send({
            paymentGatewayName: "TestGateway",
            paymentGatewayWalletName: "TestWallet",
            secretApiKey: "secretKey",
            publicApiKey: "publicKey",
            endpointSecret: "endpointSecret"
        });
    expect(c.logSpy).toHaveBeenCalledWith(`User ${userWithNoPermissions.id} does not have permission to create a recurrente payment gateway`);
    expect(response2.status).toBe(401);
});

test<ApiIntegrationTest>("Recurrente - Hosted Checkout - 500 Fetch API Failure", async (c) => {
    const publisheableApiKey = "pk_test_4e";
    const endpointSecret = "whsec_4e";
    const user = await createUser(c.server, "user1@atitlan.io", "password");
    const admin = await createAdminWithWalletAndPermissions(
        c.server,
        "admin@atitlan.io",
        "password"
    );
    const userWallet = await createUserOwnedWallet(
        c.server,
        "My stacks of auro",
        user.cookie,
    );
    const recurrentePaymentGateway = await createRecurrentePaymentGateway(
        c.server,
        admin.cookie,
        "AIO-Recurrente-PaymentGateway",
        "AIO-Recurrente-Wallet",
        "sk_test_4e",
        publisheableApiKey,
        endpointSecret
    );

    const addFundsInvoice = await createInvoice(
        c.server,
        {
            amount: "1000",
            currency: "EUR",
            description: "Add funds to my wallet",
            targetWalletId: userWallet.id,
        },
        user.cookie
    );

    const transactionIntent = await createTransactionIntent(
        c.server,
        user.cookie,
        addFundsInvoice.id.toString(),
        userWallet.id.toString()
    );

    c.fetchSpy.mockImplementation(checkoutMock500);
    const response = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.HOSTED_CHECKOUT)
        .set("Cookie", user.cookie)
        .send({
            transactionIntentId: transactionIntent.id.toString(),
            paymentGatewayId: recurrentePaymentGateway.id.toString()
        });

    expect(response.status).toBe(500);
    expect(response.body).toEqual({ message: "Internal Server Error" });
    expect(c.logSpy).toHaveBeenLastCalledWith(`Error creating during recurrente checkout api post for transactionIntentId ${transactionIntent.id.toString()} and paymentGatewayId ${recurrentePaymentGateway.id.toString()}.\nError: Error: Unavailable`);
});

test<ApiIntegrationTest>("Recurrente - full flow - add funds - recurrente failed", async (c) => {
    const publisheableApiKey = "pk_test_4e";
    const endpointSecret = "whsec_4e";
    const user = await createUser(c.server, "user1@atitlan.io", "password");
    const admin = await createAdminWithWalletAndPermissions(
        c.server,
        "admin@atitlan.io",
        "password"
    );
    const userWallet = await createUserOwnedWallet(
        c.server,
        "My stacks of auro",
        user.cookie,
    );
    const recurrentePaymentGateway = await createRecurrentePaymentGateway(
        c.server,
        admin.cookie,
        "AIO-Recurrente-PaymentGateway",
        "AIO-Recurrente-Wallet",
        "sk_test_4e",
        publisheableApiKey,
        endpointSecret
    );
    const recurrenteWallet = await getWalletDetails(
        c.server,
        admin.cookie,
        recurrentePaymentGateway.sourceWalletId.toString()
    );
    expect(recurrenteWallet.name).toBe("AIO-Recurrente-Wallet");

    const addFundsInvoice = await createInvoice(
        c.server,
        {
            amount: "1000",
            currency: "EUR",
            description: "Add funds to my wallet",
            targetWalletId: userWallet.id,
        },
        user.cookie
    );
    expect(addFundsInvoice.amount.toString()).toBe("1000");

    const transactionIntent = await createTransactionIntent(
        c.server,
        user.cookie,
        addFundsInvoice.id.toString(),
        userWallet.id.toString()
    );
    expect(transactionIntent.status).toBe("created");

    const recurrenteHostedCheckout = await startRecurrenteHostedCheckout(
        c.server,
        transactionIntent.id.toString(),
        recurrentePaymentGateway.id.toString()
    );
    expect(recurrenteHostedCheckout.recurrenteUrl).toBe(
        "https://mock-checkout-url.com/checkout_session_id"
    );
    expect(c.fetchSpy).toHaveBeenLastCalledWith(
        "https://app.recurrente.com/api/checkouts/",
        {
            method: "POST",
            headers: {
                "X-PUBLIC-KEY": "pk_test_4e",
                "X-SECRET-KEY": "sk_test_4e",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                items: [
                    {
                        name: "Thor AtitlanIO",
                        currency: "EUR",
                        amount_in_cents: "100000",
                    },
                ],
                success_url: `https://thor.atitlan.io/success?transactionIntentId=${transactionIntent.id}`,
                cancel_url: `https://thor.atitlan.io/cancel?transactionIntentId=${transactionIntent.id}`,
            }),
        }
    );

    const transactionIntentAfterCheckout = await getTransactionIntent(
        c.server,
        user.cookie,
        transactionIntent.id.toString()
    );
    console.log("transactionIntentAfterCheckout: ", transactionIntentAfterCheckout);

    expect(transactionIntentAfterCheckout.status).toBe("processing");
    expect(transactionIntentAfterCheckout.paymentGatewayIdentifier).toBe(mockCheckoutId);

    c.logSpy.mockClear();
    const recurrenteEvent: {
        api_version: string;
        checkout: { id: string };
        created_at: string;
        customer_id: string;
        event_type: string;
        id: string;
        product: { id: string };
    } = await TestUtils.readFileToJson(checkoutFailedJSONPath);
    const recurrenteLoggingEvent = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_LOGGING)
        .send(recurrenteEvent);
    expect(recurrenteLoggingEvent.status).toBe(200);
    expect(c.logSpy).toHaveBeenLastCalledWith(`Recurrente logging webhook - body: ${JSON.stringify(recurrenteEvent, null, 2)}`);

    console.log("checkout_session fixture: ", recurrenteEvent);
    const recurrenteCheckoutEvent = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_CHECKOUT_SESSION)
        .send(recurrenteEvent);
    expect(recurrenteCheckoutEvent.status).toBe(200);
    const failedTransactionIntent = await getTransactionIntent(
        c.server,
        user.cookie,
        transactionIntent.id.toString()
    );
    if (!failedTransactionIntent.completedTransactions) {
        throw new Error("Transaction not found");
    }
    console.log("failedTransactionIntent: ", failedTransactionIntent);
    expect(failedTransactionIntent.completedTransactions?.length).toBe(0);
    expect(failedTransactionIntent.status).toBe("failed");
});

test<ApiIntegrationTest>("Recurrente - full flow - add funds - recurrente unknown event", async (c) => {
    const publisheableApiKey = "pk_test_4e";
    const endpointSecret = "whsec_4e";
    const user = await createUser(c.server, "user1@atitlan.io", "password");
    const admin = await createAdminWithWalletAndPermissions(
        c.server,
        "admin@atitlan.io",
        "password"
    );
    const userWallet = await createUserOwnedWallet(
        c.server,
        "My stacks of auro",
        user.cookie,
    );
    const recurrentePaymentGateway = await createRecurrentePaymentGateway(
        c.server,
        admin.cookie,
        "AIO-Recurrente-PaymentGateway",
        "AIO-Recurrente-Wallet",
        "sk_test_4e",
        publisheableApiKey,
        endpointSecret
    );
    const recurrenteWallet = await getWalletDetails(
        c.server,
        admin.cookie,
        recurrentePaymentGateway.sourceWalletId.toString()
    );
    expect(recurrenteWallet.name).toBe("AIO-Recurrente-Wallet");

    const addFundsInvoice = await createInvoice(
        c.server,
        {
            amount: "1000",
            currency: "EUR",
            description: "Add funds to my wallet",
            targetWalletId: userWallet.id,
        },
        user.cookie
    );
    expect(addFundsInvoice.amount.toString()).toBe("1000");

    const transactionIntent = await createTransactionIntent(
        c.server,
        user.cookie,
        addFundsInvoice.id.toString(),
        userWallet.id.toString()
    );
    expect(transactionIntent.status).toBe("created");

    const recurrenteHostedCheckout = await startRecurrenteHostedCheckout(
        c.server,
        transactionIntent.id.toString(),
        recurrentePaymentGateway.id.toString()
    );
    expect(recurrenteHostedCheckout.recurrenteUrl).toBe(
        "https://mock-checkout-url.com/checkout_session_id"
    );
    expect(c.fetchSpy).toHaveBeenLastCalledWith(
        "https://app.recurrente.com/api/checkouts/",
        {
            method: "POST",
            headers: {
                "X-PUBLIC-KEY": "pk_test_4e",
                "X-SECRET-KEY": "sk_test_4e",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                items: [
                    {
                        name: "Thor AtitlanIO",
                        currency: "EUR",
                        amount_in_cents: "100000",
                    },
                ],
                success_url: `https://thor.atitlan.io/success?transactionIntentId=${transactionIntent.id}`,
                cancel_url: `https://thor.atitlan.io/cancel?transactionIntentId=${transactionIntent.id}`,
            }),
        }
    );

    const transactionIntentAfterCheckout = await getTransactionIntent(
        c.server,
        user.cookie,
        transactionIntent.id.toString()
    );
    console.log("transactionIntentAfterCheckout: ", transactionIntentAfterCheckout);

    expect(transactionIntentAfterCheckout.status).toBe("processing");
    expect(transactionIntentAfterCheckout.paymentGatewayIdentifier).toBe(mockCheckoutId);

    c.logSpy.mockClear();
    let recurrenteEvent: {
        api_version: string;
        checkout: { id: string };
        created_at: string;
        customer_id: string;
        event_type: string;
        id: string;
        product: { id: string };
    } = await TestUtils.readFileToJson(checkoutUnknownJSONPath);
    const recurrenteLoggingEvent = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_LOGGING)
        .send(recurrenteEvent);
    expect(recurrenteLoggingEvent.status).toBe(200);
    expect(c.logSpy).toHaveBeenLastCalledWith(`Recurrente logging webhook - body: ${JSON.stringify(recurrenteEvent, null, 2)}`);

    const recurrenteCheckoutEvent = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_CHECKOUT_SESSION)
        .send(recurrenteEvent);
    expect(c.logSpy).toHaveBeenCalledWith(`Unhandled event type payment_intent.unknown for recurrenteId ch_act3edhg85ruj72e paymentGatewayId ${recurrentePaymentGateway.id.toString()} and transactionIntentId ${transactionIntent.id.toString()}`);
    expect(recurrenteCheckoutEvent.status).toBe(200);
    const afterUnknownTransactionIntent = await getTransactionIntent(
        c.server,
        user.cookie,
        transactionIntent.id.toString()
    );
    if (!afterUnknownTransactionIntent.completedTransactions) {
        throw new Error("Transaction not found");
    }
    expect(afterUnknownTransactionIntent.completedTransactions?.length).toBe(0);
    expect(afterUnknownTransactionIntent.status).toBe("processing");

    recurrenteEvent = await TestUtils.readFileToJson(checkoutCompletedJSONPath);
    const recurrenteLoggingEvent2 = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_LOGGING)
        .send(recurrenteEvent);
    expect(recurrenteLoggingEvent2.status).toBe(200);
    const recurrenteCheckoutEvent2 = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_CHECKOUT_SESSION)
        .send(recurrenteEvent);
    expect(recurrenteCheckoutEvent2.status).toBe(200);
    const completedTransactionIntent = await getTransactionIntent(
        c.server,
        user.cookie,
        transactionIntent.id.toString()
    );
    if (!completedTransactionIntent.completedTransactions) {
        throw new Error("Transaction not found");
    }
    expect(completedTransactionIntent.status).toBe("success");
    expect(completedTransactionIntent.completedTransactions?.length).toBe(1);
});

test<ApiIntegrationTest>("Recurrente - full flow - pay business - 200 ok", async (c) => {
    const publisheableApiKey = "pk_test_4e";
    const endpointSecret = "whsec_4e";
    const user = await createUser(c.server, "user1@atitlan.io", "password");
    const businessUser = await createUser(
        c.server,
        "user2@atitlan.io",
        "password"
    );
    const businessWallet = await createUserOwnedWallet(
        c.server,
        "Restaurant",
        businessUser.cookie,
    );
    const admin = await createAdminWithWalletAndPermissions(
        c.server,
        "admin@atitlan.io",
        "password"
    );
    const userWallet = await createUserOwnedWallet(
        c.server,
        "My stacks of auro",
        user.cookie,
    );
    const recurrentePaymentGateway = await createRecurrentePaymentGateway(
        c.server,
        admin.cookie,
        "AIO-Recurrente-PaymentGateway",
        "AIO-Recurrente-Wallet",
        "sk_test_4e",
        publisheableApiKey,
        endpointSecret
    );
    const recurrenteWallet = await getWalletDetails(
        c.server,
        admin.cookie,
        recurrentePaymentGateway.sourceWalletId.toString()
    );
    expect(recurrenteWallet.name).toBe("AIO-Recurrente-Wallet");

    const addFundsInvoice = await createInvoice(
        c.server,
        {
            amount: "1000",
            currency: "EUR",
            description: "Paying a restaurant",
            targetWalletId: businessWallet.id,
        },
        user.cookie
    );
    expect(addFundsInvoice.amount.toString()).toBe("1000");

    const transactionIntent = await createTransactionIntent(
        c.server,
        user.cookie,
        addFundsInvoice.id.toString(),
        userWallet.id.toString()
    );
    expect(transactionIntent.status).toBe("created");

    const recurrenteHostedCheckout = await startRecurrenteHostedCheckout(
        c.server,
        transactionIntent.id.toString(),
        recurrentePaymentGateway.id.toString()
    );
    expect(recurrenteHostedCheckout.recurrenteUrl).toBe(
        "https://mock-checkout-url.com/checkout_session_id"
    );
    expect(c.fetchSpy).toHaveBeenLastCalledWith(
        "https://app.recurrente.com/api/checkouts/",
        {
            method: "POST",
            headers: {
                "X-PUBLIC-KEY": "pk_test_4e",
                "X-SECRET-KEY": "sk_test_4e",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                items: [
                    {
                        name: "Thor AtitlanIO",
                        currency: "EUR",
                        amount_in_cents: "100000",
                    },
                ],
                success_url: `https://thor.atitlan.io/success?transactionIntentId=${transactionIntent.id}`,
                cancel_url: `https://thor.atitlan.io/cancel?transactionIntentId=${transactionIntent.id}`,
            }),
        }
    );
    const transactionIntentAfterCheckout = await getTransactionIntent(
        c.server,
        user.cookie,
        transactionIntent.id.toString()
    );
    console.log("transactionIntentAfterCheckout: ", transactionIntentAfterCheckout);

    expect(transactionIntentAfterCheckout.status).toBe("processing");
    expect(transactionIntentAfterCheckout.paymentGatewayIdentifier).toBe(mockCheckoutId);

    c.logSpy.mockClear();
    const recurrenteEvent: RecurrenteEvent = await TestUtils.readFileToJson(checkoutCompletedJSONPath);
    const recurrenteLoggingEvent = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_LOGGING)
        .send(recurrenteEvent);
    expect(recurrenteLoggingEvent.status).toBe(200);
    expect(c.logSpy).toHaveBeenLastCalledWith(`Recurrente logging webhook - body: ${JSON.stringify(recurrenteEvent, null, 2)}`);

    console.log("checkout_session fixture: ", recurrenteEvent);
    const recurrenteCheckoutEvent = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_CHECKOUT_SESSION)
        .send(recurrenteEvent);
    expect(recurrenteCheckoutEvent.status).toBe(200);
    const completedTransactionIntent = await getTransactionIntent(
        c.server,
        user.cookie,
        transactionIntent.id.toString()
    );
    if (!completedTransactionIntent.completedTransactions) {
        throw new Error("Transaction not found");
    }
    const addFundsCompletedTransaction =
        completedTransactionIntent.completedTransactions.find(i => i.transaction?.targetWalletId === userWallet.id);
    const completedInvoiceTransaction =
        completedTransactionIntent.completedTransactions.find(i => i.transaction?.targetWalletId === businessWallet.id);

    expect(completedTransactionIntent.status).toBe("success");
    expect(completedTransactionIntent.completedTransactions?.length).toBe(2);
    expect(addFundsCompletedTransaction?.transaction?.id).toBeDefined();
    expect(addFundsCompletedTransaction?.transaction?.sourceCurrency).toBe("EUR");
    expect(addFundsCompletedTransaction?.transaction?.sourceAmount).toBe("1000");
    expect(addFundsCompletedTransaction?.transaction?.targetCurrency).toBe("EUR");
    expect(addFundsCompletedTransaction?.transaction?.targetAmount).toBe("1000");
    expect(addFundsCompletedTransaction?.transaction?.sourceWalletId).toBe(
        recurrenteWallet.id
    );
    expect(addFundsCompletedTransaction?.transaction?.targetWalletId).toBe(
        userWallet.id
    );

    expect(completedInvoiceTransaction?.transaction?.id).toBeDefined();
    expect(completedInvoiceTransaction?.transaction?.sourceCurrency).toBe("EUR");
    expect(completedInvoiceTransaction?.transaction?.sourceAmount).toBe("1000");
    expect(completedInvoiceTransaction?.transaction?.targetCurrency).toBe("EUR");
    expect(completedInvoiceTransaction?.transaction?.targetAmount).toBe("1000");
    expect(completedInvoiceTransaction?.transaction?.sourceWalletId).toBe(
        userWallet.id
    );
    expect(completedInvoiceTransaction?.transaction?.targetWalletId).toBe(
        businessWallet.id
    );
});

test<ApiIntegrationTest>("Recurrente - full flow - pay business - recurrente failed", async (c) => {
    const publisheableApiKey = "pk_test_4e";
    const endpointSecret = "whsec_4e";
    const user = await createUser(c.server, "user1@atitlan.io", "password");
    const businessUser = await createUser(
        c.server,
        "user2@atitlan.io",
        "password"
    );
    const businessWallet = await createUserOwnedWallet(
        c.server,
        "Restaurant",
        businessUser.cookie,
    );
    const admin = await createAdminWithWalletAndPermissions(
        c.server,
        "admin@atitlan.io",
        "password"
    );
    const userWallet = await createUserOwnedWallet(
        c.server,
        "My stacks of auro",
        user.cookie,
    );
    const recurrentePaymentGateway = await createRecurrentePaymentGateway(
        c.server,
        admin.cookie,
        "AIO-Recurrente-PaymentGateway",
        "AIO-Recurrente-Wallet",
        "sk_test_4e",
        publisheableApiKey,
        endpointSecret
    );
    const recurrenteWallet = await getWalletDetails(
        c.server,
        admin.cookie,
        recurrentePaymentGateway.sourceWalletId.toString()
    );
    expect(recurrenteWallet.name).toBe("AIO-Recurrente-Wallet");

    const addFundsInvoice = await createInvoice(
        c.server,
        {
            amount: "1000",
            currency: "EUR",
            description: "Paying a restaurant",
            targetWalletId: businessWallet.id,
        },
        user.cookie
    );
    expect(addFundsInvoice.amount.toString()).toBe("1000");

    const transactionIntent = await createTransactionIntent(
        c.server,
        user.cookie,
        addFundsInvoice.id.toString(),
        userWallet.id.toString()
    );
    expect(transactionIntent.status).toBe("created");

    const recurrenteHostedCheckout = await startRecurrenteHostedCheckout(
        c.server,
        transactionIntent.id.toString(),
        recurrentePaymentGateway.id.toString()
    );
    expect(recurrenteHostedCheckout.recurrenteUrl).toBe(
        "https://mock-checkout-url.com/checkout_session_id"
    );
    expect(c.fetchSpy).toHaveBeenLastCalledWith(
        "https://app.recurrente.com/api/checkouts/",
        {
            method: "POST",
            headers: {
                "X-PUBLIC-KEY": "pk_test_4e",
                "X-SECRET-KEY": "sk_test_4e",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                items: [
                    {
                        name: "Thor AtitlanIO",
                        currency: "EUR",
                        amount_in_cents: "100000",
                    },
                ],
                success_url: `https://thor.atitlan.io/success?transactionIntentId=${transactionIntent.id}`,
                cancel_url: `https://thor.atitlan.io/cancel?transactionIntentId=${transactionIntent.id}`,
            }),
        }
    );

    const transactionIntentAfterCheckout = await getTransactionIntent(
        c.server,
        user.cookie,
        transactionIntent.id.toString()
    );
    console.log("transactionIntentAfterCheckout: ", transactionIntentAfterCheckout);

    expect(transactionIntentAfterCheckout.status).toBe("processing");
    expect(transactionIntentAfterCheckout.paymentGatewayIdentifier).toBe(mockCheckoutId);

    c.logSpy.mockClear();
    const recurrenteEvent: {
        api_version: string;
        checkout: { id: string };
        created_at: string;
        customer_id: string;
        event_type: string;
        id: string;
        product: { id: string };
    } = await TestUtils.readFileToJson(checkoutFailedJSONPath);
    const recurrenteLoggingEvent = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_LOGGING)
        .send(recurrenteEvent);
    expect(recurrenteLoggingEvent.status).toBe(200);
    expect(c.logSpy).toHaveBeenLastCalledWith(`Recurrente logging webhook - body: ${JSON.stringify(recurrenteEvent, null, 2)}`);

    console.log("checkout_session fixture: ", recurrenteEvent);
    const recurrenteCheckoutEvent = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_CHECKOUT_SESSION)
        .send(recurrenteEvent);
    expect(recurrenteCheckoutEvent.status).toBe(200);
    const completedTransactionIntent = await getTransactionIntent(
        c.server,
        user.cookie,
        transactionIntent.id.toString()
    );
    if (!completedTransactionIntent.completedTransactions) {
        throw new Error("Transaction not found");
    }
    expect(completedTransactionIntent.status).toBe("failed");
    expect(completedTransactionIntent.completedTransactions?.length).toBe(0);
});

test<ApiIntegrationTest>("Recurrente - createGateway - 403 Unauthorized Access", async (c) => {
    const response = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.CREATE_PAYMENT_GATEWAY)
        .send({
            paymentGatewayName: "TestGateway",
            paymentGatewayWalletName: "TestWallet",
            secretApiKey: "secretKey",
            publicApiKey: "publicKey",
            endpointSecret: "endpointSecret"
        });

    expect(response.status).toBe(401);
});

test<ApiIntegrationTest>("Recurrente - recurrenteHostedCheckoutPaymentGateway - 400 Invalid Input", async (c) => {
    if (!c.admin) throw new Error("Admin not found");

    const invalidTransactionIntentId = "invalidId";
    const invalidPaymentGatewayId = "invalidId";

    const response = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.HOSTED_CHECKOUT)
        .set("Cookie", c.admin.cookie)
        .send({
            transactionIntentId: invalidTransactionIntentId,
            paymentGatewayId: invalidPaymentGatewayId
        });

    expect(response.status).toBe(400);
});

test<ApiIntegrationTest>("Recurrente - Get Settings - 200 ok", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@atitlan.io", "password");
    const recurrentePaymentGateway = await createRecurrentePaymentGateway(
        c.server,
        admin.cookie,
        "AIO-Recurrente-PaymentGateway",
        "AIO-Recurrente-Wallet",
        "sk_test_4e",
        "pk_test_4e",
        "whsec_4e"
    );
    c.logSpy.mockClear();
    const details = await getPaymentGatewayDetails(c.server, admin.cookie, recurrentePaymentGateway.id.toString());
    expect(details.id).toBe(recurrentePaymentGateway.id.toString());
    expect(c.logSpy).toHaveBeenCalledWith(`PaymentGatewayController.getExtendedWithSettings - payment gateway found id:${details.id} sourceWalletId:${details.sourceWalletId} userId:${admin.id}`);
    const settings = details.settings;
    const apiKeys = details.apiKeys;
    const hasEndpointSetting = settings.some(setting =>
        setting.name === RecurrenteSettings.endpointSecret
        && setting.value === "whsec_4e");
    expect(hasEndpointSetting).toBe(true);

    const hasPublicApiKey = apiKeys.some(apiKey =>
        apiKey.name === RecurrenteSettings.publicApiKey
        && apiKey.apiKey === "pk_test_4e");
    expect(hasPublicApiKey).toBe(true);
    const hasSecretApiKey = apiKeys.some(apiKey =>
        apiKey.name === RecurrenteSettings.secretApiKey
        && apiKey.apiKey === "sk_test_4e");
    expect(hasSecretApiKey).toBe(true);
});

test<ApiIntegrationTest>("Recurrente - Get Settings - 401 no permission", async (c) => {
    const user = await createUser(c.server, "test@test.com", "password");
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@admin.com", "goodgood");
    const recurrentePaymentGateway = await createRecurrentePaymentGateway(
        c.server,
        admin.cookie,
        "AIO-Recurrente-PaymentGateway",
        "AIO-Recurrente-Wallet",
        "sk_test_4e",
        "pk_test_4e",
        "whsec_4e"
    );
    const response = await c.server.get(
        AtitlanApiRoutes.PAYMENT_GATEWAY.GET_DETAILS(recurrentePaymentGateway.id))
        .set("Cookie", user.cookie);
    expect(response.status).toBe(401);
    expect(c.logSpy).toHaveBeenLastCalledWith(`PaymentGatewayController.getExtendedWithSettings - user does not have permission to view payment gateway requestingUserId:${user.id} paymentGatewayId:${recurrentePaymentGateway.id}`);
});

test<ApiIntegrationTest>("Recurrente - Get Settings - 400 wrong paymentGatway", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "aio@adio.adieu", "password");
    const fakeId = "adlj13oiu1lu01=1=23192i3p1231";
    const response = await c.server.get(
        AtitlanApiRoutes.PAYMENT_GATEWAY.GET_DETAILS(fakeId))
        .set("Cookie", admin.cookie);
    console.log("Recurrente - Get Settings - 400 wrong paymentGatway  - response get ", response.body);

    expect(response.status).toBe(401);
    expect(c.logSpy).toHaveBeenCalledWith(`PaymentGatewayController.getExtendedWithSettings - user does not have permission to view payment gateway requestingUserId:${admin.id} paymentGatewayId:${fakeId}`);

    // const stripePaymentGateway = await createStripePaymentGateway(
    //     c.server,
    //     admin.cookie,
    //     "AIO-Stripe-PaymentGateway",
    //     "AIO-Stripe-Wallet",
    //     "sk_test_4e",
    //     "pk_test_4e",
    //     "whsec_4e"
    // );
    // const response2 = await c.server
    //     .get(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.UPDATE_SETTINGS(stripePaymentGateway.id.toString()))
    //     .set("Cookie", admin.cookie);
    // console.log("Recurrente - Get Settings - 400 wrong paymentGatway - response2: ", response2.body);
    // expect(response2.status).toBe(400);
    // expect(c.logSpy).toHaveBeenCalledWith(`Payment gateway ${stripePaymentGateway.id.toString()} is not a recurrente payment gateway`);

    // const recurrentePaymentGateway = await createRecurrentePaymentGateway(
    //     c.server,
    //     admin.cookie,
    //     "AIO-Recurrente-PaymentGateway",
    //     "AIO-Recurrente-Wallet",
    //     "sk_test_4e",
    //     "pk_test_4e",
    //     "whsec_4e"
    // );
    // const res = await getPrisma().apiKey.deleteMany({
    //     where: {
    //         PaymentGatewayApiKeyLink: {
    //             type: "Public",
    //         }
    //     }
    // });
    // console.log(res);
    // const response3 = await c.server
    //     .get(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.GET_SETTINGS(recurrentePaymentGateway.id.toString()))
    //     .set("Cookie", admin.cookie);
    // console.log("response3: ", response3.body);
    // expect(response3.status).toBe(400);
    // expect(c.logSpy).toHaveBeenLastCalledWith(`Payment gateway ${recurrentePaymentGateway.id.toString()} does not have a public api key`);
});

test<ApiIntegrationTest>("Recurrente - createGateway - 403 Unauthorized Access", async (c) => {
    const response = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.CREATE_PAYMENT_GATEWAY)
        .send({
            paymentGatewayName: "TestGateway",
            paymentGatewayWalletName: "TestWallet",
            secretApiKey: "secretKey",
            publicApiKey: "publicKey",
            endpointSecret: "endpointSecret",
        });
    console.log("Recurrente - createGateway - 403 Unauthorized Access - response: ", response.body);
    expect(response.status).toBe(401);
});

test<ApiIntegrationTest>("Recurrente - recurrenteHostedCheckoutPaymentGateway - 400 Invalid Input", async (c) => {
    if (!c.admin) throw new Error("Admin not found");
    const invalidTransactionIntentId = "invalidId";
    const invalidPaymentGatewayId = "invalidId";

    const response = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.HOSTED_CHECKOUT)
        .set("Cookie", c.admin.cookie)
        .send({
            transactionIntentId: invalidTransactionIntentId,
            paymentGatewayId: invalidPaymentGatewayId
        });
    console.log("Recurrente - recurrenteHostedCheckoutPaymentGateway - 400 Invalid Input - response: ", response.body);
    expect(response.status).toBe(400);
});

test<ApiIntegrationTest>("Recurrente - Org - Create Payment Gateway - 200 ok", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@at.it", "password");
    const org = await createOrganization(c.server, admin.cookie, "AtitlanIO");
    const recurrentePaymentGateway = await createRecurrentePaymentGateway(
        c.server,
        admin.cookie,
        "AIO-Recurrente-PaymentGateway",
        "AIO-Recurrente-Wallet",
        "sk_test_4e",
        "pk_test_4e",
        "whsec_4e",
        org.id
    );
    const adminWalletRole = org.defaultWalletRoles.find(role => role.name === DefaultWalletRoleNames.PaymentGatewayAdmin);
    const ownerWalletRole = org.defaultWalletRoles.find(role => role.name === DefaultWalletRoleNames.PaymentGatewayOwner);
    if (!adminWalletRole || !ownerWalletRole) throw new Error("Roles not found");

    const orgWallet = await getOrganizationWalletDetails(c.server, admin.cookie, org.id, recurrentePaymentGateway.sourceWalletId);
    console.log(JSON.stringify(orgWallet, null, 2));
    expect(orgWallet.name).toBe("AIO-Recurrente-Wallet");
    expect(orgWallet.authorizedOrganizationUsers.length).toBe(1);
    expect(orgWallet.authorizedOrganizationUsers[0].organizationId).toBe(org.id);
    expect(orgWallet.authorizedOrganizationUsers[0].userId).toBe(admin.id);
    expect(orgWallet.authorizedOrganizationUsers[0].roleId).toBe(adminWalletRole.id);
    expect(orgWallet.authorizedOrganizationUsers[0].walletId).toBe(orgWallet.id);
    expect(orgWallet.authorizedOrganizations.length).toBe(1);
    expect(orgWallet.authorizedOrganizations[0].organizationId).toBe(org.id);
    expect(orgWallet.authorizedOrganizations[0].roleId).toBe(ownerWalletRole.id);
    expect(orgWallet.authorizedOrganizations[0].walletId).toBe(orgWallet.id);
    expect(orgWallet.authorizedUsers.length).toBe(0);

    const orgAfterPaymentGateway = await getOrganizationDetails(c.server, admin.cookie, org.id);
    expect(orgAfterPaymentGateway.wallets.length).toBe(1);
    expect(orgAfterPaymentGateway.wallets[0].name).toBe("AIO-Recurrente-Wallet");
    expect(orgAfterPaymentGateway.wallets[0].role.id).toBe(ownerWalletRole.id);
    expect(orgAfterPaymentGateway.wallets[0].role.name).toBe(ownerWalletRole.name);
    expect(orgAfterPaymentGateway.wallets[0].role.permissions).toEqual(ownerWalletRole.permissions);
    expect(orgAfterPaymentGateway.wallets[0].paymentGateway?.name).toBe("AIO-Recurrente-PaymentGateway");
    expect(orgAfterPaymentGateway.authorizedWalletUsers.length).toBe(1);
    expect(orgAfterPaymentGateway.authorizedWalletUsers[0].userId).toBe(admin.id);
    expect(orgAfterPaymentGateway.authorizedWalletUsers[0].roleId).toBe(adminWalletRole.id);
    expect(orgAfterPaymentGateway.authorizedWalletUsers[0].walletId).toBe(recurrentePaymentGateway.sourceWalletId);
    expect(orgAfterPaymentGateway.authorizedWalletUsers[0].organizationId).toBe(org.id);
});

test<ApiIntegrationTest>("Recurrente - Anonymous wallet flow - 200 ok", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@a.io", "password");
    const recurrentePaymentGateway = await createRecurrentePaymentGateway(
        c.server,
        admin.cookie,
        "AIO-Recurrente-PaymentGateway",
        "AIO-Recurrente-Wallet",
        "sk_test_4e",
        "pk_test_4e",
        "whsec_4e"
    );
    const bizUser = await createUser(c.server, "user@biz.com", "password");
    const bizWallet = await createUserOwnedWallet(c.server, "My personal Business Wallet", bizUser.cookie);

    const invoice = await createInvoice(
        c.server,
        {
            amount: "1000",
            currency: "EUR",
            description: "Anonymous wallet flow",
            targetWalletId: bizWallet.id,
        },
        bizUser.cookie
    );

    const anonymousIntent = await createAnonymousTransactionIntent(
        c.server,
        invoice.id.toString(),
    );
    expect(anonymousIntent.status).toBe(TransactionIntentStatus.Created);
    expect(anonymousIntent.sourceWalletId).toBe("");
    expect(anonymousIntent.anonymous).toBe(true);
    expect(anonymousIntent.invoiceId).toBe(invoice.id.toString());
    const intentAfterAnonymousIntent = await getTransactionIntent(c.server, admin.cookie, anonymousIntent.id.toString());
    expect(intentAfterAnonymousIntent.status).toBe(TransactionIntentStatus.Created);
    expect(intentAfterAnonymousIntent.completedTransactions.length).toBe(0);

    const hostedCheckout = await startRecurrenteHostedCheckout(
        c.server,
        anonymousIntent.id.toString(),
        recurrentePaymentGateway.id.toString()
    );
    const intentAfterHostedCheckout = await getTransactionIntent(c.server, admin.cookie, anonymousIntent.id.toString());
    expect(intentAfterHostedCheckout.status).toBe(TransactionIntentStatus.Processing);
    expect(intentAfterHostedCheckout.anonymous).toBe(true);
    expect(intentAfterHostedCheckout.completedTransactions.length).toBe(0);
    expect(intentAfterHostedCheckout.sourceWalletId).toBe("");
    expect(intentAfterHostedCheckout.requestingUserId).toBe("");

    const successfullCheckout: RecurrenteEvent = await TestUtils.readFileToJson(checkoutCompletedJSONPath);
    const webhookCheckout = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_CHECKOUT_SESSION)
        .send(successfullCheckout);
    expect(webhookCheckout.status).toBe(200);
    const intentAfterWebhook = await getTransactionIntent(c.server, admin.cookie, anonymousIntent.id.toString());
    expect(intentAfterWebhook.status).toBe(TransactionIntentStatus.Success);
    expect(intentAfterWebhook.anonymous).toBe(true);
    expect(intentAfterWebhook.requestingUserId).toBe("");
    expect(intentAfterWebhook.sourceWalletId).toBeDefined();
    expect(intentAfterWebhook.completedTransactions?.length).toBe(2);
    if (!intentAfterWebhook.sourceWalletId) throw new Error("Source wallet should now be set");
    const trx1 = intentAfterWebhook.completedTransactions.find(trx => trx.transaction?.sourceWalletId === recurrentePaymentGateway.sourceWalletId);
    const trx2 = intentAfterWebhook.completedTransactions.find(trx => trx.transaction?.targetWalletId === bizWallet.id);
    if (!trx1 || !trx2) throw new Error("Transaction not found");
    expect(trx1?.type).toBe(TransactionIntentCompletionType.PaymentGateway);
    expect(trx2?.type).toBe(TransactionIntentCompletionType.Internal);
    expect(trx1?.transaction?.sourceAmount).toBe("1000");
    expect(trx1?.transaction?.sourceCurrency).toBe("EUR");
    expect(trx1?.transaction?.targetAmount).toBe("1000");
    expect(trx1?.transaction?.targetCurrency).toBe("EUR");
    expect(trx1?.transaction?.sourceWalletId).toBe(recurrentePaymentGateway.sourceWalletId);
    expect(trx1?.transaction?.targetWalletId).toBe(intentAfterWebhook.sourceWalletId);
    expect(trx2?.transaction?.sourceAmount).toBe("1000");
    expect(trx2?.transaction?.sourceCurrency).toBe("EUR");
    expect(trx2?.transaction?.targetAmount).toBe("1000");
    expect(trx2?.transaction?.targetCurrency).toBe("EUR");
    expect(trx2?.transaction?.sourceWalletId).toBe(intentAfterWebhook.sourceWalletId);
    expect(trx2?.transaction?.targetWalletId).toBe(bizWallet.id);

    const anonymousWallet = await getPrisma().wallet.findUnique({
        where: { id: intentAfterWebhook.sourceWalletId },
        include: {
            incomingTransactions: true,
            outgoingTransactions: true,
            authorizedUsers: true,
            authorizedOrganizations: true,
            authorizedOrganizationUsers: true,
        }
    });
    if (!anonymousWallet) throw new Error();
    expect(anonymousWallet.name).toBe("Anonymous Wallet");
    expect(anonymousWallet.authorizedUsers.length).toBe(0);
    expect(anonymousWallet.authorizedOrganizations.length).toBe(0);
    expect(anonymousWallet.authorizedOrganizationUsers.length).toBe(0);
    expect(anonymousWallet.incomingTransactions?.length).toBe(1);
    expect(anonymousWallet.outgoingTransactions?.length).toBe(1);
    const walletTrxIn = anonymousWallet.incomingTransactions[0];
    const walletTrxOut = anonymousWallet.outgoingTransactions[0];
    expect(walletTrxIn.sourceAmount.toString()).toBe("1000");
    expect(walletTrxIn.sourceCurrency).toBe("EUR");
    expect(walletTrxIn.targetAmount.toString()).toBe("1000");
    expect(walletTrxIn.targetCurrency).toBe("EUR");
    expect(walletTrxIn.sourceWalletId).toBe(recurrentePaymentGateway.sourceWalletId);
    expect(walletTrxIn.targetWalletId).toBe(anonymousWallet.id);
    expect(walletTrxOut.sourceAmount.toString()).toBe("1000");
    expect(walletTrxOut.sourceCurrency).toBe("EUR");
    expect(walletTrxOut.targetAmount.toString()).toBe("1000");
    expect(walletTrxOut.targetCurrency).toBe("EUR");
    expect(walletTrxOut.sourceWalletId).toBe(anonymousWallet.id);
    expect(walletTrxOut.targetWalletId).toBe(bizWallet.id);
});
test<ApiIntegrationTest>("Recurrente - Anonymous wallet flow failed payment -  200 ok", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@a.io", "password");
    const recurrentePaymentGateway = await createRecurrentePaymentGateway(
        c.server,
        admin.cookie,
        "AIO-Recurrente-PaymentGateway",
        "AIO-Recurrente-Wallet",
        "sk_test_4e",
        "pk_test_4e",
        "whsec_4e"
    );
    const bizUser = await createUser(c.server, "user@biz.com", "password");
    const bizWallet = await createUserOwnedWallet(c.server, "My personal Business Wallet", bizUser.cookie);

    const invoice = await createInvoice(
        c.server,
        {
            amount: "1000",
            currency: "EUR",
            description: "Anonymous wallet flow",
            targetWalletId: bizWallet.id,
        },
        bizUser.cookie
    );

    const anonymousIntent = await createAnonymousTransactionIntent(
        c.server,
        invoice.id.toString(),
    );
    expect(anonymousIntent.status).toBe(TransactionIntentStatus.Created);
    expect(anonymousIntent.sourceWalletId).toBe("");
    expect(anonymousIntent.anonymous).toBe(true);
    expect(anonymousIntent.invoiceId).toBe(invoice.id.toString());
    const intentAfterAnonymousIntent = await getTransactionIntent(c.server, admin.cookie, anonymousIntent.id.toString());
    expect(intentAfterAnonymousIntent.status).toBe(TransactionIntentStatus.Created);
    expect(intentAfterAnonymousIntent.completedTransactions.length).toBe(0);

    const hostedCheckout = await startRecurrenteHostedCheckout(
        c.server,
        anonymousIntent.id.toString(),
        recurrentePaymentGateway.id.toString()
    );
    const intentAfterHostedCheckout = await getTransactionIntent(c.server, admin.cookie, anonymousIntent.id.toString());
    expect(intentAfterHostedCheckout.status).toBe(TransactionIntentStatus.Processing);
    expect(intentAfterHostedCheckout.anonymous).toBe(true);
    expect(intentAfterHostedCheckout.completedTransactions.length).toBe(0);
    expect(intentAfterHostedCheckout.sourceWalletId).toBe("");
    expect(intentAfterHostedCheckout.requestingUserId).toBe("");

    const unknownCheckoutEvent: RecurrenteEvent = await TestUtils.readFileToJson(checkoutUnknownJSONPath);
    const unknownWebhookCheckout = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_CHECKOUT_SESSION)
        .send(unknownCheckoutEvent);
    expect(unknownWebhookCheckout.status).toBe(200);
    const intentAfterUnknownWebhook = await getTransactionIntent(c.server, admin.cookie, anonymousIntent.id.toString());
    expect(intentAfterUnknownWebhook.status).toBe(TransactionIntentStatus.Processing);
    expect(intentAfterUnknownWebhook.anonymous).toBe(true);
    expect(intentAfterUnknownWebhook.requestingUserId).toBe("");
    expect(intentAfterUnknownWebhook.sourceWalletId).toBe("");

    const failedCheckout: RecurrenteEvent = await TestUtils.readFileToJson(checkoutFailedJSONPath);
    const webhookCheckout = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_CHECKOUT_SESSION)
        .send(failedCheckout);
    expect(webhookCheckout.status).toBe(200);
    const intentAfterWebhook = await getTransactionIntent(c.server, admin.cookie, anonymousIntent.id.toString());
    expect(intentAfterWebhook.status).toBe(TransactionIntentStatus.Failed);
    expect(intentAfterWebhook.anonymous).toBe(true);
    expect(intentAfterWebhook.requestingUserId).toBe("");
    expect(intentAfterWebhook.sourceWalletId).toBe("");
    expect(intentAfterWebhook.completedTransactions.length).toBe(0);
});

test<ApiIntegrationTest>("Recurrente - Anonymous wallet flow to login/signup -  200 ok", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@a.io", "password");
    const recurrentePaymentGateway = await createRecurrentePaymentGateway(
        c.server,
        admin.cookie,
        "AIO-Recurrente-PaymentGateway",
        "AIO-Recurrente-Wallet",
        "sk_test_4e",
        "pk_test_4e",
        "whsec_4e"
    );
    const bizUser = await createUser(c.server, "user@biz.com", "password");
    const bizWallet = await createUserOwnedWallet(c.server, "My personal Business Wallet", bizUser.cookie);

    const invoice = await createInvoice(
        c.server,
        {
            amount: "1000",
            currency: "EUR",
            description: "Anonymous wallet flow",
            targetWalletId: bizWallet.id,
        },
        bizUser.cookie
    );

    const anonymousIntent = await createAnonymousTransactionIntent(
        c.server,
        invoice.id.toString(),
    );
    expect(anonymousIntent.status).toBe(TransactionIntentStatus.Created);
    expect(anonymousIntent.sourceWalletId).toBe("");
    expect(anonymousIntent.anonymous).toBe(true);
    expect(anonymousIntent.invoiceId).toBe(invoice.id.toString());
    const intentAfterAnonymousIntent = await getTransactionIntent(c.server, admin.cookie, anonymousIntent.id.toString());
    expect(intentAfterAnonymousIntent.status).toBe(TransactionIntentStatus.Created);
    expect(intentAfterAnonymousIntent.completedTransactions.length).toBe(0);

    const newUser = await createUser(c.server, "new@aio.io", "password");
    const newWallet = await createUserOwnedWallet(c.server, "New Wallet", newUser.cookie);

    const updatedToNonAnonymous = await updateAnonymousIntentWithUserWallet(
        c.server,
        anonymousIntent.id.toString(),
        newWallet.id.toString(),
        newUser.id,
        newUser.cookie
    );
    const intentAfterUpdate = await getTransactionIntent(c.server, admin.cookie, anonymousIntent.id.toString());
    expect(intentAfterUpdate.status).toBe(TransactionIntentStatus.Created);
    expect(intentAfterUpdate.anonymous).toBe(false);
    expect(intentAfterUpdate.completedTransactions.length).toBe(0);
    expect(intentAfterUpdate.sourceWalletId).toBe(newWallet.id);
    expect(intentAfterUpdate.requestingUserId).toBe(newUser.id);

    const hostedCheckout = await startRecurrenteHostedCheckout(
        c.server,
        anonymousIntent.id.toString(),
        recurrentePaymentGateway.id.toString()
    );
    const intentAfterHostedCheckout = await getTransactionIntent(c.server, admin.cookie, anonymousIntent.id.toString());
    expect(intentAfterHostedCheckout.status).toBe(TransactionIntentStatus.Processing);
    expect(intentAfterHostedCheckout.anonymous).toBe(false);
    expect(intentAfterHostedCheckout.completedTransactions.length).toBe(0);
    expect(intentAfterHostedCheckout.sourceWalletId).toBe(newWallet.id);
    expect(intentAfterHostedCheckout.requestingUserId).toBe(newUser.id);

    const successfullCheckout: RecurrenteEvent = await TestUtils.readFileToJson(checkoutCompletedJSONPath);
    const webhookCheckout = await c.server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_CHECKOUT_SESSION)
        .send(successfullCheckout);
    expect(webhookCheckout.status).toBe(200);
    const intentAfterWebhook = await getTransactionIntent(c.server, admin.cookie, anonymousIntent.id.toString());
    expect(intentAfterWebhook.status).toBe(TransactionIntentStatus.Success);
    expect(intentAfterWebhook.anonymous).toBe(false);
    expect(intentAfterWebhook.requestingUserId).toBe(newUser.id);
    expect(intentAfterWebhook.sourceWalletId).toBe(newWallet.id);
    expect(intentAfterWebhook.completedTransactions?.length).toBe(2);
    if (!intentAfterWebhook.sourceWalletId) throw new Error("Source wallet should now be set");
    const trx1 = intentAfterWebhook.completedTransactions.find(trx => trx.transaction?.sourceWalletId === recurrentePaymentGateway.sourceWalletId);
    const trx2 = intentAfterWebhook.completedTransactions.find(trx => trx.transaction?.targetWalletId === bizWallet.id);
    if (!trx1 || !trx2) throw new Error("Transaction not found");
    expect(trx1?.type).toBe(TransactionIntentCompletionType.PaymentGateway);
    expect(trx2?.type).toBe(TransactionIntentCompletionType.Internal);
    expect(trx1?.transaction?.sourceAmount).toBe("1000");
    expect(trx1?.transaction?.sourceCurrency).toBe("EUR");
    expect(trx1?.transaction?.targetAmount).toBe("1000");
    expect(trx1?.transaction?.targetCurrency).toBe("EUR");
    expect(trx1?.transaction?.sourceWalletId).toBe(recurrentePaymentGateway.sourceWalletId);
    expect(trx1?.transaction?.targetWalletId).toBe(intentAfterWebhook.sourceWalletId);
    expect(trx2?.transaction?.sourceAmount).toBe("1000");
    expect(trx2?.transaction?.sourceCurrency).toBe("EUR");
    expect(trx2?.transaction?.targetAmount).toBe("1000");
    expect(trx2?.transaction?.targetCurrency).toBe("EUR");
    expect(trx2?.transaction?.sourceWalletId).toBe(intentAfterWebhook.sourceWalletId);
    expect(trx2?.transaction?.targetWalletId).toBe(bizWallet.id);
});

test<ApiIntegrationTest>("Recurrente - Anonymous wallet flow to login/signup -  400 & 401", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@a.io", "password");
    const recurrentePaymentGateway = await createRecurrentePaymentGateway(
        c.server,
        admin.cookie,
        "AIO-Recurrente-PaymentGateway",
        "AIO-Recurrente-Wallet",
        "sk_test_4e",
        "pk_test_4e",
        "whsec_4e"
    );
    const bizUser = await createUser(c.server, "user@biz.com", "password");
    const bizWallet = await createUserOwnedWallet(c.server, "My personal Business Wallet", bizUser.cookie);

    const invoice = await createInvoice(
        c.server,
        {
            amount: "1000",
            currency: "EUR",
            description: "Anonymous wallet flow",
            targetWalletId: bizWallet.id,
        },
        bizUser.cookie
    );

    const anonymousIntent = await createAnonymousTransactionIntent(
        c.server,
        invoice.id.toString(),
    );
    expect(anonymousIntent.status).toBe(TransactionIntentStatus.Created);
    expect(anonymousIntent.sourceWalletId).toBe("");
    expect(anonymousIntent.anonymous).toBe(true);
    expect(anonymousIntent.invoiceId).toBe(invoice.id.toString());
    const intentAfterAnonymousIntent = await getTransactionIntent(c.server, admin.cookie, anonymousIntent.id.toString());
    expect(intentAfterAnonymousIntent.status).toBe(TransactionIntentStatus.Created);
    expect(intentAfterAnonymousIntent.completedTransactions.length).toBe(0);

    const newUser = await createUser(c.server, "new@aio.io", "password");
    const newWallet = await createUserOwnedWallet(c.server, "New Wallet", newUser.cookie);
    c.logSpy.mockReset();
    const notAllowedToUpdateToNonAnonymousWithNoWalletAccess = await c.server
        .post(AtitlanApiRoutes.TRANSACTION_INTENT.UPDATE_USER_AND_WALLET)
        .set('Cookie', newUser.cookie)
        .send({
            transactionIntentId: anonymousIntent.id,
            userWalletId: bizWallet.id,
        });
    expect(notAllowedToUpdateToNonAnonymousWithNoWalletAccess.status).toBe(401);
    expect(c.logSpy).toHaveBeenCalledWith(`updateIntentWithUserWallet - intent:${anonymousIntent.id} ` +
        `userId:${newUser.id} does not have permission to pay from walletId:${bizWallet.id}`);

    c.logSpy.mockReset();
    const notAllowedToUpdateToNormalIntentWithWrongId = await c.server
        .post(AtitlanApiRoutes.TRANSACTION_INTENT.UPDATE_USER_AND_WALLET)
        .set('Cookie', newUser.cookie)
        .send({
            transactionIntentId: newWallet.id,
            userWalletId: newWallet.id,
        });
    expect(notAllowedToUpdateToNormalIntentWithWrongId.status).toBe(400);
    expect(c.logSpy).toHaveBeenCalledWith(`updateIntentWithUserWallet - no intent found for intentId:${newWallet.id} for userId:${newUser.id}`);

    const newNormalIntent = await createTransactionIntent(c.server, newUser.cookie, invoice.id.toString(), newWallet.id);
    const notAllowedToUpdateToNormalIntentWhenAlreadyNormal = await c.server
        .post(AtitlanApiRoutes.TRANSACTION_INTENT.UPDATE_USER_AND_WALLET)
        .set('Cookie', newUser.cookie)
        .send({
            transactionIntentId: newWallet.id,
            userWalletId: newWallet.id,
        });

});