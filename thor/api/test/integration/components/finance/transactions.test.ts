import {
    beforeAll,
    expect,
    test,
    afterEach,
    beforeEach,
    vi,
} from "vitest"
import { ApiIntegrationTest, IntegrationTestFastResetDB } from "../../util/integration-test";
import supertest from "supertest";
import { getPrisma } from "../../../../src/util/prisma";
import { createTransactionHash } from "../../../../src/components/finance/transactions/transactions.service"
import { createUser, createAdminWithWalletAndPermissions, createInternalTransactionResponse, createInternalTransaction, createFundsTransactionResponse, createFundsTransaction, mintFunds, createStripePaymentGateway } from "../../factories/test-data-factory";
import { BalanceSheet } from "../../../../src/components/finance/wallet/wallet.types";
import AtitlanApiRoutes from "../../../../src/components/shared/routes";
import { Transaction } from "@prisma/client";
import { createUserOwnedWallet, getWalletDetails } from "../../factories/wallets.factory";

let server: supertest.SuperTest<supertest.Test>;

beforeAll(async () => {
    server = await IntegrationTestFastResetDB.beforeAll();
});

beforeEach<ApiIntegrationTest>(async (context: ApiIntegrationTest) => {
    await IntegrationTestFastResetDB.beforeEach(context, server);
});

afterEach<ApiIntegrationTest>(IntegrationTestFastResetDB.afterEach);

test<ApiIntegrationTest>("Transactions - seed, fund & transact - 200 ok", async (c) => {
    const user1 = await createUser(c.server, "user1@atitlan.io", "password");
    const user2 = await createUser(c.server, "user2@atitlan.io", "password");
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@atitlan.io", "password");

    const getPermissionResponse = await c.server
        .get(AtitlanApiRoutes.USER.PERMISSIONS)
        .set("Cookie", admin.cookie);
    expect(getPermissionResponse.status).toBe(200);
    const permissionsResult = getPermissionResponse.body;
    const permissions: {
        userId: string, action: string, resource: string, expiresAt: string
    }[] = permissionsResult.permissions;
    expect(permissions.length).toBe(4);

    const walletsResponse = await c.server
        .get(AtitlanApiRoutes.USER.WALLETS())
        .set("Cookie", admin.cookie);
    const wallets: {
        id: string;
        createdAt: Date;
        updatedAt: Date;
        lastUsed: Date | null;
        name: string;
        userId: string;
    }[] = walletsResponse.body.wallets;
    expect(walletsResponse.status).toBe(200);
    expect(wallets.length).toBe(2);
    const walletNames = wallets.map((w) => w.name);
    expect(walletNames).toContain("aio-minter");
    expect(walletNames).toContain("aio-funds");
    const minterWallet = wallets.find(wallet => wallet.name === "aio-minter");
    const fundsWallet = wallets.find(wallet => wallet.name === "aio-funds");
    if (!minterWallet || !fundsWallet) {
        throw new Error();
    }
    const minterWalletId = minterWallet.id
    const fundsWalletId = fundsWallet.id;

    const transactionDetails = {
        source: {
            walletId: minterWalletId,
            amount: "1",
            currency: "BTC",
        },
        target: {
            walletId: fundsWalletId,
            amount: "1",
            currency: "BTC",
        },
        description: "Adding funds",
    };
    const mintTransaction = await createFundsTransaction(
        c.server,
        transactionDetails.source,
        transactionDetails.target,
        "Initial mint during createInitialUserWalletWithFunds for admin.",
        admin.cookie);

    let walletDetails = await getWalletDetails(c.server, admin.cookie, fundsWalletId.toString());
    if (!walletDetails || !walletDetails.balance) {
        throw new Error();
    }

    // Check wallet balance
    const fundsBalance: BalanceSheet = walletDetails.balance;
    console.log(fundsBalance);
    console.log("HERE");
    expect(fundsBalance.BTC.net).toBe("1");
    expect(fundsBalance.BTC.incoming).toBe("1");
    expect(fundsBalance.BTC.outgoing).toBe("0");
    expect(Object.keys(fundsBalance).length).toBe(1);

    walletDetails = await getWalletDetails(c.server, admin.cookie, minterWalletId.toString());
    if (!walletDetails || !walletDetails.balance) {
        throw new Error();
    }
    const minterBalance: BalanceSheet = walletDetails.balance;
    expect(minterBalance.BTC.net).toBe("-1");
    expect(minterBalance.BTC.incoming).toBe("0");
    expect(minterBalance.BTC.outgoing).toBe("1");
    expect(Object.keys(minterBalance).length).toBe(1);

    // Check wallet transactions
    const fundsWalletDetails = await getWalletDetails(c.server, admin.cookie, fundsWalletId.toString());

    expect(fundsWalletDetails.id).toBe(fundsWalletId.toString());
    expect(fundsWalletDetails.name).toBe("aio-funds");
    if (!fundsWalletDetails.incomingTransactions) {
        throw new Error();
    }
    expect(fundsWalletDetails.incomingTransactions[0].sourceCurrency).toBe("BTC");
    expect(fundsWalletDetails.incomingTransactions[0].sourceAmount).toBe("1");
    expect(fundsWalletDetails.incomingTransactions[0].targetCurrency).toBe("BTC");
    expect(fundsWalletDetails.incomingTransactions[0].targetAmount).toBe("1");
    expect(fundsWalletDetails.incomingTransactions[0].description).toBe(`Initial mint during createInitialUserWalletWithFunds for admin.`);
    expect(fundsWalletDetails.incomingTransactions[0].sourceWalletId).toBe(minterWalletId.toString());
    expect(fundsWalletDetails.incomingTransactions[0].targetWalletId).toBe(fundsWalletId.toString());

    const mintTransactions = await getWalletDetails(c.server, admin.cookie, minterWalletId.toString());

    expect(mintTransactions.id).toBe(minterWalletId.toString());
    expect(mintTransactions.name).toBe("aio-minter");
    if (!mintTransactions.outgoingTransactions) {
        throw new Error();
    }
    expect(mintTransactions.outgoingTransactions[0].sourceCurrency).toBe("BTC");
    expect(mintTransactions.outgoingTransactions[0].sourceAmount).toBe("1");
    expect(mintTransactions.outgoingTransactions[0].targetCurrency).toBe("BTC");
    expect(mintTransactions.outgoingTransactions[0].description).toBe(`Initial mint during createInitialUserWalletWithFunds for admin.`);
    expect(mintTransactions.outgoingTransactions[0].sourceWalletId).toBe(minterWalletId.toString());
    expect(mintTransactions.outgoingTransactions[0].targetWalletId).toBe(fundsWalletId.toString());
    expect(mintTransactions.outgoingTransactions[0].targetAmount).toBe("1");
});

test<ApiIntegrationTest>("Transactions - user transaction - 401 no balance, 200 ok", async (c) => {
    const user1 = await createUser(c.server, "user1@atitlan.io", "password");
    const user2 = await createUser(c.server, "user2@atitlan.io", "password");
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@atitlan.io", "password");

    const user1Wallet = await createUserOwnedWallet(c.server, "user1-wallet1", user1.cookie);
    const user2Wallet = await createUserOwnedWallet(c.server, "user2-wallet1", user2.cookie);

    const getPermissionResponse = await c.server
        .get(AtitlanApiRoutes.USER.PERMISSIONS)
        .set("Cookie", admin.cookie);
    expect(getPermissionResponse.status).toBe(200);
    const permissionsResult = getPermissionResponse.body;
    const permissions: {
        userId: string, action: string, resource: string, expiresAt: string
    }[] = permissionsResult.permissions;
    expect(permissions.length).toBe(4);

    const adminWalletResponse = await c.server
        .get(AtitlanApiRoutes.USER.WALLETS())
        .set("Cookie", admin.cookie);
    expect(adminWalletResponse.status).toBe(200);
    const adminWallets: {
        id: string;
        createdAt: Date;
        updatedAt: Date;
        lastUsed: Date | null;
        name: string;
        userId: string;
    }[] = adminWalletResponse.body.wallets;
    expect(adminWallets.length).toBe(2);
    const adminWalletNames = adminWallets.map((w) => w.name);
    expect(adminWalletNames).toContain("aio-minter");
    expect(adminWalletNames).toContain("aio-funds");
    const minterWalletId = admin.wallets.sourceWallet.id.toString();
    const fundsWalletId = admin.wallets.fundsWallet.id.toString();  
    const transactionDetails = {
        source: {
            walletId: minterWalletId,
            amount: "1",
            currency: "BTC",
        },
        target: {
            walletId: fundsWalletId,
            amount: "1",
            currency: "BTC",
        },
        description: "Adding funds",
    };
    const mintTransaction = await createFundsTransaction(
        c.server,
        transactionDetails.source,
        transactionDetails.target,
        "Initial mint during createInitialUserWalletWithFunds for admin.",
        admin.cookie);

    let walletDetails = await getWalletDetails(
        c.server, admin.cookie, fundsWalletId.toString());

    if (!walletDetails || !walletDetails.balance) {
        throw new Error();
    }
    const fundsBalance: BalanceSheet = walletDetails.balance;
    expect(fundsBalance.BTC.net).toBe("1");
    expect(fundsBalance.BTC.incoming).toBe("1");
    expect(fundsBalance.BTC.outgoing).toBe("0");
    expect(Object.keys(fundsBalance).length).toBe(1);

    const walletsUser1Response = await c.server
        .get(AtitlanApiRoutes.USER.WALLETS())
        .set("Cookie", user1.cookie);
    const user1wallets: {
        id: string;
        createdAt: Date;
        updatedAt: Date;
        lastUsed: Date | null;
        name: string;
        userId: string;
    }[] = walletsUser1Response.body.wallets;
    expect(walletsUser1Response.status).toBe(200);
    expect(user1wallets.length).toBe(1);
    expect(user1wallets[0].name).toBe("user1-wallet1");

    const user2WalletResponse = await c.server
        .get(AtitlanApiRoutes.USER.WALLETS())
        .set("Cookie", user2.cookie);
    const user2wallets: {
        id: string;
        createdAt: Date;
        updatedAt: Date;
        lastUsed: Date | null;
        name: string;
        userId: string;
    }[] = user2WalletResponse.body.wallets;
    expect(user2WalletResponse.status).toBe(200);
    expect(user2wallets.length).toBe(1);
    expect(user2wallets[0].name).toBe("user2-wallet1");

    const noBalanceTransactionResponse = await createInternalTransactionResponse(
        c.server,
        {
            walletId: user1wallets[0].id.toString(),
            amount: "0.1",
            currency: "BTC",

        },
        {
            walletId: user2wallets[0].id.toString(),
            amount: "0.1",
            currency: "BTC",
        },
        "Transaction from user1 to user2 - this is going to fail because of no funds",
        user1.cookie);
    expect(noBalanceTransactionResponse.status).toBe(401);
    expect(noBalanceTransactionResponse.body.message).toBe("Insufficient funds");

    // transfer funds to user1 from the aio-funds wallet
    const user1BuysFromAIO = await createInternalTransaction(
        c.server,
        {
            walletId: fundsWalletId.toString(),
            amount: "0.1",
            currency: "BTC",

        },
        {
            walletId: user1wallets[0].id.toString(),
            amount: "0.1",
            currency: "BTC",
        },
        "Transaction from admin funds wallet to user1",
        admin.cookie);

    // Verify that the balance is now on the user1 wallet
    const user1WalletId = user1wallets[0].id;
    let user1WalletBalanceDetails = await getWalletDetails(c.server, user1.cookie, user1WalletId.toString());

    if (!user1WalletBalanceDetails || !user1WalletBalanceDetails.balance) {
        throw new Error();
    }
    let user1WalletBalance = user1WalletBalanceDetails.balance;
    expect(user1WalletBalance.BTC.net).toBe("0.1");
    expect(user1WalletBalance.BTC.incoming).toBe("0.1");
    expect(user1WalletBalance.BTC.outgoing).toBe("0");

    const withBalanceTransaction = await createInternalTransaction(
        c.server,
        {
            walletId: user1wallets[0].id.toString(),
            amount: "0.1",
            currency: "BTC",

        },
        {
            walletId: user2wallets[0].id.toString(),
            amount: "0.1",
            currency: "BTC",
        },
        "Transaction from user1 to user2",
        user1.cookie);

    user1WalletBalanceDetails = await getWalletDetails(
        c.server,
        user1.cookie,
        user1WalletId.toString());

    if (!user1WalletBalanceDetails || !user1WalletBalanceDetails.balance) {
        throw new Error();
    }
    user1WalletBalance = user1WalletBalanceDetails.balance;
    expect(user1WalletBalance.BTC.net).toBe("0");
    expect(user1WalletBalance.BTC.incoming).toBe("0.1");
    expect(user1WalletBalance.BTC.outgoing).toBe("0.1");

    const user2WalletId = user2wallets[0].id;
    let user2WalletBalanceResponse = await getWalletDetails(
        c.server,
        user2.cookie,
        user2WalletId.toString());
    if (!user2WalletBalanceResponse.balance) {
        throw Error();
    }
    let user2WalletBalance = user2WalletBalanceResponse.balance;
    expect(user2WalletBalance.BTC.net).toBe("0.1");
    expect(user2WalletBalance.BTC.incoming).toBe("0.1");
    expect(user2WalletBalance.BTC.outgoing).toBe("0");
});

test<ApiIntegrationTest>("Transactions - validate hash integrity - 200 ok", async (c) => {
    const user = await createUser(c.server, "user1@atitlan.io", "password");
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@atitlan.io", "password");
    const userWallet = await createUserOwnedWallet(c.server, "user1-wallet1", user.cookie);
    const fundWalletId = admin.wallets.fundsWallet.id.toString();;
    const userWalletId = userWallet.id.toString();

    const t1 = await createFundsTransaction(
        c.server,
        {
            walletId: fundWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        {
            walletId: userWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        "Transaction from fundsWallet to user1",
        admin.cookie);
    console.log(t1);

    const t2 = await createFundsTransaction(
        c.server,
        {
            walletId: fundWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        {
            walletId: userWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        "Transaction from fundsWallet to user1",
        admin.cookie);

    console.log(t2);
    const t3 = await createInternalTransaction(
        c.server,
        {
            walletId: userWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        {
            walletId: fundWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        "Transaction from fundsWallet to user1",
        user.cookie);
    console.log(t3);

    // Now check the integrity of all transactions
    const integrityCheckResponse = await c.server
        .get(AtitlanApiRoutes.TRANSACTIONS.CHECK_INTEGRITY)
        .set("Cookie", admin.cookie);

    expect(integrityCheckResponse.status).toBe(200);
    const integrityCheckResult = integrityCheckResponse.body;
    expect(integrityCheckResult.isIntegrityMaintained).toBe(true);
});

test<ApiIntegrationTest>("Transactions - validate hash integrity and fail if modified - 200 ok", async (c) => {
    const logSpy = vi.spyOn(console, 'log');

    const user = await createUser(c.server, "user1@atitlan.io", "password");
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@atitlan.io", "password");
    const userWallet = await createUserOwnedWallet(c.server, "user1-wallet1", user.cookie);
    const fundWalletId = admin.wallets.fundsWallet.id.toString();;
    const userWalletId = userWallet.id.toString();

    const t1 = await createFundsTransaction(
        c.server,
        {
            walletId: fundWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        {
            walletId: userWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        "Transaction from fundsWallet to user1",
        admin.cookie);

    const t2 = await createFundsTransaction(
        c.server,
        {
            walletId: fundWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        {
            walletId: userWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        "Transaction from fundsWallet to user1",
        admin.cookie);

    const t3 = await createInternalTransaction(
        c.server,
        {
            walletId: userWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        {
            walletId: fundWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        "Transaction from fundsWallet to user1",
        user.cookie);

    // Now check the integrity of all transactions
    const integrityCheckResponse = await c.server
        .get(AtitlanApiRoutes.TRANSACTIONS.CHECK_INTEGRITY)
        .set("Cookie", admin.cookie);

    expect(integrityCheckResponse.status).toBe(200);
    const integrityCheckResult = integrityCheckResponse.body;
    expect(integrityCheckResult.isIntegrityMaintained).toBe(true);

    // Now imagine a hacker modifies one of the transaction description and amounts (so naughty, especially that description!)
    const modifiedT2 = await getPrisma().transaction.update({
        where: {
            id: t2.id,
        },
        data: {
            targetAmount: 1000000,
            description: "This is legit don't worry",
        },
    });

    console.log("Modified transaction", modifiedT2);
    const t1Orignal = await getOriginalTransactionFromPrisma(t1.id);
    const previousHash = modifiedT2.previousHash !== "" && modifiedT2.previousHash ? modifiedT2.previousHash : t1Orignal?.hash;
    if (!previousHash) {
        throw Error();
    }
    const expectedHashCheck = createTransactionHash(
        previousHash,
        modifiedT2.sourceCurrency,
        modifiedT2.sourceAmount.toString(),
        modifiedT2.targetCurrency,
        modifiedT2.targetAmount.toString(),
        modifiedT2.sourceWalletId.toString(),
        modifiedT2.targetWalletId.toString(),
        modifiedT2.description ? modifiedT2.description : ""
    );
    const expectedLogMessage = `Transaction:${modifiedT2.id} hash:${modifiedT2.hash} does not match calculated hash:${expectedHashCheck}. This entry is most likely comprimised. Investigation advised!`;
    // Now check the integrity of all transactions
    const integrityCheckWhenModifiedResponse = await c.server
        .get(AtitlanApiRoutes.TRANSACTIONS.CHECK_INTEGRITY)
        .set("Cookie", admin.cookie);

    expect(integrityCheckWhenModifiedResponse.status).toBe(200);
    const integrityCheckResultWhenModified = integrityCheckWhenModifiedResponse.body;
    expect(integrityCheckResultWhenModified.isIntegrityMaintained).toBe(false);
    expect(logSpy).toHaveBeenLastCalledWith(expectedLogMessage);
    logSpy.mockRestore();
});

test<ApiIntegrationTest>("Transactions - validate hash integrity chain and fail when broken - 200 ok", async (c) => {
    const logSpy = vi.spyOn(console, 'log');

    const user = await createUser(c.server, "user1@atitlan.io", "password");
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@atitlan.io", "password");
    const userWallet = await createUserOwnedWallet(c.server, "user1-wallet1", user.cookie);
    const fundWalletId = admin.wallets.fundsWallet.id.toString();;
    const userWalletId = userWallet.id.toString();

    const t1 = await createFundsTransaction(
        c.server,
        {
            walletId: fundWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        {
            walletId: userWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        "Transaction from fundsWallet to user1",
        admin.cookie);

    const t2 = await createFundsTransaction(
        c.server,
        {
            walletId: fundWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        {
            walletId: userWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        "Transaction from fundsWallet to user1",
        admin.cookie);

    const t3 = await createInternalTransaction(
        c.server,
        {
            walletId: userWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        {
            walletId: fundWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        "Transaction from fundsWallet to user1",
        user.cookie);

    // Now check the integrity of all transactions
    const integrityCheckResponse = await c.server
        .get(AtitlanApiRoutes.TRANSACTIONS.CHECK_INTEGRITY)
        .set("Cookie", admin.cookie);

    expect(integrityCheckResponse.status).toBe(200);
    const integrityCheckResult = integrityCheckResponse.body;
    expect(integrityCheckResult.isIntegrityMaintained).toBe(true);
    const t1Orignal = await getOriginalTransactionFromPrisma(t1.id);
    const t2Orignal = await getOriginalTransactionFromPrisma(t2.id);
    const t3Orignal = await getOriginalTransactionFromPrisma(t3.id);
    if (!t1Orignal || !t2Orignal || !t3Orignal) throw new Error();
    const previousHash = getPreviousHash(t2Orignal, t1Orignal);

    // Now imagine a hacker modifies one of the transaction description 
    // with a technically legal transactionHash
    const insertedNaugthyDescription = "This is legit don't worry";
    const illegalHashThatShouldPass = createTransactionHash(
        previousHash,
        t2.sourceCurrency,
        t2.sourceAmount.toString(),
        t2.targetCurrency,
        t2.targetAmount.toString(),
        t2.sourceWalletId.toString(),
        t2.targetWalletId.toString(),
        insertedNaugthyDescription
    );
    const modifiedTransaction = await getPrisma().transaction.update({
        where: {
            id: t2.id,
        },
        data: {
            hash: illegalHashThatShouldPass,
            description: insertedNaugthyDescription,
        },
    });
    console.log("Modified transaction", modifiedTransaction);
    const expectedLogMessage = `Transaction:${t3Orignal.id} previousHash:${t3Orignal.previousHash} does not match lastHash:${modifiedTransaction.hash}.  Previous entry with id ${t2.id} most likely comprimised.  Investigation advised!`
    // Now check the integrity of all transactions again
    const integrityCheckWhenModifiedResponse = await c.server
        .get(AtitlanApiRoutes.TRANSACTIONS.CHECK_INTEGRITY)
        .set("Cookie", admin.cookie);
    expect(integrityCheckWhenModifiedResponse.status).toBe(200);
    const integrityCheckResultWhenModified = integrityCheckWhenModifiedResponse.body;
    expect(integrityCheckResultWhenModified.isIntegrityMaintained).toBe(false);
    expect(logSpy).toHaveBeenLastCalledWith(expectedLogMessage);
    logSpy.mockRestore();
});

async function getOriginalTransactionFromPrisma(id: string): Promise<Transaction | null> {
    return getPrisma().transaction.findUnique({
        where: {
            id: id,
        }
    });

}

function getPreviousHash(t2: Transaction, t1: Transaction) {
    return t2.previousHash !== "" && t2.previousHash ? t2.previousHash : t1.hash;
}

test<ApiIntegrationTest>("Transactions - get transaction details", async (c) => {
    const user = await createUser(c.server, "user1@atitlan.io", "password");
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@atitlan.io", "password");
    const userWallet = await createUserOwnedWallet(c.server, "user1-wallet1", user.cookie);
    const fundWalletId = admin.wallets.fundsWallet.id.toString();;
    const userWalletId = userWallet.id.toString();

    const transaction = await createFundsTransaction(
        c.server,
        {
            walletId: fundWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        {
            walletId: userWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        "Transaction from fundsWallet to user",
        admin.cookie);
    console.log(transaction);
    const transactionId = transaction.id;
    const transactionDetailsResponse = await c.server
        .get(AtitlanApiRoutes.TRANSACTIONS.GET(transactionId))
        .set("Cookie", admin.cookie);
    console.log(transactionDetailsResponse.body);

    const transactionDetails = transactionDetailsResponse.body.transaction;
    expect(transactionDetailsResponse.status).toBe(200);
    expect(transactionDetailsResponse.body.message).toBe("Transaction details retrieved successfully");
    expect(transactionDetails.id).toBe(transactionId.toString());
    expect(transactionDetails.description).toBe("Transaction from fundsWallet to user");
    expect(transactionDetails.sourceAmount).toBe("0.1");
    expect(transactionDetails.targetAmount).toBe("0.1");
    expect(transactionDetails.sourceCurrency).toBe("BTC");
    expect(transactionDetails.targetCurrency).toBe("BTC");
    expect(transactionDetails.sourceWalletId).toBe(fundWalletId.toString());
    expect(transactionDetails.targetWalletId).toBe(userWalletId.toString());
});

// Permission checks for all the endpoints with permissions
test<ApiIntegrationTest>("Transaction - Permission Check of Create Internal Transaction - 400, 401 & 200", async (c) => {
    const regularUser = await createUser(c.server, "regular@user.com", "password");
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@user.com", "password");
    const userWallet = await createUserOwnedWallet(c.server, "user1-wallet1", regularUser.cookie);
    const fundWalletId = admin.wallets.fundsWallet.id.toString();;
    const userWalletId = userWallet.id.toString();
    const mintedFunds = await mintFunds(c.server, "1", "BTC", "mint", admin.cookie, admin.wallets.sourceWallet.id, admin.wallets.fundsWallet.id);

    const transactionDetails = {
        source: {
            walletId: fundWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        target: {
            walletId: userWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        description: "Adding funds",
    };

    // Attempt to create internal transaction for a wallet that is not theirs
    const walletNotOwnedResponse = await createInternalTransactionResponse(
        c.server,
        transactionDetails.source,
        transactionDetails.target,
        transactionDetails.description,
        regularUser.cookie
    );
    expect(walletNotOwnedResponse.status).toBe(401); // Unauthorized
    expect(walletNotOwnedResponse.body.message).toBe("Unauthorized");

    // Attempt to create internal transaction as a user who owns the wallet (and happens to be the admin)
    const adminResponse = await createInternalTransactionResponse(
        c.server,
        transactionDetails.source,
        transactionDetails.target,
        transactionDetails.description,
        admin.cookie
    );
    expect(adminResponse.status).toBe(200); // Success response expected for admin

    const incorrectWalletId = Number.MAX_SAFE_INTEGER;
    const incorrectSource = {
        walletId: incorrectWalletId.toString(),
        amount: "100",
        currency: "BTC",
    }

    let incorrectWalletResponse = await createInternalTransactionResponse(
        c.server,
        transactionDetails.source,
        incorrectSource,
        transactionDetails.description,
        admin.cookie
    );
    expect(incorrectWalletResponse.status).toBe(400);
    expect(incorrectWalletResponse.body.message).toBe("Bad Request");

    incorrectWalletResponse = await createInternalTransactionResponse(
        c.server,
        incorrectSource,
        transactionDetails.target,
        transactionDetails.description,
        admin.cookie
    );
    expect(incorrectWalletResponse.status).toBe(400);
    expect(incorrectWalletResponse.body.message).toBe("Bad Request");
});

test<ApiIntegrationTest>("Transaction - Get Transaction Details by multiple users - 200", async (c) => {
    const regularUser = await createUser(c.server, "regular@user.com", "password");
    const anotherUser = await createUser(c.server, "another@user.com", "password");
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@user.com", "password");
    const userWallet = await createUserOwnedWallet(c.server, "user1-wallet1", regularUser.cookie);
    const fundWalletId = admin.wallets.fundsWallet.id.toString();;
    const userWalletId = userWallet.id.toString();

    // Presuming a transaction ID is available (use a real transaction ID here)
    const transaction = await createFundsTransaction(
        c.server,
        {
            walletId: fundWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        {
            walletId: userWalletId,
            amount: "0.1",
            currency: "BTC",
        },
        "Transaction from fundsWallet to user",
        admin.cookie
    );
    const transactionId = transaction.id;

    const regularUserResponse = await c.server
        .get(AtitlanApiRoutes.TRANSACTIONS.GET(transactionId))
        .set("Cookie", regularUser.cookie);
    expect(regularUserResponse.status).toBe(200);

    const adminResponse = await c.server
        .get(AtitlanApiRoutes.TRANSACTIONS.GET(transactionId))
        .set("Cookie", admin.cookie);
    expect(adminResponse.status).toBe(200);

    const anotherUserResponse = await c.server
        .get(AtitlanApiRoutes.TRANSACTIONS.GET(transactionId))
        .set("Cookie", anotherUser.cookie);
    expect(anotherUserResponse.status).toBe(200);
});

test<ApiIntegrationTest>("Transaction - Permission Check Transaction Integrity Check", async (c) => {
    const regularUser = await createUser(c.server, "regular@user.com", "password");
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@user.com", "password");

    const regularUserResponse = await c.server
        .get(AtitlanApiRoutes.TRANSACTIONS.CHECK_INTEGRITY)
        .set("Cookie", regularUser.cookie);
    expect(regularUserResponse.status).toBe(401); // Unauthorized or a similar response for regular user
    expect(regularUserResponse.body.message).toBe("Unauthorized");

    const adminResponse = await c.server
        .get(AtitlanApiRoutes.TRANSACTIONS.CHECK_INTEGRITY)
        .set("Cookie", admin.cookie);
    expect(adminResponse.status).toBe(200); // Success response expected for admin
});

test<ApiIntegrationTest>("Transaction - Add funds permission check - 200, 400 & 401", async (c) => {
    // Create a regular user and an admin user
    const regularUser = await createUser(c.server, "user@regular.com", "password123");
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@atitlan.io", "adminpassword");
    const userWallet = await createUserOwnedWallet(c.server, "user1-wallet1", regularUser.cookie);
    const fundWalletId = admin.wallets.fundsWallet.id.toString();;
    const userWalletId = userWallet.id.toString();

    const transactionDetails = {
        source: {
            walletId: fundWalletId,
            amount: "100",
            currency: "BTC",
        },
        target: {
            walletId: userWalletId,
            amount: "100",
            currency: "BTC",
        },
        description: "Adding funds",
    };

    // Attempt to add funds as a regular user
    const regularUserResponse = await createFundsTransactionResponse(
        c.server,
        transactionDetails.source,
        transactionDetails.target,
        transactionDetails.description,
        regularUser.cookie
    );
    expect(regularUserResponse.status).toBe(401);
    expect(regularUserResponse.body.message).toBe("Unauthorized");

    // Attempt to add funds as an admin and test for all fields
    const adminResponse = await createFundsTransaction(
        c.server,
        transactionDetails.source,
        transactionDetails.target,
        transactionDetails.description,
        admin.cookie
    );

    const incorrectWalletId = Number.MAX_SAFE_INTEGER;
    const incorrectSource = {
        walletId: incorrectWalletId.toString(),
        amount: "100",
        currency: "BTC",
    }

    let incorrectWalletResponse = await createFundsTransactionResponse(
        c.server,
        transactionDetails.source,
        incorrectSource,
        transactionDetails.description,
        admin.cookie
    );
    expect(incorrectWalletResponse.status).toBe(400);
    expect(incorrectWalletResponse.body.message).toBe("Bad Request");

    incorrectWalletResponse = await createFundsTransactionResponse(
        c.server,
        incorrectSource,
        transactionDetails.target,
        transactionDetails.description,
        admin.cookie
    );
    expect(incorrectWalletResponse.status).toBe(400);
    expect(incorrectWalletResponse.body.message).toBe("Bad Request");
});

test<ApiIntegrationTest>("Transaction - No same wallet - 400", async (c) => {
    // Create a regular user and an admin user
    const regularUser = await createUser(c.server, "user@regular.com", "password123");
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@atitlan.io", "adminpassword");
    const userWallet = await createUserOwnedWallet(c.server, "user1-wallet1", regularUser.cookie);
    const userWalletId = userWallet.id.toString();
    const adminWalletId = admin.wallets.fundsWallet.id.toString();

    const transactionDetails = {
        source: {
            walletId: userWalletId,
            amount: "100",
            currency: "BTC",
        },
        target: {
            walletId: userWalletId,
            amount: "100",
            currency: "BTC",
        },
        description: "Adding funds",
    };

    // Attempt to add funds as a regular user
    const sameWalletInternalTransactionResponse = await createInternalTransactionResponse(
        c.server,
        transactionDetails.source,
        transactionDetails.target,
        transactionDetails.description,
        regularUser.cookie
    );
    expect(sameWalletInternalTransactionResponse.status).toBe(400);
    expect(sameWalletInternalTransactionResponse.body.message).toBe("Bad Request");

    const transactionFundsDetails = {
        source: {
            walletId: adminWalletId,
            amount: "100",
            currency: "BTC",
        },
        target: {
            walletId: adminWalletId,
            amount: "100",
            currency: "BTC",
        },
        description: "Adding funds",
    };

    // Attempt to add funds as an admin and test for all fields
    const sameWalletAddFundsTransactionResponse = await createFundsTransactionResponse(
        c.server,
        transactionFundsDetails.source,
        transactionFundsDetails.target,
        transactionFundsDetails.description,
        admin.cookie
    );

    expect(sameWalletAddFundsTransactionResponse.status).toBe(400);
    expect(sameWalletAddFundsTransactionResponse.body.message).toBe("Bad Request");
});

test<ApiIntegrationTest>("Transactions - Unauthorized for payment gateway wallets", async (c) => {
    const user = await createUser(c.server, "user@domain.com", "password");
    const admin = await createAdminWithWalletAndPermissions(c.server, "admin@leet.com", "password");
    const adminUser2 = await createAdminWithWalletAndPermissions(c.server, "admin2@leet.io", "prsswlkejr");
    const userWallet = await createUserOwnedWallet(c.server, "user-wallet", user.cookie);
    const gateway = await createStripePaymentGateway(
        c.server,
        admin.cookie,
        "stripe-gateway",
        "stripe-wallet",
        "sk_test_1337",
        "pk_test_1337",
        "whsec_123");

    // Attempt internal transaction with gateway wallet as target
    const internalTransactionSourceGateawyResponse = await c.server
        .post(AtitlanApiRoutes.TRANSACTIONS.CREATE_INTERNAL)
        .set("Cookie", admin.cookie)
        .send({
            sourceWalletId: gateway.sourceWalletId,
            targetWalletId: userWallet.id,
            sourceAmount: "100",
            targetAmount: "100",
            sourceCurrency: "BTC",
            targetCurrency: "BTC",
            description: "Transaction from gateway to user",
        });
    console.log(internalTransactionSourceGateawyResponse.body);
    expect(internalTransactionSourceGateawyResponse.status).toBe(401);
    expect(internalTransactionSourceGateawyResponse.body.message).toBe("Payment gateways are not allowed to send funds to other wallets");

    // Attempt internal transaction with gateway wallet as target
    const internalTransactionTargetGatewayResponse = await c.server
        .post(AtitlanApiRoutes.TRANSACTIONS.CREATE_INTERNAL)
        .set("Cookie", user.cookie)
        .send({
            sourceWalletId: userWallet.id,
            targetWalletId: gateway.sourceWalletId,
            sourceAmount: "100",
            targetAmount: "100",
            sourceCurrency: "BTC",
            targetCurrency: "BTC",
            description: "Transaction from user to gateway",
        });

    expect(internalTransactionTargetGatewayResponse.status).toBe(401);
    expect(internalTransactionTargetGatewayResponse.body.message).toBe("Payment gateways are not allowed to receive funds from other wallets");

    // Attempt add funds transaction with gateway wallet as target
    const addFundsTransactionSourceGatewayResponse = await c.server
        .post(AtitlanApiRoutes.TRANSACTIONS.ADD_FUNDS)
        .set("Cookie", admin.cookie)
        .send({
            sourceWalletId: gateway.sourceWalletId, // Replace with a valid non-gateway wallet ID
            targetWalletId: userWallet.id,
            sourceAmount: "100",
            targetAmount: "100",
            sourceCurrency: "BTC",
            targetCurrency: "BTC",
            description: "Transaction from gateway to user",
        });

    // Expect 401 Unauthorized response
    expect(addFundsTransactionSourceGatewayResponse.status).toBe(401);
    expect(addFundsTransactionSourceGatewayResponse.body.message).toBe("Payment gateways are not allowed to send funds to other wallets");

    // Attempt add funds transaction with gateway wallet as target
    const addFundsTransactionTargetGatewayResponse = await c.server
        .post(AtitlanApiRoutes.TRANSACTIONS.ADD_FUNDS)
        .set("Cookie", adminUser2.cookie)
        .send({
            sourceWalletId: adminUser2.wallets.sourceWallet.id.toString(), // Replace with a valid non-gateway wallet ID
            targetWalletId: gateway.sourceWalletId,
            sourceAmount: "100",
            targetAmount: "100",
            sourceCurrency: "BTC",
            targetCurrency: "BTC",
            description: "Transaction from user to gateway",
        });

    // Expect 401 Unauthorized response
    expect(addFundsTransactionTargetGatewayResponse.status).toBe(401);
    expect(addFundsTransactionTargetGatewayResponse.body.message).toBe("Payment gateways are not allowed to receive funds from other wallets");
});
