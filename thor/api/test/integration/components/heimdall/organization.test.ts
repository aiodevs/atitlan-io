import supertest from "supertest";
import {
    beforeAll,
    expect,
    test,
    afterEach,
    beforeEach,
} from "vitest"
import { IntegrationTestFastResetDB, ApiIntegrationTest } from "../../util/integration-test";
import { createAdminWithWalletAndPermissions, createFundsTransaction, createUser } from "../../factories/test-data-factory";
import {
    createOrganization, createOrganizationOwnedWallet,
    updateOrganization,
    checkRoleTemplatePermissions,
    getOrganizationsFromUserById,
    getOrganizationWalletDetails,
    getOrganizationDetails,
    checkDefaultRolePermissions,
    getRole,
    revokeUsersFromOrganizations,
    getOrganizationsFromUser,
    getOrganizationsFromUserById401
} from "../../factories/organization.factory";
import { createUserOwnedWallet, getUserWallets, getWalletDetails, getWalletsFromUserId } from "../../factories/wallets.factory";
import { OrganizationDetails, OrganizationGeneralInfo } from "../../../../src/components/heimdall/organization/organization.types";
import { WalletDetails, WalletGeneralInfo } from "../../../../src/components/finance/wallet/wallet.types";
import AtitlanApiRoutes from "../../../../src/components/shared/routes";
import { DefaultUserOrganizationRoleNames, DefaultWalletRoleNames, UserOrganizationPermissions } from "../../../../src/components/heimdall/authorization/authorization.type";


let server: supertest.SuperTest<supertest.Test>;

beforeAll(async () => {
    server = await IntegrationTestFastResetDB.beforeAll();
});

beforeEach<ApiIntegrationTest>(async (context: ApiIntegrationTest) => {
    await IntegrationTestFastResetDB.beforeEach(context, server);
});

afterEach<ApiIntegrationTest>(IntegrationTestFastResetDB.afterEach);

test<ApiIntegrationTest>("Organizaton - create - Success 200", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "a@dot.com", "password");
    const organizationName = "Test Organization";
    const newOrg = await createOrganization(c.server, admin.cookie, organizationName);

    const ownerRole = getRole(DefaultUserOrganizationRoleNames.Owner);
    const retrievedPermissions = newOrg.authorizedUsers[0].role.permissions;
    checkDefaultRolePermissions(ownerRole, retrievedPermissions);
    expect(newOrg.name).toBe(organizationName);
    expect(newOrg.roleTemplates.length).toBe(0);
    expect(newOrg.authorizedUsers.length).toBe(1);
    expect(newOrg.authorizedUsers[0].user.email).toBe("a@dot.com");
    expect(newOrg.authorizedUsers[0].role.name).toBe(DefaultUserOrganizationRoleNames.Owner);
    expect(newOrg.authorizedUsers[0].role.permissions.length).toBe(8);
    expect(newOrg.authorizedWalletUsers.length).toBe(0);
    expect(newOrg.wallets.length).toBe(0);
});

test<ApiIntegrationTest>("Organization - create wallet - 200 ok", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "aio@aio.aio", "password");
    const organizationName = "Test Organization";
    const newOrg = await createOrganization(c.server, admin.cookie, organizationName);
    const walletName = "Test Wallet";
    const orgWallet = await createOrganizationOwnedWallet(
        c.server,
        admin.cookie,
        newOrg.id,
        walletName);
    expect(orgWallet.name).toBe(walletName);
    expect(orgWallet.organizationName).toBe(organizationName);

    const walletDetails = await getWalletDetails(c.server, admin.cookie, orgWallet.id);
    expect(walletDetails.name).toBe(walletName);

    expect(walletDetails.authorizedOrganizations.length).toBe(1);
    expect(walletDetails.authorizedOrganizations[0].organizationId).toBe(newOrg.id);
    expect(walletDetails.authorizedOrganizations[0].organization.name).toBe(organizationName);
    expect(walletDetails.authorizedOrganizations[0].role.name).toBe(DefaultWalletRoleNames.Owner);
    expect(walletDetails.authorizedOrganizations[0].role.permissions.length).toBe(6);

    expect(walletDetails.authorizedUsers.length).toBe(0);

    expect(walletDetails.authorizedOrganizationUsers.length).toBe(1);
    expect(walletDetails.authorizedOrganizationUsers[0].organizationId).toBe(newOrg.id);
    expect(walletDetails.authorizedOrganizationUsers[0].organization.name).toBe(organizationName);
    expect(walletDetails.authorizedOrganizationUsers[0].role.name)
        .toBe(DefaultWalletRoleNames.Admin);
    expect(walletDetails.authorizedOrganizationUsers[0].user.email).toBe("aio@aio.aio");
    expect(walletDetails.authorizedOrganizationUsers[0].role.permissions.length).toBe(5);
    expect(walletDetails.authorizedOrganizationUsers[0].userId).toBe(admin.id);

});

test<ApiIntegrationTest>("Organization - update name - Success 200", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "a@dot.com", "password");
    const organizationName = "Initial Organization";
    const newOrg = await createOrganization(c.server, admin.cookie, organizationName);
    expect(newOrg.name).toBe(organizationName);
    // Data for updating the organization
    const updatedOrganizationName = "Updated Organization";
    const updateOrgNameRequest = {
        organizationId: newOrg.id,
        name: updatedOrganizationName,
        addOrUpdateOrgRoleTemplates: [],
        addOrUpdateUserLinks: [],
        addOrUpdateUserWalletLinks: [],
        addOrUpdateWalletLinks: [],
    };
    const updatedNameOrg = await updateOrganization(c.server, admin.cookie, updateOrgNameRequest);

    expect(updatedNameOrg.name).toBe(updatedOrganizationName);
});

test<ApiIntegrationTest>("Organization - update - add users to org - Success 200", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "aio@iao.oai", "password");
    const org = await createOrganization(c.server, admin.cookie, "Flowing Water");
    const wallet = await createOrganizationOwnedWallet(c.server, admin.cookie, org.id, "Test Wallet");
    const managerUser = await createUser(c.server, "user@user.user", "password");
    const employeeUser = await createUser(c.server, "user2@user.user", "password");
    const ownerRole = org.defaultUserOrganizationRoles.find(role => role.name === DefaultUserOrganizationRoleNames.Owner);
    const adminOrgRole = org.defaultUserOrganizationRoles.find(role => role.name === DefaultUserOrganizationRoleNames.Admin);
    const employeeOrgRole = org.defaultUserOrganizationRoles.find(role => role.name === DefaultUserOrganizationRoleNames.Employee);
    if (!adminOrgRole || !employeeOrgRole || !ownerRole) { throw new Error(); }
    const updateOrgRequest = {
        organizationId: org.id,
        name: "Flowing Water",
        addOrUpdateOrgRoleTemplates: [],
        addOrUpdateUserLinks: [
            {
                userId: managerUser.id,
                roleId: adminOrgRole.roleId,
            },
            {
                userId: employeeUser.id,
                roleId: employeeOrgRole.roleId,
            }
        ],
        addOrUpdateUserWalletLinks: [],
        addOrUpdateWalletLinks: []
    };
    const updatedOrg = await updateOrganization(c.server, admin.cookie, updateOrgRequest);
    console.log(JSON.stringify(updatedOrg, null, 2));
    expect(updatedOrg.authorizedUsers.length).toBe(3);
    const employeeAuth = updatedOrg.authorizedUsers.find(item => item.user.email === employeeUser.email);
    const managerAuth = updatedOrg.authorizedUsers.find(item => item.user.email === managerUser.email);
    const adminAuth = updatedOrg.authorizedUsers.find(item => item.user.email === admin.email);
    if (!employeeAuth || !managerAuth || !adminAuth) { throw new Error(); }
    expect(employeeAuth.role.id).toBe(employeeOrgRole.roleId);
    expect(managerAuth.role.id).toBe(adminOrgRole.roleId);
    expect(adminAuth.role.id).toBe(ownerRole.roleId);
    expect(employeeAuth.role.permissions.length).toBe(2);
    expect(managerAuth.role.permissions.length).toBe(7);
    expect(adminAuth.role.permissions.length).toBe(8);
    expect(employeeAuth.role.name).toBe(DefaultUserOrganizationRoleNames.Employee);
    expect(managerAuth.role.name).toBe(DefaultUserOrganizationRoleNames.Admin);
    expect(adminAuth.role.name).toBe(DefaultUserOrganizationRoleNames.Owner);
    checkDefaultRolePermissions(getRole(DefaultUserOrganizationRoleNames.Employee), employeeAuth.role.permissions);
    checkDefaultRolePermissions(getRole(DefaultUserOrganizationRoleNames.Admin), managerAuth.role.permissions);
    checkDefaultRolePermissions(getRole(DefaultUserOrganizationRoleNames.Owner), adminAuth.role.permissions);
});

test<ApiIntegrationTest>("Organization - update (+ random tests) - add users to wallet - Success 200", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "aio@iao.oai", "password");
    const org = await createOrganization(c.server, admin.cookie, "Flowing Water");
    const orgWallet = await createOrganizationOwnedWallet(c.server, admin.cookie, org.id, "AIO Organization Wallet");
    const managerUser = await createUser(c.server, "user@user.user", "password");
    const employeeUser = await createUser(c.server, "user2@user.user", "password");
    const investorUser = await createUser(c.server, "investor@love.tech", "password");
    const ownerRole = org.defaultUserOrganizationRoles.find(role => role.name === DefaultUserOrganizationRoleNames.Owner);
    const adminOrgRole = org.defaultUserOrganizationRoles.find(role => role.name === DefaultUserOrganizationRoleNames.Admin);
    const readOnlyOrgRole = org.defaultUserOrganizationRoles.find(role => role.name === DefaultUserOrganizationRoleNames.ReadOnly);
    const employeeOrgRole = org.defaultUserOrganizationRoles.find(role => role.name === DefaultUserOrganizationRoleNames.Employee);
    const adminWalletRole = org.defaultWalletRoles.find(role => role.name === DefaultWalletRoleNames.Admin);
    const ownerWalletRole = org.defaultWalletRoles.find(role => role.name === DefaultWalletRoleNames.Owner);
    const employeeWalletRole = org.defaultWalletRoles.find(role => role.name === DefaultWalletRoleNames.ReadOnly);

    if (!adminOrgRole || !employeeOrgRole || !ownerRole || !adminWalletRole || !readOnlyOrgRole ||
        !ownerWalletRole || !employeeWalletRole) { throw new Error(); }
    const updateOrgUsersRequest = {
        organizationId: org.id,
        name: "Atitlan IO - Radical Insight",
        addOrUpdateOrgRoleTemplates: [],
        addOrUpdateUserLinks: [
            {
                userId: managerUser.id,
                roleId: adminOrgRole.roleId,
            },
            {
                userId: employeeUser.id,
                roleId: employeeOrgRole.roleId,
            },
            {
                userId: investorUser.id,
                roleId: readOnlyOrgRole.roleId,
            }
        ],
        addOrUpdateUserWalletLinks: [],
        addOrUpdateWalletLinks: []
    };
    const updatedUsersOrg = await updateOrganization(c.server, admin.cookie, updateOrgUsersRequest);

    const managerWallet = await createOrganizationOwnedWallet(c.server, managerUser.cookie, org.id, "Manager Wallet");
    const employeeWallet = await createOrganizationOwnedWallet(c.server, managerUser.cookie, org.id, "Employee Wallet");
    const updateOrgWalletsRequest = {
        organizationId: org.id,
        name: "Atitlan IO - Inner explosions emenating wisdom and peace",
        addOrUpdateOrgRoleTemplates: [],
        addOrUpdateUserLinks: [],
        addOrUpdateUserWalletLinks: [
            {
                userId: admin.id,
                walletId: employeeWallet.id,
                roleId: adminWalletRole.id,
            },
            {
                userId: employeeUser.id,
                walletId: employeeWallet.id,
                roleId: employeeWalletRole.id,
            }
        ],
        addOrUpdateWalletLinks: []
    };
    const updatedWalletsOrg = await updateOrganization(c.server, managerUser.cookie, updateOrgWalletsRequest);
    console.log(JSON.stringify(updatedWalletsOrg, null, 2));

    expect(updatedWalletsOrg.wallets.length).toBe(3);
    const employeeWalletAuth = updatedWalletsOrg.authorizedWalletUsers.find(item => item.walletId === employeeWallet.id && item.userId === employeeUser.id);
    const managerWalletAuth = updatedWalletsOrg.authorizedWalletUsers.find(item => item.walletId === managerWallet.id && item.userId === managerUser.id);
    const orgWalletAuth = updatedWalletsOrg.authorizedWalletUsers.find(item => item.walletId === orgWallet.id && item.userId === admin.id);
    if (!employeeWalletAuth || !managerWalletAuth || !orgWalletAuth) { throw new Error(); }
    expect(employeeWalletAuth.role.id).toBe(employeeWalletRole.id);
    expect(managerWalletAuth.role.id).toBe(adminWalletRole.id);
    expect(orgWalletAuth.role.id).toBe(adminWalletRole.id);
    expect(employeeWalletAuth.role.name).toBe(DefaultWalletRoleNames.ReadOnly);
    expect(managerWalletAuth.role.name).toBe(DefaultWalletRoleNames.Admin);
    expect(orgWalletAuth.role.name).toBe(DefaultWalletRoleNames.Admin);
    expect(employeeWalletAuth.role.permissions.length).toBe(1);
    expect(managerWalletAuth.role.permissions.length).toBe(5);
    expect(orgWalletAuth.role.permissions.length).toBe(5);

    expect(updatedWalletsOrg.roleTemplates.length).toBe(0);
    const extraCapitalFromInvestor = await createUserOwnedWallet(c.server, "Investor Wallet", investorUser.cookie);
    const privateCapital = await createUserOwnedWallet(c.server, "Private Capital", admin.cookie);
    const updateTemplateRequest = {
        organizationId: org.id,
        name: "Atitlan IO - Verbal realities vibrating the freshest beats",
        addOrUpdateOrgRoleTemplates: [
            {
                role: {
                    name: "Investor",
                    permissions: [
                        UserOrganizationPermissions.Finance,
                        UserOrganizationPermissions.Read,
                        UserOrganizationPermissions.WalletsRead,
                        UserOrganizationPermissions.ConnectWalletToOrg,
                        UserOrganizationPermissions.Owner,
                    ]
                }
            }
        ],
        addOrUpdateUserLinks: [],
        addOrUpdateUserWalletLinks: [],
        addOrUpdateWalletLinks: []
    };
    const updatedTemplateOrg = await updateOrganization(c.server, admin.cookie, updateTemplateRequest);
    expect(updatedTemplateOrg.roleTemplates.length).toBe(1);
    const investorRole = updatedTemplateOrg.roleTemplates.find(roleTemplate => roleTemplate.role.name === "Investor");
    if (!investorRole) { throw new Error(); }
    expect(investorRole.role.permissions.length).toBe(5);
    const investorRolePermissions = investorRole.role.permissions;
    const remappedInvestorRoleActions = investorRolePermissions.map(permission => permission.action);
    expect(remappedInvestorRoleActions).toContain(UserOrganizationPermissions.Finance);
    expect(remappedInvestorRoleActions).toContain(UserOrganizationPermissions.Read);
    expect(remappedInvestorRoleActions).toContain(UserOrganizationPermissions.WalletsRead);
    expect(remappedInvestorRoleActions).toContain(UserOrganizationPermissions.ConnectWalletToOrg);
    expect(remappedInvestorRoleActions).toContain(UserOrganizationPermissions.Owner);

    const updateInvestorRoleRequest = {
        organizationId: org.id,
        name: "Atitlan IO - Cosmic vibes for all beings",
        addOrUpdateOrgRoleTemplates: [],
        addOrUpdateUserLinks: [{
            userId: investorUser.id,
            roleId: investorRole.roleId,
        }],
        addOrUpdateUserWalletLinks: [],
        addOrUpdateWalletLinks: []
    };
    const updatedRoleFromInvestorToOrg = await updateOrganization(c.server, admin.cookie, updateInvestorRoleRequest);
    expect(updatedRoleFromInvestorToOrg.authorizedUsers.length).toBe(4);
    const investorAuth = updatedRoleFromInvestorToOrg.authorizedUsers.find(item => item.user.email === investorUser.email);
    if (!investorAuth) { throw new Error(); }
    expect(investorAuth.role.id).toBe(investorRole.roleId);
    expect(investorAuth.role.name).toBe("Investor");
    expect(investorAuth.role.permissions.length).toBe(5);
    checkRoleTemplatePermissions(investorRole.role, investorAuth.role.permissions);

    const orgsFromInvestor: OrganizationGeneralInfo[] = await getOrganizationsFromUserById(c.server, admin.cookie, investorUser.id);
    console.log(JSON.stringify(orgsFromInvestor, null, 2));
    expect(orgsFromInvestor.length).toBe(1);
    const orgFromInvestor = orgsFromInvestor[0];
    expect(orgFromInvestor.id).toBe(org.id);
    expect(orgFromInvestor.name).toBe("Atitlan IO - Cosmic vibes for all beings");

    const updateInvestorWalletRequest = {
        organizationId: org.id,
        name: "",
        addOrUpdateOrgRoleTemplates: [],
        addOrUpdateUserLinks: [],
        addOrUpdateUserWalletLinks: [],
        addOrUpdateWalletLinks: [
            {
                walletId: privateCapital.id,
                roleId: ownerWalletRole.id,
            },
            {
                walletId: extraCapitalFromInvestor.id,
                roleId: adminWalletRole.id,
            }
        ]
    };
    const updatedWalletFromInvestorToOrg = await updateOrganization(c.server, investorUser.cookie, updateInvestorWalletRequest);

    expect(updatedWalletFromInvestorToOrg.wallets.length).toBe(5);
    const privateCapitalWallet = updatedWalletFromInvestorToOrg.wallets.find(wallet => wallet.id === privateCapital.id);
    const extraCapitalWallet = updatedWalletFromInvestorToOrg.wallets.find(wallet => wallet.id === extraCapitalFromInvestor.id);
    if (!privateCapitalWallet || !extraCapitalWallet) { throw new Error(); }
    expect(privateCapitalWallet.role.id).toBe(ownerWalletRole.id);
    expect(extraCapitalWallet.role.id).toBe(adminWalletRole.id);
    expect(privateCapitalWallet.role.name).toBe(DefaultWalletRoleNames.Owner);
    expect(extraCapitalWallet.role.name).toBe(DefaultWalletRoleNames.Admin);
    expect(privateCapitalWallet.role.permissions.length).toBe(6);
    expect(extraCapitalWallet.role.permissions.length).toBe(5);

    const orgWalletFromAdmin: WalletDetails = await getOrganizationWalletDetails(
        c.server,
        admin.cookie,
        org.id,
        orgWallet.id);

    expect(orgWalletFromAdmin.authorizedOrganizations.length).toBe(1);
    expect(orgWalletFromAdmin.authorizedOrganizations[0].organizationId).toBe(org.id);
    expect(orgWalletFromAdmin.authorizedOrganizations[0].role.name).toBe(DefaultWalletRoleNames.Owner);
    expect(orgWalletFromAdmin.authorizedUsers.length).toBe(0);
    expect(orgWalletFromAdmin.authorizedOrganizationUsers.length).toBe(1);
    expect(orgWalletFromAdmin.authorizedOrganizationUsers[0].organizationId).toBe(org.id);
    expect(orgWalletFromAdmin.authorizedOrganizationUsers[0].userId).toBe(admin.id);
    expect(orgWalletFromAdmin.authorizedOrganizationUsers[0].role.name).toBe(DefaultWalletRoleNames.Admin);

    const orgWalletFromInvestor: WalletDetails = await getWalletDetails(c.server, investorUser.cookie, extraCapitalWallet.id);
    expect(orgWalletFromInvestor.authorizedOrganizations.length).toBe(1);
    expect(orgWalletFromInvestor.authorizedOrganizations[0].organizationId).toBe(org.id);
    expect(orgWalletFromInvestor.authorizedOrganizations[0].role.name).toBe(DefaultWalletRoleNames.Admin);
    expect(orgWalletFromInvestor.authorizedUsers.length).toBe(1);
    expect(orgWalletFromInvestor.authorizedUsers[0].role.name).toBe(DefaultWalletRoleNames.Owner);

    const allWalletsFromAdmin: WalletGeneralInfo[] = await getWalletsFromUserId(c.server, admin.cookie, admin.id);
    expect(allWalletsFromAdmin.length).toBe(5);
    const allWalletFromAdminNames = allWalletsFromAdmin.map(wallet => wallet.name);
    console.log(JSON.stringify(allWalletsFromAdmin, null, 2));
    expect(allWalletFromAdminNames).toContain("AIO Organization Wallet");
    expect(allWalletFromAdminNames).toContain("Employee Wallet");
    expect(allWalletFromAdminNames).toContain("Private Capital");
    expect(allWalletFromAdminNames).toContain("aio-funds");
    expect(allWalletFromAdminNames).toContain("aio-minter");

    const employeePersonalWallet = await createUserOwnedWallet(c.server, "My Personal Wallet", employeeUser.cookie);
    const allWalletsFromEmployee: WalletGeneralInfo[] = await getUserWallets(c.server, employeeUser.cookie);
    expect(allWalletsFromEmployee.length).toBe(2);
    const allWalletFromEmployeeNames = allWalletsFromEmployee.map(wallet => wallet.name);
    expect(allWalletFromEmployeeNames).toContain("Employee Wallet");
    expect(allWalletFromEmployeeNames).toContain("My Personal Wallet");

    const orgFromEmployeePerspective = await getOrganizationDetails(c.server, employeeUser.cookie, org.id);
    expect(orgFromEmployeePerspective.id).toBe(org.id);
    expect(orgFromEmployeePerspective.name).toBe("Atitlan IO - Cosmic vibes for all beings");
    expect(orgFromEmployeePerspective.authorizedUsers.length).toBe(1);
    expect(orgFromEmployeePerspective.authorizedWalletUsers.length).toBe(1);
    expect(orgFromEmployeePerspective.roleTemplates.length).toBe(0);
    expect(orgFromEmployeePerspective.wallets.length).toBe(1);
    expect(orgFromEmployeePerspective.defaultUserOrganizationRoles.length).toBe(0);
    expect(orgFromEmployeePerspective.defaultUserOrganizationPermissions.length).toBe(0);
    expect(orgFromEmployeePerspective.defaultWalletRoles.length).toBe(0);

    const orgFromAdminPerspective = await getOrganizationDetails(c.server, admin.cookie, org.id);
    expect(orgFromAdminPerspective.id).toBe(org.id);
    expect(orgFromAdminPerspective.name).toBe("Atitlan IO - Cosmic vibes for all beings");
    expect(orgFromAdminPerspective.authorizedUsers.length).toBe(4);
    expect(orgFromAdminPerspective.authorizedWalletUsers.length).toBe(5);
    expect(orgFromAdminPerspective.roleTemplates.length).toBe(1);
    expect(orgFromAdminPerspective.wallets.length).toBe(5);
    expect(orgFromAdminPerspective.defaultUserOrganizationRoles.length).toBe(4);
    expect(orgFromAdminPerspective.defaultUserOrganizationPermissions.length).toBe(10);
    expect(orgFromAdminPerspective.defaultWalletRoles.length).toBe(5);

    const newOrg = await createOrganization(c.server, admin.cookie, "New Organization");
    const adminOrgs = await getOrganizationsFromUser(c.server, admin.cookie);
    expect(adminOrgs.length).toBe(2);
    const newRandoUser = await createUser(c.server, "mr@rand.om", "password");
    const randoUserOrgs = await getOrganizationsFromUser(c.server, newRandoUser.cookie);
    expect(randoUserOrgs.length).toBe(0);
    const randoUserOrgDetailsRequest = await c.server
        .get(AtitlanApiRoutes.ORGANIZATION.GET_DETAILS(org.id))
        .set("Cookie", newRandoUser.cookie);
    expect(randoUserOrgDetailsRequest.status).toBe(401);
    expect(randoUserOrgDetailsRequest.body.message).toBe("Unauthorized");

    const randomOrgDetailsRequest = await c.server
        .get(AtitlanApiRoutes.ORGANIZATION.GET_DETAILS("random-id"))
        .set("Cookie", newRandoUser.cookie);
    expect(randomOrgDetailsRequest.status).toBe(401);
    expect(randomOrgDetailsRequest.body.message).toBe("Unauthorized");

    const updateTemplateRequest2 = {
        organizationId: org.id,
        name: "Atitlan IO - Verbal realities vibrating the freshest beats",
        addOrUpdateOrgRoleTemplates: [
            {
                role: {
                    id: investorRole.roleId,
                    name: "Investor",
                    permissions: [
                        UserOrganizationPermissions.Finance,
                        UserOrganizationPermissions.Read,
                        UserOrganizationPermissions.WalletsRead,
                        UserOrganizationPermissions.Owner,
                    ]
                }
            }
        ],
        addOrUpdateUserLinks: [],
        addOrUpdateUserWalletLinks: [],
        addOrUpdateWalletLinks: []
    };
    const updatedTemplateOrg2 = await updateOrganization(c.server, admin.cookie, updateTemplateRequest2);
    expect(updatedTemplateOrg2.roleTemplates.length).toBe(1);
    const investorRole2 = updatedTemplateOrg2.roleTemplates.find(roleTemplate => roleTemplate.role.name === "Investor");
    if (!investorRole2) { throw new Error(); }
    expect(investorRole2.role.permissions.length).toBe(4);
    const remappedInvestorRoleActions2 = investorRole2.role.permissions.map(permission => permission.action);
    expect(remappedInvestorRoleActions2).toContain(UserOrganizationPermissions.Finance);
    expect(remappedInvestorRoleActions2).toContain(UserOrganizationPermissions.Read);
    expect(remappedInvestorRoleActions2).toContain(UserOrganizationPermissions.WalletsRead);
    expect(remappedInvestorRoleActions2).toContain(UserOrganizationPermissions.Owner);

    // TODO think about what to expect (revoke because of still being the owner of that walled or nah)
    const updateInvestorWalletRequest2 = {
        organizationId: org.id,
        name: "",
        addOrUpdateOrgRoleTemplates: [],
        addOrUpdateUserLinks: [],
        addOrUpdateUserWalletLinks: [],
        addOrUpdateWalletLinks: [
            {
                walletId: privateCapital.id,
                roleId: ownerWalletRole.id,
                revokedAt: new Date().toISOString()
            }
        ]
    };
    const updatedWalletFromInvestorToOrgFailed = updateOrganization(c.server, investorUser.cookie, updateInvestorWalletRequest2);


    // console.log(JSON.stringify(updatedWalletFromInvestorToOrgFailed.body, null, 2));
});

test<ApiIntegrationTest>("Organization - revoke user - Success 200", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "aio@iao.oai", "password");
    const org = await createOrganization(c.server, admin.cookie, "Flowing Water");
    const wallet = await createOrganizationOwnedWallet(c.server, admin.cookie, org.id, "Test Wallet");
    const managerUser = await createUser(c.server, "user@user.user", "password");
    const employeeUser = await createUser(c.server, "user2@user.user", "password");
    const ownerRole = org.defaultUserOrganizationRoles.find(role => role.name === DefaultUserOrganizationRoleNames.Owner);
    const adminOrgRole = org.defaultUserOrganizationRoles.find(role => role.name === DefaultUserOrganizationRoleNames.Admin);
    const employeeOrgRole = org.defaultUserOrganizationRoles.find(role => role.name === DefaultUserOrganizationRoleNames.Employee);
    if (!adminOrgRole || !employeeOrgRole || !ownerRole) { throw new Error(); }
    const updateOrgRequest = {
        organizationId: org.id,
        name: "Flowing Water",
        addOrUpdateOrgRoleTemplates: [],
        addOrUpdateUserLinks: [
            {
                userId: managerUser.id,
                roleId: adminOrgRole.roleId,
            },
            {
                userId: employeeUser.id,
                roleId: employeeOrgRole.roleId,
            }
        ],
        addOrUpdateUserWalletLinks: [],
        addOrUpdateWalletLinks: []
    };
    const updatedOrg = await updateOrganization(c.server, admin.cookie, updateOrgRequest);
    const employeeAuth = updatedOrg.authorizedUsers.find(item => item.user.email === employeeUser.email);
    const managerAuth = updatedOrg.authorizedUsers.find(item => item.user.email === managerUser.email);
    const adminAuth = updatedOrg.authorizedUsers.find(item => item.user.email === admin.email);
    if (!employeeAuth || !managerAuth || !adminAuth) { throw new Error(); }
    expect(employeeAuth.role.id).toBe(employeeOrgRole.roleId);
    expect(managerAuth.role.id).toBe(adminOrgRole.roleId);
    expect(adminAuth.role.id).toBe(ownerRole.roleId);
    expect(employeeAuth.role.name).toBe(DefaultUserOrganizationRoleNames.Employee);
    expect(managerAuth.role.name).toBe(DefaultUserOrganizationRoleNames.Admin);
    expect(adminAuth.role.name).toBe(DefaultUserOrganizationRoleNames.Owner);
    checkDefaultRolePermissions(getRole(DefaultUserOrganizationRoleNames.Employee), employeeAuth.role.permissions);
    checkDefaultRolePermissions(getRole(DefaultUserOrganizationRoleNames.Admin), managerAuth.role.permissions);
    checkDefaultRolePermissions(getRole(DefaultUserOrganizationRoleNames.Owner), adminAuth.role.permissions);
    expect(employeeAuth.revokedAt).toBeFalsy();
    expect(managerAuth.revokedAt).toBeFalsy();
    expect(adminAuth.revokedAt).toBeFalsy();

    const revokedEmployeeRequest: {
        userId: string,
        organizationId: string,
        revokedAt: Date
    }[] = [{
        userId: employeeUser.id,
        organizationId: org.id,
        revokedAt: new Date()
    }];
    const orgAfterRevocations: OrganizationDetails[] = await revokeUsersFromOrganizations(c.server, admin.cookie, revokedEmployeeRequest);
    const orgAfterRevocation = orgAfterRevocations[0];
    expect(orgAfterRevocation.id).toBe(org.id);
    expect(orgAfterRevocation.authorizedUsers.length).toBe(3);
    if (!orgAfterRevocation) { throw new Error(); }
    const employeeAuthAfterRevocation = orgAfterRevocation.authorizedUsers.find(item => item.user.email === employeeUser.email);
    const managerAuthAfterRevocation = orgAfterRevocation.authorizedUsers.find(item => item.user.email === managerUser.email);
    const adminAuthAfterRevocation = orgAfterRevocation.authorizedUsers.find(item => item.user.email === admin.email);
    if (!employeeAuthAfterRevocation || !managerAuthAfterRevocation || !adminAuthAfterRevocation) { throw new Error(); }
    expect(employeeAuthAfterRevocation.revokedAt).toBeTruthy();
    expect(managerAuthAfterRevocation.revokedAt).toBeFalsy();
    expect(adminAuthAfterRevocation.revokedAt).toBeFalsy();
});

test<ApiIntegrationTest>("Organization - evil access - Success 200", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "tes@t.me", "password");
    const org = await createOrganization(c.server, admin.cookie, "Flowing Water");
    const wallet = await createOrganizationOwnedWallet(c.server, admin.cookie, org.id, "Test Wallet");
    const addFunds = await createFundsTransaction(
        c.server,
        {
            walletId: admin.wallets.sourceWallet.id,
            amount: "100",
            currency: "USD"
        },
        {
            walletId: wallet.id,
            amount: "100",
            currency: "USD"
        },
        "Seed funds",
        admin.cookie);
    const evilUser = await createUser(c.server, "e@vi.l", "password");
    const evilOrg = await createOrganization(c.server, evilUser.cookie, "Evil Organization");
    await getOrganizationsFromUserById401(c.server, evilUser.cookie, admin.id);
    expect(c.logSpy).toHaveBeenCalledWith(`User ${evilUser.id} does not have admin permission to see any organizations`);
    const reverseProxied: OrganizationGeneralInfo[] = await getOrganizationsFromUserById(c.server, admin.cookie, evilUser.id);
    console.log(JSON.stringify(reverseProxied, null, 2));
    expect(reverseProxied.length).toBe(1);
});
