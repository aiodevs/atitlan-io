import {
    expect,
    test,
} from "vitest";
import TestUtils from "../../util/test-util";
import supertest from "supertest";
import Api from "../../../../src/api";
import AtitlanApiRoutes from "../../../../src/components/shared/routes";

test("Health server", async () => {
    const api = new Api();
    await api.app.ready();
    const request = supertest(api.app.server);

    const response = await request.get(AtitlanApiRoutes.HEALTH.SERVER);

    expect(response.status).toBe(200);
    expect(response.body.status).toBe("OK");
});

test("Health db - down", async () => {
    const api = new Api("aFakeDatabaseURLToMimicDBDown");
    await api.app.ready();
    const request = supertest(api.app.server);

    const response = await request.get(AtitlanApiRoutes.HEALTH.DB);

    expect(response.status).toBe(503);
    expect(response.body.status).toBe("Error");
});

test("Health db - up", async () => {
    const props = TestUtils.getEnvProperties();
    const url = TestUtils.databaseURL(props);
    const api = new Api(url);

    await TestUtils.initDB(url);
    await api.app.ready();
    const request = supertest(api.app.server);

    const response = await request.get(AtitlanApiRoutes.HEALTH.DB);

    expect(response.status).toBe(200);
    expect(response.body.status).toBe("OK");
});