import {
  beforeAll,
  expect,
  test,
  afterEach,
  beforeEach,
} from "vitest"
import { ApiIntegrationTest, IntegrationTestFastResetDB } from "../../util/integration-test";
import supertest from "supertest";
import AtitlanApiRoutes from "../../../../src/components/shared/routes";
import { createUser } from "../../factories/test-data-factory";

let server: supertest.SuperTest<supertest.Test>;

beforeAll(async () => {
  server = await IntegrationTestFastResetDB.beforeAll();
});

beforeEach<ApiIntegrationTest>(async (context: ApiIntegrationTest) => {
  await IntegrationTestFastResetDB.beforeEach(context, server);
});

afterEach<ApiIntegrationTest>(IntegrationTestFastResetDB.afterEach);

test<ApiIntegrationTest>("Auth - create user - 200 - ok", async (c) => {
  const response = await c.server.post(AtitlanApiRoutes.AUTH.SIGNUP).send({
    email: "test@test.com",
    password: "longasspassword",
  });

  expect(response.status).toBe(200);
  expect(response.body.user.email).toBe("test@test.com");
  expect(response.body.message).toBe("Signup successful");

  const aioSessionCookie = getCookieFromResponse(response);
  console.log("cookie:", response.headers["set-cookie"]);
  expect(aioSessionCookie).toBeDefined();
  expect(aioSessionCookie).not.toBe("");
});

test<ApiIntegrationTest>("Auth - create user - 400 - short password, missing fields", async (c) => {
  let response = await c.server.post(AtitlanApiRoutes.AUTH.SIGNUP).send({
    email: "test@test.com",
    password: "toshort",
  });

  expect(response.status).toBe(400);
  expect(response.body.message).toBe(
    "password: Password must be at least 8 characters long"
  );

  response = await c.server.post(AtitlanApiRoutes.AUTH.SIGNUP).send({
    password: "goodlengthbaby",
  });

  expect(response.status).toBe(400);
  expect(response.body.message).toBe("must have required property 'email'");

  response = await c.server.post(AtitlanApiRoutes.AUTH.SIGNUP).send({
    email: "test@test.com",
  });

  expect(response.status).toBe(400);
  expect(response.body.message).toBe("must have required property 'password'");
});

test<ApiIntegrationTest>("Auth - create user - 409 - user exists", async (c) => {
  await c.server.post(AtitlanApiRoutes.AUTH.SIGNUP).send({
    email: "test@test.com",
    password: "password",
  });
  const response = await c.server.post(AtitlanApiRoutes.AUTH.SIGNUP).send({
    email: "test@test.com",
    password: "password",
  });

  expect(response.status).toBe(409);
  expect(response.body.message).toBe("User already exists");
});

test<ApiIntegrationTest>("Auth - login - 200 - ok", async (c) => {
  await c.server.post(AtitlanApiRoutes.AUTH.SIGNUP).send({
    email: "test@test.com",
    password: "password",
  });

  const response = await c.server.post(AtitlanApiRoutes.AUTH.LOGIN).send({
    email: "test@test.com",
    password: "password",
  });

  expect(response.status).toBe(200);
  expect(response.body.message).toBe("Login successful");
  expect(response.body.user.email).toBe("test@test.com");

  const aioSessionCookie = getCookieFromResponse(response);

  expect(aioSessionCookie).toBeDefined();
  expect(aioSessionCookie).not.toBe("");
});

test<ApiIntegrationTest>("Auth - login - 401/404 - wrong login", async (c) => {
  await c.server.post(AtitlanApiRoutes.AUTH.SIGNUP).send({
    email: "test@test.com",
    password: "password",
  });

  // Wrong password
  let response = await c.server.post(AtitlanApiRoutes.AUTH.LOGIN).send({
    email: "test@test.com",
    password: "wrongpassword",
  });

  expect(response.status).toBe(401);
  expect(response.body.message).toBe("User or password incorrect");

  let aioSessionCookie = response.headers["set-cookie"];

  expect(aioSessionCookie).toBeUndefined();

  // User wrong
  response = await c.server.post(AtitlanApiRoutes.AUTH.LOGIN).send({
    email: "wronguser@test.com",
    password: "password",
  });

  expect(response.status).toBe(401);
  expect(response.body.message).toBe("User or password incorrect");

  aioSessionCookie = response.headers["set-cookie"];

  expect(aioSessionCookie).toBeUndefined();
});

function getCookieFromResponse(response: supertest.Response) {
  const setCookieHeader = response.headers["set-cookie"];
  const cookies = Array.isArray(setCookieHeader) ? setCookieHeader : [setCookieHeader];
  return cookies.find((cookie: string) =>
    cookie.startsWith("aio-userSession=")
  );
}

test<ApiIntegrationTest>("Auth - valid-session - 200 ok", async (c) => {
  const user = await createUser(c.server, "user@user.com", "beeepbeeep");

  const protectedResponse = await c.server
    .get(AtitlanApiRoutes.AUTH.VALID_SESSION)
    .set("Cookie", user.cookie)
    .send();
  console.log("HERE");
  console.log(protectedResponse.body);

  expect(protectedResponse.status).toBe(200);
  expect(protectedResponse.body.message).toBe("User session is valid");
  expect(protectedResponse.body.user.email).toBe("user@user.com");
});

test<ApiIntegrationTest>("Auth - valid-session - 401 - without cookie ", async (c) => {
  const response = await c.server.get(AtitlanApiRoutes.AUTH.VALID_SESSION).send();

  expect(response.status).toBe(401);
  expect(response.body.message).toBe("Unauthorized");
});

test<ApiIntegrationTest>("Auth - Logout - 200 ok", async (c) => {
  const user = await createUser(c.server, "user@user.com", "beeepbeeep");

  const logoutResponse = await c.server
    .post(AtitlanApiRoutes.AUTH.LOGOUT)
    .set("Cookie", user.cookie)
    .send();

  expect(logoutResponse.status).toBe(200);
  expect(logoutResponse.body.message).toBe("Logout successful");
});

test<ApiIntegrationTest>("Auth - Logout - 400 double logout", async (c) => {
  const user = await createUser(c.server, "user@user.com", "beeepbeeep");

  await c.server.post(AtitlanApiRoutes.AUTH.LOGOUT)
    .set("Cookie", user.cookie)
    .send();

  const logoutResponse = await c.server
    .post(AtitlanApiRoutes.AUTH.LOGOUT)
    .set("Cookie", user.cookie)
    .send();
  expect(logoutResponse.status).toBe(400);
  // expect(logoutResponse.body.message).toBe(
  //   "Session not found - trying to logout for a non-existant user session"
  // );
});

test<ApiIntegrationTest>("Auth - Logout - 404 - without cookie", async (c) => {
  const response = await c.server.post(AtitlanApiRoutes.AUTH.LOGOUT).send();

  expect(response.status).toBe(404);
  expect(response.body.message).toBe("No session id found");
});

// TODO session expiration
// TODO session manipulations (cookie manipulation) anti-tampering
// TODO invalidate sessions with logout test
// TODO deleted/invalidated keys -> OTP new key