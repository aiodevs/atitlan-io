import {
    beforeAll,
    expect,
    test,
    afterEach,
    beforeEach,
} from "vitest"
import { ApiIntegrationTest, IntegrationTestFastResetDB } from "../../util/integration-test";
import supertest from "supertest";
import { createUser, createPermissions, createAdminWithWalletAndPermissions } from "../../factories/test-data-factory";
import AtitlanApiRoutes from "../../../../src/components/shared/routes";
import { Resources, ThorAdminPermissions } from "../../../../src/components/heimdall/authorization/authorization.type";

let server: supertest.SuperTest<supertest.Test>;

beforeAll(async () => {
    server = await IntegrationTestFastResetDB.beforeAll();
});

beforeEach<ApiIntegrationTest>(async (context: ApiIntegrationTest) => {
    await IntegrationTestFastResetDB.beforeEach(context, server);
});

afterEach<ApiIntegrationTest>(IntegrationTestFastResetDB.afterEach);

test<ApiIntegrationTest>("Auth - seed & permission/add - 200 ok", async (c) => {
    const email = "test@atitlanio.io";
    const password = "password";
    let loginResponse = await c.server.post(AtitlanApiRoutes.AUTH.LOGIN).send({
        email: email,
        password: password,
    });
    expect(loginResponse.status).toBe(401);

    const admin = await createAdminWithWalletAndPermissions(c.server, email, password);

    const getPermissionResponse = await c.server
        .get(AtitlanApiRoutes.USER.PERMISSIONS)
        .set("Cookie", admin.cookie);
    expect(getPermissionResponse.status).toBe(200);
    const permissionsResult = getPermissionResponse.body;
    const permissions: {
        userId: string, action: string, resource: string, expiresAt: string
    }[] = permissionsResult.permissions;
    expect(permissions.length).toBe(4);
    const thorAdmin = permissions.find((p) => p.resource === Resources.ThorAdmin);
    const ThorLockedFeatures = permissions.find((p) => p.resource === Resources.ThorLockedFeatures);
    const traefik = permissions.find((p) => p.resource === Resources.Traefik);
    const portainer = permissions.find((p) => p.resource === Resources.Portainer);
    if (!thorAdmin || !ThorLockedFeatures || !traefik || !portainer) {
        throw new Error("Permissions not found");
    }
    expect(thorAdmin.action).toBe("*");
    expect(ThorLockedFeatures.action).toBe("*");
    expect(traefik.action).toBe("*");
    expect(portainer.action).toBe("*");

    const newPermissions = [
        {
            userId: admin.id.toString(),
            action: "read",
            resource: "wallet-custodian",
            expiresAt: "2999-12-15T16:11:47.689Z"
        }
    ];

    const addPermissionResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.ADD_PERMISSION)
        .set("Cookie", admin.cookie)
        .send(newPermissions);
    expect(addPermissionResponse.status).toBe(200);

    const permissionResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.CHECK_PERMISSION)
        .set("Cookie", admin.cookie)
        .send({
            action: "read",
            resource: "wallet-custodian",
        });

    expect(permissionResponse.status).toBe(200);

    const permissionResponse2 = await c.server
        .post(AtitlanApiRoutes.AUTH.CHECK_PERMISSION)
        .set("Cookie", admin.cookie)
        .send({
            action: "write",
            resource: "wallet-custodian",
        });

    expect(permissionResponse2.status).toBe(401);
});

test<ApiIntegrationTest>("Auth - check-permission - 200 ok", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "test@atitlanio.io", "securepassword");
    const user = await createUser(c.server, "user@example.com", "securepassword");
    const newPermissions = createPermissions(user.id.toString(), "read", "portainer");
    const wildCardPermission = createPermissions(user.id.toString(), "*", "thor-ui");

    const incorrectCookiesPermissionsAdd = await c.server
        .post(AtitlanApiRoutes.AUTH.ADD_PERMISSION)
        .set("Cookie", user.cookie)
        .send(newPermissions);
    expect(incorrectCookiesPermissionsAdd.status).toBe(401);

    const correctCookiePermissionsAdd = await c.server
        .post(AtitlanApiRoutes.AUTH.ADD_PERMISSION)
        .set("Cookie", admin.cookie)
        .send(newPermissions);
    expect(correctCookiePermissionsAdd.status).toBe(200);

    const addWildCardResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.ADD_PERMISSION)
        .set("Cookie", admin.cookie)
        .send(wildCardPermission);
    expect(addWildCardResponse.status).toBe(200);

    let writeNotSetResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.CHECK_PERMISSION).
        send({
            action: "write",
            resource: "portainer",
        });
    expect(writeNotSetResponse.status).toBe(401);

    const readWhichIsSetResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.CHECK_PERMISSION)
        .set("Cookie", user.cookie)
        .send({
            action: "read",
            resource: "portainer",
        });
    expect(readWhichIsSetResponse.status).toBe(200);

    let wildCardResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.CHECK_PERMISSION)
        .set("Cookie", user.cookie)
        .send({
            action: "read",
            resource: "thor-ui",
        });
    expect(wildCardResponse.status).toBe(200);

    wildCardResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.CHECK_PERMISSION)
        .set("Cookie", user.cookie)
        .send({
            action: "some-unknown-thing",
            resource: "thor-ui",
        });
    expect(wildCardResponse.status).toBe(200);
});

test<ApiIntegrationTest>("Auth - check-permission - 401 Unauthorized", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "test@atitlanio.io", "securepassword");
    const user = await createUser(c.server, "user@example.com", "securepassword");

    const userResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.CHECK_PERMISSION)
        .set("Cookie", user.cookie)
        .send({
            action: "write",
            resource: Resources.ThorAdmin,
        });
    expect(userResponse.status).toBe(401);

    const noSessionResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.CHECK_PERMISSION)
        .send({
            action: "write",
            resource: Resources.ThorAdmin,
        });
    expect(noSessionResponse.status).toBe(401);

    const adminResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.CHECK_PERMISSION)
        .set("Cookie", admin.cookie)
        .send({
            action: "write",
            resource: Resources.ThorAdmin,
        });
    expect(adminResponse.status).toBe(200);
});

test<ApiIntegrationTest>("Auth - user/permissions - 200 & 401 unauthorized", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "test@atitlanio.io", "securepassword");
    const user = await createUser(c.server, "user@example.com", "securepassword");

    const noSessionResponse = await c.server
        .get(AtitlanApiRoutes.USER.PERMISSIONS);
    expect(noSessionResponse.status).toBe(401);
    expect(noSessionResponse.body.message).toBe("Unauthorized");

    const userSessionResponse = await c.server
        .get(AtitlanApiRoutes.USER.PERMISSIONS)
        .set("Cookie", user.cookie);
    expect(userSessionResponse.status).toBe(200);
    expect(userSessionResponse.body.permissions.length).toBe(0);

    const adminSessionResponse = await c.server
        .get(AtitlanApiRoutes.USER.PERMISSIONS)
        .set("Cookie", admin.cookie);
    expect(adminSessionResponse.status).toBe(200);
    expect(adminSessionResponse.body.permissions.length).toBe(4);
});

test<ApiIntegrationTest>("Auth - permission/add - 200 ok", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "test@atitlanio.io", "securepassword");
    const user = await createUser(c.server, "user@example.com", "securepassword");
    const newPermissions = createPermissions(
        user.id.toString(),
        "read",
        "portainer",
    );

    const newAddPermissions = createPermissions(
        user.id.toString(),
        ThorAdminPermissions.ManageAllPermissions,
        Resources.ThorAdmin,
    );

    const invalidUserResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.ADD_PERMISSION)
        .set("Cookie", user.cookie)
        .send(newPermissions);
    console.log(invalidUserResponse.body);
    expect(invalidUserResponse.status).toBe(401);
    expect(invalidUserResponse.body.message).toBe("Not authorized to add permissions");

    const validAdminResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.ADD_PERMISSION)
        .set("Cookie", admin.cookie)
        .send(newAddPermissions);
    expect(validAdminResponse.status).toBe(200);

    const validUserResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.ADD_PERMISSION)
        .set("Cookie", user.cookie)
        .send(newPermissions);
    expect(validUserResponse.status).toBe(200);
});

test<ApiIntegrationTest>("Auth - permission/add - 401 Unauthorized User", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "test@atitlanio.io", "securepassword");
    const user = await createUser(c.server, "user@example.com", "securepassword");
    const newPermissions = createPermissions(user.id.toString(), "read", "portainer");

    const invalidUserResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.ADD_PERMISSION)
        .set("Cookie", user.cookie)
        .send(newPermissions);
    console.log(invalidUserResponse.body);
    expect(invalidUserResponse.status).toBe(401);
    expect(invalidUserResponse.body.message).toBe("Not authorized to add permissions");

    const validUserResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.ADD_PERMISSION)
        .set("Cookie", admin.cookie)
        .send(newPermissions);
    expect(validUserResponse.status).toBe(200);
});

test<ApiIntegrationTest>("Auth - permission/remove - 200 ok", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "test@atitlanio.io", "securepassword");

    let getPermissionResponse = await c.server
        .get(AtitlanApiRoutes.USER.PERMISSIONS)
        .set("Cookie", admin.cookie);
    expect(getPermissionResponse.status).toBe(200);
    let permissionsResult = getPermissionResponse.body;
    let permissions: {
        id: string,
        userId: string,
        action: string,
        resource: string,
        expiresAt: string
    }[] = permissionsResult.permissions;
    expect(permissions.length).toBe(4);

    const newPermissions = createPermissions(admin.id.toString(), "read", "wallet");
    const addPermissionResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.ADD_PERMISSION)
        .set("Cookie", admin.cookie)
        .send(newPermissions);
    expect(addPermissionResponse.status).toBe(200);

    let permissionResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.CHECK_PERMISSION)
        .set("Cookie", admin.cookie)
        .send({
            action: "read",
            resource: "wallet",
        });
    expect(permissionResponse.status).toBe(200);

    getPermissionResponse = await c.server
        .get(AtitlanApiRoutes.USER.PERMISSIONS)
        .set("Cookie", admin.cookie);
    expect(getPermissionResponse.status).toBe(200);
    permissionsResult = getPermissionResponse.body;
    permissions = permissionsResult.permissions;
    expect(permissions.length).toBe(5);
    console.log(permissions);
    const permissionWithReadWallet = permissions.find((p) => p.action === "read" && p.resource === "wallet");
    if (!permissionWithReadWallet) { throw new Error("Permission not found"); }
    expect(permissionWithReadWallet.action).toBe("read");
    expect(permissionWithReadWallet.resource).toBe("wallet");
    const validPermissionId = permissionWithReadWallet.id;

    const response = await c.server
        .delete(AtitlanApiRoutes.AUTH.REMOVE_PERMISSION)
        .set("Cookie", admin.cookie)
        .send([{ permissionId: validPermissionId }]);
    expect(response.status).toBe(200);
    expect(response.body.message).toBe("Permission removed");

    permissionResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.CHECK_PERMISSION)
        .set("Cookie", admin.cookie)
        .send({
            action: "read",
            resource: "wallet",
        });

    expect(permissionResponse.status).toBe(401);

    getPermissionResponse = await c.server
        .get(AtitlanApiRoutes.USER.PERMISSIONS)
        .set("Cookie", admin.cookie);
    expect(getPermissionResponse.status).toBe(200);
    permissionsResult = getPermissionResponse.body;
    permissions = permissionsResult.permissions;
    expect(permissions.length).toBe(4);
});

test<ApiIntegrationTest>("Auth - permission/remove - Unauthorized User", async (c) => {
    const admin = await createAdminWithWalletAndPermissions(c.server, "test@atitlanio.io", "securepassword");
    const user = await createUser(c.server, "user@example.com", "securepassword");
    const newRemovePermissions = createPermissions(user.id.toString(),
        ThorAdminPermissions.ManageAllPermissions,
        Resources.ThorAdmin);

    const allAdminPermissions = await c.server
        .get(AtitlanApiRoutes.USER.PERMISSIONS)
        .set("Cookie", admin.cookie);
    expect(allAdminPermissions.status).toBe(200);
    const validPermissionId = allAdminPermissions.body.permissions[0].id;

    const response = await c.server
        .delete(AtitlanApiRoutes.AUTH.REMOVE_PERMISSION)
        .set("Cookie", user.cookie)
        .send([{ permissionId: validPermissionId }]);
    expect(response.status).toBe(401);
    expect(response.body.message).toBe("Not authorized to add permissions");

    const addResponse = await c.server
        .post(AtitlanApiRoutes.AUTH.ADD_PERMISSION)
        .set("Cookie", admin.cookie)
        .send(newRemovePermissions);
    expect(addResponse.status).toBe(200);

    const deleteRespone = await c.server
        .delete(AtitlanApiRoutes.AUTH.REMOVE_PERMISSION)
        .set("Cookie", user.cookie)
        .send([{ permissionId: validPermissionId }]);
    expect(deleteRespone.status).toBe(200);
    expect(deleteRespone.body.message).toBe("Permission removed");
});