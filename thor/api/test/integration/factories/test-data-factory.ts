import { Transaction, Invoice, PaymentGateway, Wallet, TransactionIntent, Permission } from "@prisma/client";
import supertest, { Response } from "supertest";
import { expect } from "vitest";
import { seedAdminUser } from "../../../src/prisma/seedAdminRights";
import AtitlanApiRoutes from "../../../src/components/shared/routes";
import { createUserOwnedWallet } from "./wallets.factory";
import { WalletDetails } from "../../../src/components/finance/wallet/wallet.types";
import { ExtendedTransactionIntent } from "../../../src/components/finance/transaction-intent/transaction-intent.type";

export async function createUser(
    api: supertest.SuperTest<supertest.Test>,
    email: string,
    password: string): Promise<{ id: string, cookie: string, email: string }> {
    const registerResponse = await api
        .post(AtitlanApiRoutes.AUTH.SIGNUP)
        .send({
            email: email,
            password: password,
        });
    expect(registerResponse.status).toBe(200);
    expect(registerResponse.body.user.email).toBe(email);

    return {
        id: registerResponse.body.user.id.toString(),
        cookie: registerResponse.headers["set-cookie"],
        email: registerResponse.body.user.email,
    }
}

export async function createInvoice(
    api: supertest.SuperTest<supertest.Test>,
    invoiceData: {
        amount: string,
        currency: string,
        description: string,
        targetWalletId: string
    },
    cookie: string
): Promise<Invoice> {
    const response = await api
        .post(AtitlanApiRoutes.INVOICE.CREATE)
        .set("Cookie", cookie)
        .send(invoiceData);

    expect(response.status).toBe(200);
    expect(response.body.invoice).toBeDefined();
    const invoice: Invoice = response.body.invoice;

    expect(invoice.amount.toString()).toBe(invoiceData.amount);
    expect(invoice.currency).toBe(invoiceData.currency);
    expect(invoice.description).toBe(invoiceData.description);
    expect(invoice.targetWalletId.toString()).toBe(invoiceData.targetWalletId);

    return invoice;
}

export type AdminWithWalletAndPermissions = {
    id: string,
    cookie: string,
    email: string,
    wallets: {
        sourceWallet: WalletDetails;
        fundsWallet: WalletDetails;
    },
    permissions: Permission[];
}

export async function createAdminWithWalletAndPermissions(
    api: supertest.SuperTest<supertest.Test>,
    email: string,
    password: string): Promise<AdminWithWalletAndPermissions> {
    const admin = await seedAdminUser(email, password);
    const loginResponse = await api.post(AtitlanApiRoutes.AUTH.LOGIN).send({
        email: email,
        password: password,
    });
    expect(loginResponse.status).toBe(200);
    expect(loginResponse.body.user.email).toBe(email);
    const cookie = loginResponse.headers["set-cookie"]
    const mintWallet = await createUserOwnedWallet(api, "aio-minter", cookie);
    const fundsallet = await createUserOwnedWallet(api, "aio-funds", cookie);
    return {
        id: loginResponse.body.user.id.toString(),
        cookie: loginResponse.headers["set-cookie"],
        email: loginResponse.body.user.email,
        wallets: {
            sourceWallet: mintWallet,
            fundsWallet: fundsallet
        },
        permissions: admin.permissions
    }
}

export async function mintFunds(
    api: supertest.SuperTest<supertest.Test>,
    amount: string,
    currency: string,
    description: string,
    cookie: any,
    mintWalletId: string,
    fundsWalletId: string): Promise<Transaction> {
    const response = await createFundsTransaction(
        api,
        {
            walletId: mintWalletId,
            amount: amount,
            currency: currency
        },
        {
            walletId: fundsWalletId,
            amount: amount,
            currency: currency
        },
        description,
        cookie);
    return response;
}

export async function createInternalTransactionResponse(
    api: supertest.SuperTest<supertest.Test>,
    source: {
        walletId: string,
        amount: string,
        currency: string,
    },
    target: {
        walletId: string,
        amount: string,
        currency: string,
    },
    description: string,
    cookie: any): Promise<Response> {

    const body = {
        sourceWalletId: source.walletId,
        targetWalletId: target.walletId,
        sourceAmount: source.amount,
        targetAmount: target.amount,
        sourceCurrency: source.currency,
        targetCurrency: target.currency,
        description: description
    };

    const transactionResponse: Response = await api
        .post(AtitlanApiRoutes.TRANSACTIONS.CREATE_INTERNAL)
        .set("Cookie", cookie)
        .send(body);

    return transactionResponse;
}

export async function createInternalTransaction(
    api: supertest.SuperTest<supertest.Test>,
    source: {
        walletId: string,
        amount: string,
        currency: string,
    },
    target: {
        walletId: string,
        amount: string,
        currency: string,
    },
    description: string,
    cookie: any)
    : Promise<Transaction> {

    const response = await createInternalTransactionResponse(
        api,
        source,
        target,
        description,
        cookie);

    expect(response.status).toBe(200);
    const transaction: Transaction = response.body.transaction;
    expect(transaction.sourceWalletId.toString()).toBe(source.walletId);
    expect(transaction.targetWalletId.toString()).toBe(target.walletId);
    expect(transaction.sourceAmount).toBe(source.amount);
    expect(transaction.targetAmount).toBe(target.amount);
    expect(transaction.sourceCurrency).toBe(source.currency);
    expect(transaction.targetCurrency).toBe(target.currency);
    expect(transaction.description).toBe(description);
    const transactionResponse = await api
        .get(AtitlanApiRoutes.TRANSACTIONS.GET(transaction.id.toString()))
        .set("Cookie", cookie);

    transaction.hash = transactionResponse.body.transaction.hash;
    transaction.previousHash = transactionResponse.body.transaction.previousHash;
    return transaction;
}

export async function createFundsTransaction(
    api: supertest.SuperTest<supertest.Test>,
    source: {
        walletId: string,
        amount: string,
        currency: string,
    },
    target: {
        walletId: string,
        amount: string,
        currency: string,
    },
    description: string,
    cookie: any)
    : Promise<Transaction> {

    const response = await createFundsTransactionResponse(
        api,
        source,
        target,
        description,
        cookie);

    expect(response.status).toBe(200);
    const transaction: Transaction = response.body.transaction;
    expect(transaction.sourceWalletId.toString()).toBe(source.walletId);
    expect(transaction.targetWalletId.toString()).toBe(target.walletId);
    expect(transaction.sourceAmount).toBe(source.amount);
    expect(transaction.targetAmount).toBe(target.amount);
    expect(transaction.sourceCurrency).toBe(source.currency);
    expect(transaction.targetCurrency).toBe(target.currency);
    expect(transaction.description).toBe(description);
    return transaction;
}

export async function createFundsTransactionResponse(
    api: supertest.SuperTest<supertest.Test>,
    source: {
        walletId: string,
        amount: string,
        currency: string,
    },
    target: {
        walletId: string,
        amount: string,
        currency: string,
    },
    description: string,
    cookie: any)
    : Promise<Response> {
    const body = {
        sourceWalletId: source.walletId,
        targetWalletId: target.walletId,
        sourceAmount: source.amount,
        targetAmount: target.amount,
        sourceCurrency: source.currency,
        targetCurrency: target.currency,
        description: description
    };

    const transactionResponse = await api
        .post(AtitlanApiRoutes.TRANSACTIONS.ADD_FUNDS)
        .set("Cookie", cookie)
        .send(body);
    return transactionResponse;
}

export function createPermissions(userId: string, action: string, resource: string)
    : { userId: string, action: string, resource: string }[] {
    return [
        {
            userId: userId,
            action: action,
            resource: resource,
        }
    ];
}

export async function createStripePaymentGateway(
    api: supertest.SuperTest<supertest.Test>,
    cookie: any,
    publicGatewayName: string,
    publicGatewayWalletName: string,
    stripeSecretApiKey: string,
    stripePublicApiKey: string,
    stripeEndpointSecret: string)
    : Promise<PaymentGateway> {
    const response = await api
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.STRIPE.CREATE_PAYMENT_GATEWAY)
        .set("Cookie", cookie)
        .send({
            paymentGatewayName: publicGatewayName,
            paymentGatewayWalletName: publicGatewayWalletName,
            secretApiKey: stripeSecretApiKey,
            publicApiKey: stripePublicApiKey,
            endpointSecret: stripeEndpointSecret
        });
    expect(response.status).toBe(200);

    const paymentGateway: PaymentGateway = response.body.paymentGateway;
    expect(paymentGateway.name).toBe(publicGatewayName);
    expect(paymentGateway.paymentIntegrationChannel).toBe("Stripe");
    return paymentGateway;
}

export type StripeHostedCheckoutResponse = {
    message: string,
    stripeUrl: string,
    publisheableApiKey: string
}

export async function createStripeHostedCheckout(
    api: supertest.SuperTest<supertest.Test>,
    cookie: any,
    transactionIntentId: string,
    paymentGatewayId: string): Promise<StripeHostedCheckoutResponse> {
    const stripeHostedCheckoutResponse = await api
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.STRIPE.HOSTED_CHECKOUT)
        .set("Cookie", cookie)
        .send({
            transactionIntentId: transactionIntentId.toString(),
            paymentGatewayId: paymentGatewayId.toString()
        });
    console.log(stripeHostedCheckoutResponse.body);
    expect(stripeHostedCheckoutResponse.status).toBe(200);
    const body: StripeHostedCheckoutResponse = stripeHostedCheckoutResponse.body;
    expect(body.message).toBe("Succes. Stripe checkout session created");
    expect(body.stripeUrl).toBeDefined();
    expect(body.publisheableApiKey).toBeDefined();
    return body;
}
