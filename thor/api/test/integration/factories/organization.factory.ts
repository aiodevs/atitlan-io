import supertest from "supertest";
import { OrganizationDetails, OrganizationGeneralInfo, RoleTemplateWithPermissions } from "../../../src/components/heimdall/organization/organization.types";
import AtitlanApiRoutes from "../../../src/components/shared/routes";
import { expect } from "vitest";
import { WalletDetails, WalletGeneralInfo } from "../../../src/components/finance/wallet/wallet.types";
import { PermissionResult } from "../../../src/components/heimdall/authorization/permission/permission.type";
import { DefaultUserOrganizationRoleNames, DefaultUserOrganizationRole, userOrganizationRoles } from "../../../src/components/heimdall/authorization/authorization.type";
import { RoleWithPermissionsResult } from "../../../src/components/heimdall/authorization/role/role.types";

export async function createOrganization(
    server: supertest.SuperTest<supertest.Test>,
    cookie: any,
    name: string): Promise<OrganizationDetails> {
    const response = await server
        .post(AtitlanApiRoutes.ORGANIZATION.CREATE)
        .set("Cookie", cookie)
        .send({ name });
    expect(response.status).toBe(200);
    const organization: OrganizationDetails = response.body.organization;
    expect(organization.name).toBe(name);
    return organization;
}

export async function updateOrganization(
    server: supertest.SuperTest<supertest.Test>,
    cookie: any,
    request: {
        organizationId: string,
        name: string,
        addOrUpdateOrgRoleTemplates: {
            role: {
                id?: string,
                name: string,
                permissions: string[],
            }
        }[],
        addOrUpdateUserLinks: {
            userId: string,
            roleId: string,
            revokedAt?: string,
        }[],
        addOrUpdateUserWalletLinks: {
            userId: string,
            walletId: string,
            roleId: string,
            revokedAt?: string,
        }[],
        addOrUpdateWalletLinks: {
            walletId: string,
            roleId: string,
            revokedAt?: string,
        }[]
    }): Promise<OrganizationDetails> {
    const response = await server
        .post(AtitlanApiRoutes.ORGANIZATION.UPDATE)
        .set("Cookie", cookie)
        .send({ ...request })
        .expect(200);
    const organization: OrganizationDetails = response.body.organization;
    expect(organization.id).toBe(request.organizationId);
    return organization;
}

export async function revokeUsersFromOrganizations(
    server: supertest.SuperTest<supertest.Test>,
    cookie: any,
    userRevocations: {
        organizationId: string,
        userId: string,
        revokedAt: Date
    }[]): Promise<OrganizationDetails[]> {
    const response = await server
        .post(AtitlanApiRoutes.ORGANIZATION.REVOKE_USERS)
        .set("Cookie", cookie)
        .send({ userRevocations })
        .expect(200);
    const organizations: OrganizationDetails[] = response.body.organizations;
    return organizations;
}

export async function getOrganizationDetails(
    server: supertest.SuperTest<supertest.Test>,
    cookie: any,
    organizationId: string
): Promise<OrganizationDetails> {
    const response = await server
        .get(AtitlanApiRoutes.ORGANIZATION.GET_DETAILS(organizationId))
        .set("Cookie", cookie)
        .expect(200);
    const organization: OrganizationDetails = response.body.organization;
    expect(organization.id).toBe(organizationId);
    return organization;
}

export async function createOrganizationOwnedWallet(
    server: supertest.SuperTest<supertest.Test>,
    cookie: any,
    organizationId: string,
    name: string): Promise<WalletGeneralInfo> {
    const response = await server
        .post(AtitlanApiRoutes.ORGANIZATION.CREATE_WALLET)
        .set("Cookie", cookie)
        .send({ organizationId, name })
        .expect(200);
    const wallet: WalletGeneralInfo = response.body.wallet;
    expect(wallet.name).toBe(name);
    return wallet;
}

export async function getOrganizationWalletDetails(
    server: supertest.SuperTest<supertest.Test>,
    cookie: any,
    organizationId: string,
    walletId: string): Promise<WalletDetails> {
    const response = await server
        .get(AtitlanApiRoutes.ORGANIZATION.GET_WALLET_DETAILS(organizationId, walletId))
        .set("Cookie", cookie)
        .expect(200);
    const wallet: WalletDetails = response.body.wallet;
    expect(wallet.id).toBe(walletId);
    return wallet;
}

export async function getOrganizationsFromUser(
    server: supertest.SuperTest<supertest.Test>,
    cookie: any): Promise<OrganizationDetails[]> {
    const response = await server
        .get(AtitlanApiRoutes.ORGANIZATION.GET_USER_ORGANIZATIONS)
        .set("Cookie", cookie)
        .expect(200);
    const organizations: OrganizationDetails[] = response.body.organizations;
    return organizations;
}

export async function getOrganizationsFromUserById(
    server: supertest.SuperTest<supertest.Test>,
    cookie: any,
    userId: string): Promise<OrganizationGeneralInfo[]> {
    const response = await server
        .get(AtitlanApiRoutes.ORGANIZATION.GET_USER_ID_ORGANIZATIONS(userId))
        .set("Cookie", cookie)
        .expect(200);
    const organizations: OrganizationGeneralInfo[] = response.body.organizations;
    return organizations;
}

export async function getOrganizationsFromUserById401(
    server: supertest.SuperTest<supertest.Test>,
    cookie: any,
    userId: string): Promise<void> {
    const response = await server
        .get(AtitlanApiRoutes.ORGANIZATION.GET_USER_ID_ORGANIZATIONS(userId))
        .set("Cookie", cookie)
        .expect(401);
}

export function getRole(roleName: DefaultUserOrganizationRoleNames): DefaultUserOrganizationRole {
    const role = userOrganizationRoles.find(role => role.name === roleName);
    if (!role) {
        throw new Error("Role not found");
    }
    return role;
}

export function checkRoleTemplatePermissions(roleTemplate: RoleWithPermissionsResult, permissionsToCheck: PermissionResult[]) {
    const roleActions = roleTemplate.permissions.map(permission => permission.action.toString());
    const retrievedPermissionsActions = permissionsToCheck.map(permissionLink => permissionLink.action);
    roleActions.forEach(expectedPermission => {
        expect(retrievedPermissionsActions).toContain(expectedPermission);
    });
    expect(permissionsToCheck.length).toBe(roleActions.length);
}

export function checkDefaultRolePermissions(role: DefaultUserOrganizationRole, permissionsToCheck: PermissionResult[]) {
    const expectedPermissions = role.permissions.map(permission => permission.toString());
    const retrievedPermissionsActions = permissionsToCheck.map(permissionLink => permissionLink.action);
    expectedPermissions.forEach(expectedPermission => {
        expect(retrievedPermissionsActions).toContain(expectedPermission);
    });
    expect(permissionsToCheck.length).toBe(expectedPermissions.length);
}