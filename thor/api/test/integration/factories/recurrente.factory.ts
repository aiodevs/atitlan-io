import { PaymentGateway, PaymentGatewayApiKeyType } from "@prisma/client";
import supertest from "supertest";
import { expect } from "vitest";
import AtitlanApiRoutes from "../../../src/components/shared/routes";
import { ExtendedPaymentGateway } from "../../../src/components/finance/payment-gateways/payment-gateways.type";
import { RecurrenteSettings } from "../../../src/components/finance/integration-channels/recurrente/recurrente.type";
import TestUtils from "../util/test-util";

export type RecurrenteHostedCheckoutResponse = {
    message: string,
    recurrenteUrl: string,
}

export async function startRecurrenteHostedCheckout(
    api: supertest.SuperTest<supertest.Test>,
    transactionIntentId: string,
    paymentGatewayId: string): Promise<RecurrenteHostedCheckoutResponse> {
    const hostedCheckoutResponse = await api
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.HOSTED_CHECKOUT)
        .send({
            transactionIntentId: transactionIntentId.toString(),
            paymentGatewayId: paymentGatewayId.toString()
        });
    expect(hostedCheckoutResponse.status).toBe(200);
    const body: RecurrenteHostedCheckoutResponse = hostedCheckoutResponse.body;
    expect(body.message).toBe("Success. Recurrente checkout session created");
    expect(body.recurrenteUrl).toBeDefined();
    return body;
}

export async function createRecurrentePaymentGateway(
    api: supertest.SuperTest<supertest.Test>,
    cookie: any,
    publicGatewayName: string,
    publicGatewayWalletName: string,
    secretApiKey: string,
    publicApiKey: string,
    endpointSecret: string,
    organizationId?: string)
    : Promise<PaymentGateway> {
    const response = await api
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.CREATE_PAYMENT_GATEWAY)
        .set("Cookie", cookie)
        .send({
            paymentGatewayName: publicGatewayName,
            paymentGatewayWalletName: publicGatewayWalletName,
            secretApiKey: secretApiKey,
            publicApiKey: publicApiKey,
            endpointSecret: endpointSecret,
            organizationId: organizationId,
        });
    console.log(response.body);
    expect(response.status).toBe(200);

    const paymentGateway: PaymentGateway = response.body.paymentGateway;
    expect(paymentGateway.name).toBe(publicGatewayName);
    expect(paymentGateway.paymentIntegrationChannel).toBe("Recurrente");
    return paymentGateway;
}

export async function updateRecurrenteSettings(
    api: supertest.SuperTest<supertest.Test>,
    cookie: any,
    paymentGatewayId: string,
    publicGatewayName: string,
    secretApiKey: string,
    publicApiKey: string,
    endpointSecret: string,
    disabledAt: Date | null = null)
    : Promise<ExtendedPaymentGateway> {
    const response = await api
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.UPDATE_SETTINGS)
        .set("Cookie", cookie)
        .send({
            paymentGatewayId: paymentGatewayId,
            paymentGatewayName: publicGatewayName,
            secretApiKey: secretApiKey,
            publicApiKey: publicApiKey,
            endpointSecret: endpointSecret,
            disabledAt: disabledAt ? disabledAt.toISOString() : undefined,
        });
    expect(response.status).toBe(200);

    const paymentGateway: ExtendedPaymentGateway = response.body.paymentGateway;
    expect(paymentGateway.name).toBe(publicGatewayName);
    expect(paymentGateway.paymentIntegrationChannel).toBe("Recurrente");
    expect(paymentGateway.disabledAt).toBe(disabledAt?.toISOString() ?? "");
    expect(paymentGateway.sourceWalletId).toBeDefined();
    const privateKey = paymentGateway.apiKeys.find(k => k.apiKey === secretApiKey && k.type === "Secret");
    const publicKey = paymentGateway.apiKeys.find(k => k.apiKey === publicApiKey && k.type === PaymentGatewayApiKeyType.Public.toString());
    const settings = paymentGateway.settings.find(k => k.value === endpointSecret && k.encrypted && k.name === RecurrenteSettings.endpointSecret);
    if (!privateKey || !publicKey || !settings) {
        throw new Error("Missing key or setting");
    }

    return paymentGateway;
}

export type RecurrenteEvent = {
    api_version: string;
    checkout: { id: string };
    created_at: string;
    customer_id: string;
    event_type: string;
    id: string;
    product: { id: string };
};