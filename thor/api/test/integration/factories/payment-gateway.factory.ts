import supertest from "supertest";
import AtitlanApiRoutes from "../../../src/components/shared/routes";
import { PaymentGateway } from "@prisma/client";
import { ExtendedPaymentGateway } from "../../../src/components/finance/payment-gateways/payment-gateways.type";
import { expect } from "vitest";

export async function updatePaymentGateway(
    server: supertest.SuperTest<supertest.Test>,
    cookie: any,
    id: string,
    name: string,
): Promise<PaymentGateway> {
    const response = await server
        .post(AtitlanApiRoutes.PAYMENT_GATEWAY.UPDATE)
        .set("Cookie", cookie)
        .send({
            id: id,
            name: name,
        })
        .expect(200);
    const paymentGatway: PaymentGateway = response.body.paymentGateway;
    return paymentGatway;
}

export async function getPaymentGateway(
    server: supertest.SuperTest<supertest.Test>,
    cookie: any,
    id: string
): Promise<PaymentGateway> {
    const response = await server
        .get(AtitlanApiRoutes.PAYMENT_GATEWAY.GET(id))
        .set("Cookie", cookie)
        .expect(200);
    const paymentGateway: PaymentGateway = response.body.paymentGateway;
    return paymentGateway;
}

export async function getPaymentGatewayDetails(
    server: supertest.SuperTest<supertest.Test>,
    cookie: any,
    id: string
): Promise<ExtendedPaymentGateway> {
    const response = await server
        .get(AtitlanApiRoutes.PAYMENT_GATEWAY.GET_DETAILS(id))
        .set("Cookie", cookie)
        .expect(200);
    expect(response.body.message).toBe("Payment gateway details retrieved");

    const paymentGateway: ExtendedPaymentGateway = response.body.paymentGateway;
    return paymentGateway;
}