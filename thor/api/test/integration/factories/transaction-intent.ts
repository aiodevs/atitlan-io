import supertest from "supertest";
import { expect } from "vitest";
import { WalletDetails } from "../../../src/components/finance/wallet/wallet.types";
import AtitlanApiRoutes from "../../../src/components/shared/routes";
import { ExtendedTransactionIntent } from "../../../src/components/finance/transaction-intent/transaction-intent.type";
import { TransactionIntent } from "@prisma/client";
import { c } from "vitest/dist/reporters-5f784f42.js";


export async function createAnonymousTransactionIntent(
    api: supertest.SuperTest<supertest.Test>,
    invoiceId: string
): Promise<TransactionIntent> {
    const walletResponse = await api
        .post(AtitlanApiRoutes.TRANSACTION_INTENT.CREATE_ANONYMOUS)
        .send({
            invoiceId: invoiceId,
        });
    expect(walletResponse.status).toBe(200);
    const transactionIntent: TransactionIntent = walletResponse.body.transactionIntent;
    expect(transactionIntent.anonymous).toBe(true);
    expect(transactionIntent.invoiceId).toBe(invoiceId);
    return transactionIntent;
}

export async function updateAnonymousIntentWithUserWallet(
    api: supertest.SuperTest<supertest.Test>,
    transactionIntentId: string,
    userWalletId: string,
    userId: string,
    cookie: any
): Promise<TransactionIntent> {
    const walletResponse = await api
        .post(AtitlanApiRoutes.TRANSACTION_INTENT.UPDATE_USER_AND_WALLET)
        .set('Cookie', cookie)
        .send({
            transactionIntentId: transactionIntentId,
            userWalletId: userWalletId,
        });
    expect(walletResponse.status).toBe(200);
    const transactionIntent: TransactionIntent = walletResponse.body.transactionIntent;
    expect(transactionIntent.anonymous).toBeFalsy();
    expect(transactionIntent.sourceWalletId).toBe(userWalletId);
    expect(transactionIntent.requestingUserId).toBe(userId);

    return transactionIntent;
}

export async function createTransactionIntent(
    api: supertest.SuperTest<supertest.Test>,
    cookie: any,
    invoiceId: string,
    userWalletId: string): Promise<TransactionIntent> {
    const transactionIntentResponse = await api
        .post(AtitlanApiRoutes.TRANSACTION_INTENT.CREATE)
        .set("Cookie", cookie)
        .send({
            invoiceId: invoiceId.toString(),
            userWalletId: userWalletId.toString()
        });
    const transactionIntent = transactionIntentResponse.body.transactionIntent;
    expect(transactionIntentResponse.status).toBe(200);
    expect(transactionIntent.invoiceId.toString()).toBe(invoiceId);
    expect(transactionIntent.status).toBe("created");
    return transactionIntent;
}

export async function getTransactionIntent(
    api: supertest.SuperTest<supertest.Test>,
    cookie: any,
    transactionIntentId: string
): Promise<ExtendedTransactionIntent> {
    const response = await api
        .get(AtitlanApiRoutes.TRANSACTION_INTENT.GET(transactionIntentId))
        .set("Cookie", cookie);
    console.log(response.body);
    expect(response.status).toBe(200);
    const transactionIntent: ExtendedTransactionIntent = response.body.transactionIntent;
    expect(transactionIntent.id).toBe(transactionIntentId);
    return transactionIntent;
}