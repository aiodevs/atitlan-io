import supertest from "supertest";
import { expect } from "vitest";
import { WalletDetails, WalletGeneralInfo } from "../../../src/components/finance/wallet/wallet.types";
import AtitlanApiRoutes from "../../../src/components/shared/routes";
import { Wallet } from "@prisma/client";

export async function createUserOwnedWallet(
    api: supertest.SuperTest<supertest.Test>,
    walletName: string,
    cookie: any): Promise<WalletDetails> {
    const walletResponse = await api
        .post(AtitlanApiRoutes.WALLET.CREATE)
        .set("Cookie", cookie)
        .send({
            name: walletName,
        });
    expect(walletResponse.status).toBe(200);
    expect(walletResponse.body.wallet.name).toBe(walletName);
    const wallet: WalletDetails = walletResponse.body.wallet;
    return wallet;
}

export async function updateWallets(
    api: supertest.SuperTest<supertest.Test>,
    cookie: any,
    wallets: {
        walletId: string,
        name?: string | null,
        paymentGateway?: {
            disabledAt: Date | null,
        } | null,
        authorizedUsers?: {
            userId: string,
            roleId: string,
            revokedAt?: Date,
        }[],
        authorizedOrganizations?: {
            organizationId: string,
            roleId: string,
            revokedAt?: Date,
        }[],
        authorizedOrganizationUsers?: {
            organizationId: string,
            userId: string,
            roleId: string,
            revokedAt?: Date,
        }[],
    }[],
): Promise<WalletDetails[]> {
    const walletResponse = await api
        .post(AtitlanApiRoutes.WALLET.UPDATE)
        .set("Cookie", cookie)
        .send({ wallets });
    expect(walletResponse.status).toBe(200);
    const wallet: WalletDetails[] = walletResponse.body.wallets;
    return wallet;
}

export async function getUserWallets(
    api: supertest.SuperTest<supertest.Test>,
    cookie: any,
    balance: boolean = true,
): Promise<Wallet[]> {
    const walletsResponse = await api
        .get(AtitlanApiRoutes.USER.WALLETS({ balance }))
        .set("Cookie", cookie);
    expect(walletsResponse.status).toBe(200);
    const wallets: Wallet[] = walletsResponse.body.wallets;
    return wallets;
}

export async function getWalletDetails(
    api: supertest.SuperTest<supertest.Test>,
    cookie: any,
    walletId: string,
    balance: boolean = true,
    transactions: boolean = true,
    invoices: boolean = true,
    intents: boolean = true
): Promise<WalletDetails> {
    const walletResponse = await api
        .get(AtitlanApiRoutes.USER.WALLET_DETAILS(walletId, {
            balance: balance,
            transactions: transactions,
            invoices: invoices,
            intents: intents
        }))
        .set("Cookie", cookie);
    expect(walletResponse.status).toBe(200);
    expect(walletResponse.body.wallet).toBeDefined();
    expect(walletResponse.body.wallet.id.toString()).toBe(walletId);
    const walletDetails: WalletDetails = walletResponse.body.wallet as WalletDetails;
    if (balance) {
        expect(walletDetails.balance).toBeDefined();
    }
    if (transactions) {
        expect(walletDetails.incomingTransactions).toBeDefined();
        expect(walletDetails.outgoingTransactions).toBeDefined();
    }
    if (invoices) {
        expect(walletDetails.invoices).toBeDefined();
    }
    if (intents) {
        expect(walletDetails.transactionIntents).toBeDefined();
    }
    return walletDetails;
}

export async function getWalletsFromUserId(
    api: supertest.SuperTest<supertest.Test>,
    cookie: any,
    userId: string,
    balance: boolean = true
): Promise<WalletGeneralInfo[]> {
    const walletsResponse = await api
        .get(AtitlanApiRoutes.WALLET.GET_USER_WALLETS(userId, { balance }))
        .set("Cookie", cookie);
    expect(walletsResponse.status).toBe(200);
    const wallets: WalletGeneralInfo[] = walletsResponse.body.wallets;
    return wallets;
}

export async function getWalletDetailsFromUserId(
    api: supertest.SuperTest<supertest.Test>,
    cookie: any,
    userId: string,
    walletId: string,
    options?: {
        balance: boolean,
        transactions: boolean,
        invoices: boolean,
        intents: boolean
    }): Promise<WalletDetails> {
    const walletResponse = await api
        .get(AtitlanApiRoutes.WALLET.GET_USER_WALLET_DETAILS(userId, walletId, options))
        .set("Cookie", cookie);
    expect(walletResponse.status).toBe(200);
    expect(walletResponse.body.wallet).toBeDefined();
    expect(walletResponse.body.wallet.id.toString()).toBe(walletId);
    const walletDetails: WalletDetails = walletResponse.body.wallet as WalletDetails;
    if (options?.balance) {
        expect(walletDetails.balance).toBeDefined();
    }
    if (options?.transactions) {
        expect(walletDetails.incomingTransactions).toBeDefined();
        expect(walletDetails.outgoingTransactions).toBeDefined();
    }
    if (options?.invoices) {
        expect(walletDetails.invoices).toBeDefined();
    }
    if (options?.intents) {
        expect(walletDetails.transactionIntents).toBeDefined();
    }
    return walletDetails;
}