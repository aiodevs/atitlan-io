import { Type } from "@sinclair/typebox";
import { config } from "dotenv";

loadConfigFile();
setDatabaseURL();

export const envSchema = Type.Object({
  NODE_ENV: Type.String(),
  THOR_API_VERSION: Type.String(),
  THOR_API_COOKIE_SECRET: Type.String()
});

export const {
  NODE_ENV,
  THOR_API_PORT,
  THOR_API_VERSION,
  THOR_API_COOKIE_SECRET,
  THOR_API_INITIAL_ADMIN_EMAIL,
  THOR_API_INITIAL_ADMIN_PASSWORD,
  THOR_ENCRYPTION_KEY,
  THOR_ENCRYPTION_KEY_OLD,
  THOR_ENCRYPTION_KEY_NEW,
} =
  process.env;

function loadConfigFile(path?: string) {
  const isProduction = process.env.NODE_ENV === "production";
  const isTest = process.env.NODE_ENV === "test";

  if (isProduction || isTest) {
    console.log('In production or test, skipping .env file load.');
    // Assume environment variables are provided by gitlab cicd or docker-compose
    return;
  }
  // load config file from local path/.env file for local development
  const result = path ? config({ path }) : config();

  if (result.error) {
    console.log(`loading config failed from env file: ${result.error}`);
    throw result.error;
  }
}

function setDatabaseURL() {
  process.env.DATABASE_URL = generateDatabaseURL();
}

function generateDatabaseURL(): string {
  const POSTGRES_USER = process.env.THOR_API_DB_USER!;
  const POSTGRES_PASSWORD = process.env.THOR_API_DB_PASSWORD!;
  const POSTGRES_DB = process.env.THOR_API_DB_SCHEMA!;
  const POSTGRES_HOST = process.env.THOR_API_DB_HOST!;
  const POSTGRES_PORT = process.env.THOR_API_DB_PORT!;

  const DATABASE_URL = `postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}?schema=public`;
  return DATABASE_URL;
}
