import Fastify, { FastifyInstance } from "fastify";
import ajvErrors from "ajv-errors";
import fastifyHelmet from "@fastify/helmet";
import cookie from "@fastify/cookie";
import fastifyCompress from "@fastify/compress";
import { TypeBoxTypeProvider } from "@fastify/type-provider-typebox";
import { initializeRoutes } from "@/plugins/initializeRoutes";
import { authentication } from "@/plugins/authentication";
import { initSwagger } from "@/plugins/swagger";
import { schemaErrorFormatter } from "@/util/schemaErrorFormatter";
import { setPrismaDbUrl } from "./util/prisma";
import fastifyEnv from "@fastify/env";
import {
  THOR_API_VERSION,
  NODE_ENV,
  THOR_API_PORT,
  THOR_API_COOKIE_SECRET,
  envSchema as schema,
} from "@/config";
import fastifyRawBody from "fastify-raw-body";
import { StripeFactory, createStripe } from "./util/stripe-factory";


export default class Api {
  public app: FastifyInstance;
  public env: string;
  public port: number;
  private stripeFactory: StripeFactory;

  constructor(
    databaseUrl?: string,
    stripeFactory: StripeFactory = createStripe
  ) {
    this.app = Fastify({
      schemaErrorFormatter,
      ajv: {
        customOptions: {
          coerceTypes: false,
          allErrors: true,
        },
        plugins: [ajvErrors],
      },
      logger: true,
    }).withTypeProvider<TypeBoxTypeProvider>();

    this.env = NODE_ENV ?? "development";
    this.port = Number(THOR_API_PORT) ?? 3001;
    this.stripeFactory = stripeFactory;
    this.init();
    if (databaseUrl)
      setPrismaDbUrl(databaseUrl);
  }

  public async listen() {
    try {
      await this.app.listen({ port: this.port, host: "0.0.0.0" });
    } catch (err) {
      this.app.log.error(err);
    }
  }

  private init(): void {
    this.initializePlugins();
    this.initializeRoutes();
  }

  private initializePlugins(): void {
    this.app.register(fastifyEnv, { dotenv: true, schema });
    this.app.register(cookie, { secret: THOR_API_COOKIE_SECRET });
    // TODO CSRF protection
    this.app.register(fastifyHelmet);
    this.app.register(fastifyRawBody, {
      field: 'rawBody', // change the default request.rawBody property name
      global: false, // add the rawBody to every request. **Default true**
      encoding: 'utf8', // set it to false to set rawBody as a Buffer **Default utf8**
      runFirst: true, // get the body before any preParsing hook change/uncompress it. **Default false**
    });

    this.app.register(fastifyCompress);
    this.app.register(authentication);
    this.app.register(initSwagger);
  }

  private initializeRoutes() {
    this.app.register(initializeRoutes, {
      stripeFactory: this.stripeFactory
    });
  }
}
