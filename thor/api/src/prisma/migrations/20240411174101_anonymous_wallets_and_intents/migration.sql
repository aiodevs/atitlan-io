-- AlterTable
ALTER TABLE "TransactionIntent" ADD COLUMN     "anonymous" BOOLEAN NOT NULL DEFAULT false,
ALTER COLUMN "requestingUserId" DROP NOT NULL,
ALTER COLUMN "sourceWalletId" DROP NOT NULL;
