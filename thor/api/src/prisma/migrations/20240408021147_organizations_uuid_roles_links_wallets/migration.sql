/*
  Warnings:

  - The primary key for the `ApiKey` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `CompletedTransactionLink` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `Invoice` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `Key` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `PaymentGateway` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `PaymentGatewayApiKeyLink` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `PaymentGatewaySettingLink` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `Permission` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `expiresAt` on the `Permission` table. All the data in the column will be lost.
  - You are about to drop the column `lastUsed` on the `Permission` table. All the data in the column will be lost.
  - You are about to drop the column `userId` on the `Permission` table. All the data in the column will be lost.
  - The primary key for the `Session` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `Settings` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `Transaction` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `TransactionIntent` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `userWalletId` on the `TransactionIntent` table. All the data in the column will be lost.
  - The primary key for the `User` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `Wallet` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `userId` on the `Wallet` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[id]` on the table `CompletedTransactionLink` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[id]` on the table `Transaction` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[paymentGatewayIdentifier]` on the table `TransactionIntent` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `requestingUserId` to the `TransactionIntent` table without a default value. This is not possible if the table is not empty.
  - Added the required column `sourceWalletId` to the `TransactionIntent` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "CompletedTransactionLink" DROP CONSTRAINT "CompletedTransactionLink_invoiceId_fkey";

-- DropForeignKey
ALTER TABLE "CompletedTransactionLink" DROP CONSTRAINT "CompletedTransactionLink_transactionId_fkey";

-- DropForeignKey
ALTER TABLE "CompletedTransactionLink" DROP CONSTRAINT "CompletedTransactionLink_transactionIntentId_fkey";

-- DropForeignKey
ALTER TABLE "Key" DROP CONSTRAINT "Key_userId_fkey";

-- DropForeignKey
ALTER TABLE "PaymentGateway" DROP CONSTRAINT "PaymentGateway_sourceWalletId_fkey";

-- DropForeignKey
ALTER TABLE "PaymentGatewayApiKeyLink" DROP CONSTRAINT "PaymentGatewayApiKeyLink_apiKeyId_fkey";

-- DropForeignKey
ALTER TABLE "PaymentGatewayApiKeyLink" DROP CONSTRAINT "PaymentGatewayApiKeyLink_paymentGatewayId_fkey";

-- DropForeignKey
ALTER TABLE "PaymentGatewaySettingLink" DROP CONSTRAINT "PaymentGatewaySettingLink_paymentGatewayId_fkey";

-- DropForeignKey
ALTER TABLE "PaymentGatewaySettingLink" DROP CONSTRAINT "PaymentGatewaySettingLink_settingId_fkey";

-- DropForeignKey
ALTER TABLE "Permission" DROP CONSTRAINT "Permission_userId_fkey";

-- DropForeignKey
ALTER TABLE "Session" DROP CONSTRAINT "Session_userId_fkey";

-- DropForeignKey
ALTER TABLE "Transaction" DROP CONSTRAINT "Transaction_sourceWalletId_fkey";

-- DropForeignKey
ALTER TABLE "Transaction" DROP CONSTRAINT "Transaction_targetWalletId_fkey";

-- DropForeignKey
ALTER TABLE "TransactionIntent" DROP CONSTRAINT "TransactionIntent_invoiceId_fkey";

-- DropForeignKey
ALTER TABLE "TransactionIntent" DROP CONSTRAINT "TransactionIntent_paymentGatewayId_fkey";

-- DropForeignKey
ALTER TABLE "Wallet" DROP CONSTRAINT "Wallet_userId_fkey";

-- DropIndex
DROP INDEX "Permission_userId_idx";

-- DropIndex
DROP INDEX "Wallet_userId_idx";

-- AlterTable
ALTER TABLE "ApiKey" DROP CONSTRAINT "ApiKey_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ADD CONSTRAINT "ApiKey_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "ApiKey_id_seq";

-- AlterTable
ALTER TABLE "CompletedTransactionLink" DROP CONSTRAINT "CompletedTransactionLink_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ALTER COLUMN "transactionIntentId" SET DATA TYPE TEXT,
ALTER COLUMN "transactionId" SET DATA TYPE TEXT,
ALTER COLUMN "invoiceId" SET DATA TYPE TEXT,
ADD CONSTRAINT "CompletedTransactionLink_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "CompletedTransactionLink_id_seq";

-- AlterTable
ALTER TABLE "Invoice" DROP CONSTRAINT "Invoice_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ALTER COLUMN "targetWalletId" SET DATA TYPE TEXT,
ADD CONSTRAINT "Invoice_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "Invoice_id_seq";

-- AlterTable
ALTER TABLE "Key" DROP CONSTRAINT "Key_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ALTER COLUMN "userId" SET DATA TYPE TEXT,
ADD CONSTRAINT "Key_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "Key_id_seq";

-- AlterTable
ALTER TABLE "PaymentGateway" DROP CONSTRAINT "PaymentGateway_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ALTER COLUMN "sourceWalletId" SET DATA TYPE TEXT,
ADD CONSTRAINT "PaymentGateway_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "PaymentGateway_id_seq";

-- AlterTable
ALTER TABLE "PaymentGatewayApiKeyLink" DROP CONSTRAINT "PaymentGatewayApiKeyLink_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ALTER COLUMN "apiKeyId" SET DATA TYPE TEXT,
ALTER COLUMN "paymentGatewayId" SET DATA TYPE TEXT,
ADD CONSTRAINT "PaymentGatewayApiKeyLink_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "PaymentGatewayApiKeyLink_id_seq";

-- AlterTable
ALTER TABLE "PaymentGatewaySettingLink" DROP CONSTRAINT "PaymentGatewaySettingLink_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ALTER COLUMN "paymentGatewayId" SET DATA TYPE TEXT,
ALTER COLUMN "settingId" SET DATA TYPE TEXT,
ADD CONSTRAINT "PaymentGatewaySettingLink_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "PaymentGatewaySettingLink_id_seq";

-- AlterTable
ALTER TABLE "Permission" DROP CONSTRAINT "Permission_pkey",
DROP COLUMN "expiresAt",
DROP COLUMN "lastUsed",
DROP COLUMN "userId",
ADD COLUMN     "isDefault" BOOLEAN NOT NULL DEFAULT false,
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ADD CONSTRAINT "Permission_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "Permission_id_seq";

-- AlterTable
ALTER TABLE "Session" DROP CONSTRAINT "Session_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ALTER COLUMN "userId" SET DATA TYPE TEXT,
ADD CONSTRAINT "Session_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "Session_id_seq";

-- AlterTable
ALTER TABLE "Settings" DROP CONSTRAINT "Settings_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ADD CONSTRAINT "Settings_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "Settings_id_seq";

-- AlterTable
ALTER TABLE "Transaction" DROP CONSTRAINT "Transaction_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ALTER COLUMN "sourceWalletId" SET DATA TYPE TEXT,
ALTER COLUMN "targetWalletId" SET DATA TYPE TEXT,
ADD CONSTRAINT "Transaction_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "Transaction_id_seq";

-- AlterTable
ALTER TABLE "TransactionIntent" DROP CONSTRAINT "TransactionIntent_pkey",
DROP COLUMN "userWalletId",
ADD COLUMN     "paymentGatewayIdentifier" TEXT,
ADD COLUMN     "requestingUserId" TEXT NOT NULL,
ADD COLUMN     "sourceWalletId" TEXT NOT NULL,
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ALTER COLUMN "invoiceId" SET DATA TYPE TEXT,
ALTER COLUMN "paymentGatewayId" SET DATA TYPE TEXT,
ADD CONSTRAINT "TransactionIntent_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "TransactionIntent_id_seq";

-- AlterTable
ALTER TABLE "User" DROP CONSTRAINT "User_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ADD CONSTRAINT "User_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "User_id_seq";

-- AlterTable
ALTER TABLE "Wallet" DROP CONSTRAINT "Wallet_pkey",
DROP COLUMN "userId",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ADD CONSTRAINT "Wallet_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "Wallet_id_seq";

-- CreateTable
CREATE TABLE "Role" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "name" TEXT NOT NULL,
    "isDefault" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "Role_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "UserPermissionLink" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "lastUsed" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "revokedAt" TIMESTAMP(3),
    "userId" TEXT NOT NULL,
    "permissionId" TEXT NOT NULL,

    CONSTRAINT "UserPermissionLink_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "PermissionRoleLink" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "roleId" TEXT NOT NULL,
    "permissionId" TEXT NOT NULL,

    CONSTRAINT "PermissionRoleLink_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Organization" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "name" TEXT NOT NULL,

    CONSTRAINT "Organization_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "OrganizationRoleLink" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "disabledAt" TIMESTAMP(3),
    "organizationId" TEXT NOT NULL,
    "roleId" TEXT NOT NULL,

    CONSTRAINT "OrganizationRoleLink_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "UserOrganizationRoleAccessLink" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "lastUsed" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "revokedAt" TIMESTAMP(3),
    "userId" TEXT NOT NULL,
    "organizationId" TEXT NOT NULL,
    "roleId" TEXT NOT NULL,

    CONSTRAINT "UserOrganizationRoleAccessLink_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "OrganizationUserWalletRoleAccessLink" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "revokedAt" TIMESTAMP(3),
    "organizationId" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "walletId" TEXT NOT NULL,
    "roleId" TEXT NOT NULL,

    CONSTRAINT "OrganizationUserWalletRoleAccessLink_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "OrganizationWalletRoleAccessLink" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "lastUsed" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "revokedAt" TIMESTAMP(3),
    "walletId" TEXT NOT NULL,
    "organizationId" TEXT NOT NULL,
    "roleId" TEXT NOT NULL,

    CONSTRAINT "OrganizationWalletRoleAccessLink_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "UserWalletRoleAccessLink" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "lastUsed" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "revokedAt" TIMESTAMP(3),
    "walletId" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "roleId" TEXT NOT NULL,

    CONSTRAINT "UserWalletRoleAccessLink_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Role_id_key" ON "Role"("id");

-- CreateIndex
CREATE INDEX "Role_id_idx" ON "Role"("id");

-- CreateIndex
CREATE UNIQUE INDEX "UserPermissionLink_id_key" ON "UserPermissionLink"("id");

-- CreateIndex
CREATE INDEX "UserPermissionLink_userId_idx" ON "UserPermissionLink"("userId");

-- CreateIndex
CREATE INDEX "UserPermissionLink_permissionId_idx" ON "UserPermissionLink"("permissionId");

-- CreateIndex
CREATE UNIQUE INDEX "UserPermissionLink_userId_permissionId_key" ON "UserPermissionLink"("userId", "permissionId");

-- CreateIndex
CREATE UNIQUE INDEX "PermissionRoleLink_id_key" ON "PermissionRoleLink"("id");

-- CreateIndex
CREATE INDEX "PermissionRoleLink_roleId_idx" ON "PermissionRoleLink"("roleId");

-- CreateIndex
CREATE INDEX "PermissionRoleLink_permissionId_idx" ON "PermissionRoleLink"("permissionId");

-- CreateIndex
CREATE UNIQUE INDEX "PermissionRoleLink_roleId_permissionId_key" ON "PermissionRoleLink"("roleId", "permissionId");

-- CreateIndex
CREATE INDEX "Organization_id_idx" ON "Organization"("id");

-- CreateIndex
CREATE INDEX "OrganizationRoleLink_organizationId_idx" ON "OrganizationRoleLink"("organizationId");

-- CreateIndex
CREATE INDEX "OrganizationRoleLink_roleId_idx" ON "OrganizationRoleLink"("roleId");

-- CreateIndex
CREATE UNIQUE INDEX "OrganizationRoleLink_organizationId_roleId_key" ON "OrganizationRoleLink"("organizationId", "roleId");

-- CreateIndex
CREATE INDEX "UserOrganizationRoleAccessLink_organizationId_idx" ON "UserOrganizationRoleAccessLink"("organizationId");

-- CreateIndex
CREATE INDEX "UserOrganizationRoleAccessLink_userId_idx" ON "UserOrganizationRoleAccessLink"("userId");

-- CreateIndex
CREATE UNIQUE INDEX "UserOrganizationRoleAccessLink_userId_organizationId_key" ON "UserOrganizationRoleAccessLink"("userId", "organizationId");

-- CreateIndex
CREATE INDEX "OrganizationUserWalletRoleAccessLink_organizationId_idx" ON "OrganizationUserWalletRoleAccessLink"("organizationId");

-- CreateIndex
CREATE INDEX "OrganizationUserWalletRoleAccessLink_userId_idx" ON "OrganizationUserWalletRoleAccessLink"("userId");

-- CreateIndex
CREATE INDEX "OrganizationUserWalletRoleAccessLink_walletId_idx" ON "OrganizationUserWalletRoleAccessLink"("walletId");

-- CreateIndex
CREATE INDEX "OrganizationUserWalletRoleAccessLink_roleId_idx" ON "OrganizationUserWalletRoleAccessLink"("roleId");

-- CreateIndex
CREATE UNIQUE INDEX "OrganizationUserWalletRoleAccessLink_organizationId_userId__key" ON "OrganizationUserWalletRoleAccessLink"("organizationId", "userId", "walletId", "roleId");

-- CreateIndex
CREATE INDEX "OrganizationWalletRoleAccessLink_organizationId_idx" ON "OrganizationWalletRoleAccessLink"("organizationId");

-- CreateIndex
CREATE INDEX "OrganizationWalletRoleAccessLink_walletId_idx" ON "OrganizationWalletRoleAccessLink"("walletId");

-- CreateIndex
CREATE UNIQUE INDEX "OrganizationWalletRoleAccessLink_walletId_organizationId_key" ON "OrganizationWalletRoleAccessLink"("walletId", "organizationId");

-- CreateIndex
CREATE UNIQUE INDEX "UserWalletRoleAccessLink_id_key" ON "UserWalletRoleAccessLink"("id");

-- CreateIndex
CREATE INDEX "UserWalletRoleAccessLink_walletId_idx" ON "UserWalletRoleAccessLink"("walletId");

-- CreateIndex
CREATE INDEX "UserWalletRoleAccessLink_userId_idx" ON "UserWalletRoleAccessLink"("userId");

-- CreateIndex
CREATE UNIQUE INDEX "UserWalletRoleAccessLink_walletId_userId_key" ON "UserWalletRoleAccessLink"("walletId", "userId");

-- CreateIndex
CREATE UNIQUE INDEX "CompletedTransactionLink_id_key" ON "CompletedTransactionLink"("id");

-- CreateIndex
CREATE INDEX "Permission_id_idx" ON "Permission"("id");

-- CreateIndex
CREATE UNIQUE INDEX "Transaction_id_key" ON "Transaction"("id");

-- CreateIndex
CREATE UNIQUE INDEX "TransactionIntent_paymentGatewayIdentifier_key" ON "TransactionIntent"("paymentGatewayIdentifier");

-- CreateIndex
CREATE INDEX "User_id_idx" ON "User"("id");

-- AddForeignKey
ALTER TABLE "UserPermissionLink" ADD CONSTRAINT "UserPermissionLink_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "UserPermissionLink" ADD CONSTRAINT "UserPermissionLink_permissionId_fkey" FOREIGN KEY ("permissionId") REFERENCES "Permission"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PermissionRoleLink" ADD CONSTRAINT "PermissionRoleLink_roleId_fkey" FOREIGN KEY ("roleId") REFERENCES "Role"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PermissionRoleLink" ADD CONSTRAINT "PermissionRoleLink_permissionId_fkey" FOREIGN KEY ("permissionId") REFERENCES "Permission"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Session" ADD CONSTRAINT "Session_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Key" ADD CONSTRAINT "Key_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrganizationRoleLink" ADD CONSTRAINT "OrganizationRoleLink_roleId_fkey" FOREIGN KEY ("roleId") REFERENCES "Role"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrganizationRoleLink" ADD CONSTRAINT "OrganizationRoleLink_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES "Organization"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "UserOrganizationRoleAccessLink" ADD CONSTRAINT "UserOrganizationRoleAccessLink_roleId_fkey" FOREIGN KEY ("roleId") REFERENCES "Role"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "UserOrganizationRoleAccessLink" ADD CONSTRAINT "UserOrganizationRoleAccessLink_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "UserOrganizationRoleAccessLink" ADD CONSTRAINT "UserOrganizationRoleAccessLink_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES "Organization"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrganizationUserWalletRoleAccessLink" ADD CONSTRAINT "OrganizationUserWalletRoleAccessLink_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES "Organization"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrganizationUserWalletRoleAccessLink" ADD CONSTRAINT "OrganizationUserWalletRoleAccessLink_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrganizationUserWalletRoleAccessLink" ADD CONSTRAINT "OrganizationUserWalletRoleAccessLink_walletId_fkey" FOREIGN KEY ("walletId") REFERENCES "Wallet"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrganizationUserWalletRoleAccessLink" ADD CONSTRAINT "OrganizationUserWalletRoleAccessLink_roleId_fkey" FOREIGN KEY ("roleId") REFERENCES "Role"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrganizationWalletRoleAccessLink" ADD CONSTRAINT "OrganizationWalletRoleAccessLink_walletId_fkey" FOREIGN KEY ("walletId") REFERENCES "Wallet"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrganizationWalletRoleAccessLink" ADD CONSTRAINT "OrganizationWalletRoleAccessLink_organizationId_fkey" FOREIGN KEY ("organizationId") REFERENCES "Organization"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrganizationWalletRoleAccessLink" ADD CONSTRAINT "OrganizationWalletRoleAccessLink_roleId_fkey" FOREIGN KEY ("roleId") REFERENCES "Role"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "UserWalletRoleAccessLink" ADD CONSTRAINT "UserWalletRoleAccessLink_walletId_fkey" FOREIGN KEY ("walletId") REFERENCES "Wallet"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "UserWalletRoleAccessLink" ADD CONSTRAINT "UserWalletRoleAccessLink_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "UserWalletRoleAccessLink" ADD CONSTRAINT "UserWalletRoleAccessLink_roleId_fkey" FOREIGN KEY ("roleId") REFERENCES "Role"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Transaction" ADD CONSTRAINT "Transaction_sourceWalletId_fkey" FOREIGN KEY ("sourceWalletId") REFERENCES "Wallet"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Transaction" ADD CONSTRAINT "Transaction_targetWalletId_fkey" FOREIGN KEY ("targetWalletId") REFERENCES "Wallet"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CompletedTransactionLink" ADD CONSTRAINT "CompletedTransactionLink_transactionIntentId_fkey" FOREIGN KEY ("transactionIntentId") REFERENCES "TransactionIntent"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CompletedTransactionLink" ADD CONSTRAINT "CompletedTransactionLink_transactionId_fkey" FOREIGN KEY ("transactionId") REFERENCES "Transaction"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CompletedTransactionLink" ADD CONSTRAINT "CompletedTransactionLink_invoiceId_fkey" FOREIGN KEY ("invoiceId") REFERENCES "Invoice"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TransactionIntent" ADD CONSTRAINT "TransactionIntent_invoiceId_fkey" FOREIGN KEY ("invoiceId") REFERENCES "Invoice"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TransactionIntent" ADD CONSTRAINT "TransactionIntent_sourceWalletId_fkey" FOREIGN KEY ("sourceWalletId") REFERENCES "Wallet"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TransactionIntent" ADD CONSTRAINT "TransactionIntent_paymentGatewayId_fkey" FOREIGN KEY ("paymentGatewayId") REFERENCES "PaymentGateway"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TransactionIntent" ADD CONSTRAINT "TransactionIntent_requestingUserId_fkey" FOREIGN KEY ("requestingUserId") REFERENCES "User"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Invoice" ADD CONSTRAINT "Invoice_targetWalletId_fkey" FOREIGN KEY ("targetWalletId") REFERENCES "Wallet"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PaymentGateway" ADD CONSTRAINT "PaymentGateway_sourceWalletId_fkey" FOREIGN KEY ("sourceWalletId") REFERENCES "Wallet"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PaymentGatewaySettingLink" ADD CONSTRAINT "PaymentGatewaySettingLink_paymentGatewayId_fkey" FOREIGN KEY ("paymentGatewayId") REFERENCES "PaymentGateway"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PaymentGatewaySettingLink" ADD CONSTRAINT "PaymentGatewaySettingLink_settingId_fkey" FOREIGN KEY ("settingId") REFERENCES "Settings"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PaymentGatewayApiKeyLink" ADD CONSTRAINT "PaymentGatewayApiKeyLink_apiKeyId_fkey" FOREIGN KEY ("apiKeyId") REFERENCES "ApiKey"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PaymentGatewayApiKeyLink" ADD CONSTRAINT "PaymentGatewayApiKeyLink_paymentGatewayId_fkey" FOREIGN KEY ("paymentGatewayId") REFERENCES "PaymentGateway"("id") ON DELETE CASCADE ON UPDATE CASCADE;
