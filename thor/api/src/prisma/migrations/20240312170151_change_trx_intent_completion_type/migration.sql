/*
  Warnings:

  - The values [Invoice] on the enum `TransactionIntentCompletionType` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "TransactionIntentCompletionType_new" AS ENUM ('Internal', 'PaymentGateway');
ALTER TABLE "CompletedTransactionLink" ALTER COLUMN "type" TYPE "TransactionIntentCompletionType_new" USING ("type"::text::"TransactionIntentCompletionType_new");
ALTER TYPE "TransactionIntentCompletionType" RENAME TO "TransactionIntentCompletionType_old";
ALTER TYPE "TransactionIntentCompletionType_new" RENAME TO "TransactionIntentCompletionType";
DROP TYPE "TransactionIntentCompletionType_old";
COMMIT;
