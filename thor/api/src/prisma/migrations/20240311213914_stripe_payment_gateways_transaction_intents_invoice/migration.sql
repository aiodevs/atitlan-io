/*
  Warnings:

  - You are about to alter the column `sourceAmount` on the `Transaction` table. The data in that column could be lost. The data in that column will be cast from `DoublePrecision` to `Decimal(65,30)`.
  - You are about to alter the column `targetAmount` on the `Transaction` table. The data in that column could be lost. The data in that column will be cast from `DoublePrecision` to `Decimal(65,30)`.

*/
-- CreateEnum
CREATE TYPE "TransactionIntentCompletionType" AS ENUM ('Invoice', 'PaymentGateway');

-- CreateEnum
CREATE TYPE "PaymentIntegrationChannel" AS ENUM ('Stripe');

-- CreateEnum
CREATE TYPE "PaymentGatewayApiKeyType" AS ENUM ('Secret', 'Public');

-- AlterTable
ALTER TABLE "Transaction" ALTER COLUMN "sourceAmount" SET DATA TYPE DECIMAL(65,30),
ALTER COLUMN "targetAmount" SET DATA TYPE DECIMAL(65,30);

-- CreateTable
CREATE TABLE "CompletedTransactionLink" (
    "id" SERIAL NOT NULL,
    "transactionIntentId" INTEGER NOT NULL,
    "transactionId" INTEGER NOT NULL,
    "invoiceId" INTEGER NOT NULL,
    "type" "TransactionIntentCompletionType" NOT NULL,

    CONSTRAINT "CompletedTransactionLink_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "TransactionIntent" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "status" TEXT NOT NULL,
    "userWalletId" INTEGER NOT NULL,
    "invoiceId" INTEGER NOT NULL,
    "paymentGatewayId" INTEGER,

    CONSTRAINT "TransactionIntent_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Invoice" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "amount" DECIMAL(65,30) NOT NULL,
    "currency" TEXT NOT NULL,
    "targetWalletId" INTEGER NOT NULL,
    "description" TEXT,

    CONSTRAINT "Invoice_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "PaymentGateway" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "disabledAt" TIMESTAMP(3),
    "name" TEXT NOT NULL,
    "paymentIntegrationChannel" "PaymentIntegrationChannel" NOT NULL,
    "sourceWalletId" INTEGER NOT NULL,

    CONSTRAINT "PaymentGateway_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "PaymentGatewaySettingLink" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "paymentGatewayId" INTEGER NOT NULL,
    "settingId" INTEGER NOT NULL,

    CONSTRAINT "PaymentGatewaySettingLink_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Settings" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "name" TEXT NOT NULL,
    "value" TEXT NOT NULL,
    "encrypted" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "Settings_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "PaymentGatewayApiKeyLink" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "apiKeyId" INTEGER NOT NULL,
    "paymentGatewayId" INTEGER NOT NULL,
    "type" "PaymentGatewayApiKeyType" NOT NULL,

    CONSTRAINT "PaymentGatewayApiKeyLink_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "ApiKey" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "name" TEXT NOT NULL,
    "encryptedKey" TEXT NOT NULL,

    CONSTRAINT "ApiKey_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "CompletedTransactionLink_transactionId_key" ON "CompletedTransactionLink"("transactionId");

-- CreateIndex
CREATE UNIQUE INDEX "CompletedTransactionLink_transactionIntentId_transactionId__key" ON "CompletedTransactionLink"("transactionIntentId", "transactionId", "type");

-- CreateIndex
CREATE UNIQUE INDEX "TransactionIntent_id_key" ON "TransactionIntent"("id");

-- CreateIndex
CREATE INDEX "TransactionIntent_invoiceId_paymentGatewayId_idx" ON "TransactionIntent"("invoiceId", "paymentGatewayId");

-- CreateIndex
CREATE UNIQUE INDEX "Invoice_id_key" ON "Invoice"("id");

-- CreateIndex
CREATE UNIQUE INDEX "PaymentGateway_id_key" ON "PaymentGateway"("id");

-- CreateIndex
CREATE UNIQUE INDEX "PaymentGateway_sourceWalletId_key" ON "PaymentGateway"("sourceWalletId");

-- CreateIndex
CREATE INDEX "PaymentGateway_id_idx" ON "PaymentGateway"("id");

-- CreateIndex
CREATE UNIQUE INDEX "PaymentGatewaySettingLink_id_key" ON "PaymentGatewaySettingLink"("id");

-- CreateIndex
CREATE UNIQUE INDEX "PaymentGatewaySettingLink_settingId_key" ON "PaymentGatewaySettingLink"("settingId");

-- CreateIndex
CREATE UNIQUE INDEX "Settings_id_key" ON "Settings"("id");

-- CreateIndex
CREATE UNIQUE INDEX "PaymentGatewayApiKeyLink_id_key" ON "PaymentGatewayApiKeyLink"("id");

-- CreateIndex
CREATE UNIQUE INDEX "PaymentGatewayApiKeyLink_apiKeyId_key" ON "PaymentGatewayApiKeyLink"("apiKeyId");

-- CreateIndex
CREATE UNIQUE INDEX "PaymentGatewayApiKeyLink_apiKeyId_paymentGatewayId_key" ON "PaymentGatewayApiKeyLink"("apiKeyId", "paymentGatewayId");

-- CreateIndex
CREATE UNIQUE INDEX "ApiKey_id_key" ON "ApiKey"("id");

-- CreateIndex
CREATE UNIQUE INDEX "ApiKey_encryptedKey_key" ON "ApiKey"("encryptedKey");

-- CreateIndex
CREATE INDEX "ApiKey_encryptedKey_idx" ON "ApiKey"("encryptedKey");

-- CreateIndex
CREATE INDEX "Transaction_sourceWalletId_targetWalletId_idx" ON "Transaction"("sourceWalletId", "targetWalletId");

-- CreateIndex
CREATE INDEX "Transaction_transactionTime_idx" ON "Transaction"("transactionTime");

-- AddForeignKey
ALTER TABLE "CompletedTransactionLink" ADD CONSTRAINT "CompletedTransactionLink_transactionIntentId_fkey" FOREIGN KEY ("transactionIntentId") REFERENCES "TransactionIntent"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CompletedTransactionLink" ADD CONSTRAINT "CompletedTransactionLink_transactionId_fkey" FOREIGN KEY ("transactionId") REFERENCES "Transaction"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CompletedTransactionLink" ADD CONSTRAINT "CompletedTransactionLink_invoiceId_fkey" FOREIGN KEY ("invoiceId") REFERENCES "Invoice"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TransactionIntent" ADD CONSTRAINT "TransactionIntent_invoiceId_fkey" FOREIGN KEY ("invoiceId") REFERENCES "Invoice"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TransactionIntent" ADD CONSTRAINT "TransactionIntent_paymentGatewayId_fkey" FOREIGN KEY ("paymentGatewayId") REFERENCES "PaymentGateway"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PaymentGateway" ADD CONSTRAINT "PaymentGateway_sourceWalletId_fkey" FOREIGN KEY ("sourceWalletId") REFERENCES "Wallet"("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PaymentGatewaySettingLink" ADD CONSTRAINT "PaymentGatewaySettingLink_paymentGatewayId_fkey" FOREIGN KEY ("paymentGatewayId") REFERENCES "PaymentGateway"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PaymentGatewaySettingLink" ADD CONSTRAINT "PaymentGatewaySettingLink_settingId_fkey" FOREIGN KEY ("settingId") REFERENCES "Settings"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PaymentGatewayApiKeyLink" ADD CONSTRAINT "PaymentGatewayApiKeyLink_apiKeyId_fkey" FOREIGN KEY ("apiKeyId") REFERENCES "ApiKey"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PaymentGatewayApiKeyLink" ADD CONSTRAINT "PaymentGatewayApiKeyLink_paymentGatewayId_fkey" FOREIGN KEY ("paymentGatewayId") REFERENCES "PaymentGateway"("id") ON DELETE CASCADE ON UPDATE CASCADE;
