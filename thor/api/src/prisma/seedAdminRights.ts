
import { createInitialUserPermissions } from "@/components/heimdall/authorization/authorization.utility";
import PermissionService from "@/components/heimdall/authorization/permission/permission.service";
import UserService from "@/components/heimdall/user/user.service";
import { Permission, PrismaClient } from '@prisma/client';
import { User } from "@prisma/client";

const prisma = new PrismaClient();
const userService = new UserService();
const permisionService = new PermissionService();

export type AdminUser = {
  user: {
    id: string;
    email: string;
  },
  permissions: Permission[];
}

export async function seedAdminUser(email: string, password: string)
  : Promise<AdminUser> {

  let existingAdmin: User | null = await prisma.user.findUnique({
    where: { email },
  });

  if (!existingAdmin) {
    existingAdmin = await userService.createUser({
      email,
      password
    });
    console.log('Super admin user created: ', email);
  } else {
    console.log('Super admin user already exists: ', email);
  }

  if (!existingAdmin) {
    throw new Error('Admin user not found after creation');
  }

  await createInitialUserPermissions(existingAdmin.id);

  const permisions = await permisionService.getUserPermissions(existingAdmin.id);

  return {
    user: {
      id: existingAdmin.id.toString(),
      email: existingAdmin.email
    },
    permissions: permisions,
  }
}
