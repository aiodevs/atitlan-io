import { seedAdminUser } from "./seedAdminRights";
import { THOR_API_INITIAL_ADMIN_EMAIL, THOR_API_INITIAL_ADMIN_PASSWORD } from "@/config";
const email = THOR_API_INITIAL_ADMIN_EMAIL;
const password = THOR_API_INITIAL_ADMIN_PASSWORD;

init();

function init() {
    if (!email || !password) {
        console.log('No initial admin user details provided, skipping seeding');
        return;
    }
    seedAdminUser(email, password)
        .catch(e => {
            console.error(e);
            process.exit(1);
        })

    // TODO seed wallet with funds
}

