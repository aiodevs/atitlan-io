import crypto from 'crypto';
import { THOR_ENCRYPTION_KEY } from '@/config';
import { hash as bcryptHash } from 'bcrypt';
import util from 'node:util';

const IV_LENGTH = 16; // For AES, this is always 16 - Must be 256 bits (32 characters)
const saltRounds = 13;
const encryptionAlgorithm = 'aes-256-cbc';

const scryptAsync = util.promisify(crypto.scrypt) as (password: string | Buffer | NodeJS.TypedArray | DataView, salt: string | Buffer | NodeJS.TypedArray | DataView, keylen: number) => Promise<Buffer>;
const randomFillAsync = util.promisify(crypto.randomFill);

export async function hash(unHashedValue: string): Promise<string> {
    return bcryptHash(unHashedValue, saltRounds);
}

export async function encrypt(value: string): Promise<string> {
    if (!THOR_ENCRYPTION_KEY) {
        console.log('THOR_ENCRYPTION_KEY is not set, skipping encryption');
        throw new Error();
    }

    try {
        const salt: Buffer = crypto.randomBytes(32);
        const key: Buffer = await getEncryptionKey(salt);
        const iv: Buffer = Buffer.alloc(IV_LENGTH);
        await randomFillAsync(iv);
        const saltHex = salt.toString('hex');
        const ivHex = Buffer.from(iv).toString('hex');

        const cipher = crypto.createCipheriv(encryptionAlgorithm, key, iv);
        let encrypted = cipher.update(value, 'utf8', 'hex');
        encrypted += cipher.final('hex');

        return `${saltHex}:${ivHex}:${encrypted}`;
    } catch (err) {
        console.error('Encryption failed:', err);
        throw err;
    }
}

export async function decrypt(encryptedValue: string): Promise<string> {
    if (!THOR_ENCRYPTION_KEY) {
        console.log('THOR_ENCRYPTION_KEY is not set, skipping decryption');
        throw new Error();
    }

    try {
        const parts = encryptedValue.split(':');
        if (parts.length !== 3) throw new Error('Invalid encrypted value');

        const salt: Buffer = Buffer.from(parts[0], 'hex');
        const iv: Buffer = Buffer.from(parts[1], 'hex');
        const encryptedText: string = parts[2];

        const key: Buffer = await getEncryptionKey(salt);

        const decipher = crypto.createDecipheriv(encryptionAlgorithm, key, iv);
        let decrypted = decipher.update(encryptedText, 'hex', 'utf8');
        decrypted += decipher.final('utf8');

        return decrypted;
    } catch (err) {
        console.error('Decryption failed:', err);
        throw err;
    }
}

// Todo refactor encrypt and decrypt to receive the key as a parameter
// Wrap to thor_encrypt and thor_decrypt

export async function updateEncryptionKey(encryptedValue: string, currentKey: string, targetKey: string): Promise<string> {
    var decrypted = await decrypt(encryptedValue);
    return await encrypt(decrypted);
}
// export async function changeKey(currentKey: string, targetKey: string): Promise<void> {
// target key 
// current key
// decrypt current key
// encrypt with target key
// set current from target key

async function getEncryptionKey(salt: Buffer): Promise<Buffer> {
    if (!THOR_ENCRYPTION_KEY) {
        console.log('THOR_ENCRYPTION_KEY is not set, cannot create encryption key');
        throw new Error();
    }
    return await scryptAsync(THOR_ENCRYPTION_KEY, salt, 32);

}