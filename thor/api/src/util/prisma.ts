import { PrismaClient } from "@prisma/client";

let prisma: PrismaClient | null = null;
let databaseURL: string | undefined = undefined;

export function getDatabaseURL() {
  return databaseURL;
}

export function getPrisma() {
  if (!prisma) {
    databaseURL = process.env.DATABASE_URL;
    prisma = new PrismaClient({
      datasources: {
        db: {
          url: databaseURL,
        },
      },
    });
  }
  return prisma;
}

export async function setPrismaDbUrl(dbUrl: string) {
  process.env.DATABASE_URL = dbUrl;
  databaseURL = dbUrl;
  if (prisma) {
    await prisma.$disconnect();
  }
  prisma = new PrismaClient({
    datasources: {
      db: {
        url: dbUrl,
      },
    },
  });
}

export async function connectToDb() {
  try {
    await getPrisma().$connect();
    console.log("Database connection successful - connect");
  } catch (error) {
    const url = process.env.DATABASE_URL;
    const cleanUrl = url?.split("@")[1];
    console.log(`FAILED - Attempting to connect to DB with URL: ${cleanUrl}`);
  }
}
