export const responseProperty = {
  message: {
    type: "string",
  },
};

export const ERROR400 = {
  description: "Bad request",
  type: "object",
  properties: responseProperty,
};

export const ERROR401 = {
  description: "Unauthorized",
  type: "object",
  properties: responseProperty,
};

export const ERROR403 = {
  description: "Forbidden Request",
  properties: responseProperty,
};

export const ERROR404 = {
  description: "Not found",
  properties: responseProperty,
};

export const ERROR409 = {
  description: "Conflict",
  properties: responseProperty,
};

export const ERROR500 = {
  description: "Internal Sever Error",
  properties: responseProperty,
};

export const ERROR503 = {
  description: "Service Unavailable",
  properties: responseProperty,
};

export class NotFound extends Error {
  statusCode: number;

  constructor(message = "Not found") {
    super(message);
    this.statusCode = 404;
  }
}

export class Conflict extends Error {
  statusCode: number;

  constructor(message = "Conflict") {
    super(message);
    this.statusCode = 409;
  }
}

export class Unauthorized extends Error {
  statusCode: number;

  constructor(message = "Unauthorized") {
    super(message);
    this.statusCode = 401;
  }
}

export class BadRequest extends Error {
  statusCode: number;

  constructor(message = "Bad Request") {
    super(message);
    this.statusCode = 400;
  }
}

export class Forbidden extends Error {
  statusCode: number;

  constructor(message = "Forbidden") {
    super(message);
    this.statusCode = 403;
  }
}

export class InternalServerError extends Error {
  statusCode: number;

  constructor(message = "Internal Server Error") {
    super(message);
    this.statusCode = 500;
  }
}

export class Unavailable extends Error {
  statusCode: number;

  constructor(message = "Unavailable") {
    super(message);
    this.statusCode = 503;
  }
}
