import fs from 'node:fs';
import path from 'node:path';
import { fileURLToPath } from 'node:url';

// Get the current directory name in ES module
const __dirname = path.dirname(fileURLToPath(import.meta.url));
const packageJsonPath = path.join(__dirname, '../../package.json');
const packageJson = JSON.parse(fs.readFileSync(packageJsonPath, 'utf-8'));

export function getVersion(): string {
    return packageJson.version;
}

export function stringToBool(inputBool: string): boolean {
    return inputBool === "true" || inputBool === "1";
}