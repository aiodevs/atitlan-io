import Stripe from 'stripe';

export type StripeFactory = (apiKey: string) => Stripe;

export const createStripe: StripeFactory = (apiKey: string) => new Stripe(apiKey);
