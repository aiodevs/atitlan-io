export default class AtitlanApiRoutes {
    static readonly AUTH = {
        SIGNUP: "/api/v1/auth/signup",
        LOGIN: "/api/v1/auth/login",
        LOGOUT: "/api/v1/auth/logout",
        VALID_SESSION: "/api/v1/auth/valid-session",
        CHECK_PERMISSION: "/api/v1/auth/check-permission",
        ADD_PERMISSION: "/api/v1/auth/permission/add",
        REMOVE_PERMISSION: "/api/v1/auth/permission/remove"
    };

    static readonly USER = {
        PERMISSIONS: "/api/v1/user/permissions",
        INFO: "/api/v1/user/info",
        WALLETS: (options?: { balance?: boolean }) => {
            let queryParams = new URLSearchParams();
            if (options && options.balance !== undefined) {
                queryParams.append('balance', String(options.balance));
            }
            let queryString = queryParams.toString();
            return `/api/v1/user/wallets${queryString ? `?${queryString}` : ''}`;
        },
        WALLETS_URL: `/api/v1/user/wallets`,
        WALLET_DETAILS: (id: string, options?: {
            balance?: boolean,
            transactions?: boolean,
            invoices?: boolean,
            intents?: boolean
        }) => {
            let queryParams = new URLSearchParams();
            if (options) {
                if (options.balance !== undefined) {
                    queryParams.append('balance', String(options.balance));
                }
                if (options.transactions !== undefined) {
                    queryParams.append('transactions', String(options.transactions));
                }
                if (options.invoices !== undefined) {
                    queryParams.append('invoices', String(options.invoices));
                }
                if (options.intents !== undefined) {
                    queryParams.append('intents', String(options.intents));
                }
            }
            let queryString = queryParams.toString();
            return `/api/v1/user/wallets/details/${id}${queryString ? `?${queryString}` : ''}`;
        },
        WALLET_DETAILS_URL: `/api/v1/user/wallets/details/:id`,
        GET: (id: string) => `/api/v1/user/${id}`,
        GET_URL: `/api/v1/user/:id`,
    };

    static readonly ORGANIZATION = {
        CREATE: "/api/v1/organization/create",
        UPDATE: "/api/v1/organization/update",
        REVOKE_USERS: "/api/v1/organizations/revoke-users",
        GET_USER_ORGANIZATIONS: `/api/v1/user/organizations`,
        GET_USER_ID_ORGANIZATIONS: (userId: string) => `/api/v1/user/${userId}/organizations`,
        GET_USER_ID_ORGANIZATIONS_URL: `/api/v1/user/:userid/organizations`,
        GET_DETAILS: (organizationId: string) => `/api/v1/organization/${organizationId}`,
        GET_DETAILS_URL: "/api/v1/organization/:organizationid",
        CREATE_WALLET: "/api/v1/organization/wallet/create",
        GET_WALLET_DETAILS: (organizationId: string, walletId: string, options?: { balance?: boolean, transactions?: boolean, invoices?: boolean, intents?: boolean }) => {
            let queryParams = new URLSearchParams();
            if (options) {
                if (options.balance !== undefined) {
                    queryParams.append('balance', String(options.balance));
                }
                if (options.transactions !== undefined) {
                    queryParams.append('transactions', String(options.transactions));
                }
                if (options.invoices !== undefined) {
                    queryParams.append('invoices', String(options.invoices));
                }
                if (options.intents !== undefined) {
                    queryParams.append('intents', String(options.intents));
                }
            }
            let queryString = queryParams.toString();
            return `/api/v1/organization/${organizationId}/wallet/${walletId}${queryString ? `?${queryString}` : ''}`;
        },
        GET_WALLET_DETAILS_URL: "/api/v1/organization/:organizationid/wallet/:walletid",
    };

    static readonly HEALTH = {
        SERVER: "/api/v1/health/server",
        DB: "/api/v1/health/db"
    };

    static readonly WALLET = {
        CREATE: "/api/v1/user/wallets/create",
        UPDATE: "/api/v1/user/wallets/update",
        GET_USER_WALLETS: (userId: string, options?: { balance?: boolean }) => {
            let queryParams = new URLSearchParams();
            if (options?.balance !== undefined) {
                queryParams.append('balance', String(options.balance));
            }
            let queryString = queryParams.toString();
            return `/api/v1/user/${userId}/wallets${queryString ? `?${queryString}` : ''}`;
        },
        GET_USER_WALLETS_URL: `/api/v1/user/:userid/wallets`,
        GET_USER_WALLET_DETAILS: (userId: string, walletId: string, options?: { balance?: boolean, transactions?: boolean, invoices?: boolean }) => {
            let queryParams = new URLSearchParams();
            if (options) {
                if (options.balance !== undefined) {
                    queryParams.append('balance', String(options.balance));
                }
                if (options.transactions !== undefined) {
                    queryParams.append('transactions', String(options.transactions));
                }
                if (options.invoices !== undefined) {
                    queryParams.append('invoices', String(options.invoices));
                }
            }
            let queryString = queryParams.toString();
            return `/api/v1/user/${userId}/wallets/details/${walletId}${queryString ? `?${queryString}` : ''}`;
        },
        GET_USER_WALLET_DETAILS_URL: `/api/v1/user/:userid/wallets/details/:walletid`
    };

    static readonly INVOICE = {
        GET: (id: string) => `/api/v1/invoices/${id}`,
        GET_URL: `/api/v1/invoices/:id`,
        CREATE: "/api/v1/invoices/create"
    };

    static readonly TRANSACTIONS = {
        GET: (id: string) => `/api/v1/transactions/${id}`,
        GET_URL: `/api/v1/transactions/:id`,
        CREATE_INTERNAL: "/api/v1/transactions/create-internal",
        ADD_FUNDS: "/api/v1/transactions/add-funds",
        CHECK_INTEGRITY: "/api/v1/transactions/check-integrity"
    };

    static readonly TRANSACTION_INTENT = {
        GET: (id: string) => `/api/v1/transaction-intent/${id}`,
        GETURL: `/api/v1/transaction-intent/:id`,
        CREATE: "/api/v1/transaction-intent/create",
        CREATE_ANONYMOUS: "/api/v1/transaction-intent/create-anonymous",
        UPDATE_USER_AND_WALLET: "/api/v1/transaction-intent/update-user-and-wallet"
    };

    static readonly PAYMENT_GATEWAY = {
        GET: (id: string) => `/api/v1/payment-gateway/${id}`,
        GET_URL: `/api/v1/payment-gateway/:id`,
        GET_DETAILS: (id: string) => `/api/v1/payment-gateway/details/${id}`,
        GET_DETAILS_URL: `/api/v1/payment-gateway/details/:id`,
        UPDATE: "/api/v1/payment-gateway/update",
        INTEGRATIONS: {
            STRIPE: {
                CREATE_PAYMENT_GATEWAY: "/api/v1/payment-gateway/integrations/stripe/create-payment-gateway",
                HOSTED_CHECKOUT: "/api/v1/payment-gateway/integrations/stripe/hosted-checkout",
                WEBHOOK_CHECKOUT_SESSION: "/api/v1/payment-gateway/integrations/stripe/webhook/checkout-session",
                WEBHOOK_LOGGING: "/api/v1/payment-gateway/integrations/stripe/webhook/logging",
                UPDATE_SETTINGS: "/api/v1/payment-gateway/integrations/stripe/settings/update"
            },
            RECURRENTE: {
                CREATE_PAYMENT_GATEWAY: "/api/v1/payment-gateway/integrations/recurrente/create-payment-gateway",
                HOSTED_CHECKOUT: "/api/v1/payment-gateway/integrations/recurrente/hosted-checkout",
                WEBHOOK_CHECKOUT_SESSION: "/api/v1/payment-gateway/integrations/recurrente/webhook/checkout-session",
                WEBHOOK_LOGGING: "/api/v1/payment-gateway/integrations/recurrente/webhook/logging",
                UPDATE_SETTINGS: "/api/v1/payment-gateway/integrations/recurrente/settings/update"
            }
        }
    };

    static readonly API_KEYS = {
        UPDATE: "/api/v1/api-keys/update",
        DELETE: "/api/v1/api-keys/delete"
    };
}
