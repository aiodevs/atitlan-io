import { ExtendedOrganizationDetailsSchemaType as ExtendedOrganizationDetailsSchemaType, ExtendedWalletDetailsType, ExtendedWalletGeneralInfoType, OrganizationGeneralInfoSchemaType } from '@/components/finance/finance.schema.types';
import { ERROR400, ERROR401, ERROR404, responseProperty } from '@/util/constants';
import { Type } from '@sinclair/typebox';
import { FastifySchema } from 'fastify';

const CreateOrganizationWithUserOwnerBody = Type.Object({
  name: Type.String(),
});

export const CreateOrganizationWithUserOwnerSchema: FastifySchema = {
  description: 'Create an organization with the user as the owner.',
  tags: ['organization'],
  body: CreateOrganizationWithUserOwnerBody,
  response: {
    200: {
      description: 'Successful get response',
      type: 'object',
      properties: {
        ...responseProperty,
        organization: ExtendedOrganizationDetailsSchemaType
      },
    },
    400: ERROR400,
    401: ERROR401
  },
  security: [
    {
      cookieAuth: []
    }
  ]
};

const UpdateOrganizationBody = Type.Object({
  organizationId: Type.String(),
  name: Type.Optional(Type.String()),
  addOrUpdateOrgRoleTemplates: Type.Array(Type.Object({
    role: Type.Object({
      id: Type.Optional(Type.String()),
      name: Type.String(),
      permissions: Type.Array((Type.String()))
    })
  })),
  addOrUpdateUserLinks: Type.Array(Type.Object({
    userId: Type.String(),
    roleId: Type.String(),
    revokedAt: Type.Optional(Type.String())
  })),
  addOrUpdateUserWalletLinks: Type.Array(Type.Object({
    userId: Type.String(),
    walletId: Type.String(),
    roleId: Type.String(),
    revokedAt: Type.Optional(Type.String())
  })),
  addOrUpdateWalletLinks: Type.Array(Type.Object({
    walletId: Type.String(),
    roleId: Type.String(),
    revokedAt: Type.Optional(Type.String())
  }))
});

export const UpdateOrganizationSchema: FastifySchema = {
  description: 'Update an organizations user and wallet permissions and role templates.',
  tags: ['organization'],
  body: UpdateOrganizationBody,
  response: {
    200: {
      description: 'Successful get response',
      type: 'object',
      properties: {
        ...responseProperty,
        organization: ExtendedOrganizationDetailsSchemaType
      },
    },
    400: ERROR400,
    401: ERROR401
  },
  security: [
    {
      cookieAuth: []
    }
  ]
};

export const RevokeUserFromOrganizationBody = Type.Object({
  userRevocations: Type.Array(Type.Object({
    userId: Type.String(),
    organizationId: Type.String(),
    revokedAt: Type.Optional(Type.String())
  }))
});

export const RevokeUserFromOrganizationSchema: FastifySchema = {
  description: 'Revoke a user from an organization',
  tags: ['organization'],
  body: RevokeUserFromOrganizationBody,
  response: {
    200: {
      description: 'Successful get response',
      type: 'object',
      properties: {
        ...responseProperty,
        organizations: Type.Array(ExtendedOrganizationDetailsSchemaType)
      },
    },
    400: ERROR400,
    401: ERROR401
  },
  security: [
    {
      cookieAuth: []
    }
  ]
};

export const CreateWalletOrganizationSchema: FastifySchema = {
  description: 'Create a wallet owned by an organization and managed by this authenticated user.',
  tags: ['organization', 'wallet'],
  body: Type.Object({
    organizationId: Type.String(),
    name: Type.String(),
  }),
  response: {
    200: {
      description: 'Successful get response',
      type: 'object',
      properties: {
        ...responseProperty,
        wallet: ExtendedWalletGeneralInfoType
      },
    },
    400: ERROR400,
    401: ERROR401
  },
  security: [
    {
      cookieAuth: []
    }
  ]
};

export const GetOrganizationWalletDetailsSchema: FastifySchema = {
  description: 'Get organization wallet details',
  params: Type.Object({
    organizationid: Type.String(),
    walletid: Type.String(),
  }),
  querystring: Type.Object({
    balance: Type.Optional(Type.String()),
    transactions: Type.Optional(Type.String()),
    invoices: Type.Optional(Type.String()),
    intents: Type.Optional(Type.String())
  }),
  response: {
    200: {
      description: 'Successful get response',
      type: 'object',
      properties: {
        ...responseProperty,
        wallet: ExtendedWalletDetailsType
      },
    },
    401: ERROR401,
    404: ERROR404
  },
  tags: ['organization', 'wallet'],
  security: [
    {
      cookieAuth: []
    }
  ]
};

export const GetOrganizationDetailsSchema: FastifySchema = {
  description: 'Get organization details',
  params: Type.Object({
    organizationid: Type.String(),
  }),
  response: {
    200: {
      description: 'Successful get response',
      type: 'object',
      properties: {
        ...responseProperty,
        organization: ExtendedOrganizationDetailsSchemaType
      },
    },
    401: ERROR401,
    404: ERROR404
  },
  tags: ['organization'],
  security: [
    {
      cookieAuth: []
    }
  ]
};

export const GetUserIdOrganizationsSchema: FastifySchema = {
  description: 'Get all linked organizations for a user',
  params: Type.Object({
    userid: Type.String(),
  }),
  response: {
    200: {
      description: 'Successful get response',
      type: 'object',
      properties: {
        ...responseProperty,
        organizations: Type.Array(OrganizationGeneralInfoSchemaType)
      },
    },
    401: ERROR401
  },
  tags: ['organization', `user`],
  security: [
    {
      cookieAuth: []
    }
  ]
};

export const GetUserOrganizationsSchema: FastifySchema = {
  description: 'Get all linked organizations for the authenticated user',
  response: {
    200: {
      description: 'Successful get response',
      type: 'object',
      properties: {
        ...responseProperty,
        organizations: Type.Array(OrganizationGeneralInfoSchemaType)
      },
    },
    401: ERROR401
  },
  tags: ['organization', `user`],
  security: [
    {
      cookieAuth: []
    }
  ]
};
