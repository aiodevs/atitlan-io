import { WalletGeneralInfo } from "@/components/finance/wallet/wallet.types";
import { Organization, UserOrganizationRoleAccessLink, OrganizationRoleLink, OrganizationUserWalletRoleAccessLink, User } from "@prisma/client";
import { UserOrganizationPermissions, DefaultUserOrganizationRoleNames } from "../authorization/authorization.type";
import { RoleWithPermissionsResult } from "../authorization/role/role.types";

export type OrganizationGeneralInfo = Organization;

export type OrganizationDetails = Organization & {
    authorizedUsers: UserOrganizationRoleAccessLinkWithPermission[],
    authorizedWalletUsers: OrganizationUserWalletRoleAccessLinkDetails[],
    roleTemplates: RoleTemplateWithPermissions[],
    wallets: OrgWalletWithRole[],
    defaultUserOrganizationRoles: DefaultUserOrganizationRoleResult[],
    defaultUserOrganizationPermissions: DefaultUserOrganizationPermissionResult[],
    defaultWalletRoles: RoleWithPermissionsResult[],
}

export type DefaultUserOrganizationPermissionResult = {
    name: UserOrganizationPermissions,
    permissionId: string,
}

export type DefaultUserOrganizationRoleResult = {
    name: DefaultUserOrganizationRoleNames,
    roleId: string,
}

export type UserOrganizationRoleAccessLinkWithPermission = UserOrganizationRoleAccessLink & {
    role: RoleWithPermissionsResult,
    user: User,
    organization: Organization,
}

export type RoleTemplateWithPermissions = OrganizationRoleLink & {
    role: RoleWithPermissionsResult,
}

export type OrganizationUserWalletRoleAccessLinkDetails = OrganizationUserWalletRoleAccessLink & {
    user: User,
    wallet: WalletGeneralInfo,
    role: RoleWithPermissionsResult,
    organization: Organization,
}

export type OrgWalletWithRole = WalletGeneralInfo & {
    role: RoleWithPermissionsResult,
}

