import { FastifyInstance, RouteOptions } from "fastify";
import { Routes } from "@/plugins/initializeRoutes";
import OrganizationController from "./organization.controller.js";
import { CreateOrganizationWithUserOwnerSchema, UpdateOrganizationSchema, GetOrganizationDetailsSchema, GetUserIdOrganizationsSchema, GetUserOrganizationsSchema, CreateWalletOrganizationSchema, GetOrganizationWalletDetailsSchema, RevokeUserFromOrganizationSchema } from "./organization.schema.js";
import AtitlanApiRoutes from "@/components/shared/routes";


export default class OrganizationRoutes implements Routes {
    public organizationController = new OrganizationController();

    public initializeRoutes(
        fastify: FastifyInstance,
        opts: RouteOptions,
        done: () => void
    ) {
        fastify.route({
            method: "post",
            url: AtitlanApiRoutes.ORGANIZATION.CREATE,
            schema: CreateOrganizationWithUserOwnerSchema,
            preHandler: fastify.authenticateUser,
            handler: this.organizationController.createOrganizationWithUserOwner
        });

        fastify.route({
            method: "post",
            url: AtitlanApiRoutes.ORGANIZATION.UPDATE,
            schema: UpdateOrganizationSchema,
            preHandler: fastify.authenticateUser,
            handler: this.organizationController.UpdateOrganization
        });

        fastify.route({
            method: "post",
            url: AtitlanApiRoutes.ORGANIZATION.REVOKE_USERS,
            schema: RevokeUserFromOrganizationSchema,
            preHandler: fastify.authenticateUser,
            handler: this.organizationController.RevokeUserFromOrganizaion
        });

        fastify.route({
            method: "get",
            url: AtitlanApiRoutes.ORGANIZATION.GET_DETAILS_URL,
            schema: GetOrganizationDetailsSchema,
            preHandler: fastify.authenticateUser,
            handler: this.organizationController.GetOrganizationDetails
        });

        fastify.route({
            method: "post",
            url: AtitlanApiRoutes.ORGANIZATION.CREATE_WALLET,
            schema: CreateWalletOrganizationSchema,
            preHandler: fastify.authenticateUser,
            handler: this.organizationController.CreateOrganizationWallet
        });

        fastify.route({
            method: "get",
            url: AtitlanApiRoutes.ORGANIZATION.GET_WALLET_DETAILS_URL,
            schema: GetOrganizationWalletDetailsSchema,
            preHandler: fastify.authenticateUser,
            handler: this.organizationController.GetOrganizationWalletDetails
        });

        fastify.route({
            method: "get",
            url: AtitlanApiRoutes.ORGANIZATION.GET_USER_ORGANIZATIONS,
            schema: GetUserOrganizationsSchema,
            preHandler: fastify.authenticateUser,
            handler: this.organizationController.GetUserOrganizations
        });

        fastify.route({
            method: "get",
            url: AtitlanApiRoutes.ORGANIZATION.GET_USER_ID_ORGANIZATIONS_URL,
            schema: GetUserIdOrganizationsSchema,
            preHandler: fastify.authenticateUser,
            handler: this.organizationController.GetUserIdOrganizations
        });




        done();
    }
}
