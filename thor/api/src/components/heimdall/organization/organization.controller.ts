import { Unauthorized } from "@/util/constants";
import { FastifyRequest } from "fastify";
import OrganizationService from "./organization.service";
import { OrganizationDetails } from "./organization.types";
import WalletService from "@/components/finance/wallet/wallet.service";
import { stringToBool } from "@/util/util";
import { RevokeUserFromOrganizationBody } from "./organization.schema";
import AuthorizationService from "../authorization/authorization.service";
import { UserOrganizationPermissions, Resources, ThorAdminPermissions } from "../authorization/authorization.type";

export default class OrganizationController {

    private organizationService: OrganizationService = new OrganizationService();
    private walletService: WalletService = new WalletService();
    private authorizationService: AuthorizationService = new AuthorizationService();

    public createOrganizationWithUserOwner = async (
        req: FastifyRequest<{
            Body: {
                name: string,
            }
        }>) => {
        if (!req.user) { throw new Unauthorized(); }

        const newOrg: OrganizationDetails = await this.organizationService.createOrganizationWithUserOwner(req.body.name, req.user.id);
        return {
            message: 'Organization created successfully',
            organization: newOrg
        }
    }

    public UpdateOrganization = async (
        req: FastifyRequest<{
            Body: {
                organizationId: string,
                name: string | null,
                addOrUpdateOrgRoleTemplates: {
                    role: {
                        id?: string,
                        name: string,
                        permissions: UserOrganizationPermissions[],
                    }
                }[],
                addOrUpdateUserLinks: {
                    userId: string,
                    roleId: string,
                    revokedAt?: Date,
                }[],
                addOrUpdateUserWalletLinks: {
                    userId: string,
                    walletId: string,
                    roleId: string,
                    revokedAt?: Date,
                }[],
                addOrUpdateWalletLinks: {
                    walletId: string,
                    roleId: string,
                    revokedAt?: Date,
                }[]
            }
        }>) => {
        if (!req.user) { throw new Unauthorized(); }

        const {
            organizationId,
            name,
            addOrUpdateOrgRoleTemplates,
            addOrUpdateUserLinks,
            addOrUpdateUserWalletLinks,
            addOrUpdateWalletLinks } = req.body;
        const updatedOrg = await this.organizationService.updateOrganization(
            req.user.id,
            organizationId,
            name,
            addOrUpdateOrgRoleTemplates,
            addOrUpdateUserLinks,
            addOrUpdateUserWalletLinks,
            addOrUpdateWalletLinks,
        );

        return {
            message: `Organization updated successfully`,
            organization: updatedOrg
        }
    }

    public RevokeUserFromOrganizaion = async (req: FastifyRequest<{
        Body: typeof RevokeUserFromOrganizationBody
    }>) => {
        if (!req.user) { throw new Unauthorized(); }

        const userRevocations: {
            userId: string,
            organizationId: string,
            revokedAt: Date
        }[] = req.body.userRevocations;

        await this.organizationService.revokeUsersFromOrganization(
            req.user.id,
            userRevocations
        );

        const uniqueOrgIds: string[] = userRevocations.map(revocation => revocation.organizationId)

        const updatedOrg: OrganizationDetails[] = [];
        for (const orgId of uniqueOrgIds) {
            const org = await this.organizationService.getOrganizationDetails(orgId, req.user.id);
            updatedOrg.push(org);
        }

        return {
            message: `User revoked from organization successfully`,
            organizations: updatedOrg
        }
    }

    public CreateOrganizationWallet = async (
        req: FastifyRequest<{
            Body: {
                organizationId: string,
                name: string,
            }
        }>) => {
        if (!req.user) { throw new Unauthorized(); }

        const wallet = await this.walletService.createOrganizationOwnedWallet(
            req.body.organizationId,
            req.user.id,
            req.body.name,
            false
        );
        return {
            message: 'Organization wallet created successfully',
            wallet: wallet
        }
    }

    public GetOrganizationWalletDetails = async (
        req: FastifyRequest<{
            Params: {
                organizationid: string,
                walletid: string,
            },
            Querystring: {
                balance: string,
                transactions: string,
                invoices: string,
                intents: string
            }
        }>) => {
        if (!req.user) { throw new Unauthorized(); }
        const balance: boolean = stringToBool(req.query.balance);
        const transactions: boolean = stringToBool(req.query.transactions);
        const invoices: boolean = stringToBool(req.query.invoices);
        const intents: boolean = stringToBool(req.query.intents);

        const walletDetails = await this.walletService
            .getUserOrganizationWalletDetails(
                req.params.walletid,
                req.user.id,
                req.params.organizationid,
                balance,
                transactions,
                invoices,
                intents);
        return {
            message: 'Organization wallet details retrieved successfully',
            wallet: walletDetails
        }
    }

    public GetUserOrganizations = async (
        req: FastifyRequest) => {
        if (!req.user) { throw new Unauthorized(); }

        const orgs = await this.organizationService.getUserOrganizations(req.user.id);
        return {
            message: 'User organizations retrieved successfully',
            organizations: orgs
        }
    }

    public GetUserIdOrganizations = async (
        req: FastifyRequest<{
            Params: {
                userid: string,
            }
        }>) => {
        if (!req.user) { throw new Unauthorized(); }

        const hasAdminOrgAccess = await this.authorizationService.checkUserPermission(req.user.id, Resources.ThorAdmin, ThorAdminPermissions.SeeAllOrganizations);
        console.log(`User ${req.user.id} has admin permission: ${hasAdminOrgAccess}`)
        if (!hasAdminOrgAccess) {
            console.log(`User ${req.user.id} does not have admin permission to see any organizations`);
            throw new Unauthorized();
        }
        const userId = req.params.userid;
        console.log(userId);
        const orgs = await this.organizationService.getUserOrganizations(req.params.userid);
        console.log(orgs);
        return {
            message: 'User organizations retrieved successfully',
            organizations: orgs
        }
    }

    public GetOrganizationDetails = async (
        req: FastifyRequest<{
            Params: {
                organizationid: string,
            }
            // Querystring: { balance: string, transactions: string, invoices: string, intents: string }
        }>) => {
        if (!req.user) { throw new Unauthorized(); }

        const organizationDetails: OrganizationDetails = await this
            .organizationService.getOrganizationDetails(req.params.organizationid, req.user.id);

        return {
            message: 'Organization details retrieved successfully',
            organization: organizationDetails
        };
    }
}