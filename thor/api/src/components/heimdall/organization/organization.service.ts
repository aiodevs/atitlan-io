import { getPrisma } from "@/util/prisma";
import RoleService from "../authorization/role/role.service";
import { BadRequest, Unauthorized } from "@/util/constants";
import WalletService from "@/components/finance/wallet/wallet.service";
import { WalletGeneralInfo } from "@/components/finance/wallet/wallet.types";
import { PaymentGateway } from "@prisma/client";
import { OrganizationGeneralInfo, OrganizationDetails, OrganizationUserWalletRoleAccessLinkDetails, OrgWalletWithRole, DefaultUserOrganizationRoleResult, DefaultUserOrganizationPermissionResult, RoleTemplateWithPermissions, UserOrganizationRoleAccessLinkWithPermission } from "./organization.types";
import AuthorizationService from "../authorization/authorization.service";
import PermissionService from "../authorization/permission/permission.service";
import { DefaultUserOrganizationRoleNames, UserOrganizationPermissions, userOrganizationRoles, Resources } from "../authorization/authorization.type";
import { convertRoleToResult, RoleWithPermissions, RoleWithPermissionsResult } from "../authorization/role/role.types";

export default class OrganizationService {
    private walletService: WalletService = new WalletService();
    private authorizationService: AuthorizationService = new AuthorizationService();
    private roleService: RoleService = new RoleService();
    private permissionService: PermissionService = new PermissionService();

    public async createOrganizationWithUserOwner(name: string, ownerUserId: string)
        : Promise<OrganizationDetails> {
        const ownerRole = await this.authorizationService.getOrCreateDefaultOrganizationRole(
            DefaultUserOrganizationRoleNames.Owner
        );
        const organization = await getPrisma().organization.create({
            data: {
                name: name,
                authorizedUsers: {
                    create: {
                        userId: ownerUserId,
                        roleId: ownerRole.id,
                    }
                }
            },
        });
        console.log(`Thor API - Organization created ${organization.id} with owner user id ${ownerUserId}`);
        return this.getOrganizationDetails(organization.id, ownerUserId);
    }

    public async getUserOrganizations(userId: string): Promise<OrganizationGeneralInfo[]> {
        const organizations = await getPrisma().organization.findMany({
            where: {
                authorizedUsers: {
                    some: {
                        userId: userId,
                    }
                }
            }
        });
        console.log(`Thor API - User ${userId} has ${organizations.length} organizations`)
        let authorizedOrganizations: OrganizationGeneralInfo[] = [];
        for (const org of organizations) {
            const hasPermission = await this.authorizationService.userHasOrganizationPermission(userId, org.id, UserOrganizationPermissions.Read);
            console.log(`Thor API - User ${userId} has permission to read organization ${org.id}: ${hasPermission ? "Yes" : "No"}`)
            if (hasPermission) {
                authorizedOrganizations.push(org);
            }
        };
        return authorizedOrganizations;
    }

    public async getOrganizationDetails(organizationId: string, requestUserId: string): Promise<OrganizationDetails> {
        const hasOrganizationPermission: boolean = await this.authorizationService.userHasOrganizationPermission(
            requestUserId,
            organizationId,
            UserOrganizationPermissions.Read
        );
        if (!hasOrganizationPermission) {
            console.log(`Thor API - User ${requestUserId} does not have permission to read organization ${organizationId}`);
            throw new Unauthorized();
        }
        const hasWalletPermission: boolean = await this.authorizationService.userHasOrganizationPermission(
            requestUserId,
            organizationId,
            UserOrganizationPermissions.WalletsRead
        );
        const hasFinancePermission: boolean = await this.authorizationService.userHasOrganizationPermission(
            requestUserId,
            organizationId,
            UserOrganizationPermissions.Finance
        );
        const hasConnectUserToOrgPermission: boolean = await this.authorizationService.userHasOrganizationPermission(
            requestUserId,
            organizationId,
            UserOrganizationPermissions.ConnectUserToOrg
        );
        const organization = await getPrisma().organization.findUnique({
            where: {
                id: organizationId,
            },
            include: {
                authorizedUsers: {
                    include: {
                        user: true,
                        organization: true,
                        role: {
                            include: {
                                permissions: {
                                    include: {
                                        permission: true
                                    }
                                },
                            }
                        },
                    }
                },
                wallets: {
                    include: {
                        wallet: {
                            include: {
                                paymentGateway: true,
                            }
                        },
                        role: {
                            include: {
                                permissions: {
                                    include: {
                                        permission: true
                                    }
                                }
                            }
                        }
                    }
                },
                authorizedWalletUsers: {
                    include: {
                        user: true,
                        organization: true,
                        wallet: {
                            include: {
                                paymentGateway: true,
                            }
                        },
                        role: {
                            include: {
                                permissions: {
                                    include: {
                                        permission: true
                                    }

                                },
                            }
                        },
                    }
                },
                roleTemplates: {
                    include: {
                        role: {
                            include: {
                                permissions: {
                                    include: {
                                        permission: true
                                    }
                                },
                            }
                        }
                    }
                },
            }
        });
        console.log(`Thor API - Organization found ${organizationId}`);
        if (!organization) {
            console.log(`Thor API - Organization not found ${organizationId}`);
            throw new BadRequest();
        }

        let walletInfo: OrgWalletWithRole[] = [];
        if (hasWalletPermission) {
            walletInfo = organization.wallets.map((link) => {
                return {
                    ...link.wallet,
                    role: convertRoleToResult(link.role),
                }
            });
        } else { // Basic read auth, only autherized wallets
            const validWallets = await this.walletService.getUserOrganizationWallets(requestUserId, organizationId);
            walletInfo = organization.wallets
                .filter((link) => validWallets // all the wallets the user has access to with the remappedRole
                    .filter((w) => w.id === link.walletId).length > 0)
                .map((link) => {
                    return {
                        ...link.wallet,
                        role: convertRoleToResult(link.role),
                    }
                });
        }
        let authOrgWalletUsers: OrganizationUserWalletRoleAccessLinkDetails[] = [];
        authOrgWalletUsers = organization.authorizedWalletUsers
            .filter(l => hasWalletPermission || l.userId === requestUserId)
            .map((link) => {
                const paymentGateway: PaymentGateway | undefined = !!link.wallet.paymentGateway
                    ? link.wallet.paymentGateway
                    : undefined;
                const wallet: WalletGeneralInfo = {
                    id: link.wallet.id,
                    createdAt: link.wallet.createdAt,
                    updatedAt: link.wallet.updatedAt,
                    lastUsed: link.wallet.lastUsed,
                    name: link.wallet.name,
                    paymentGateway: paymentGateway,
                };

                return {
                    id: link.id,
                    createdAt: link.createdAt,
                    updatedAt: link.updatedAt,
                    organizationId: link.organizationId,
                    revokedAt: link.revokedAt,
                    userId: link.userId,
                    roleId: link.roleId,
                    walletId: link.walletId,
                    wallet: wallet,
                    role: convertRoleToResult(link.role),
                    user: link.user,
                    organization: link.organization,
                }
            });

        const defaultUserOrganizationRoles = await this.authorizationService.getDefaultUserOrganizationRoles();
        const defaultUserOrganizationPermissions = await this.authorizationService.getDefaultUserOrganizationPermissions();
        const defaultWalletRoles: RoleWithPermissionsResult[] = await this.authorizationService.getDefaultWalletRoles();
        let convertedAuthUsers: UserOrganizationRoleAccessLinkWithPermission[] = []
        let convertedRoleTemplates: RoleTemplateWithPermissions[] = [];
        convertedAuthUsers = organization.authorizedUsers
            .filter(l => hasConnectUserToOrgPermission || l.userId === requestUserId)
            .map((link) => {
                return {
                    ...link,
                    role: convertRoleToResult(link.role),
                }
            });
        convertedRoleTemplates = organization.roleTemplates
            .filter(l => hasConnectUserToOrgPermission)
            .map((link) => {
                return {
                    ...link,
                    role: convertRoleToResult(link.role),
                }
            });

        const result: OrganizationDetails = {
            id: organization.id,
            createdAt: organization.createdAt,
            updatedAt: organization.updatedAt,
            name: organization.name,
            authorizedUsers: convertedAuthUsers,
            authorizedWalletUsers: authOrgWalletUsers,
            roleTemplates: convertedRoleTemplates,
            wallets: walletInfo,
            defaultUserOrganizationRoles: hasConnectUserToOrgPermission ? defaultUserOrganizationRoles : [],
            defaultUserOrganizationPermissions: hasConnectUserToOrgPermission ? defaultUserOrganizationPermissions : [],
            defaultWalletRoles: hasWalletPermission ? defaultWalletRoles : [],
        };
        console.log(`Thor API - Organization details found ${organizationId} `);
        return result;
    }

    public async updateOrganization(
        requestUserId: string,
        organizationId: string,
        name: string | null,
        addOrUpdateOrgRoleTemplates: {
            role: {
                id?: string,
                name: string,
                permissions: UserOrganizationPermissions[],
            }
        }[],
        addOrUpdateUserLinks: {
            userId: string,
            roleId: string,
            revokedAt?: Date,
        }[],
        addOrUpdateUserWalletLinks: {
            userId: string,
            walletId: string,
            roleId: string,
            revokedAt?: Date,
        }[],
        addOrUpdateWalletLinks: {
            walletId: string,
            roleId: string,
            revokedAt?: Date,
        }[],
    ): Promise<OrganizationDetails> {

        if (!!name && name.length > 0) {
            const hasUpdateProfilePermission = await this.authorizationService.userHasOrganizationPermission(
                requestUserId,
                organizationId,
                UserOrganizationPermissions.UpdateProfile
            );
            if (!hasUpdateProfilePermission) {
                console.log(`Thor API - updateOrganization - User ${requestUserId} not authorized to update organization ${organizationId} `);
                throw new Unauthorized();
            }
            await this.updateName(organizationId, name);
        }

        await this.updateOrganizationTemplates(organizationId, addOrUpdateOrgRoleTemplates);

        await this.connectAndUpdateUsersToOrganizationRoles(
            requestUserId,
            organizationId,
            addOrUpdateUserLinks);

        const remappedAddOrUpdateUserWalletLinks: {
            userId: string,
            organizationId: string,
            walletId: string,
            roleId: string,
            revokedAt?: Date,
        }[] = addOrUpdateUserWalletLinks.map((link) => {
            return {
                userId: link.userId,
                organizationId: organizationId,
                walletId: link.walletId,
                roleId: link.roleId,
                revokedAt: link.revokedAt,
            }
        });
        await this.walletService.connectAndUpdateUserOrganizationWalletLink(
            requestUserId,
            remappedAddOrUpdateUserWalletLinks
        );

        const remappedAddOrUpdateWalletLink: {
            organizationId: string,
            walletId: string,
            roleId: string,
            revokedAt?: Date,
        }[] = addOrUpdateWalletLinks.map((link) => {
            return {
                organizationId: organizationId,
                walletId: link.walletId,
                roleId: link.roleId,
                revokedAt: link.revokedAt,
            }
        });
        await this.walletService.connectAndUpdateOrganizationWalletLinks(
            requestUserId,
            remappedAddOrUpdateWalletLink);

        return await this.getOrganizationDetails(organizationId, requestUserId);
    }

    private async updateName(
        organizationId: string,
        name: string,
    ): Promise<void> {
        await getPrisma().organization.update({
            where: {
                id: organizationId,
            },
            data: {
                name: name,
            }
        });
        console.log(`Thor API - updateName - Organization name updated to ${name} orgid: ${organizationId} `);
    }

    private async updateOrganizationTemplates(
        organizationId: string,
        newRoleTemplates: {
            role: {
                id?: string,
                name: string,
                permissions: UserOrganizationPermissions[],
            }
        }[],
    ): Promise<void> {
        for (const newRoleTemplate of newRoleTemplates) {
            const remappedPermissions: {
                resource: string,
                action: string,
            }[] = newRoleTemplate.role.permissions.map((permissionName) => {
                return {
                    resource: Resources.Organization,
                    action: permissionName,
                }
            });

            let hasExistingRole: boolean = false;
            if (!!newRoleTemplate.role.id) {
                const existingRole = await this.roleService.getRole(newRoleTemplate.role.id);
                if (existingRole) {
                    hasExistingRole = true;
                }
            }

            const permissionsCreated = await this.permissionService.getOrCreatePermissions(remappedPermissions);
            const permissionIds = permissionsCreated.map((perm) => perm.id);
            let role: RoleWithPermissions;
            if (hasExistingRole && newRoleTemplate.role.id) {
                role = await this.roleService.updateRole(newRoleTemplate.role.id, newRoleTemplate.role.name, permissionIds, false, false);
            } else {
                role = await this.roleService.createRole(newRoleTemplate.role.name, permissionIds, false);
            }

            const existingRoleTemplateLink = await getPrisma().organizationRoleLink.findFirst({
                where: {
                    organizationId: organizationId,
                    roleId: role.id,
                }
            });
            if (existingRoleTemplateLink) {
                console.log(`Thor API - updateOrganizationTemplates - Role template ${role.id} already connected to organization ${organizationId}`);
                continue;
            }
            await getPrisma().organizationRoleLink.create({
                data: {
                    organizationId: organizationId,
                    roleId: role.id,
                }
            });
            console.log(`Thor API - updateOrganizationTemplates - Role template ${role.id} created for organization ${organizationId}`);
        }
    }

    private async connectAndUpdateUsersToOrganizationRoles(
        requestUserId: string,
        organizationId: string,
        newLinks: {
            userId: string,
            roleId: string | null,
            revokedAt?: Date,
        }[],
    ): Promise<void> {
        if (newLinks.length === 0) return;
        const organization = await getPrisma().organization.findUnique({
            where: {
                id: organizationId,
            },
        });

        if (!organization) {
            console.log(`Thor API - connectAndUpdateUsersToOrganizationRoles - Organization not found ${organizationId} `)
            throw new BadRequest();
        }

        const hasOrganizationPermission = await this.authorizationService.userHasOrganizationPermission(
            requestUserId,
            organizationId,
            UserOrganizationPermissions.ConnectUserToOrg
        );

        if (!hasOrganizationPermission) {
            console.log(`Thor API - connectAndUpdateUsersToOrganizationRoles - User ${requestUserId} not authorized to update organization ${organizationId} `);
            throw new Unauthorized();
        }

        for (const newLink of newLinks) {
            const existingLink = await getPrisma().userOrganizationRoleAccessLink.findFirst({
                where: {
                    userId: newLink.userId,
                    organizationId: organizationId,
                }
            });
            if (existingLink) {
                // Do we want to allow setting it so that the org has no owner?
                await getPrisma().userOrganizationRoleAccessLink.update({
                    where: {
                        id: existingLink.id,
                    },
                    data: {
                        revokedAt: newLink.revokedAt,
                        roleId: newLink.roleId ? newLink.roleId : existingLink.roleId,
                    }
                });
                if (newLink.revokedAt) {
                    console.log(`Thor API - connectAndUpdateUsersToOrganizationRoles - User ${newLink.userId} revokedAt ${newLink.revokedAt} from organization ${organizationId} `);
                } else {
                    console.log(`Thor API - connectAndUpdateUsersToOrganizationRoles - User ${newLink.userId} updated to organization ${organizationId} `);
                }
                continue;
            }

            if (!newLink.roleId) {
                console.log(`Thor API - connectAndUpdateUsersToOrganizationRoles - User ${newLink.userId} does not have a role to connect to organization ${organizationId} `);
                continue;
            }

            await getPrisma().userOrganizationRoleAccessLink.create({
                data: {
                    organizationId: organizationId,
                    userId: newLink.userId,
                    roleId: newLink.roleId,
                    revokedAt: newLink.revokedAt,
                }
            });
            if (newLink.revokedAt) {
                console.log(`Thor API - connectAndUpdateUsersToOrganizationRoles - User ${newLink.userId} revokedAt ${newLink.revokedAt} from organization ${organizationId} `);
                console.log(`Thor API - connectAndUpdateUsersToOrganizationRoles - Also a bit strange that a new connection is revoked`)
                continue;
            }
            console.log(`Thor API - connectAndUpdateUsersToOrganizationRoles - User ${newLink.userId} connected to organization ${organizationId} `);
        }
    }

    public async revokeUsersFromOrganization(
        requestingUserId: string,
        userRevocations: {
            userId: string,
            organizationId: string,
            revokedAt: Date
        }[],
    ): Promise<void> {
        for (const userRevocation of userRevocations) {
            console.log(`Thor API - revokeUsersFromOrganization - Revoking user ${userRevocation.userId} from organization ${userRevocation.organizationId} `);
            // Revoke user access to org
            await this.connectAndUpdateUsersToOrganizationRoles(
                requestingUserId,
                userRevocation.organizationId,
                [{
                    userId: userRevocation.userId,
                    roleId: null,
                    revokedAt: userRevocation.revokedAt,
                }]
            );

            // Revoke all wallets access to org wallets
            const userOrgWallets = await this.walletService.getUserOrganizationWallets(
                userRevocation.userId,
                userRevocation.organizationId
            );
            const remappedWalletLinks: {
                organizationId: string,
                walletId: string,
                userId: string,
                roleId: string | null,
                revokedAt?: Date,
            }[] = userOrgWallets.map((link) => {
                return {
                    userId: userRevocation.userId,
                    organizationId: userRevocation.organizationId,
                    walletId: link.id,
                    roleId: null,
                    revokedAt: userRevocation.revokedAt,
                }
            });
            await this.walletService.connectAndUpdateUserOrganizationWalletLink(
                requestingUserId,
                remappedWalletLinks
            );
        }
    }

    public async organizationValid(organizationId: string): Promise<boolean> {
        const organization = await getPrisma().organization.findFirst({
            where: {
                id: organizationId,
            }
        });
        return !!organization;
    }
}
