import { encrypt } from "@/util/crypto";
import { getPrisma } from "@/util/prisma";
import { PaymentGatewaySettingLink, Settings } from "@prisma/client";

export type PaymentGatewaySetting = Settings & {
    paymentGatewayLink: PaymentGatewaySettingLink | null;
};

export default class SettingsService {

    public async createEncryptedPaymentGatewaySetting(
        name: string,
        value: string,
        paymentGatewayId: string
    ): Promise<PaymentGatewaySetting> {
        const encryptedValue = await encrypt(value);
        const setting: PaymentGatewaySetting = await getPrisma().settings.create({
            data: {
                name: name,
                value: encryptedValue,
                encrypted: true,
                paymentGatewayLink: {
                    create: {
                        paymentGatewayId: paymentGatewayId
                    }
                }
            },
            include: {
                paymentGatewayLink: true
            }
        });
        return setting;
    }

    public async updatePaymentGatewaySetting(
        paymentGatewayId: string,
        settingsName: string,
        value: string,
        encrypted: boolean,
    ): Promise<Settings | null> {
        const setting = await getPrisma().paymentGatewaySettingLink.findFirst({
            include: { setting: true },
            where: {
                paymentGatewayId: paymentGatewayId,
                setting: {
                    name: settingsName
                }
            }
        });

        if (!setting) {
            console.log('Setting not found');
            return null;
        }
        const finalValue = encrypted ? await encrypt(value) : value;
        return await getPrisma().settings.update({
            where: { id: setting.setting.id },
            data: {
                value: finalValue,
                encrypted: encrypted
            }
        });
    }
} 