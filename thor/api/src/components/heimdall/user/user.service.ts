import { CreateUser, GetUser } from '@/components/heimdall/user/user.schema.js';
import { getPrisma } from '@/util/prisma';
import { Conflict, NotFound } from '@/util/constants.js';
import { User } from '@prisma/client';
import SessionService from '@/components/heimdall/authentication/session.service.js';
import { hash } from '@/util/crypto';

export type EnrichedUser = User & {
  authSession: {
    id: string;
    userId: string;
    activeExpires: bigint;
    idleExpires: bigint;
  }[];
};

export default class UserService {

  private sessionService: SessionService = new SessionService();

  public async createUser(newUser: CreateUser): Promise<EnrichedUser> {
    const checkUserExists = await getPrisma().user.findUnique({
      where: {
        email: newUser.email
      }
    });

    if (checkUserExists) {
      throw new Conflict('User already exists');
    }

    const hashedPassword = await hash(newUser.password);
    const now = Date.now();
    const activeExpires = this.sessionService.activeExpiration(now);
    const idleExpires = this.sessionService.idleExpiration(now);

    const user = await getPrisma().user.create({
      data: {
        email: newUser.email,
        key: {
          create: {
            hashedPassword: hashedPassword,
          },
        },
        authSession: {
          create: {
            activeExpires: activeExpires,
            idleExpires: idleExpires,
          },
        },
      },
      include: {
        authSession: true,
      },
    });

    console.log(`Thor API - User created ${user.id}`);

    return user;
  }

  public async getUserInfo(id: string): Promise<User> {
    const user = await getPrisma().user.findUnique({
      where: {
        id: id
      }
    });

    if (!user) {
      throw new NotFound('User not found');
    }

    return user;
  }


}