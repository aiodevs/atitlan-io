import { FastifyInstance, FastifyReply, FastifyRequest, RouteOptions } from "fastify";
import { GetUserSchema } from "./user.schema";
import UserController from "./user.controller";
import AtitlanApiRoutes from "@/components/shared/routes";
import { Routes } from "@/plugins/initializeRoutes";
import PermissionController from "../authorization/permission/permission.controller";
import { GetPermissionSchema } from "../authorization/permission/permission.schema";

export default class UserRoutes implements Routes {
    private permissionController = new PermissionController();
    private userController = new UserController();

    public initializeRoutes(
        fastify: FastifyInstance,
        opts: RouteOptions,
        done: () => void
    ) {
        fastify.route({
            method: "get",
            url: AtitlanApiRoutes.USER.PERMISSIONS,
            schema: GetPermissionSchema,
            preHandler: fastify.authenticateUser,
            handler: this.permissionController.getUserPermissions
        });

        fastify.route({
            method: "get",
            url: AtitlanApiRoutes.USER.INFO,
            schema: GetUserSchema,
            preHandler: fastify.authenticateUser,
            handler: this.userController.getLoggedInUserInfo
        });

        fastify.route({
            method: "get",
            url: AtitlanApiRoutes.USER.GET_URL,
            schema: GetUserSchema,
            preHandler: fastify.authenticateUser,
            handler: this.userController.getUser
        });

        done();
    }
}
