import { FastifyReply, FastifyRequest } from "fastify";
import { CreateUser } from "./user.schema";
import UserService, { EnrichedUser } from "./user.service";
import SessionService from "@/components/heimdall/authentication/session.service";
import { Unauthorized } from "@/util/constants";
import AuthorizationService from "../authorization/authorization.service";
import { Resources, ThorAdminPermissions } from "../authorization/authorization.type";

export default class UserController {
  private userService: UserService = new UserService();
  private sessionService: SessionService = new SessionService();
  private authorizationService: AuthorizationService = new AuthorizationService();
  public createUser = async (req: FastifyRequest<{ Body: CreateUser }>, reply: FastifyReply) => {
    // TODO consider removing the creation of a session straight away
    const user: EnrichedUser = await this.userService.createUser(req.body);
    const sessionId = user.authSession[0]?.id;
    console.log(`Thor API - User created ${user.id} with sessionId ${sessionId}`);
    if (sessionId) {
      this.sessionService.setSessionCookie(reply, sessionId);
    }

    return {
      message: "Signup successful",
      user: {
        email: user.email,
        id: user.id,
      }
    };
  }

  public getUser = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
    const userId = req.params.id;
    if (!req.user) {
      throw new Unauthorized();
    }

    const hasPermission = await this.authorizationService.checkUserPermission(
      req.user.id,
      Resources.ThorAdmin,
      ThorAdminPermissions.Admin);
    if (!hasPermission) {
      throw new Unauthorized();
    }

    const user = await this.userService.getUserInfo(userId);

    return {
      message: "user info",
      user: user
    };
  }

  public getLoggedInUserInfo = async (req: FastifyRequest, reply: FastifyReply) => {
    return {
      message: "user info",
      user: req.user
    };
  }
}
