import { Static, Type } from "@fastify/type-provider-typebox";
import { FastifySchema } from "fastify";
import { ERROR400, ERROR401, ERROR404, ERROR409, ERROR500, responseProperty } from "@/util/constants";

export type CreateUser = Static<typeof CreateUserBody>;

export const CreateUserBody = Type.Object({
  email: Type.String({ format: "email", errorMessage: { format: 'Invalid Email' } }),
  password: Type.String({ minLength: 8, errorMessage: { minLength: 'Password must be at least 8 characters long' } })
});

export const CreateUserSchema: FastifySchema = {
  description: 'Create a user and a session db + session cookie',
  tags: ['auth'],
  body: CreateUserBody,
  response: {
    201: {
      description: 'Successful create response',
      type: 'object',
      properties: {
        ...responseProperty,
        user: {
          type: 'object', properties: {
            email: { type: 'string' },
            id: { type: 'string' }
          }
        }
      }
    },
    400: ERROR400,
    409: ERROR409,
    500: ERROR500
  },
  security: [
    {
      cookieAuth: []
    }
  ]
}

export interface GetUser {
  email: string;
}

export const GetUserSchema: FastifySchema = {
  description: 'Get user api',
  tags: ['user'],
  params: Type.Object({
    id: Type.String(),
  }),
  response: {
    200: {
      description: 'Successful get response',
      type: 'object',
      properties: {
        ...responseProperty,
        user: {
          type: "object", properties: {
            id: { type: "string" },
            email: { type: "string" },
            createdAt: { type: "string" },
            updatedAt: { type: "string" }
          }
        },
      },
    },
    400: ERROR400,
    401: ERROR401,
    404: ERROR404,
    500: ERROR500
  },
  security: [
    {
      cookieAuth: []
    }
  ]
};