import { BadRequest } from "@/util/constants";
import { UserSession } from "@prisma/client";
import { getPrisma } from "@/util/prisma";
import { FastifyReply, FastifyRequest } from "fastify";

export const sessionCookieName = "aio-userSession";

export default class SessionService {
  // TODO move to env?
  private activeExpirationTime = 12 * 60 * 60 * 1000; // 3 hours
  private idleExpirationTime = 30 * 60 * 1000; // 30 min

  public activeExpiration(now: number): number {
    return now + this.activeExpirationTime;
  }

  public idleExpiration(now: number): number {
    return now + this.idleExpirationTime;
  }

  public async createSession(userId: string): Promise<UserSession> {
    const now = Date.now();
    const session = await getPrisma().userSession.create({
      data: {
        userId: userId,
        activeExpires: this.activeExpiration(now),
        idleExpires: this.idleExpiration(now),
      },
    });
    console.log(`Creating session for user ${userId} \n with sessionId: ${session.id}`)
    return session;
  }

  public async getSession(sessionId: string): Promise<UserSession | null> {
    return getPrisma().userSession.findUnique({
      where: { id: sessionId },
    });
  }

  public async updateSession(sessionId: string): Promise<UserSession> {
    const currentTime = Date.now();
    const idleExpires = currentTime + this.idleExpirationTime;

    const session = await getPrisma().userSession.update({
      where: { id: sessionId },
      data: {
        idleExpires: idleExpires,
        lastUsedAt: new Date(),
      },
    });
    console.log(`Updating session for sessionId: ${sessionId}`);
    return session;
  }

  public async deleteSession(sessionId: string): Promise<UserSession> {
    const userSession = await this.getSession(sessionId);
    if (!userSession) {
      console.log(`Session not found - trying to logout for a non-existant user session`)
      throw new BadRequest();
    }

    const session = getPrisma().userSession.delete({
      where: { id: sessionId },
    });
    console.log(`Deleting session for sessionId: ${sessionId}`);
    return session;
  }

  public setSessionCookie(reply: FastifyReply, sessionId: string): void {
    console.log(`Setting session cookie for sessionId: ${sessionId}`);
    reply.setCookie(sessionCookieName, sessionId.toString(), {
      path: "/",
      signed: true,
      httpOnly: true,
      sameSite: "strict",
      secure: process.env.NODE_ENV === "production",
    });
  }

  public deleteSessionCookie(reply: FastifyReply): void {
    reply.clearCookie(sessionCookieName);
  }

  public getSessionIdFromCookie(req: FastifyRequest): string | null {
    const sessionIdString = req.cookies[sessionCookieName];
    if (!sessionIdString) {
      console.log(`getSessionIdFromCookie - No session cookie found`);
      return null;
    }
    const unsignedCookie = req.unsignCookie(sessionIdString);
    if (!unsignedCookie.valid) {
      console.log(`getSessionIdFromCookie - Invalid session cookie found for ${sessionIdString} we might be under attack!`)
      return null;
    }

    return unsignedCookie.value;
  }
}