import { compare } from "bcrypt";
import { getPrisma } from "@/util/prisma";
import { Unauthorized } from "@/util/constants";
import { User } from "@prisma/client";
import { LoginUser } from "./authentication.schema";

export default class AuthenticationService {

  public async loginUser(loginData: LoginUser): Promise<User> {
    const user = await getPrisma().user.findUnique({
      where: {
        email: loginData.email,
      },
    });

    if (!user) {
      console.log(`User not found ${loginData.email}`);
      throw new Unauthorized("User or password incorrect");
    }

    const key = await getPrisma().key.findFirst({
      where: {
        userId: user.id,
      },
    });

    if (!key || key.hashedPassword === null) {
      throw new Unauthorized("User or password incorrect");
    }

    const validPassword = await compare(loginData.password, key.hashedPassword);
    if (!validPassword) {
      throw new Unauthorized("User or password incorrect");
    }

    await getPrisma().key.update({
      where: {
        id: key.id,
      },
      data: {
        lastUsedAt: new Date(),
      },
    });

    return user;
  }
}
