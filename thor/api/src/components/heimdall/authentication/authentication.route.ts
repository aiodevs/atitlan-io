import { FastifyInstance, RouteOptions } from "fastify";
import { CreateUserSchema } from "@/components/heimdall/user/user.schema";
import AuthenticationController from "@/components/heimdall/authentication/authentication.controller";
import UserController from "@/components/heimdall/user/user.controller";
import { Routes } from "@/plugins/initializeRoutes";
import AtitlanApiRoutes from "@/components/shared/routes";
import PermissionController from "../authorization/permission/permission.controller";
import { LoginUserSchema, LogoutSchema, ValidSessionSchema } from "./authentication.schema";

export default class AuthenticationRoute implements Routes {
  public path = "/auth";
  public userController = new UserController();
  public authController = new AuthenticationController();
  public permissionController = new PermissionController();

  public initializeRoutes(
    fastify: FastifyInstance,
    opts: RouteOptions,
    done: () => void
  ) {
    fastify.route({
      method: "post",
      url: AtitlanApiRoutes.AUTH.SIGNUP,
      schema: CreateUserSchema,
      handler: this.userController.createUser,
    });

    fastify.route({
      method: "post",
      url: AtitlanApiRoutes.AUTH.LOGIN,
      schema: LoginUserSchema,
      handler: this.authController.login,
    });

    fastify.route({
      method: "post",
      url: AtitlanApiRoutes.AUTH.LOGOUT,
      schema: LogoutSchema,
      handler: this.authController.logout,
    });

    fastify.route({
      method: "get",
      url: AtitlanApiRoutes.AUTH.VALID_SESSION,
      schema: ValidSessionSchema,
      preHandler: fastify.authenticateUser,
      handler: async (req, reply) => {
        return {
          message: "User session is valid",
          user: req.user,
        };
      }
    });

    done();
  }
}
