import { FastifyReply, FastifyRequest } from "fastify";
import AuthenticationService from "./authentication.service";
import SessionService from "@/components/heimdall/authentication/session.service";
import { NotFound } from "@/util/constants";
import { LoginUser } from "./authentication.schema";

export default class AuthenticationController {
  private authService: AuthenticationService = new AuthenticationService();
  private sessionService: SessionService = new SessionService();

  public login = async (
    req: FastifyRequest<{ Body: LoginUser }>,
    reply: FastifyReply
  ) => {
    const user = await this.authService.loginUser(req.body);
    const session = await this.sessionService.createSession(user.id);
    this.sessionService.setSessionCookie(reply, session.id);

    return {
      message: "Login successful",
      user: {
        email: user.email,
        id: user.id
      }
    };
  }

  public logout = async (
    req: FastifyRequest,
    reply: FastifyReply
  ) => {
    const sessionId: string | null = this.sessionService.getSessionIdFromCookie(req);
    if (sessionId === null) {
      throw new NotFound("No session id found");
    }

    const promise = this.sessionService.deleteSession(sessionId);
    this.sessionService.deleteSessionCookie(reply);

    await promise;
    return {
      message: "Logout successful"
    }
  }
}
