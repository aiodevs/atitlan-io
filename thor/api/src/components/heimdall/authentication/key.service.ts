import { hash } from "@/util/crypto";
import { getPrisma } from "@/util/prisma";
import { Key } from "@prisma/client";

export default class KeyService {
    public async createKey(password: string, userId: string): Promise<Key> {
        const hashedPassword = await hash(password);

        const key = getPrisma().key.create({
            data: {
                hashedPassword: hashedPassword,
                userId: userId
            }
        })
        return key;
    }
}