import AtitlanApiRoutes from "@/components/shared/routes";
import { Routes } from "@/plugins/initializeRoutes";
import { FastifyInstance, RouteOptions } from "fastify";
import PermissionController from "./permission.controller";
import { PermissionSchema, AddPermissionSchema, RemovePermissionSchema } from "./permission.schema";


export default class PermissionRoute implements Routes {
    public path = "/permission";
    public permissionController = new PermissionController();

    public initializeRoutes(
        fastify: FastifyInstance,
        opts: RouteOptions,
        done: () => void
    ) {
        fastify.route({
            method: "post",
            url: AtitlanApiRoutes.AUTH.CHECK_PERMISSION,
            schema: PermissionSchema,
            preHandler: fastify.authenticateUser,
            handler: this.permissionController.checkUserPermission
        });

        fastify.route({
            method: "post",
            url: AtitlanApiRoutes.AUTH.ADD_PERMISSION,
            schema: AddPermissionSchema,
            preHandler: fastify.authenticateUser,
            handler: this.permissionController.addPermissions
        });

        fastify.route({
            method: "delete",
            url: AtitlanApiRoutes.AUTH.REMOVE_PERMISSION,
            schema: RemovePermissionSchema,
            preHandler: fastify.authenticateUser,
            handler: this.permissionController.removePermissions
        });

        done();
    }
}