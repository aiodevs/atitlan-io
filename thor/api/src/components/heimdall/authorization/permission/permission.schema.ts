import { Static, Type } from "@fastify/type-provider-typebox";
import { FastifySchema } from "fastify";
import {
  ERROR400,
  ERROR401,
  ERROR403,
  ERROR404,
  ERROR500,
} from "@/util/constants";
import { PermissionType } from "@/components/finance/finance.schema.types";

export type PermissionRequest = Static<typeof PermissionRequestBody>;

export const PermissionRequestBody = Type.Object({
  resource: Type.String(),
  action: Type.String(),
});

export const PermissionSchema: FastifySchema = {
  description: "Check if the user has the permission for this action on a resource",
  tags: ["permission"],
  body: PermissionRequestBody,
  response: {
    200: {
      description: "Permission check successful",
      type: "object",
      properties: {
        message: { type: "string" }
      }
    },
    403: ERROR403,
    500: ERROR500
  },
  security: [
    {
      cookieAuth: []
    }
  ]
};

export type RemovePermissionRequest = Static<typeof RemovePermissionRequestBody>;

export const RemovePermissionRequestBody = Type.Array(
  Type.Object({
    permissionId: Type.String(),
  }));

export const RemovePermissionSchema: FastifySchema = {
  description: "Remove a permission from a user",
  tags: ["permission"],
  body: RemovePermissionRequestBody,
  response: {
    200: {
      description: "Permission removed successfully",
      type: "object",
      properties: {
        message: { type: "string" }
      }
    },
    400: ERROR400,
    401: ERROR401,
    500: ERROR500
  },
  security: [
    {
      cookieAuth: []
    }
  ]
};

export const GetPermissionSchema: FastifySchema = {
  description: "Get permissions of the logged-in user",
  tags: ["user", "permission"],
  response: {
    200: {
      description: "Permissions retrieval successful",
      type: "object",
      properties: {
        permissions: Type.Array(PermissionType)
      }
    },
    401: ERROR401,
    404: ERROR404,
    500: ERROR500
  },
  security: [
    {
      cookieAuth: []
    }
  ]
};

export type AddPermissionsRequest = Static<typeof AddPermissionsRequestBody>;

export const AddPermissionsRequestBody = Type.Array(
  Type.Object({
    userId: Type.String(),
    resource: Type.String(),
    action: Type.String(),
  })
);

export type AddPermissionRequest = Static<typeof AddPermissionRequestObject>;
const AddPermissionRequestObject = Type.Object({
  userId: Type.String(),
  resource: Type.String(),
  action: Type.String(),
})

export const AddPermissionSchema: FastifySchema = {
  description: "Add a new permission to a user",
  tags: ["permission"],
  body: AddPermissionsRequestBody,
  response: {
    201: {
      description: "Permission added successfully",
      type: "object",
      properties: {
        message: { type: "string" },
        permission: {
          type: "object",
          properties: {
            id: { type: "string" },
            userId: { type: "string" },
            resource: { type: "string" },
            action: { type: "string" },
            expiresAt: { type: "string" },
          }
        }
      }
    },
    400: ERROR400,
    401: ERROR401,
    500: ERROR500
  },
  security: [
    {
      cookieAuth: []
    }
  ]
};