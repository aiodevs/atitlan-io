import { Permission, PermissionRoleLink } from "@prisma/client";

export type PermissionLinkWithPermission = PermissionRoleLink & {
    permission: Permission,
}

export type PermissionResult = {
    id: string,
    resource: string,
    action: string,
    isDefault: boolean,
}