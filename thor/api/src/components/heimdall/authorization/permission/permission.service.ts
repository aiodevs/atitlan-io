import { getPrisma } from "@/util/prisma";
import { Permission } from "@prisma/client";
import { DefaultUserOrganizationPermissionResult } from "../../organization/organization.types";
import { UserOrganizationPermissions, Resources } from "../authorization.type";

export default class PermissionService {

    public async getUserPermissions(userId: string): Promise<Permission[]> {
        const userPermissions = await getPrisma().userPermissionLink.findMany({
            where: {
                userId: userId,
                revokedAt: null,
            },
            include: {
                permission: true
            }
        }).then(links => links.map(link => link.permission));
        return userPermissions;
    }

    public async getOrCreatePermissions(
        permissions: {
            resource: string,
            action: string,
            isDefault?: boolean
        }[]): Promise<Permission[]> {
        const result: Permission[] = [];
        for (const permission of permissions) {
            let existingPermissionRecord: Permission | null = await getPrisma().permission.findFirst({
                where: {
                    resource: permission.resource,
                    action: permission.action,
                    isDefault: permission.isDefault || false
                }
            });
            if (!existingPermissionRecord) {
                existingPermissionRecord = await getPrisma().permission.create({
                    data: {
                        action: permission.action,
                        resource: permission.resource,
                        isDefault: permission.isDefault || false
                    }
                });
                console.log(`role.service.ts:addOrCreateRolePermissions: Permission ${permission.action} for resource ${permission.resource} not found - created new one`);
            }
            result.push(existingPermissionRecord);
        }
        return result;
    }

    public async checkValidPermissionLogic(
        permissions: {
            action: string,
            resource: string
        }[],
        target: {
            action: string,
            resource: string
        }): Promise<boolean> {
        for (const permission of permissions) {
            const isValid = permission.resource === target.resource
                && (permission.action === target.action || permission.action === '*');
            if (isValid) return true;
        }
        return false;
    }


    public async addUserPermissionsNoAuth(
        permissions: Array<{
            userId: string,
            resource: string,
            action: string,
        }>): Promise<void> {
        for (const { userId, resource, action } of permissions) {
            const exists = await getPrisma().permission.findFirst({
                where: { resource, action }
            });

            if (!exists) {
                console.log(`Creating permission on resource ${resource} with action ${action}`);
                const newPermission = await getPrisma().permission.create({
                    data: { resource, action }
                });

                console.log(`Linking permission to user ${userId}`);
                await getPrisma().userPermissionLink.create({
                    data: { userId: userId, permissionId: newPermission.id }
                });
                continue;
            }

            const existsLink = await getPrisma().userPermissionLink.findFirst({
                where: {
                    userId: userId,
                    permissionId: exists.id
                }
            });
            if (existsLink) {
                console.log(`Permission already exists for resource ${resource} with action ${action}. User: ${userId}`);
                continue;
            }
            console.log(`Permission already exists for resource ${resource} with action ${action}. Linking to user ${userId}`);
            await getPrisma().userPermissionLink.create({
                data: { userId: userId, permissionId: exists.id }
            });

        }
    }

    public async revokeUserPermissionsNow(userId: string, permissionIds: string[]): Promise<void> {
        await getPrisma().userPermissionLink.updateMany({
            where: {
                userId: userId,
                permissionId: { in: permissionIds }
            },
            data: {
                revokedAt: new Date()
            }
        });
    }

    public async removePermissions(permissionIds: string[]): Promise<void> {
        await getPrisma().permission.deleteMany({
            where: {
                id: { in: permissionIds }
            }
        });
    }
}
