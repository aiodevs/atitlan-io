import { BadRequest, Unauthorized } from "@/util/constants";
import PermissionService from "./permission.service";
import { FastifyReply, FastifyRequest } from "fastify";
import { AddPermissionsRequest, PermissionRequest, RemovePermissionRequest } from "./permission.schema";
import AuthorizationService from "../authorization.service";
import { Resources, ThorAdminPermissions } from "../authorization.type";


export default class PermissionController {
    private permisionService: PermissionService = new PermissionService();
    private authorizationService: AuthorizationService = new AuthorizationService();

    public checkUserPermission = async (req: FastifyRequest<{ Body: PermissionRequest }>, reply: FastifyReply) => {
        const userId = req.user?.id;
        if (!userId) {
            throw new BadRequest("No user id found");
        }
        const { resource, action } = req.body;
        const hasPermission = await this.authorizationService.checkUserPermission(userId, resource, action);
        if (!hasPermission) {
            console.log(`User ${userId} does not have permission to ${action} on ${resource}`);
            throw new Unauthorized();
        }

        return reply.send({ message: "Permission granted" });
    }

    public getUserPermissions = async (req: FastifyRequest, reply: FastifyReply) => {
        const userId = req.user?.id;

        if (!userId) {
            throw new BadRequest();
        }

        const permissions = await this.permisionService.getUserPermissions(userId);
        return reply.send({ permissions });
    }

    public addPermissions = async (req: FastifyRequest<{ Body: AddPermissionsRequest }>, reply: FastifyReply) => {
        const requestUserId = req.user?.id;

        if (!requestUserId) {
            throw new BadRequest();
        }

        const hasPermissionToAdd = await this.authorizationService
            .checkUserPermission(requestUserId, Resources.ThorAdmin, ThorAdminPermissions.ManageAllPermissions);
        if (!hasPermissionToAdd) {
            throw new Unauthorized("Not authorized to add permissions");
        }

        const permissionsData: {
            userId: string;
            resource: string;
            action: string;
        }[] = req.body.map(permission => {
            return {
                userId: permission.userId,
                resource: permission.resource,
                action: permission.action,
            };
        });

        await this.permisionService.addUserPermissionsNoAuth(permissionsData);
        return reply.send({ message: "Permissions added or updated" });
    };

    public removePermissions = async (
        req: FastifyRequest<{
            Body: RemovePermissionRequest
        }>,
        reply: FastifyReply) => {
        const userId = req.user?.id;

        if (!userId) {
            throw new BadRequest();
        }

        const hasPermissionToAdd = await this.authorizationService
            .checkUserPermission(userId, Resources.ThorAdmin, ThorAdminPermissions.ManageAllPermissions);
        if (!hasPermissionToAdd) {
            throw new Unauthorized("Not authorized to add permissions");
        }

        const permissionIds = req.body.map(permission => permission.permissionId);

        await this.permisionService.removePermissions(permissionIds);
        return reply.send({ message: "Permission removed" });
    }
}