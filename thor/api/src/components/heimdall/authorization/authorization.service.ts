import { getPrisma } from "@/util/prisma";
import PermissionService from "./permission/permission.service";
import { WalletPermissions, Resources, UserOrganizationPermissions, DefaultUserOrganizationRoleNames, DefaultWalletRoleNames, defaultWalletRoles, userOrganizationRoles } from "./authorization.type";
import { InternalServerError } from "@/util/constants";
import { DefaultUserOrganizationPermissionResult, DefaultUserOrganizationRoleResult } from "../organization/organization.types";
import { PermissionResult } from "./permission/permission.type";
import { RoleWithPermissions, RoleWithPermissionsResult } from "./role/role.types";
import RoleService from "./role/role.service";

export default class AuthorizationService {

    private permissionService: PermissionService = new PermissionService();
    private roleService: RoleService = new RoleService();

    public async checkUserPermission(userId: string, resource: string, action: string): Promise<boolean> {
        const permissionLinks = await getPrisma().userPermissionLink.findMany({
            where: {
                userId: userId,
                revokedAt: null,
            },
            include: {
                permission: true
            }
        });

        const validPermission = await this.permissionService.checkValidPermissionLogic(
            permissionLinks.map(link => link.permission),
            { resource, action }
        );

        if (validPermission) {
            const permissionLinkIds = permissionLinks.map(link => link.id);
            await getPrisma().userPermissionLink.updateMany({
                where: {
                    id: { in: permissionLinkIds }
                },
                data: { lastUsed: new Date() }
            });
            return true;
        }

        return false;
    }

    public async hasPermission(
        roleId: string,
        resource: string,
        action: string) {
        const role = await getPrisma()
            .role.findUnique({
                where: {
                    id: roleId
                },
                include: {
                    permissions: {
                        include: {
                            permission: true
                        }
                    }
                }
            });
        if (!role) {
            console.log(`authorization.service.ts:hasPermission: Role with id ${roleId} not found`);
            return false;
        }

        const remappedPermissions = role.permissions.map(permission => {
            return {
                action: permission.permission.action,
                resource: permission.permission.resource
            }
        });
        const targetPermission = {
            action,
            resource
        }
        const hasPermission = await this.permissionService.checkValidPermissionLogic(remappedPermissions, targetPermission);
        return hasPermission;
    }

    public async userHasAnyWalletPermission(userId: string, walletId: string, permission: WalletPermissions) {
        const userPermission: boolean = await this.userHasUserWalletPermission(userId, walletId, permission);
        const orgPermission: boolean = await this.userHasOrganizationWalletPermissionNoOrg(userId, walletId, permission);
        const inheritedPermission: boolean = await this.userHasInheritedOrganizationWalletPermission(userId, walletId, permission);
        return userPermission || orgPermission || inheritedPermission;
    }

    private async userHasOrganizationWalletPermissionNoOrg(userId: string, walletId: string, permission: WalletPermissions) {
        const userOrgWalletRoleLink = await getPrisma().organizationUserWalletRoleAccessLink.findFirst({
            where: {
                userId: userId,
                walletId: walletId,
                OR: [{
                    revokedAt: { equals: null }
                }, {
                    revokedAt: { gt: new Date() }
                }]
            },
            include: {
                role: {
                    include: {
                        permissions: true
                    }
                }
            }
        });
        if (!userOrgWalletRoleLink) {
            console.log(`User ${userId} does not have access to wallet ${walletId}`);
            return false;
        }
        const hasPermission = await this.hasPermission(
            userOrgWalletRoleLink.roleId,
            Resources.Wallet,
            permission);
        if (!hasPermission) {
            console.log(`User ${userId} does not have ${permission} access to wallet ${walletId}`);
            return false;
        }
        console.log(`User ${userId} has ${permission} access to wallet ${walletId}`);
        return true;
    }

    private async userHasUserWalletPermission(userId: string, walletId: string, permission: WalletPermissions) {
        const userRoleLink = await this.findUnRevokedAllUserWalletLinkRoleIds(userId, walletId);
        if (!userRoleLink || userRoleLink.length === 0) {
            console.log(`User ${userId} does not have access to wallet ${walletId}`);
            return false;
        }
        for (const link of userRoleLink) {
            const hasPermission = await this.hasPermission(
                link,
                Resources.Wallet,
                permission);
            if (hasPermission) {
                console.log(`User ${userId} has ${permission} access to wallet ${walletId}`);
                return true;
            } else {
                console.log(`User ${userId} does not have ${permission} access to wallet ${walletId}`);
                continue;
            }
        }
        console.log(`User ${userId} does not have ${permission} access to wallet ${walletId}`);
        return false;
    }

    public async userHasOrganizationWalletPermission(
        userId: string,
        organizationId: string,
        walletId: string,
        permission: WalletPermissions
    ): Promise<boolean> {
        const userOrgWalletRoleLink = await getPrisma().organizationUserWalletRoleAccessLink.findFirst({
            where: {
                userId: userId,
                walletId: walletId,
                organizationId: organizationId,
                OR: [{
                    revokedAt: { equals: null }
                }, {
                    revokedAt: { gt: new Date() }
                }]
            },
            include: {
                role: {
                    include: {
                        permissions: true
                    }
                },
                organization: true
            }
        });
        if (!userOrgWalletRoleLink) {
            console.log(`User ${userId} does not have access to wallet ${walletId}`);
            return false;
        }
        const hasPermission = await this.hasPermission(
            userOrgWalletRoleLink.roleId,
            Resources.Wallet,
            permission);

        if (!hasPermission) {
            console.log(`User ${userId} does not have ${permission} access to wallet ` +
                `${walletId} in organization ${userOrgWalletRoleLink.organization.name} - ` +
                `${userOrgWalletRoleLink.organizationId} role ${userOrgWalletRoleLink.role.name} ` +
                `id: ${userOrgWalletRoleLink.roleId}`);
            return false
        }
        console.log(`User ${userId} has ${permission} access to wallet ${walletId} ` +
            `in organization ${userOrgWalletRoleLink.organization.name} - ` +
            `${userOrgWalletRoleLink.organizationId} role ${userOrgWalletRoleLink.role.name} ` +
            `id: ${userOrgWalletRoleLink.roleId}`);
        return true;
    }

    private async userHasInheritedOrganizationWalletPermission(userId: string, walletId: string, permission: WalletPermissions) {
        const orgWalletRoleAccessLink = await getPrisma().organizationWalletRoleAccessLink.findFirst({
            where: {
                walletId: walletId,
                OR: [{
                    revokedAt: { equals: null }
                }, {
                    revokedAt: { gt: new Date() }
                }]
            }
        });
        if (!orgWalletRoleAccessLink) {
            console.log(`authorization.service.ts - userHasInheritedOrganizationWalletPermission - Wallet ${walletId}`
                + ` does not have any organization connections`);
            return false;
        }
        const userOrgLinks = await getPrisma().userOrganizationRoleAccessLink.findMany({
            where: {
                userId: userId,
                OR: [{
                    revokedAt: { equals: null }
                }, {
                    revokedAt: { gt: new Date() }
                }]
            }
        });
        if (!userOrgLinks) {
            console.log(`authorization.service.ts - userHasInheritedOrganizationWalletPermission - User ${userId}`
                + ` does not have any organization connections`);
            return false;
        }
        const userOrgWalletMatch = userOrgLinks.find(link => link.organizationId === orgWalletRoleAccessLink.organizationId);
        if (!userOrgWalletMatch) {
            console.log(`authorization.service.ts - userHasInheritedOrganizationWalletPermission - User ${userId}`
                + ` does not have access to organization ${orgWalletRoleAccessLink.organizationId}`);
            return false;
        }
        const hasWalletInheritencePermission = await this.userHasOrganizationPermission(userId, userOrgWalletMatch.organizationId, UserOrganizationPermissions.WalletsPermissionsInheritence);
        if (!hasWalletInheritencePermission) {
            console.log(`authorization.service.ts - userHasInheritedOrganizationWalletPermission - User ${userId}`
                + ` does not have wallet inheritence permission for organization ${userOrgWalletMatch.organizationId}`);
            return false;
        }
        const hasPermission = await this.hasPermission(
            userOrgWalletMatch.roleId,
            Resources.Wallet,
            permission);
        return hasPermission;
    }

    public async userHasOrganizationPermission(userId: string, organizationId: string, permission: UserOrganizationPermissions) {
        const organizationUser = await getPrisma().userOrganizationRoleAccessLink.findFirst({
            where: {
                userId: userId,
                organizationId: organizationId,
                OR: [{
                    revokedAt: { equals: null }
                }, {
                    revokedAt: { gt: new Date() }
                }]
            },
            include: {
                role: {
                    include: {
                        permissions: true,
                    }

                }
            }
        });
        if (!organizationUser) {
            return false;
        }
        const hasPermission = await this.hasPermission(
            organizationUser.roleId,
            Resources.Organization,
            permission);
        console.log(`Thor API - User ${userId} has permission ${permission} on organization ${organizationId}: ${hasPermission ? "Yes" : "No"}`);
        return hasPermission;
    }

    public async getOrCreateDefaultWalletRole(roleName: DefaultWalletRoleNames): Promise<RoleWithPermissions> {
        const defaultPermissionRoles = defaultWalletRoles.filter(role => role.name === roleName);
        if (!defaultPermissionRoles || defaultPermissionRoles.length === 0) {
            console.log(`authorization.service.ts:getOrCreateDefaultWalletRole: Default wallet role ${roleName} not found`);
            throw new InternalServerError();
        }
        const defaultPermissionActions = defaultPermissionRoles[0].permissionActions;
        const defaultPermissions = defaultPermissionActions.map(permissionAction => ({
            resource: Resources.Wallet,
            action: permissionAction,
            isDefault: true
        }));
        const defaultRole = await getPrisma().role.findFirst({
            where: {
                name: roleName,
                isDefault: true
            },
            include: {
                permissions: {
                    include: {
                        permission: true
                    }
                }
            }
        });
        if (defaultRole) {
            const hasAllPermissions = await this.hasAllPermission(defaultRole.id, defaultPermissions);
            if (!hasAllPermissions) {
                console.log(`authorization.service.ts:getOrCreateDefaultWalletRole: Default wallet role ${roleName} does not have all the permissions. Adding missing permissions`);
                return await this.roleService.updateRoleAndGetOrCreatePermissions(defaultRole.id, defaultRole.name, defaultPermissions, true, true);
            }
            return defaultRole;
        }
        const role = await getPrisma().role.create({
            data: {
                name: roleName,
                isDefault: true
            }
        });
        const newRole: RoleWithPermissions = await this.roleService.updateRoleAndGetOrCreatePermissions(role.id, role.name, defaultPermissions, true, true);
        console.log(`authorization.service.ts:getOrCreateDefaultWalletRole: Created new default wallet role ${roleName} with all the permissions: \n${newRole.permissions.map(p => p.permission.action)}`);
        return newRole;
    }

    private async hasAllPermission(roleId: string, permissions: {
        resource: string,
        action: string
    }[]): Promise<boolean> {
        const role = await getPrisma().role.findUnique({
            where: {
                id: roleId
            },
            include: {
                permissions: {
                    include: {
                        permission: true
                    }
                }
            }
        });
        if (!role) {
            console.log(`authorization.service.ts:hasAllPermission: Role with id ${roleId} not found`);
            return false;
        }
        const hasAllPermissions = permissions.every(permission => {
            return role.permissions.some(rolePermission => {
                return rolePermission.permission.action === permission.action
                    && rolePermission.permission.resource === permission.resource;
            });
        });
        return hasAllPermissions;
    }

    public async getOrCreateDefaultOrganizationRole(roleName: DefaultUserOrganizationRoleNames): Promise<RoleWithPermissions> {
        const defaultPermissionActions: UserOrganizationPermissions[] =
            userOrganizationRoles.filter(role => role.name === roleName)[0].permissions;
        const defaultPermissions: {
            resource: string,
            action: string,
            isDefault?: boolean
        }[] = defaultPermissionActions.map(permission => ({
            resource: Resources.Organization,
            action: permission,
            isDefault: true
        }));

        const defaultRole = await getPrisma().role.findFirst({
            where: {
                name: roleName,
                isDefault: true
            },
            include: {
                permissions: {
                    include: {
                        permission: true
                    }
                }
            }
        });
        if (defaultRole) {
            const hasAllPermissions = await this.hasAllPermission(defaultRole.id, defaultPermissions);
            if (!hasAllPermissions) {
                console.log(`authorization.service.ts:getOrCreateDefaultOrganizationRole:`
                    + `Default organization role ${roleName} does not have all the permissions.`
                    + ` Adding missing permissions`);

                const updatedPermissions = await this.roleService.updateRoleAndGetOrCreatePermissions(defaultRole.id, defaultRole.name, defaultPermissions, true, true);
            }
            return defaultRole;
        }
        const role = await getPrisma().role.create({
            data: {
                name: roleName,
                isDefault: true
            }
        });
        const newPermissions = await this.roleService.updateRoleAndGetOrCreatePermissions(role.id, role.name, defaultPermissions, true, true);
        console.log(`authorization.service.ts:getOrCreateDefaultOrganizationRole: Created new default organization role ${roleName} with all the permissions: ${defaultPermissions.map(p => p.action).join(', ')}`);
        const RoleWithPermissions = await this.roleService.getRole(role.id);
        if (!RoleWithPermissions) throw new InternalServerError();
        return RoleWithPermissions;
    }

    public async getDefaultUserOrganizationRoles(): Promise<DefaultUserOrganizationRoleResult[]> {
        const defaultUserOrganizationRoles: DefaultUserOrganizationRoleResult[] = [];
        for (const role of userOrganizationRoles) {
            const realRole = await this.getOrCreateDefaultOrganizationRole(role.name);
            defaultUserOrganizationRoles.push({
                name: role.name,
                roleId: realRole.id,
            });
        }
        return defaultUserOrganizationRoles;
    }

    public async getDefaultUserOrganizationPermissions(): Promise<DefaultUserOrganizationPermissionResult[]> {
        const allOrgPermissions: {
            resource: string,
            action: string,
            isDefault: boolean
        }[] = [];
        for (const permission in UserOrganizationPermissions) {
            allOrgPermissions.push({
                resource: Resources.Organization,
                action: permission,
                isDefault: true
            });
        }
        const defaultOrgPermissions = await this.permissionService.getOrCreatePermissions(allOrgPermissions)
        const defaultUserOrganizationPermissions: DefaultUserOrganizationPermissionResult[] = [];
        for (const permission in UserOrganizationPermissions) {
            const realPermission = defaultOrgPermissions.find((perm) => {
                return perm.action === permission;
            });
            if (!realPermission) {
                console.log(`Thor API - Organization details - Could not find permission ${permission}`);
                continue;
            }
            defaultUserOrganizationPermissions.push({
                name: permission as UserOrganizationPermissions,
                permissionId: realPermission.id,
            });
        }
        return defaultUserOrganizationPermissions;
    }

    public async getDefaultWalletRoles(): Promise<RoleWithPermissionsResult[]> {
        const defaultWalletRoles: RoleWithPermissionsResult[] = [];
        for (const role of Object.values(DefaultWalletRoleNames)) {
            const roleAsType = role as DefaultWalletRoleNames;
            const realRole: RoleWithPermissions = await this.getOrCreateDefaultWalletRole(roleAsType);
            const remappedPermissionActions: PermissionResult[] = realRole.permissions.map(p => {
                return {
                    id: p.id,
                    resource: p.permission.resource,
                    action: p.permission.action,
                    isDefault: p.permission.isDefault,
                }
            });
            defaultWalletRoles.push({
                id: realRole.id,
                createdAt: realRole.createdAt,
                updatedAt: realRole.updatedAt,
                name: roleAsType,
                isDefault: realRole.isDefault,
                permissions: remappedPermissionActions,
            });
        }
        return defaultWalletRoles;
    }


    private async findUnRevokedAllUserWalletLinkRoleIds(userId: string, walletId: string)
        : Promise<string[]> {
        const orgWalletlinks = await getPrisma().organizationUserWalletRoleAccessLink.findMany({
            where: {
                userId: userId,
                walletId: walletId,
                OR: [{
                    revokedAt: { equals: null }
                }, {
                    revokedAt: { gt: new Date() }
                }]
            }
        });
        const userWalletlinks = await getPrisma().userWalletRoleAccessLink.findMany({
            where: {
                userId: userId,
                walletId: walletId,
                OR: [{
                    revokedAt: { equals: null }
                }, {
                    revokedAt: { gt: new Date() }
                }]
            }
        });
        const roleIdResult = orgWalletlinks.map(link => link.roleId).concat(userWalletlinks.map(link => link.roleId));
        return roleIdResult;
    }
}