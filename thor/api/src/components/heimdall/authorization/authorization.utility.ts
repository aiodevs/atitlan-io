import { Resources } from "./authorization.type";
import { AddPermissionRequest } from "./permission/permission.schema";
import PermissionService from "./permission/permission.service";

const permisionService: PermissionService = new PermissionService();

export async function createInitialUserPermissions(userId: string): Promise<void> {

    const permissions: AddPermissionRequest[] = [{
        userId: userId,
        resource: Resources.ThorAdmin,
        action: '*'
    }, {
        userId: userId,
        resource: Resources.ThorLockedFeatures,
        action: '*',
    }, {
        userId: userId,
        resource: Resources.Traefik,
        action: '*',
    }, {
        userId: userId,
        resource: Resources.Portainer,
        action: '*',
    },
    ];

    await permisionService.addUserPermissionsNoAuth(permissions);
}