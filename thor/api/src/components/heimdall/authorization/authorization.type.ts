import { IDefaultWalletRole } from "./role/role.types";

export enum Resources {
    Wallet = "wallet",
    Organization = "organization",
    ThorAdmin = "thor-admin",
    ThorLockedFeatures = "thor-locked-features",
    Traefik = "traefik",
    Portainer = "portainer",
}

export enum ThorLockedFeaturePermissions {
    Admin = "*",
    CreateRecurrentePaymentGateway = "thorfeatures:create:recurrente-payment-gateway",
    CreateStripePaymentGateway = "thorfeatures:create:stripe-payment-gateway",
    AddFunds = "thorfeatures:add-funds",
}

export enum ThorAdminPermissions {
    Admin = "*",
    SeeAllUserWallets = "thor-admin:see-all-user-wallets",
    SeeAllOrganizations = "thor-admin:see-all-organizations",
    ManageAllPermissions = "thor-admin:manage-all-permissions",
}

export enum WalletPermissions {
    Owner = "wallet:owner",
    Read = "wallet:read",
    Invoice = "wallet:invoice",
    Pay = "wallet:pay",
    Update = "wallet:update",
    ConnectUserToWallet = "wallet:connect",
    PaymentGatewayManagement = "wallet:payment-gateway-update",
}

export enum UserOrganizationPermissions {
    Owner = "organization:owner",
    Read = "organization:read",
    WalletsRead = "organization:wallets-read",
    WalletsPermissionsInheritence = "organization:wallets-permissions-inheritence",
    WalletsPaymentGateway = "organization:wallets-payment-gateway",
    UpdateProfile = "organization:update",
    Finance = "organization:finance",
    ConnectUserToOrgWallet = "organization:connect-user-orgwallet",
    ConnectUserToOrg = "organization:connect-user-org",
    ConnectWalletToOrg = "organization:connect-org-wallet",
}

export enum DefaultWalletRoleNames {
    Owner = "wallet:owner",
    Admin = "wallet:admin",
    ReadOnly = "wallet:read-only",
    PaymentGatewayOwner = "wallet:payment-gateway-owner",
    PaymentGatewayAdmin = "wallet:payment-gateway-admin",
}

export const defaultWalletRoles: IDefaultWalletRole[] = [
    {
        name: DefaultWalletRoleNames.Owner,
        permissionActions: [
            WalletPermissions.Read,
            WalletPermissions.Invoice,
            WalletPermissions.Pay,
            WalletPermissions.Update,
            WalletPermissions.ConnectUserToWallet,
            WalletPermissions.Owner,
        ],
        isDefault: true,
    },
    {
        name: DefaultWalletRoleNames.Admin,
        permissionActions: [
            WalletPermissions.Read,
            WalletPermissions.Invoice,
            WalletPermissions.Pay,
            WalletPermissions.Update,
            WalletPermissions.ConnectUserToWallet,
        ],
        isDefault: true,
    },
    {
        name: DefaultWalletRoleNames.ReadOnly,
        permissionActions: [WalletPermissions.Read],
        isDefault: true,
    },
    {
        name: DefaultWalletRoleNames.PaymentGatewayOwner,
        permissionActions: [
            WalletPermissions.Read,
            WalletPermissions.Invoice,
            WalletPermissions.Pay,
            WalletPermissions.Update,
            WalletPermissions.ConnectUserToWallet,
            WalletPermissions.PaymentGatewayManagement,
            WalletPermissions.Owner,
        ],
        isDefault: true,
    },
    {
        name: DefaultWalletRoleNames.PaymentGatewayAdmin,
        permissionActions: [
            WalletPermissions.Read,
            WalletPermissions.Invoice,
            WalletPermissions.Pay,
            WalletPermissions.Update,
            WalletPermissions.PaymentGatewayManagement,
            WalletPermissions.ConnectUserToWallet,
        ],
        isDefault: true,
    },
];

export enum DefaultUserOrganizationRoleNames {
    Owner = "organization:owner",
    Admin = "organization:admin",
    Employee = "organization:employee",
    ReadOnly = "organization:read-only"
}

export interface UserOrganizationRole {
    name: string;
    permissions: string[];
    isDefault: boolean;
}

export interface DefaultUserOrganizationRole extends UserOrganizationRole {
    name: DefaultUserOrganizationRoleNames;
    permissions: UserOrganizationPermissions[];
}

export const userOrganizationRoles: DefaultUserOrganizationRole[] = [
    {
        name: DefaultUserOrganizationRoleNames.Owner,
        permissions: [
            UserOrganizationPermissions.Read,
            UserOrganizationPermissions.WalletsRead,
            UserOrganizationPermissions.ConnectUserToOrgWallet,
            UserOrganizationPermissions.ConnectWalletToOrg,
            UserOrganizationPermissions.ConnectUserToOrg,
            UserOrganizationPermissions.Finance,
            UserOrganizationPermissions.UpdateProfile,
            UserOrganizationPermissions.Owner
        ],
        isDefault: true,
    },
    {
        name: DefaultUserOrganizationRoleNames.Admin,
        permissions: [
            UserOrganizationPermissions.Read,
            UserOrganizationPermissions.WalletsRead,
            UserOrganizationPermissions.ConnectUserToOrgWallet,
            UserOrganizationPermissions.ConnectWalletToOrg,
            UserOrganizationPermissions.ConnectUserToOrg,
            UserOrganizationPermissions.Finance,
            UserOrganizationPermissions.UpdateProfile,
        ],
        isDefault: true,
    },
    {
        name: DefaultUserOrganizationRoleNames.Employee,
        permissions: [
            UserOrganizationPermissions.Finance,
            UserOrganizationPermissions.Read
        ],
        isDefault: true,
    },
    {
        name: DefaultUserOrganizationRoleNames.ReadOnly,
        permissions: [UserOrganizationPermissions.Read],
        isDefault: true,
    },
];