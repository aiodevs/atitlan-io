import { Role } from "@prisma/client";
import { PermissionLinkWithPermission, PermissionResult } from "../permission/permission.type";
import { DefaultWalletRoleNames, WalletPermissions } from "../authorization.type";

export type RoleWithPermissions = Role & {
    permissions: PermissionLinkWithPermission[],
}

export type RoleWithPermissionsResult = Role & {
    permissions: PermissionResult[],
}

export function convertRoleToResult(role: RoleWithPermissions): RoleWithPermissionsResult {
    return {
        ...role,
        permissions: role.permissions.map(permission => ({
            id: permission.id,
            resource: permission.permission.resource,
            action: permission.permission.action,
            isDefault: permission.permission.isDefault
        }))
    }
}

export interface WalletRole {
    name: string;
    permissionActions: string[];
    isDefault: boolean;
}

export interface IDefaultWalletRole extends WalletRole {
    name: DefaultWalletRoleNames;
    permissionActions: WalletPermissions[];
}


