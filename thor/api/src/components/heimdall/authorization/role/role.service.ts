import { BadRequest, InternalServerError } from "@/util/constants";
import { getPrisma } from "@/util/prisma";
import { Permission, Role } from "@prisma/client";
import { PermissionResult } from "../permission/permission.type";
import PermissionService from "../permission/permission.service";
import { DefaultWalletRoleNames, defaultWalletRoles, DefaultUserOrganizationRoleNames, userOrganizationRoles, Resources, UserOrganizationPermissions } from "../authorization.type";
import { RoleWithPermissions, RoleWithPermissionsResult } from "./role.types";
import { DefaultUserOrganizationPermissionResult } from "../../organization/organization.types";

export default class RoleService {

    private permissionService: PermissionService = new PermissionService();

    public async createRole(
        roleName: string,
        permissionIds: string[],
        isDefault: boolean = false): Promise<RoleWithPermissions> {
        const role = await getPrisma().role.create({
            data: {
                name: roleName,
                isDefault: isDefault,
                permissions: {
                    create: permissionIds.map(id => ({
                        permission: { connect: { id } }
                    })
                    )
                }
            },
            include: {
                permissions: {
                    include: {
                        permission: true
                    }
                }
            }
        });
        console.log(`role.service.ts:createRole: Created new role ${roleName} with permissions: ${role.permissions.map(p => p.permission.action + " ")}`);
        return role;
    }

    public async updateRoleAndGetOrCreatePermissions(
        roleId: string,
        roleName: string,
        permissions: {
            resource: string,
            action: string,
            isDefault?: boolean
        }[],
        isDefault: boolean,
        canOverrideDefault: boolean = false): Promise<RoleWithPermissions> {
        const permissionsFound = await this.permissionService.getOrCreatePermissions(permissions);
        const permissionIds = permissionsFound.map(p => p.id);
        return await this.updateRole(roleId, roleName, permissionIds, isDefault, canOverrideDefault);
    }

    public async updateRole(
        roleId: string,
        roleName: string,
        permissionIds: string[],
        isDefault: boolean = false,
        canOverrideDefault: boolean = false
    ): Promise<RoleWithPermissions> {
        const role = await this.getRole(roleId);
        if (!role) {
            console.log(`role.service.ts:updateRole: Role with id ${roleId} not found`);
            throw new BadRequest();
        }
        if (role.isDefault && !canOverrideDefault) {
            console.log(`role.service.ts:updateRole: Role with id ${roleId} is default and cannot be updated by this user`);
            throw new BadRequest();
        }
        const updatedRole = await getPrisma().role.update({
            where: { id: roleId },
            data: {
                name: roleName,
                isDefault: isDefault,
                permissions: {
                    deleteMany: [{ roleId: roleId }],
                    create: permissionIds.map(id => ({
                        permission: { connect: { id } }
                    }))
                }
            },
            include: {
                permissions: {
                    include: {
                        permission: true
                    }

                }
            }
        });
        const updatedPermissions: string = updatedRole.permissions.map(p => p.permission.action).join(", ");
        console.log(`role.service.ts:updateRole: Updated role ${roleId} with permissions: ${updatedPermissions}`);
        return updatedRole;
    }

    public async getRole(roleId: string): Promise<RoleWithPermissions | null> {
        const role = await getPrisma().role.findUnique({
            where: {
                id: roleId
            },
            include: {
                permissions: {
                    include: {
                        permission: true
                    }
                }
            }
        });

        if (!role) {
            console.log(`role.service.ts:getRole: Role with id ${roleId} not found`);
        }

        return role;
    }


}