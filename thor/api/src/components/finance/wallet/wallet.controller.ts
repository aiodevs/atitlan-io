import { FastifyRequest } from "fastify";
import { CreateUserWalletBody } from "./wallet.schema";
import WalletService from "./wallet.service.js";
import { Unauthorized } from "@/util/constants";
import { WalletDetails, WalletGeneralInfo } from "./wallet.types";
import { stringToBool } from "@/util/util";
import PermissionService from "@/components/heimdall/authorization/permission/permission.service";
import AuthorizationService from "@/components/heimdall/authorization/authorization.service";
import { Resources, ThorAdminPermissions } from "@/components/heimdall/authorization/authorization.type";

export default class WalletController {
    private walletService: WalletService = new WalletService();
    private permissionService: PermissionService = new PermissionService();
    private authorizationService: AuthorizationService = new AuthorizationService();

    public CreateUserWallet = async (req: FastifyRequest<{ Body: typeof CreateUserWalletBody }>) => {
        if (!req.user) {
            throw new Unauthorized();
        }
        const wallet = await this.walletService.createUserOwnedWallet(
            req.user.id,
            req.body.name
        );
        const walletDetails = await this.walletService.getUserWalletDetails(wallet.id, req.user.id, false, false, false, false);
        return {
            message: "Wallet created",
            wallet: walletDetails,
        };
    }

    public UpdateWallets = async (
        req: FastifyRequest<{
            Body: {
                wallets: {
                    walletId: string,
                    name?: string | null,
                    paymentGateway?: {
                        disabledAt: Date | null,
                    } | null,
                    authorizedUsers?: {
                        userId: string,
                        roleId: string,
                        revokedAt?: Date,
                    }[],
                    authorizedOrganizations?: {
                        organizationId: string,
                        roleId: string,
                        revokedAt?: Date,
                    }[],
                    authorizedOrganizationUsers?: {
                        organizationId: string,
                        userId: string,
                        roleId: string,
                        revokedAt?: Date,
                    }[],
                }[],
            }
        }>) => {
        if (!req.user) {
            throw new Unauthorized();
        }
        const wallets: WalletDetails[] = await this.walletService.updateWallets(
            req.user.id,
            req.body.wallets
        );
        return {
            message: "Wallet updated",
            wallets: wallets,
        };
    }

    public GetAllUserWalletDetail = async (
        req: FastifyRequest<{
            Params: {
                id: string,
            }
            Querystring: { balance: string, transactions: string, invoices: string, intents: string }
        }>) => {
        if (!req.user) {
            throw new Unauthorized();
        }
        const balance = stringToBool(req.query.balance);
        const transactions = stringToBool(req.query.transactions);
        const invoices = stringToBool(req.query.invoices);
        const intents = stringToBool(req.query.intents);
        const walletId = req.params.id;
        const wallet = await this.walletService.getUserWalletDetails(walletId, req.user.id, balance, transactions, invoices, intents);
        return {
            message: "Wallets retrieved",
            wallet: wallet,
        };
    }

    public GetUserWallets = async (
        req: FastifyRequest<{
            Querystring: { balance: string }
        }>) => {
        if (!req.user) {
            throw new Unauthorized();
        }
        const balance = stringToBool(req.query.balance);
        const userId = req.user.id;
        const wallets: WalletGeneralInfo[] = await this.walletService.getUserWallets(userId, balance);
        return {
            message: "Wallets retrieved",
            wallets: wallets,
        };
    }

    public getUserIdWalletDetail = async (
        req: FastifyRequest<{
            Params: { userid: string, walletid: string },
            Querystring: { balance: string, transactions: string, invoices: string, intents: string }
        }>) => {
        if (!req.user) {
            throw new Unauthorized();
        }
        const hasAdminUserWalletAccess = await this.authorizationService.checkUserPermission(req.user.id, Resources.ThorAdmin, ThorAdminPermissions.SeeAllUserWallets);
        if (!hasAdminUserWalletAccess) {
            console.log(`User ${req.user.id} does not have admin permission to see any users wallets`);
            throw new Unauthorized();
        }
        const balance = stringToBool(req.query.balance);
        const transactions = stringToBool(req.query.transactions);
        const invoices = stringToBool(req.query.invoices);
        const intents = stringToBool(req.query.intents);
        const userId = req.params.userid;
        const walletId = req.params.walletid;
        const wallet = await this.walletService.getUserWalletDetails(walletId, userId, balance, transactions, invoices, intents);
        return {
            message: "Wallets retrieved",
            wallet: wallet,
        };
    }

    public GetAllUserIdWallets = async (
        req: FastifyRequest<{
            Params: { userid: string },
            Querystring: { balance: string }
        }>) => {
        if (!req.user) {
            throw new Unauthorized();
        }
        const hasAdminUserWalletAccess = await this.authorizationService.checkUserPermission(req.user.id, Resources.ThorAdmin, ThorAdminPermissions.SeeAllUserWallets);
        if (!hasAdminUserWalletAccess) {
            console.log(`User ${req.user.id} does not have admin permission to see any users wallets`);
            throw new Unauthorized();
        }
        const balance = stringToBool(req.query.balance);
        const userId = req.params.userid;
        const wallets = await this.walletService.getUserWallets(userId, balance);
        return {
            message: "Wallets retrieved",
            wallets: wallets,
        };
    }
}
