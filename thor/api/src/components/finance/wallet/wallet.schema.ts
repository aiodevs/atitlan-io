import { Type } from "@fastify/type-provider-typebox";
import { FastifySchema } from "fastify";
import { ERROR400, ERROR401, ERROR404, ERROR409, ERROR500, responseProperty } from "@/util/constants";
import { BalanceType, ExtendedWalletDetailsType, ExtendedWalletGeneralInfoType, WalletType } from "../finance.schema.types";

export const CreateUserWalletBody = Type.Object({
    name: Type.String()
});

export const GetAllUserWalletsSchema: FastifySchema = {
    description: 'Get all user wallets',
    tags: ['user', 'wallet'],
    querystring: Type.Object({
        balance: Type.Optional(Type.String()),
    }),
    response: {
        200: {
            description: 'Successful get response',
            type: 'object',
            properties: {
                ...responseProperty,
                wallets: {
                    type: 'array',
                    items: ExtendedWalletGeneralInfoType
                }
            }
        }
    },
    security: [{ cookieAuth: [] }]
}

export const GetUserWalletDetailSchema: FastifySchema = {
    description: 'Get a specific wallet.',
    tags: ['user', 'wallet'],
    params: Type.Object({
        id: Type.String()
    }),
    querystring: Type.Object({
        balance: Type.Optional(Type.String()),
        transactions: Type.Optional(Type.String()),
        invoices: Type.Optional(Type.String()),
        intents: Type.Optional(Type.String())
    }),
    response: {
        200: {
            description: 'Successful get response',
            type: 'object',
            properties: {
                ...responseProperty,
                wallet: ExtendedWalletDetailsType
            }
        },
        404: ERROR404,
        401: ERROR401
    },
    security: [{ cookieAuth: [] }]
}

export const GetAllUserIdWalletsSchema: FastifySchema = {
    description: 'Get all wallets of a specific user, requires elevated permissions (thor-api - user.wallet.get)',
    tags: ['wallet'],
    params: Type.Object({
        userid: Type.String()
    }),
    querystring: Type.Object({
        balance: Type.Optional(Type.String()),
    }),
    response: {
        200: {
            description: 'Successful get response',
            type: 'object',
            properties: {
                ...responseProperty,
                wallets: {
                    type: 'array',
                    items: ExtendedWalletGeneralInfoType
                }
            }
        },
        401: ERROR401,
        404: ERROR404
    },
    security: [{ cookieAuth: [] }]
}

export const GetUserIdWalletDetailSchema: FastifySchema = {
    description: 'Get a specific wallet of a specific user, requires elevated permissions (thor-api - user.wallet.get)',
    tags: ['wallet'],
    params: Type.Object({
        userid: Type.String(),
        walletid: Type.String()
    }),
    querystring: Type.Object({
        balance: Type.Optional(Type.String()),
        transactions: Type.Optional(Type.String()),
        invoices: Type.Optional(Type.String()),
        intents: Type.Optional(Type.String())
    }),
    response: {
        200: {
            description: 'Successful get response',
            type: 'object',
            properties: {
                ...responseProperty,
                wallet: ExtendedWalletDetailsType
            }
        },
        401: ERROR401,
        404: ERROR404
    },
    security: [{ cookieAuth: [] }]
}

export const CreateUserWalletSchema: FastifySchema = {
    description: 'Create a user wallet',
    tags: ['wallet'],
    body: CreateUserWalletBody,
    response: {
        200: {
            description: 'Successful create response',
            type: 'object',
            properties: {
                ...responseProperty,
                wallet: ExtendedWalletDetailsType
            }
        },
        400: ERROR400,
        409: ERROR409,
        500: ERROR500
    },
    security: [{ cookieAuth: [] }]
}

export const UpdateWalletSchema: FastifySchema = {
    description: 'Update a wallet',
    tags: ['wallet'],
    body: Type.Object({
        wallets: Type.Array(Type.Object({
            walletId: Type.String({ description: 'The ID of the wallet' }),
            name: Type.Optional(Type.String({ description: 'The new name of the wallet' })),
            paymentGateway: Type.Optional(Type.Object({
                disabledAt: Type.Optional(Type.String({ description: 'The date when the payment gateway was disabled' }))
            })),
            authorizedUsers: Type.Optional(Type.Array(Type.Object({
                userId: Type.String({ description: 'The ID of the user' }),
                roleId: Type.String({ description: 'The ID of the role' }),
                revokedAt: Type.Optional(Type.String({ description: 'The date when the role was revoked' }))
            }))),
            authorizedOrganizations: Type.Optional(Type.Array(Type.Object({
                organizationId: Type.String({ description: 'The ID of the organization' }),
                roleId: Type.String({ description: 'The ID of the role' }),
                revokedAt: Type.Optional(Type.String({ description: 'The date when the role was revoked' }))
            }))),
            authorizedOrganizationUsers: Type.Optional(Type.Array(Type.Object({
                organizationId: Type.String({ description: 'The ID of the organization' }),
                userId: Type.String({ description: 'The ID of the user' }),
                roleId: Type.String({ description: 'The ID of the role' }),
                revokedAt: Type.Optional(Type.String({ description: 'The date when the role was revoked' }))
            })))
        }))
    }),
    response: {
        200: {
            description: 'Successful post response',
            type: 'object',
            properties: {
                ...responseProperty,
                message: Type.String(),
                wallets: Type.Array(ExtendedWalletDetailsType)
            }
        }
    },
    security: [{ cookieAuth: [] }]
}