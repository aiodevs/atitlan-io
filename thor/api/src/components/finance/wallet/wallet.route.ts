import { FastifyInstance, RouteOptions } from "fastify";
import { Routes } from "@/plugins/initializeRoutes";
import WalletController from "./wallet.controller.js";
import { CreateUserWalletSchema, GetAllUserWalletsSchema, GetUserWalletDetailSchema, UpdateWalletSchema, GetAllUserIdWalletsSchema, GetUserIdWalletDetailSchema } from "./wallet.schema.js";
import AtitlanApiRoutes from "@/components/shared/routes";


export default class UserRoutes implements Routes {
    public walletController = new WalletController();

    public initializeRoutes(
        fastify: FastifyInstance,
        opts: RouteOptions,
        done: () => void
    ) {
        fastify.route({
            method: "post",
            url: AtitlanApiRoutes.WALLET.CREATE,
            schema: CreateUserWalletSchema,
            preHandler: fastify.authenticateUser,
            handler: this.walletController.CreateUserWallet
        });

        fastify.route({
            method: "post",
            url: AtitlanApiRoutes.WALLET.UPDATE,
            schema: UpdateWalletSchema,
            preHandler: fastify.authenticateUser,
            handler: this.walletController.UpdateWallets
        });

        fastify.route({
            method: "get",
            url: AtitlanApiRoutes.USER.WALLETS_URL,
            schema: GetAllUserWalletsSchema,
            preHandler: fastify.authenticateUser,
            handler: this.walletController.GetUserWallets
        });

        fastify.route({
            method: "get",
            url: AtitlanApiRoutes.USER.WALLET_DETAILS_URL,
            schema: GetUserWalletDetailSchema,
            preHandler: fastify.authenticateUser,
            handler: this.walletController.GetAllUserWalletDetail
        });

        fastify.route({
            method: "get",
            url: AtitlanApiRoutes.WALLET.GET_USER_WALLETS_URL,
            schema: GetAllUserIdWalletsSchema,
            preHandler: fastify.authenticateUser,
            handler: this.walletController.GetAllUserIdWallets
        });

        fastify.route({
            method: "get",
            url: AtitlanApiRoutes.WALLET.GET_USER_WALLET_DETAILS_URL,
            schema: GetUserIdWalletDetailSchema,
            preHandler: fastify.authenticateUser,
            handler: this.walletController.getUserIdWalletDetail
        });

        done();
    }
}
