import { BadRequest, NotFound, Unauthorized } from '@/util/constants';
import { getPrisma } from '@/util/prisma';
import Decimal from 'decimal.js';
import { convertRoleToResult } from '@/components/heimdall/authorization/role/role.types';
import { PaymentGateway, Wallet } from '@prisma/client';
import { WalletGeneralInfo, WalletDetails, InvoiceWithCompletedTransactions, AuthorizedOrganizationUsersType, AuthorizedOrganizations, authorizedUsers, BalanceSheet, WalletWithTransactionsType, UserOrganizationWalletAccessLinkWithWallet } from './wallet.types';
import AuthorizationService from '@/components/heimdall/authorization/authorization.service';
import { DefaultWalletRoleNames, Resources, UserOrganizationPermissions, WalletPermissions } from '../../heimdall/authorization/authorization.type';

export enum OwnerType {
    Organization,
    User
}
export type RoleWallet = {
    wallet: WalletGeneralInfo,
    roleId: string
}

export default class WalletService {

    private authorizationService: AuthorizationService = new AuthorizationService();

    public async createUserOwnedWallet(owneruserId: string, walletName: string, isPaymentGateway: boolean = false) {
        const roleName = isPaymentGateway ? DefaultWalletRoleNames.PaymentGatewayOwner : DefaultWalletRoleNames.Owner;
        const ownerRole = await this.authorizationService.getOrCreateDefaultWalletRole(roleName);
        const wallet = await getPrisma().wallet.create({
            data: {
                name: walletName,
                authorizedUsers: {
                    create: [{
                        userId: owneruserId,
                        roleId: ownerRole.id,
                    }]
                }
            },
        });
        console.log(`createUserOwnedWallet - Wallet ${wallet.id} created for user ${owneruserId}`);
        return wallet;
    }

    public async createOrganizationOwnedWallet(
        ownerOrganizationId: string,
        adminUserId: string,
        walletName: string,
        isPaymentGateway: boolean = false): Promise<WalletGeneralInfo> {
        const org = await getPrisma().organization.findUnique({
            where: {
                id: ownerOrganizationId,
            },
        });
        if (!org) {
            console.log(`createOrganizationOwnedWallet - Organization ${ownerOrganizationId} not found`);
            throw new BadRequest();
        }
        const organizationRoleName = isPaymentGateway ? DefaultWalletRoleNames.PaymentGatewayOwner : DefaultWalletRoleNames.Owner;
        const userRoleName = isPaymentGateway ? DefaultWalletRoleNames.PaymentGatewayAdmin : DefaultWalletRoleNames.Admin;
        const orgRole = await this.authorizationService.getOrCreateDefaultWalletRole(organizationRoleName);
        const userRole = await this.authorizationService.getOrCreateDefaultWalletRole(userRoleName);
        const wallet = await getPrisma().wallet.create({
            data: {
                name: walletName,
                authorizedOrganizations: {
                    create: [{
                        organizationId: ownerOrganizationId,
                        roleId: orgRole.id,
                    }]
                },
                authorizedOrganizationUsers: {
                    create: [{
                        organizationId: ownerOrganizationId,
                        userId: adminUserId,
                        roleId: userRole.id,
                    }]
                }
            },
            include: {
                paymentGateway: true,
            }
        });
        console.log(`createOrganizationOwnedWallet - Wallet ${wallet.id} created for organization ${ownerOrganizationId}`);
        const generalInfo: WalletGeneralInfo = {
            id: wallet.id,
            createdAt: wallet.createdAt,
            updatedAt: wallet.updatedAt,
            lastUsed: wallet.lastUsed,
            name: wallet.name,
            paymentGateway: wallet.paymentGateway,
            organizationName: org.name,
        }
        return generalInfo;
    }

    public async createAnonymousWallet(): Promise<Wallet> {
        const wallet = await getPrisma().wallet.create({
            data: {
                name: "Anonymous Wallet",
            }
        });
        return wallet;
    }

    public async createPaymentGatewayWallet(
        walletName: string,
        ownerType: OwnerType,
        userId: string,
        organizationId: string | null = null
    ) {
        switch (ownerType) {
            case OwnerType.Organization:
                if (!organizationId) throw new BadRequest();
                return await this.createOrganizationOwnedWallet(organizationId, userId, walletName, true);
            case OwnerType.User:
                return await this.createUserOwnedWallet(userId, walletName, true);
            default:
                throw new Error(`Invalid ownerType ${ownerType}`);
        }
    }

    public async getUserWallets(
        userId: string,
        includeBalance: boolean = true
    ): Promise<WalletGeneralInfo[]> {
        const walletLinks = await this.getAllUnrevokedUserWalletAccessLinks(userId);
        const authorizedReadWalletLinks = walletLinks.filter(link =>
            this.authorizationService.hasPermission(
                link.roleId,
                Resources.Wallet,
                WalletPermissions.Read
            )
        );
        const walletsInfo: WalletGeneralInfo[] = [];
        for (const link of authorizedReadWalletLinks) {
            const walletInfo: WalletGeneralInfo = link.wallet;

            if (includeBalance) {
                walletInfo.balance = await this.getWalletBalance(link.wallet.id);
            }

            walletsInfo.push(walletInfo);
        }
        console.log(`getUserWallets for user ${userId} found ${walletsInfo.length} wallets`);
        return walletsInfo;
    }

    public async getUserOrganizationWallets(
        userId: string,
        organizationId: string,
        includeBalance: boolean = true
    ): Promise<WalletGeneralInfo[]> {
        const walletLinks = await this.getUnrevokedUserOrganizationWalletAccessLinks(userId, organizationId);
        const authorizedReadWalletLinks = walletLinks.filter(link =>
            this.authorizationService.hasPermission(
                link.roleId,
                Resources.Wallet,
                WalletPermissions.Read
            )
        );

        const walletsInfo: WalletGeneralInfo[] = [];
        for (const link of authorizedReadWalletLinks) {
            const walletInfo: WalletGeneralInfo = link.wallet;

            if (includeBalance) {
                walletInfo.balance = await this.getWalletBalance(link.wallet.id);
            }

            walletsInfo.push(walletInfo);
        }
        console.log(`getUserOrganizationWallets for user ${userId} in organization ${organizationId} found ${walletsInfo.length} wallets`)
        return walletsInfo;
    }

    private async getAllUnrevokedUserWalletAccessLinks(userId: string): Promise<RoleWallet[]> {
        const userWalletlinks = await getPrisma().userWalletRoleAccessLink.findMany({
            where: {
                userId: userId,
                OR: [{
                    revokedAt: { equals: null }
                }, {
                    revokedAt: { gt: new Date() }
                }]
            },
            include: {
                wallet: {
                    include: {
                        paymentGateway: true
                    }
                },
                role: true
            }
        });
        const userOrgWalletLinks = await getPrisma().organizationUserWalletRoleAccessLink.findMany({
            where: {
                userId: userId,
                OR: [{
                    revokedAt: { equals: null }
                }, {
                    revokedAt: { gt: new Date() }
                }]
            },
            include: {
                wallet: {
                    include: {
                        paymentGateway: true
                    }
                },
                role: true,
                organization: true
            }
        });
        const userRoleWallets = userWalletlinks.map(link => ({
            wallet: link.wallet,
            roleId: link.roleId
        }));

        const orgUserRoleWallets = userOrgWalletLinks.map(link => ({
            wallet: {
                ...link.wallet,
                organizationName: link.organization.name
            },
            roleId: link.roleId
        }));

        return [...userRoleWallets, ...orgUserRoleWallets];
    }

    private async getUnrevokedUserOrganizationWalletAccessLinks(userId: string, organizationId: string)
        : Promise<UserOrganizationWalletAccessLinkWithWallet[]> {
        const walletLinks = await getPrisma().organizationUserWalletRoleAccessLink.findMany({
            where: {
                userId: userId,
                organizationId: organizationId,
                OR: [{
                    revokedAt: { equals: null }
                }, {
                    revokedAt: { gt: new Date() }
                }]
            },
            include: {
                wallet: {
                    include: {
                        paymentGateway: true
                    }
                },
                role: true
            }
        });
        return walletLinks as UserOrganizationWalletAccessLinkWithWallet[];
    }

    public async getUserWalletDetails(
        walletId: string,
        userId: string,
        includeBalance: boolean = true,
        includeTransactions: boolean = false,
        includeInvoices: boolean = true,
        includeIntents: boolean = false,
    ): Promise<WalletDetails | undefined> {
        const hasReadPermission = await this.authorizationService.userHasAnyWalletPermission(userId, walletId, WalletPermissions.Read);
        if (!hasReadPermission) {
            console.log(`User ${userId} does not have read access to wallet ${walletId}`);
            throw new Unauthorized();
        }
        const walletDetails = await this.getWalletDetailsNoAuthorization(walletId, includeBalance, includeTransactions, includeInvoices, includeIntents);
        console.log(`getUserWalletDetails for wallet ${walletId} and user ${userId} with includeBalance: ${includeBalance}, includeTransactions: ${includeTransactions}, includeInvoices: ${includeInvoices}`)
        return walletDetails;
    }

    public async getUserOrganizationWalletDetails(
        walletId: string,
        userId: string,
        organizationId: string,
        includeBalance: boolean = true,
        includeTransactions: boolean = false,
        includeInvoices: boolean = true,
        includeIntents: boolean = false,
    ): Promise<WalletDetails | undefined> {
        const hasReadPermission = await this.authorizationService.userHasOrganizationWalletPermission(userId, organizationId, walletId, WalletPermissions.Read);
        if (!hasReadPermission) {
            console.log(`User ${userId} does not have read access to wallet ${walletId} ` +
                `in organization ${organizationId}`);
            throw new Unauthorized();
        }
        const walletDetails = await this.getWalletDetailsNoAuthorization(walletId, includeBalance, includeTransactions, includeInvoices, includeIntents);
        console.log(`getUserOrganizationWalletDetails for wallet ${walletId} and user ${userId} in organization ${organizationId} with includeBalance: ${includeBalance}, includeTransactions: ${includeTransactions}, includeInvoices: ${includeInvoices}`)
        return walletDetails;
    }

    public async getWalletDetailsNoAuthorization(
        walletId: string,
        includeBalance: boolean = true,
        includeTransactions: boolean = false,
        includeInvoices: boolean = true,
        includeIntents: boolean = false,
    ): Promise<WalletDetails | undefined> {
        const includeTransactionsInPrisma: boolean = includeBalance || includeTransactions;
        const wallet = await getPrisma().wallet.findUnique({
            where: {
                id: walletId,
            },
            include: {
                paymentGateway: true,
                incomingTransactions: true,
                outgoingTransactions: includeTransactionsInPrisma ? true : false,
                invoices: includeInvoices ? {
                    include: {
                        completedTransactions: {
                            include: {
                                transaction: true,
                                transactionIntent: true
                            }
                        }
                    }
                } : undefined,
                transactionIntents: includeIntents ? true : false,
                authorizedOrganizations: {
                    include: {
                        organization: true,
                        role: {
                            include: {
                                permissions: {
                                    include: {
                                        permission: true
                                    }
                                }
                            }
                        }
                    }
                },
                authorizedOrganizationUsers: {
                    include: {
                        organization: true,
                        user: true,
                        role: {
                            include: {
                                permissions: {
                                    include: {
                                        permission: true
                                    }
                                }
                            }
                        }
                    }
                },
                authorizedUsers: {
                    include: {
                        user: true,
                        role: {
                            include: {
                                permissions: {
                                    include: {
                                        permission: true
                                    }
                                }
                            }
                        }
                    }
                },
            },
        });
        if (!wallet) {
            console.log(`Wallet ${walletId} not found`);
            return undefined;
        }
        const balance = includeBalance
            ? await this.createWalletBalanceSheet(
                wallet as WalletWithTransactionsType)
            : undefined;
        const remappedAuthorizedOrganizationUsers: AuthorizedOrganizationUsersType[] = wallet.authorizedOrganizationUsers.map(link => ({
            ...link,
            organization: link.organization,
            user: link.user,
            role: convertRoleToResult(link.role)
        }));
        const remappedAuthorizedOrganizations: AuthorizedOrganizations[] = wallet.authorizedOrganizations.map(link => ({
            ...link,
            organization: link.organization,
            role: convertRoleToResult(link.role)
        }));
        const remappedAuthorizedUsers: authorizedUsers[] = wallet.authorizedUsers.map(link => ({
            ...link,
            user: link.user,
            role: convertRoleToResult(link.role)
        }));
        const defaultWalletRoles = await this.authorizationService.getDefaultWalletRoles();
        const walletDetails: WalletDetails = {
            ...wallet,
            balance,
            paymentGateway: wallet?.paymentGateway as PaymentGateway,
            incomingTransactions: wallet?.incomingTransactions || [],
            outgoingTransactions: wallet?.outgoingTransactions || [],
            invoices: wallet?.invoices as InvoiceWithCompletedTransactions[] || [],
            transactionIntents: wallet?.transactionIntents || [],
            authorizedOrganizationUsers: remappedAuthorizedOrganizationUsers,
            authorizedOrganizations: remappedAuthorizedOrganizations,
            authorizedUsers: remappedAuthorizedUsers,
            defaultWalletRoles: defaultWalletRoles,
        };
        return walletDetails;
    }

    public async getWalletByIdNoAuthorization(id: string) {
        try {
            const wallet = await getPrisma().wallet.findUnique({
                where: {
                    id: id,
                },
            });
            return wallet;
        } catch (err) {
            console.log(`getWalletById - Error for ${id}`);
            return null;
        }
    }

    public async getWalletBalance(walletId: string): Promise<BalanceSheet> {
        const wallet: WalletWithTransactionsType | null = await getPrisma().wallet.findUnique({
            where: {
                id: walletId,
            },
            include: {
                incomingTransactions: true,
                outgoingTransactions: true,
            },
        });

        if (!wallet) {
            throw new NotFound();
        }

        return this.createWalletBalanceSheet(wallet);
    }

    private async createWalletBalanceSheet(wallet: WalletWithTransactionsType): Promise<BalanceSheet> {
        let balanceSheet: BalanceSheet = {};

        if (wallet.incomingTransactions) {
            wallet.incomingTransactions.forEach(tx => {
                balanceSheet[tx.targetCurrency] = balanceSheet[tx.targetCurrency] || { incoming: new Decimal(0), outgoing: new Decimal(0), net: new Decimal(0) };
                balanceSheet[tx.targetCurrency].incoming = balanceSheet[tx.targetCurrency].incoming.plus(tx.targetAmount);
            });
        }

        if (wallet.outgoingTransactions) {
            wallet.outgoingTransactions.forEach(tx => {
                balanceSheet[tx.sourceCurrency] = balanceSheet[tx.sourceCurrency] || { incoming: new Decimal(0), outgoing: new Decimal(0), net: new Decimal(0) };
                balanceSheet[tx.sourceCurrency].outgoing = balanceSheet[tx.sourceCurrency].outgoing.plus(tx.sourceAmount);
            });
        }

        Object.keys(balanceSheet).forEach(currency => {
            const incoming: Decimal = balanceSheet[currency].incoming || new Decimal(0);
            const outgoing: Decimal = balanceSheet[currency].outgoing || new Decimal(0);
            balanceSheet[currency].net = incoming.minus(outgoing);
        });

        return balanceSheet;
    }

    public async updateWallets(
        requestingUserId: string,
        wallets: {
            walletId: string,
            name?: string | null,
            paymentGateway?: {
                disabledAt: Date | null,
            } | null,
            authorizedUsers?: {
                userId: string,
                roleId: string,
                revokedAt?: Date,
            }[],
            authorizedOrganizations?: {
                organizationId: string,
                roleId: string,
                revokedAt?: Date,
            }[],
            authorizedOrganizationUsers?: {
                organizationId: string,
                userId: string,
                roleId: string,
                revokedAt?: Date,
            }[],
        }[]
    ): Promise<WalletDetails[]> {
        const updateWalletDetailInfo: WalletDetails[] = [];
        for (const wallet of wallets) {
            const userHasWalletUpdatePermission = await this.authorizationService.userHasAnyWalletPermission(requestingUserId, wallet.walletId, WalletPermissions.Update);
            if (!userHasWalletUpdatePermission) {
                console.log(`updateWallets - User ${requestingUserId} does not have ` +
                    `update access to wallet ${wallet.walletId}`);
                continue;
            } else {
                if (wallet.name) {
                    await getPrisma().wallet.update({
                        where: {
                            id: wallet.walletId,
                        },
                        data: {
                            name: wallet.name,
                        },
                    });
                    console.log(`updateWallets - Wallet ${wallet.walletId} updated name ` +
                        `to ${wallet.name}`);
                }
                if (wallet.paymentGateway) {
                    await getPrisma().wallet.update({
                        where: {
                            id: wallet.walletId,
                        },
                        data: {
                            paymentGateway: {
                                update: {
                                    disabledAt: wallet.paymentGateway.disabledAt,
                                }
                            }
                        },
                    });
                    console.log(`updateWallets - Wallet ${wallet.walletId} paymentGateway `
                        + `disabledAt: ${wallet.paymentGateway.disabledAt}`);
                }
            }
            if (wallet.authorizedUsers) {
                const newRolesRemapped = wallet.authorizedUsers.map(link => ({
                    walletId: wallet.walletId,
                    userId: link.userId,
                    roleId: link.roleId,
                    revokedAt: link.revokedAt,
                }));
                await this.connectAndUpdateUserWalletRole(requestingUserId, newRolesRemapped);
            }
            if (wallet.authorizedOrganizations) {
                const newRolesRemapped = wallet.authorizedOrganizations.map(link => ({
                    walletId: wallet.walletId,
                    organizationId: link.organizationId,
                    roleId: link.roleId,
                    revokedAt: link.revokedAt,
                }));
                await this.connectAndUpdateOrganizationWalletLinks(
                    requestingUserId,
                    newRolesRemapped);
            }
            if (wallet.authorizedOrganizationUsers) {
                const newRolesRemapped = wallet.authorizedOrganizationUsers.map(link => ({
                    walletId: wallet.walletId,
                    organizationId: link.organizationId,
                    userId: link.userId,
                    roleId: link.roleId,
                    revokedAt: link.revokedAt,
                }));
                await this.connectAndUpdateUserOrganizationWalletLink(
                    requestingUserId,
                    newRolesRemapped);
            }
            const walletDetails: WalletDetails | undefined = await this.getWalletDetailsNoAuthorization(wallet.walletId);
            if (walletDetails)
                updateWalletDetailInfo.push(walletDetails);
        }
        console.log(`updateWallets - Updated ${updateWalletDetailInfo.length} wallets for ${requestingUserId}`);
        return updateWalletDetailInfo;
    }

    public async connectAndUpdateUserWalletDefaultRole(
        requestUserId: string,
        newRoles: {
            walletId: string,

            userId: string,
            roleName: DefaultWalletRoleNames,
        }[]) {
        const newRolesRemapped: {
            walletId: string,

            userId: string,
            roleId: string,
        }[] = [];
        for (const newRole of newRoles) {
            const role = await this.authorizationService.getOrCreateDefaultWalletRole(newRole.roleName);
            newRolesRemapped.push({
                userId: newRole.userId,
                roleId: role.id,
                walletId: newRole.walletId,
            });
        }
        return await this.connectAndUpdateUserWalletRole(requestUserId, newRolesRemapped);
    }

    public async connectAndUpdateUserWalletRole(
        requestUserId: string,
        links: {
            walletId: string,
            userId: string,
            roleId: string,
            revokedAt?: Date,
        }[]) {

        for (const link of links) {
            const userHasWalletConnectPermission = await this.authorizationService.userHasAnyWalletPermission(
                requestUserId,
                link.walletId,
                WalletPermissions.ConnectUserToWallet);
            if (!userHasWalletConnectPermission) {
                console.log(`connectAndUpdateUserWalletRole - User ${requestUserId} does not have connect access to wallet ${link.walletId}`);
                continue;
            }
            const currentLinks = await getPrisma().userWalletRoleAccessLink.findMany({
                where: {
                    walletId: link.walletId,
                    userId: link.userId,
                }
            });
            const currentLink = currentLinks.find(link => link.userId === link.userId);
            if (currentLink) {
                await getPrisma().userWalletRoleAccessLink.update({
                    where: {
                        id: currentLink.id,
                    },
                    data: {
                        roleId: link.roleId,
                        walletId: link.walletId,
                        userId: link.userId,
                        revokedAt: link.revokedAt,
                    },
                });
                if (link.revokedAt) {
                    // Do we want to allow setting it so that the wallet has no owner?
                    console.log(`connectAndUpdateUserWalletRole - User ${link.userId} revoked ` +
                        `access to wallet ${link.walletId}`);
                } else {
                    console.log(`connectAndUpdateUserWalletRole - User ${link.userId} updated ` +
                        `connection to wallet ${link.walletId} with role ${link.roleId}`);
                }
                continue;
            }

            await getPrisma().userWalletRoleAccessLink.create({
                data: {
                    userId: link.userId,
                    walletId: link.walletId,
                    roleId: link.roleId,
                    revokedAt: link.revokedAt,
                },
            });

            if (link.revokedAt) {
                console.log(`connectAndUpdateUserWalletRole - User ${link.userId} connected ` +
                    `to wallet ${link.walletId} and revokedAt: ${link.revokedAt}.  ` +
                    `Which is a bit strange`);

            }
            console.log(`connectAndUpdateUserWalletRole - User ${link.userId} connected to ` +
                `wallet ${link.walletId} with role ${link.roleId} and revokedAt: ${link.revokedAt}`);
        }
    }

    public async connectAndUpdateUserOrganizationWalletLink(
        requestUserId: string,
        newLinks: {
            organizationId: string,
            walletId: string,
            userId: string,
            roleId: string | null,
            revokedAt?: Date,
        }[]) {

        for (const newLink of newLinks) {
            const userHasWalletConnectPermission = await this.authorizationService.userHasOrganizationWalletPermission(
                requestUserId,
                newLink.organizationId,
                newLink.walletId,
                WalletPermissions.ConnectUserToWallet);
            if (!userHasWalletConnectPermission) {
                console.log(`connectAndUpdateOrganizationWalletRole - User ${requestUserId} does not have connect access to wallet ${newLink.walletId} in organization ${newLink.organizationId}`);
                throw new Unauthorized();
            }

            const currentLinks = await getPrisma().organizationUserWalletRoleAccessLink.findMany({
                where: {
                    walletId: newLink.walletId,
                    organizationId: newLink.organizationId,
                }
            });

            const currentLink = currentLinks.find(link => link.userId === newLink.userId);
            if (currentLink) {
                if (newLink.revokedAt) {
                    // Do we want to allow setting it so that the wallet has no owner?
                    await getPrisma().organizationUserWalletRoleAccessLink.update({
                        where: {
                            id: currentLink.id,
                        },
                        data: {
                            roleId: newLink.roleId ? newLink.roleId : currentLink.roleId,
                            walletId: newLink.walletId,
                            userId: newLink.userId,
                            revokedAt: newLink.revokedAt,
                        },
                    });
                    console.log(`connectAndUpdateOrganizationWalletRole - User ${newLink.userId} revoked access to wallet ${newLink.walletId} in organization ${newLink.organizationId}`);
                    return;
                }
                await getPrisma().organizationUserWalletRoleAccessLink.update({
                    where: {
                        id: currentLink.id,
                    },
                    data: {
                        roleId: newLink.roleId ? newLink.roleId : currentLink.roleId,
                    },
                });
                console.log(`connectAndUpdateOrganizationWalletRole - User ${newLink.userId} updated connection to wallet ${newLink.walletId} in organization ${newLink.organizationId} with role ${newLink.roleId}`);
                return;
            }
            if (newLink.revokedAt) {
                console.log(`connectAndUpdateOrganizationWalletRole - User ${newLink.userId} does not have a connection to wallet ${newLink.walletId} in organization ${newLink.organizationId} to revoke`);
                return;
            }

            if (!newLink.roleId) {
                console.log(`connectAndUpdateOrganizationWalletRole - User ${newLink.userId} does not have a roleId to connect to wallet ${newLink.walletId} in organization ${newLink.organizationId}`);
                return;
            }
            await getPrisma().organizationUserWalletRoleAccessLink.create({
                data: {
                    userId: newLink.userId,
                    walletId: newLink.walletId,
                    organizationId: newLink.organizationId,
                    roleId: newLink.roleId,
                },
            });
            console.log(`connectAndUpdateOrganizationWalletRole - User ${newLink.userId} connected to wallet ${newLink.walletId} in organization ${newLink.organizationId} with role ${newLink.roleId}`);
        }
    }

    public async connectAndUpdateOrganizationWalletLinks(
        requestingUserId: string,
        newLinks: {
            organizationId: string,
            walletId: string,
            roleId: string,
            revokedAt?: Date,
        }[],
    ): Promise<void> {
        for (const newLink of newLinks) {
            const organization = await getPrisma().organization.findUnique({
                where: {
                    id: newLink.organizationId,
                },
            });

            if (!organization) {
                console.log(`Thor API - connectAndUpdateOrganizationWalletLinks - Organization not found ${newLink.organizationId}`)
                continue;
            }

            const wallet = await getPrisma().wallet.findUnique({
                where: {
                    id: newLink.walletId,
                }
            });

            if (!wallet) {
                console.log(`Thor API - connectAndUpdateOrganizationWalletLinks - Wallet not found ${newLink.walletId}`)
                continue;
            }

            const hasOrganizationPermission = await this.authorizationService.userHasOrganizationPermission(
                requestingUserId,
                newLink.organizationId,
                UserOrganizationPermissions.ConnectWalletToOrg
            );

            if (!hasOrganizationPermission) {
                console.log(`Thor API - connectAndUpdateOrganizationWalletLinks - User ${requestingUserId} not authorized to connect organization wallets ${newLink.organizationId}`);
                continue;
            }

            const existingLink = await getPrisma().organizationWalletRoleAccessLink.findFirst({
                where: {
                    walletId: newLink.walletId,
                    organizationId: newLink.organizationId,
                }
            });
            if (existingLink) {
                // Do we want to allow setting it so that the wallet has no owner?
                await getPrisma().organizationWalletRoleAccessLink.update({
                    where: {
                        id: existingLink.id,
                    },
                    data: {
                        revokedAt: newLink.revokedAt,
                        roleId: newLink.roleId,
                        organizationId: newLink.organizationId,
                        walletId: newLink.walletId,
                    }
                });
                if (newLink.revokedAt) {
                    console.log(`Thor API - connectAndUpdateOrganizationWalletLinks - Wallet ${newLink.walletId} revokedAt ${newLink.revokedAt} from organization ${newLink.organizationId}`);
                } else {
                    console.log(`Thor API - connectAndUpdateOrganizationWalletLinks - Wallet ${newLink.walletId} updated to organization ${newLink.organizationId}`);
                }
            }

            await getPrisma().organizationWalletRoleAccessLink.create({
                data: {
                    organizationId: newLink.organizationId,
                    walletId: newLink.walletId,
                    roleId: newLink.roleId,
                    revokedAt: newLink.revokedAt,
                }
            });

            if (newLink.revokedAt) {
                console.log(`Thor API - connectAndUpdateOrganizationWalletLinks - Wallet ${newLink.walletId} revokedAt ${newLink.revokedAt} from organization ${newLink.organizationId}`);
                console.log(`Thor API - connectAndUpdateOrganizationWalletLinks - Also a bit strange that a new connection is revoked`);
                continue;
            }
            console.log(`Thor API - connectAndUpdateOrganizationWalletLinks - Wallet ${newLink.walletId} connected to organization ${newLink.organizationId}`);
        }
    }
}
