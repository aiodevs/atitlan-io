import { RoleWithPermissionsResult } from "@/components/heimdall/authorization/role/role.types";
import { OrganizationUserWalletRoleAccessLink, Organization, User, OrganizationWalletRoleAccessLink, UserWalletRoleAccessLink, Wallet, PaymentGateway, CompletedTransactionLink, Transaction, TransactionIntent, Invoice } from "@prisma/client";
import Decimal from "decimal.js";

export interface CurrencyBalance {
    incoming: Decimal;
    outgoing: Decimal;
    net: Decimal;
}

export interface BalanceSheet {
    [currency: string]: CurrencyBalance;
}

// export type DefaultWalletRoleResult = {
//     name: DefaultWalletRoleNames,
//     id: string,
//     isDefault: boolean,
//     permissions: { permissionId: string, resource: string, action: string, isDefault: boolean }[]
// }

export type AuthorizedOrganizationUsersType = OrganizationUserWalletRoleAccessLink & {
    organization: Organization,
    user: User,
    role: RoleWithPermissionsResult
};

export type AuthorizedOrganizations = OrganizationWalletRoleAccessLink & {
    organization: Organization,
    role: RoleWithPermissionsResult
};

export type authorizedUsers = UserWalletRoleAccessLink & {
    user: User,
    role: RoleWithPermissionsResult
};

export type WalletGeneralInfo = Wallet & {
    balance?: BalanceSheet,
    paymentGateway?: PaymentGateway | null,
    organizationName?: string
};

export type CompletedTransaction = CompletedTransactionLink & {
    transaction: Transaction,
    transactionIntent: TransactionIntent
};

export type InvoiceWithCompletedTransactions = Invoice & {
    completedTransactions: CompletedTransaction[]
};

export type WalletDetails = Wallet & {
    balance?: BalanceSheet,
    paymentGateway?: PaymentGateway,
    incomingTransactions?: Transaction[],
    outgoingTransactions?: Transaction[],
    invoices?: InvoiceWithCompletedTransactions[],
    transactionIntents: TransactionIntent[],
    authorizedOrganizationUsers: AuthorizedOrganizationUsersType[],
    authorizedOrganizations: AuthorizedOrganizations[],
    authorizedUsers: authorizedUsers[],
    defaultWalletRoles: RoleWithPermissionsResult[],
}

export type WalletWithTransactionsType = Wallet & {
    incomingTransactions: Transaction[],
    outgoingTransactions: Transaction[],
}

export type WalletWithPaymentGateway = Wallet & {
    paymentGateway: PaymentGateway
}

export type UserAccesLinkWithWithWallet = UserWalletRoleAccessLink & {
    wallet: WalletWithPaymentGateway,
};

export type UserOrganizationWalletAccessLinkWithWallet = OrganizationUserWalletRoleAccessLink & {
    wallet: WalletWithPaymentGateway
};