import { FastifyInstance, RouteOptions } from "fastify";
import { Routes } from "@/plugins/initializeRoutes";
import RecurrenteController from "./recurrente.controller";
import { RecurrenteHostedCheckoutPaymentGatewaySchema, RecurrenteCreatePaymentGatewaySchema, RecurrenteLoggingWebhookSchema, RecurrenteCheckoutSessionWebhookSchema, RecurrenteUpdatePaymentGatewaySettings } from "./recurrente.schema";
import AtitlanApiRoutes from "@/components/shared/routes";

export default class RecurrenteIntegrationRoutes implements Routes {

    public recurrenteController: RecurrenteController;
    public path = "/payment-gateway/integrations";

    constructor() {
        this.recurrenteController = new RecurrenteController();
    }

    public initializeRoutes(
        fastify: FastifyInstance,
        opts: RouteOptions,
        done: () => void
    ) {
        fastify.route({
            method: "POST",
            url: AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.CREATE_PAYMENT_GATEWAY,
            schema: RecurrenteCreatePaymentGatewaySchema,
            preHandler: fastify.authenticateUser,
            handler: this.recurrenteController.createRecurrentePaymentGateway
        });

        fastify.route({
            method: "POST",
            url: AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.HOSTED_CHECKOUT,
            schema: RecurrenteHostedCheckoutPaymentGatewaySchema,
            handler: this.recurrenteController.recurrenteHostedCheckoutPaymentGateway
        });

        fastify.route({
            method: "POST",
            url: AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_CHECKOUT_SESSION,
            schema: RecurrenteCheckoutSessionWebhookSchema,
            handler: this.recurrenteController.checkoutSessionWebhook
        });

        fastify.route({
            method: "POST",
            url: AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.WEBHOOK_LOGGING,
            schema: RecurrenteLoggingWebhookSchema,
            handler: this.recurrenteController.loggingWebhook
        });

        fastify.route({
            method: "POST",
            url: AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.RECURRENTE.UPDATE_SETTINGS,
            schema: RecurrenteUpdatePaymentGatewaySettings,
            preHandler: fastify.authenticateUser,
            handler: this.recurrenteController.updateRecurrenteSettings
        })

        done();
    }
}