// TODO updateRecurrentePaymentGatewaySettings

import { BadRequest, InternalServerError, Unauthorized, Unavailable } from "@/util/constants";
import {
  PaymentGatewayApiKeyType,
  PaymentIntegrationChannel,
} from "@prisma/client";
import { FastifyReply, FastifyRequest } from "fastify";
import { PaymentGatewayService } from "../../payment-gateways/payment-gateways.service";
import { ConfirmPaymentGatewayBodyType } from "./recurrente.schema";
import { RecurrenteService } from "./recurrente.service";
import { OwnerType } from "../../wallet/wallet.service";
import AuthorizationService from "@/components/heimdall/authorization/authorization.service";
import { Resources, ThorLockedFeaturePermissions, WalletPermissions } from "@/components/heimdall/authorization/authorization.type";
import TransactionIntentService from "../../transaction-intent/transaction-intent.service";
import { PaymentGatewayResult, ExtendedTransactionIntent } from "../../transaction-intent/transaction-intent.type";
import { RecurrenteSettings } from "./recurrente.type";

export default class RecurrenteController {
  private transactionIntentService: TransactionIntentService = new TransactionIntentService();
  private paymentGatewayService: PaymentGatewayService = new PaymentGatewayService();
  private authorizationService: AuthorizationService = new AuthorizationService();
  private recurrenteService: RecurrenteService = new RecurrenteService();
  private recurrenteCheckoutUrl = "https://app.recurrente.com/api/checkouts/";

  public createRecurrentePaymentGateway = async (
    req: FastifyRequest<{
      Body: {
        paymentGatewayName: string;
        paymentGatewayWalletName: string;
        secretApiKey: string;
        publicApiKey: string;
        endpointSecret: string;
        organizationId?: string;
      };
    }>) => {
    const user = req.user;
    if (!user) {
      console.log("No user provided");
      throw new Unauthorized();
    }

    const hasPermission: boolean = await this.authorizationService.checkUserPermission(
      user.id,
      Resources.ThorLockedFeatures,
      ThorLockedFeaturePermissions.CreateRecurrentePaymentGateway
    );
    if (!hasPermission) {
      console.log(
        `User ${user.id} does not have permission to create a recurrente payment gateway`
      );
      throw new Unauthorized();
    }

    const {
      paymentGatewayName,
      paymentGatewayWalletName,
      secretApiKey,
      publicApiKey,
      endpointSecret,
      organizationId,
    } = req.body;
    const filledInOrgId = !!organizationId && organizationId !== "";
    const ownerType = filledInOrgId ? OwnerType.Organization : OwnerType.User;
    const paymentGateway = await this.recurrenteService.createRecurrentePaymentGatewayWithWalletKeysAndSettings(
      paymentGatewayName,
      paymentGatewayWalletName,
      secretApiKey,
      publicApiKey,
      endpointSecret,
      user.id,
      organizationId ? organizationId : null,
      ownerType
    );

    const details = await this.paymentGatewayService.getDetailsNoAuth(paymentGateway.id);
    return {
      message: "Payment gateway created",
      paymentGateway: details,
    };
  };

  public recurrenteHostedCheckoutPaymentGateway = async (
    req: FastifyRequest<{ Body: ConfirmPaymentGatewayBodyType }>) => {
    const { transactionIntentId, paymentGatewayId } = req.body;
    const paymentIntent = await this.transactionIntentService.getIntentById(
      transactionIntentId,
      true
    );

    if (!paymentIntent) {
      console.log(`Payment intent ${transactionIntentId} not found`);
      throw new BadRequest("Payment intent not found");
    }
    if (!paymentIntent.invoice) {
      console.log(`Invoice ${paymentIntent.invoiceId} not found`);
      throw new BadRequest("Invoice not found");
    }
    const validPg = await this.paymentGatewayService.validPaymentGateway(paymentGatewayId, PaymentIntegrationChannel.Recurrente);
    if (!validPg) {
      console.log(`Payment gateway ${paymentGatewayId} not valid`);
      throw new BadRequest("Payment gateway not found");
    }

    const { secretApiKey, publicApiKey } = await this.getRecurrenteSecrets(paymentGatewayId);
    const invoiceAmount = paymentIntent.invoice.amount.mul(100);
    const recurrenteCheckoutBody = {
      items: [
        {
          name: "Thor AtitlanIO",
          currency: paymentIntent.invoice.currency,
          amount_in_cents: invoiceAmount,
        },
      ],
      success_url: `https://thor.atitlan.io/success?transactionIntentId=${transactionIntentId}`,
      cancel_url: `https://thor.atitlan.io/cancel?transactionIntentId=${transactionIntentId}`,
    };
    let recurrenteCheckoutResponse: Response;
    try {
      recurrenteCheckoutResponse = await fetch(this.recurrenteCheckoutUrl, {
        method: "POST",
        headers: {
          "X-PUBLIC-KEY": publicApiKey,
          "X-SECRET-KEY": secretApiKey,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(recurrenteCheckoutBody),
      });
      console.log("Recurrente checkout response:", recurrenteCheckoutResponse.body);
      console.log("Recurrente checkout response status:", recurrenteCheckoutResponse.status);
      if (!recurrenteCheckoutResponse.ok) {
        console.error("Recurrente checkout api response gave an error:", recurrenteCheckoutResponse.statusText);
        throw new Unavailable();
      }
    } catch (err) {
      console.log(`Error creating during recurrente checkout api post for transactionIntentId ${transactionIntentId} and paymentGatewayId ${paymentGatewayId}.\nError: ${err}`);
      throw new InternalServerError();
    }

    const body = await recurrenteCheckoutResponse.json();
    const checkoutSessionUrl = body.checkout_url;
    const recurrenteId = body.id;

    if (!recurrenteId) {
      console.log("No recurrente id provided");
      throw new BadRequest();
    }

    if (!checkoutSessionUrl) {
      console.log("No url provided");
      throw new BadRequest();
    }

    await this.transactionIntentService.confirmStartPaymentGatewayProcessing(
      transactionIntentId,
      paymentGatewayId,
      PaymentIntegrationChannel.Recurrente,
      recurrenteId
    );

    // TODO add QR code

    return {
      message: "Success. Recurrente checkout session created",
      recurrenteUrl: checkoutSessionUrl,
    };
  };

  public loggingWebhook = async (req: FastifyRequest, reply: FastifyReply) => {
    const logMessage = `Recurrente logging webhook - body: ${JSON.stringify(req.body, null, 2)}`;
    console.log(logMessage);

    reply.send({ received: true });
  };

  public checkoutSessionWebhook = async (
    req: FastifyRequest<{
      Body: {
        checkout: {
          id: string,
        },
        event_type: string
      }
    }>,
    reply: FastifyReply
  ) => {
    const event = req.body;
    const recurrenteId = event.checkout.id;

    const transactionIntent =
      await this.transactionIntentService.getIntentByPaymentGatewayIdentifier(
        recurrenteId
      );

    if (!transactionIntent) {
      console.log(`No transaction intent found for ${recurrenteId}`);
      throw new BadRequest();
    }

    const paymentGatewayId = transactionIntent.paymentGatewayId;

    if (!paymentGatewayId) {
      console.log("No payment gateway id provided");
      throw new BadRequest();
    }

    const extendedTransactionIntent =
      await this.transactionIntentService.getIntentById(
        transactionIntent.id,
        true,
        true,
        false
      );
    this.validateTransactionIntent(extendedTransactionIntent, transactionIntent.id);

    switch (event.event_type) {
      case "payment_intent.succeeded":
        await this.transactionIntentService.finalizeIntentAndCreatePaymentGatewayTransactions(
          transactionIntent.id,
          PaymentGatewayResult.success
        );
        break;
      case "payment_intent.failed":
        await this.transactionIntentService.finalizeIntentAndCreatePaymentGatewayTransactions(
          transactionIntent.id,
          PaymentGatewayResult.failed
        );
        break;
      default:
        console.log(`Unhandled event type ${event.event_type} for recurrenteId ${recurrenteId} paymentGatewayId ${paymentGatewayId} and transactionIntentId ${transactionIntent.id}`);
    }
    reply.send({ received: true });
  };

  public updateRecurrenteSettings = async (
    req: FastifyRequest<{
      Body: {
        paymentGatewayId: string;
        paymentGatewayName: string;
        secretApiKey: string;
        publicApiKey: string;
        endpointSecret: string;
        disabledAt: string | null;
      };
    }>) => {
    const user = req.user;
    if (!user) {
      console.log("No user provided");
      throw new Unauthorized();
    }

    const paymentGatewayId = req.body.paymentGatewayId;
    const paymentGateway = await this.paymentGatewayService.getNoAuth(paymentGatewayId);
    if (!paymentGateway) {
      console.log(`Payment gateway ${paymentGatewayId} not found`);
      throw new BadRequest();
    }

    const hasPermission: boolean = await this.authorizationService.userHasAnyWalletPermission(
      user.id,
      paymentGateway.sourceWalletId,
      WalletPermissions.PaymentGatewayManagement
    );

    if (!hasPermission) {
      console.log(`User ${user.id} does not have permission to update recurrente settings`);
      throw new Unauthorized();
    }

    if (paymentGateway.paymentIntegrationChannel !== PaymentIntegrationChannel.Recurrente) {
      console.log(`Payment gateway ${paymentGatewayId} is not a recurrente payment gateway`);
      throw new BadRequest();
    }
    const updated = await this.recurrenteService.updateRecurrentePaymentGatewaySettings(
      paymentGatewayId,
      req.body.paymentGatewayName,
      req.body.secretApiKey,
      req.body.publicApiKey,
      req.body.endpointSecret,
      req.body.disabledAt ? new Date(req.body.disabledAt) : null
    );
    const updatedDetails = await this.paymentGatewayService.getDetailsNoAuth(paymentGateway.id);
    return {
      message: "Recurrente settings updated successfully",
      paymentGateway: updatedDetails
    };
  };

  private validateTransactionIntent(
    transactionIntent: ExtendedTransactionIntent | null,
    transactionIntentId: string
  ) {
    if (!transactionIntent) {
      console.log(`Payment intent ${transactionIntentId} not found`);
      throw new BadRequest();
    }
    if (!transactionIntent.invoice) {
      console.log(`Invoice ${transactionIntent.invoiceId} not found`);
      throw new BadRequest();
    }
    if (!transactionIntent.paymentGateway) {
      console.log(
        `Payment gateway ${transactionIntent.paymentGatewayId} not found`
      );
      throw new BadRequest();
    }
    if (
      transactionIntent.paymentGateway.paymentIntegrationChannel !==
      PaymentIntegrationChannel.Recurrente
    ) {
      console.log(
        `Payment gateway ${transactionIntent.paymentGatewayId} is not a recurrente payment gateway`
      );
      throw new BadRequest();
    }
    if (!transactionIntent.paymentGateway.sourceWalletId) {
      console.log(
        `Source wallet ${transactionIntent.paymentGateway.sourceWalletId} not found`
      );
      throw new BadRequest();
    }
  }

  private async getRecurrenteSecrets(paymentGatewayId: string): Promise<{
    secretApiKey: string;
    endpointSecret: string;
    publicApiKey: string;
  }> {
    const paymentGateway = await this.paymentGatewayService.getDetailsNoAuth(paymentGatewayId);
    if (!paymentGateway) {
      console.log(`Payment gateway ${paymentGatewayId} not found`);
      throw new BadRequest();
    }
    if (paymentGateway.paymentIntegrationChannel !== PaymentIntegrationChannel.Recurrente) {
      console.log(`Payment gateway ${paymentGatewayId} is not a recurrente payment gateway`);
      throw new BadRequest();
    }

    const secretApiKey = paymentGateway.apiKeys.find(
      (apiKey) => apiKey.type === PaymentGatewayApiKeyType.Secret
        && apiKey.name === RecurrenteSettings.secretApiKey
    );
    if (!secretApiKey) {
      console.log(`Payment gateway ${paymentGatewayId} does not have a secret api key`);
      throw new BadRequest();
    }

    const publicApiKey = paymentGateway.apiKeys.find(
      (apiKey) => apiKey.type === PaymentGatewayApiKeyType.Public
        && apiKey.name === RecurrenteSettings.publicApiKey
    );
    if (!publicApiKey) {
      console.log(`Payment gateway ${paymentGatewayId} does not have a public api key`);
      throw new BadRequest();
    }

    const endpointSetting = paymentGateway.settings.find(
      (setting) => setting.name === RecurrenteSettings.endpointSecret);
    if (!endpointSetting) {
      console.log(`Payment gateway ${paymentGatewayId} does not have an endpoint secret`);
      throw new BadRequest();
    }

    return {
      secretApiKey: secretApiKey.apiKey,
      endpointSecret: endpointSetting.value,
      publicApiKey: publicApiKey.apiKey,
    };
  }
}
