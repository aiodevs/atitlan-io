export enum RecurrenteSettings {
    endpointSecret = 'recurrente-endpoint-secret',
    publicApiKey = 'recurrente-public-api-key',
    secretApiKey = 'recurrente-secret-api-key',
}