import { ERROR500 } from "@/util/constants";
import { Static, Type } from "@fastify/type-provider-typebox";
import { FastifySchema } from "fastify";
import { ExtendedDecryptedPaymentGatewayResultType as ExtendedDecryptedPaymentGatewaySettingsResultType, PaymentGatewayType } from "../../finance.schema.types";

export const RecurrenteCheckoutSessionWebhookSchema: FastifySchema = {
    description: "Recurrente webhook for the checkout session. This will be called by recurrente when the user completes the payment. Recurrente: checkout.session.completed events are the only ones that are supported.",
    tags: ["payment-gateway-integrations"],
    response: {
        200: {
            description: "Successful response",
            type: "object",
            properties: {
                message: { type: "string" },
            },
        },
        500: ERROR500,
    },
};

export const RecurrenteLoggingWebhookSchema: FastifySchema = {
    description: "Recurrente webhook for the logging of all recurrente events.",
    tags: ["payment-gateway-integrations"],
    response: {
        200: {
            description: "Successful response",
            type: "object",
            properties: {
                message: { type: "string" },
            },
        },
        500: ERROR500,
    },
};

const HostedCheckoutPaymentGatewayBody = Type.Object({
    transactionIntentId: Type.String({ description: 'The ID of the transaction intent' }),
    paymentGatewayId: Type.String({ description: 'The ID of the payment gateway' })
});

export const RecurrenteHostedCheckoutPaymentGatewaySchema = {
    description: 'Confirm that that the user intents to pay with recurrente. This will return the recurrente url to complete the payment.',
    tags: ['payment-gateway-integrations'],
    body: HostedCheckoutPaymentGatewayBody,
    response: {
        200: Type.Object({
            message: Type.String(),
            recurrenteUrl: Type.String(),
        }),
        400: Type.Object({
            message: Type.String(),
        }),
        500: Type.Object({
            message: Type.String(),
        }),
    },
};

export type ConfirmPaymentGatewayBodyType = Static<typeof HostedCheckoutPaymentGatewayBody>;

export const RecurrenteCreatePaymentGatewayBody = Type.Object({
    paymentGatewayName: Type.String(),
    paymentGatewayWalletName: Type.String(),
    secretApiKey: Type.String(),
    publicApiKey: Type.String(),
    endpointSecret: Type.String(),
    organizationId: Type.Optional(Type.String()),
});

export const RecurrenteCreatePaymentGatewaySchema: FastifySchema = {
    description: 'Create a new recurrente payment gateway.  If you add the org id it will create an org owned wallet instead.',
    tags: ['payment-gateway-integrations'],
    body: RecurrenteCreatePaymentGatewayBody,
    response: {
        200: Type.Object({
            message: Type.String(),
            paymentGateway: ExtendedDecryptedPaymentGatewaySettingsResultType
        }),
        400: Type.Object({
            message: Type.String(),
        }),
        500: ERROR500
    },
    security: [{ cookieAuth: [] }]
};

export const RecurrenteUpdatePaymentGatewaySettings: FastifySchema = {
    description: 'Update the settings for the recurrente payment gateway. Requires the PaymentManagement wallet permission.',
    tags: ['payment-gateway-integrations'],
    body: Type.Object({
        paymentGatewayId: Type.String({ format: 'uuid' }),
        paymentGatewayName: Type.String(),
        secretApiKey: Type.String(),
        publicApiKey: Type.String(),
        endpointSecret: Type.String(),
        disabledAt: Type.Optional(Type.String({ format: 'date-time' }))
    }),
    response: {
        200: Type.Object({
            message: Type.String(),
            paymentGateway: ExtendedDecryptedPaymentGatewaySettingsResultType
        }),
        400: Type.Object({
            message: Type.String(),
        }),
        500: ERROR500
    },
    security: [{ cookieAuth: [] }]
};