import SettingsService from "@/components/heimdall/settings/settings.service";
import { BadRequest } from "@/util/constants";
import { PaymentIntegrationChannel, PaymentGatewayApiKeyType, PaymentGateway } from "@prisma/client";
import { PaymentGatewayApiKeyService } from "../../api-key/api-key.service";
import WalletService, { OwnerType } from "../../wallet/wallet.service";
import { PaymentGatewayService } from "../../payment-gateways/payment-gateways.service";
import { getPrisma } from "@/util/prisma";
import { RecurrenteSettings } from "./recurrente.type";


export class RecurrenteService {
    private apiKeyService: PaymentGatewayApiKeyService = new PaymentGatewayApiKeyService();
    private walletService: WalletService = new WalletService();
    private settingsService: SettingsService = new SettingsService();
    private paymentGatewayService: PaymentGatewayService = new PaymentGatewayService();

    public async createRecurrentePaymentGatewayWithWalletKeysAndSettings(
        paymentGatewayName: string,
        paymentGatewayWalletName: string,
        secretApiKey: string,
        publicApiKey: string,
        endpointSecret: string,
        userId: string,
        organizationId: string | null,
        ownerType: OwnerType
    ): Promise<PaymentGateway> {
        const paymentGateway = getPrisma().$transaction(async (prisma) => {
            const paymentGatewayWallet = await this.walletService.createPaymentGatewayWallet(
                paymentGatewayWalletName,
                ownerType,
                userId,
                organizationId);
            const paymentGateway = await this.paymentGatewayService.create(
                paymentGatewayName,
                paymentGatewayWallet.id,
                PaymentIntegrationChannel.Recurrente
            );
            if (!paymentGateway) {
                console.log('Payment gateway creation failed');
                throw new BadRequest();
            }
            const publicApiKeyApiKey = await this.apiKeyService.createPaymentGatewayApiKey(
                RecurrenteSettings.publicApiKey,
                publicApiKey,
                paymentGateway.id,
                PaymentGatewayApiKeyType.Public);
            const privateApiKeyApiKey = await this.apiKeyService.createPaymentGatewayApiKey(
                RecurrenteSettings.secretApiKey,
                secretApiKey,
                paymentGateway.id,
                PaymentGatewayApiKeyType.Secret);
            const endpointSecretSetting = await this.settingsService.createEncryptedPaymentGatewaySetting(
                RecurrenteSettings.endpointSecret,
                endpointSecret,
                paymentGateway.id
            );
            return paymentGateway;
        });
        return paymentGateway;
    }

    public async updateRecurrentePaymentGatewaySettings(
        paymentGatewayId: string,
        paymentGatewayName: string,
        secretApiKey: string,
        publicApiKey: string,
        endpointSecret: string,
        disabledAt: Date | null
    ): Promise<PaymentGateway> {
        const paymentGateway = getPrisma().$transaction(async (prisma) => {
            const existingPG = await this.paymentGatewayService.getDetailsNoAuth(paymentGatewayId);
            if (!existingPG) {
                console.log(`recurrente.service.ts - updateRecurrentePaymentGatewaySettings - Payment gateway not found id:${paymentGatewayId}`);
                throw new BadRequest();
            }
            const publicApiKeyId = existingPG.apiKeys.find(k => k.type === PaymentGatewayApiKeyType.Public)?.id;
            const privateApiKeyId = existingPG.apiKeys.find(k => k.type === PaymentGatewayApiKeyType.Secret)?.id;
            if (!publicApiKeyId || !privateApiKeyId) {
                console.log(`recurrente.service.ts - updateRecurrentePaymentGatewaySettings - Api key not found for payment gateway id:${paymentGatewayId}`);
                throw new BadRequest();
            }
            const paymentGateway = await this.paymentGatewayService.update(
                paymentGatewayId,
                paymentGatewayName,
                disabledAt
            );
            if (!paymentGateway) {
                console.log('Payment gateway update failed');
                throw new BadRequest();
            }
            const publicApiKeyApiKey = await this.apiKeyService.update(
                publicApiKeyId,
                publicApiKey,
                RecurrenteSettings.publicApiKey,
            );
            const privateApiKeyApiKey = await this.apiKeyService.update(
                privateApiKeyId,
                secretApiKey,
                RecurrenteSettings.secretApiKey,
            );
            const endpointSecretSetting = await this.settingsService.updatePaymentGatewaySetting(
                paymentGateway.id,
                RecurrenteSettings.endpointSecret,
                endpointSecret,
                true
            );
            return paymentGateway;
        });
        console.log(`RecurrenteService.updateRecurrentePaymentGatewaySettings - paymentGatewayId:${paymentGatewayId} paymentGatewayName:${paymentGatewayName}`)
        return paymentGateway;
    }
}

