import SettingsService from "@/components/heimdall/settings/settings.service";
import { BadRequest } from "@/util/constants";
import { PaymentIntegrationChannel, PaymentGatewayApiKeyType, PaymentGateway } from "@prisma/client";
import { PaymentGatewayApiKeyService } from "../../api-key/api-key.service";
import WalletService, { OwnerType } from "../../wallet/wallet.service";
import { PaymentGatewayService } from "../../payment-gateways/payment-gateways.service";
import { getPrisma } from "@/util/prisma";
import { StripeSettings } from "./stripe.type";


export class StripeService {
    private apiKeyService: PaymentGatewayApiKeyService = new PaymentGatewayApiKeyService();
    private walletService: WalletService = new WalletService();
    private settingsService: SettingsService = new SettingsService();
    private paymentGatewayService: PaymentGatewayService = new PaymentGatewayService();

    public async createStripePaymentGatewayWithWalletKeysAndSettings(
        paymentGatewayName: string,
        paymentGatewayWalletName: string,
        secretApiKey: string,
        publicApiKey: string,
        endpointSecret: string,
        userId: string,
        organizationId: string | null,
        ownerType: OwnerType
    ): Promise<PaymentGateway> {
        const paymentGateway = getPrisma().$transaction(async (prisma) => {
            const paymentGatewayWallet = await this.walletService.createPaymentGatewayWallet(
                paymentGatewayWalletName,
                ownerType,
                userId,
                organizationId);
            const paymentGateway = await this.paymentGatewayService.create(
                paymentGatewayName,
                paymentGatewayWallet.id,
                PaymentIntegrationChannel.Stripe
            );
            if (!paymentGateway) {
                console.log('Payment gateway creation failed');
                throw new BadRequest();
            }
            const publicApiKeyApiKey = await this.apiKeyService.createPaymentGatewayApiKey(
                StripeSettings.publicApiKey,
                publicApiKey,
                paymentGateway.id,
                PaymentGatewayApiKeyType.Public);
            const privateApiKeyApiKey = await this.apiKeyService.createPaymentGatewayApiKey(
                StripeSettings.secretApiKey,
                secretApiKey,
                paymentGateway.id,
                PaymentGatewayApiKeyType.Secret);
            const endpointSecretSetting = await this.settingsService.createEncryptedPaymentGatewaySetting(
                StripeSettings.endpointSecret,
                endpointSecret,
                paymentGateway.id
            );
            return paymentGateway;
        });
        return paymentGateway;
    }

    public async updateStripePaymentGatewaySettings(
        paymentGatewayId: string,
        paymentGatewayName: string,
        secretApiKey: string,
        publicApiKey: string,
        endpointSecret: string,
        disabledAt: Date | null
    ): Promise<PaymentGateway> {
        const paymentGateway = getPrisma().$transaction(async (prisma) => {
            const details = await this.paymentGatewayService.getDetailsNoAuth(paymentGatewayId);
            if (!details) {
                console.log(`StripeService.updateStripePaymentGatewaySettings - payment gateway not found id:${paymentGatewayId}`);
                throw new BadRequest();
            }
            const paymentGateway = await this.paymentGatewayService.update(
                paymentGatewayId,
                paymentGatewayName,
                disabledAt
            );
            if (!paymentGateway) {
                console.log('Payment gateway update failed');
                throw new BadRequest();
            }
            const publicApiKeyId = details.apiKeys.find(k => k.name === StripeSettings.publicApiKey)?.id;
            const privateApiKeyId = details.apiKeys.find(k => k.name === StripeSettings.secretApiKey)?.id;
            if (!publicApiKeyId || !privateApiKeyId) {
                console.log(`StripeService.updateStripePaymentGatewaySettings - Api key not found for payment gateway id:${paymentGatewayId}`);
                throw new BadRequest();
            }
            const publicApiKeyApiKey = await this.apiKeyService.update(
                publicApiKeyId,
                publicApiKey,
                StripeSettings.publicApiKey,
            );
            const privateApiKeyApiKey = await this.apiKeyService.update(
                privateApiKeyId,
                secretApiKey,
                StripeSettings.secretApiKey,
            );
            const endpointSecretSetting = await this.settingsService.updatePaymentGatewaySetting(
                paymentGateway.id,
                StripeSettings.endpointSecret,
                endpointSecret,
                true
            );
            return paymentGateway;
        });
        console.log(`RecurrenteService.updateRecurrentePaymentGatewaySettings - paymentGatewayId:${paymentGatewayId} paymentGatewayName:${paymentGatewayName}`)
        return paymentGateway;
    }


}
