// TODO createStripePaymentGatewaySettings

import { FastifyReply, FastifyRequest } from "fastify";
import Stripe from "stripe";
import { BadRequest, Unauthorized } from "@/util/constants";
import { StripeHostedCheckoutPaymentGatewayBodyType } from "./stripe.schema";
import { PaymentGatewayService } from "../../payment-gateways/payment-gateways.service";
import {
  PaymentGatewayApiKeyType,
  PaymentIntegrationChannel,
} from "@prisma/client";
import { StripeService } from "./stripe.service";
import { StripeFactory } from "@/util/stripe-factory";
import { OwnerType } from "../../wallet/wallet.service";
import AuthorizationService from "@/components/heimdall/authorization/authorization.service";
import OrganizationService from "@/components/heimdall/organization/organization.service";
import {
  Resources,
  ThorLockedFeaturePermissions,
  WalletPermissions,
} from "@/components/heimdall/authorization/authorization.type";
import TransactionIntentService from "../../transaction-intent/transaction-intent.service";
import {
  PaymentGatewayResult,
  ExtendedTransactionIntent,
} from "../../transaction-intent/transaction-intent.type";
import { StripeSettings } from "./stripe.type";

export default class StripeController {
  private transactionIntentService: TransactionIntentService = new TransactionIntentService();
  private paymentGatewayService: PaymentGatewayService = new PaymentGatewayService();
  private authorizationService: AuthorizationService = new AuthorizationService();
  private stripeService: StripeService = new StripeService();
  private organizationService: OrganizationService = new OrganizationService();
  private stripeFactory: StripeFactory;

  constructor(stripeFactory: StripeFactory) {
    this.stripeFactory = stripeFactory;
  }

  public createStripePaymentGateway = async (
    req: FastifyRequest<{
      Body: {
        paymentGatewayName: string;
        paymentGatewayWalletName: string;
        secretApiKey: string;
        publicApiKey: string;
        endpointSecret: string;
        organizationId?: string;
      };
    }>
  ) => {
    const user = req.user;
    if (!user) {
      console.log("No user provided");
      throw new Unauthorized();
    }

    if (
      !this.authorizationService.checkUserPermission(
        user.id,
        Resources.ThorLockedFeatures,
        ThorLockedFeaturePermissions.CreateStripePaymentGateway
      )
    ) {
      console.log(
        `User ${user.id} does not have permission to create a payment gateway`
      );
      throw new Unauthorized();
    }

    const {
      paymentGatewayName,
      paymentGatewayWalletName,
      secretApiKey,
      publicApiKey,
      endpointSecret,
      organizationId,
    } = req.body;
    const validOrganization = organizationId
      ? await this.organizationService.organizationValid(organizationId)
      : false;
    const ownerType = validOrganization
      ? OwnerType.Organization
      : OwnerType.User;
    const paymentGateway =
      await this.stripeService.createStripePaymentGatewayWithWalletKeysAndSettings(
        paymentGatewayName,
        paymentGatewayWalletName,
        secretApiKey,
        publicApiKey,
        endpointSecret,
        user.id,
        organizationId ? organizationId : null,
        ownerType
      );

    return {
      message: "Payment gateway created",
      paymentGateway: paymentGateway,
    };
  };

  public stripeHostedCheckoutPaymentGateway = async (
    req: FastifyRequest<{ Body: StripeHostedCheckoutPaymentGatewayBodyType }>,
  ) => {
    const { transactionIntentId, paymentGatewayId } = req.body;

    const paymentIntent = await this.transactionIntentService.getIntentById(
      transactionIntentId,
      true
    );

    if (!paymentIntent) {
      console.log(`Payment intent ${transactionIntentId} not found`);
      throw new BadRequest("Payment intent not found");
    }
    if (!paymentIntent.invoice) {
      console.log(`Invoice ${paymentIntent.invoiceId} not found`);
      throw new BadRequest("Invoice not found");
    }

    await this.transactionIntentService.confirmStartPaymentGatewayProcessing(
      transactionIntentId,
      paymentGatewayId,
      PaymentIntegrationChannel.Stripe
    );

    const { stripe, publisheableApiKey } =
      await this.createStripeFromPaymentGateway(paymentGatewayId);
    let checkoutSession: Stripe.Response<Stripe.Checkout.Session>;
    try {
      checkoutSession = await stripe.checkout.sessions.create({
        mode: "payment",
        currency: paymentIntent.invoice.currency,
        // TODO replace with product data
        line_items: [
          {
            price_data: {
              currency: paymentIntent.invoice.currency,
              product_data: {
                name: "Thor",
              },
              unit_amount: Number(paymentIntent.invoice.amount) * 100,
            },
            adjustable_quantity: {
              enabled: false,
            },
            quantity: 1,
          },
        ],
        metadata: {
          paymentGatewayId: paymentGatewayId.toString(),
          transactionIntentId: transactionIntentId.toString(),
        }, // TODO fix succesURL and cancelURL
        success_url: `https://thor.atitlan.io/success?transactionIntentId=${transactionIntentId}`,
        cancel_url: `https://thor.atitlan.io/cancel?transactionIntentId=${transactionIntentId}`,
      });
    } catch (err) {
      console.log("Error creating checkout session");
      throw new BadRequest("Error creating checkout session");
    }

    if (!checkoutSession.url) {
      console.log("No url provided");
      throw new BadRequest("No url provided");
    }

    return {
      message: "Succes. Stripe checkout session created",
      stripeUrl: checkoutSession.url,
      publisheableApiKey: publisheableApiKey,
    };
  };

  public loggingWebhook = async (req: FastifyRequest, reply: FastifyReply) => {
    const sig = req.headers["stripe-signature"]?.toString();
    if (!sig) {
      console.log("No signature provided");
      throw new BadRequest();
    }

    const logMessage = `Stripe logging webhook - body: ${JSON.stringify(
      req.body,
      null,
      2
    )}`;
    console.log(logMessage);

    reply.send({ received: true });
  };

  public checkoutSessionWebhook = async (
    req: FastifyRequest,
    reply: FastifyReply
  ) => {
    const sig = req.headers["stripe-signature"]?.toString();
    if (!sig) {
      console.log("No signature provided");
      throw new BadRequest();
    }

    if (!req.rawBody) {
      console.log("No raw body provided");
      throw new BadRequest();
    }
    const rawBody = req.rawBody.toString();
    const event = JSON.parse(rawBody);
    const paymentGatewayId = event.data.object.metadata.paymentGatewayId;
    const transactionIntentId = event.data.object.metadata.transactionIntentId;

    if (!paymentGatewayId) {
      console.log("No payment gateway id provided");
      throw new BadRequest();
    }
    if (!transactionIntentId) {
      console.log("No transaction intent id provided");
      throw new BadRequest();
    }

    let stripeEvent;
    try {
      const { stripe, endpointSecret } =
        await this.createStripeFromPaymentGateway(paymentGatewayId);
      stripeEvent = stripe.webhooks.constructEvent(
        rawBody,
        sig,
        endpointSecret
      );
    } catch (err: any) {
      console.log(`Webhook Error: ${err.message}`);
      reply.status(400).send(`Webhook Error: ${err.message}`);
      throw new BadRequest();
    }
    const transactionIntent = await this.transactionIntentService.getIntentById(
      transactionIntentId,
      true,
      true,
      false
    );
    this.validateTransactionIntent(transactionIntent, transactionIntentId);

    // TODO block test events from being processed in production
    // They should not be marked as proper transactions at least
    switch (stripeEvent.type) {
      case "checkout.session.completed":
        const expired = stripeEvent.data.object.status === "expired";
        if (expired) {
          await this.transactionIntentService.finalizeIntentAndCreatePaymentGatewayTransactions(
            transactionIntentId,
            PaymentGatewayResult.expired
          );
          break;
        }
      case "checkout.session.async_payment_succeeded":
        const paymentSuccess =
          stripeEvent.data.object.payment_status === "paid";
        if (paymentSuccess) {
          await this.transactionIntentService.finalizeIntentAndCreatePaymentGatewayTransactions(
            transactionIntentId,
            PaymentGatewayResult.success
          );
        }
        break;
      case "checkout.session.async_payment_failed":
        await this.transactionIntentService.finalizeIntentAndCreatePaymentGatewayTransactions(
          transactionIntentId,
          PaymentGatewayResult.failed
        );
        break;
      default:
        console.log(`Unhandled event type ${event.type}`);
        console.log(event.data.object);
    }
    reply.send({ received: true });
  };

  public updateSettings = async (
    req: FastifyRequest<{
      Body: {
        paymentGatewayId: string;
        paymentGatewayName: string;
        secretApiKey: string;
        publicApiKey: string;
        endpointSecret: string;
        disabledAt?: string;
      };
    }>
  ) => {
    const user = req.user;
    if (!user) {
      console.log("No user provided");
      throw new Unauthorized();
    }

    const paymentGatewayId = req.body.paymentGatewayId;
    const paymentGateway = await this.paymentGatewayService.getNoAuth(
      paymentGatewayId
    );
    if (!paymentGateway) {
      console.log(`Payment gateway ${paymentGatewayId} not found`);
      throw new BadRequest();
    }

    const hasPermission: boolean =
      await this.authorizationService.userHasAnyWalletPermission(
        user.id,
        paymentGateway.sourceWalletId,
        WalletPermissions.PaymentGatewayManagement
      );

    if (!hasPermission) {
      console.log(
        `User ${user.id} does not have permission to update recurrente settings`
      );
      throw new Unauthorized();
    }

    if (
      paymentGateway.paymentIntegrationChannel !==
      PaymentIntegrationChannel.Recurrente
    ) {
      console.log(
        `Payment gateway ${paymentGatewayId} is not a recurrente payment gateway`
      );
      throw new BadRequest();
    }

    const updated = await this.stripeService.updateStripePaymentGatewaySettings(
      paymentGatewayId,
      req.body.paymentGatewayName,
      req.body.secretApiKey,
      req.body.publicApiKey,
      req.body.endpointSecret,
      req.body.disabledAt ? new Date(req.body.disabledAt) : null
    );

    const getPaymentGateway = await this.paymentGatewayService.getDetailsNoAuth(
      paymentGateway.id
    );
    return {
      message: "Recurrente settings updated successfully",
      paymentGateway: getPaymentGateway,
    };
  };
  private validateTransactionIntent(
    transactionIntent: ExtendedTransactionIntent | null,
    transactionIntentId: string
  ) {
    if (!transactionIntent) {
      console.log(`Payment intent ${transactionIntentId} not found`);
      throw new BadRequest();
    }
    if (!transactionIntent.invoice) {
      console.log(`Invoice ${transactionIntent.invoiceId} not found`);
      throw new BadRequest();
    }
    if (!transactionIntent.paymentGateway) {
      console.log(
        `Payment gateway ${transactionIntent.paymentGatewayId} not found`
      );
      throw new BadRequest();
    }
    if (
      transactionIntent.paymentGateway.paymentIntegrationChannel !==
      PaymentIntegrationChannel.Stripe
    ) {
      console.log(
        `Payment gateway ${transactionIntent.paymentGatewayId} is not a stripe payment gateway`
      );
      throw new BadRequest();
    }
    if (!transactionIntent.paymentGateway.sourceWalletId) {
      console.log(
        `Source wallet ${transactionIntent.paymentGateway.sourceWalletId} not found`
      );
      throw new BadRequest();
    }
  }

  private async createStripeFromPaymentGateway(
    paymentGatewayId: string
  ): Promise<{
    stripe: Stripe;
    endpointSecret: string;
    publisheableApiKey: string;
  }> {
    const paymentGateway = await this.paymentGatewayService.getDetailsNoAuth(
      paymentGatewayId
    );
    if (!paymentGateway) {
      console.log(`Payment gateway ${paymentGatewayId} not found`);
      throw new BadRequest();
    }
    if (
      paymentGateway.paymentIntegrationChannel !==
      PaymentIntegrationChannel.Stripe
    ) {
      console.log(
        `Payment gateway ${paymentGatewayId} is not a stripe payment gateway`
      );
      throw new BadRequest();
    }

    const secretApiKey = paymentGateway.apiKeys.find(
      (apiKey) => apiKey.type === PaymentGatewayApiKeyType.Secret
    );
    const publicApiKey = paymentGateway.apiKeys.find(
      (apiKey) =>
        apiKey.type === PaymentGatewayApiKeyType.Public &&
        apiKey.name === StripeSettings.publicApiKey
    );
    const endpointSetting = paymentGateway.settings.find(
      (setting) => setting.name === StripeSettings.endpointSecret
    );

    if (!secretApiKey) {
      console.log(
        `Payment gateway ${paymentGatewayId} does not have a secret api key`
      );
      throw new BadRequest();
    }
    if (!publicApiKey) {
      console.log(
        `Payment gateway ${paymentGatewayId} does not have a public api key`
      );
      throw new BadRequest();
    }
    if (!endpointSetting) {
      console.log(
        `Payment gateway ${paymentGatewayId} does not have an endpoint secret`
      );
      throw new BadRequest();
    }

    const stripe: Stripe = this.stripeFactory(secretApiKey.apiKey);

    return {
      stripe: stripe,
      endpointSecret: endpointSetting.value,
      publisheableApiKey: publicApiKey.apiKey,
    };
  }
}
