import AtitlanApiRoutes from "@/components/shared/routes";
import { Routes } from "@/plugins/initializeRoutes";
import { StripeFactory } from "@/util/stripe-factory";
import { FastifyInstance, RouteOptions } from "fastify";
import StripeController from "./stripe.controller";
import { StripeCreatePaymentGatewaySchema, StripeHostedCheckoutPaymentGatewaySchema, StripeCheckoutSessionWebhookSchema, StripeLoggingWebhookSchema, StripeUpdatePaymentGatewaySettings } from "./stripe.schema";


export default class StripeIntegrationRoutes implements Routes {

    public stripeController;
    public path = "/payment-gateway/integrations";

    constructor(stripeFactory: StripeFactory) {
        this.stripeController = new StripeController(stripeFactory);
    }

    public initializeRoutes(
        fastify: FastifyInstance,
        opts: RouteOptions,
        done: () => void
    ) {
        fastify.route({
            method: "POST",
            url: AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.STRIPE.CREATE_PAYMENT_GATEWAY,
            schema: StripeCreatePaymentGatewaySchema,
            preHandler: fastify.authenticateUser,
            handler: this.stripeController.createStripePaymentGateway
        });

        fastify.route({
            method: "POST",
            url: AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.STRIPE.UPDATE_SETTINGS,
            schema: StripeUpdatePaymentGatewaySettings,
            preHandler: fastify.authenticateUser,
            handler: this.stripeController.updateSettings
        })

        fastify.route({
            method: "POST",
            url: AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.STRIPE.HOSTED_CHECKOUT,
            schema: StripeHostedCheckoutPaymentGatewaySchema,
            handler: this.stripeController.stripeHostedCheckoutPaymentGateway
        });

        fastify.route({
            method: "POST",
            url: AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.STRIPE.WEBHOOK_CHECKOUT_SESSION,
            schema: StripeCheckoutSessionWebhookSchema,
            config: {
                rawBody: true
            },
            handler: this.stripeController.checkoutSessionWebhook
        });

        fastify.route({
            method: "POST",
            url: AtitlanApiRoutes.PAYMENT_GATEWAY.INTEGRATIONS.STRIPE.WEBHOOK_LOGGING,
            schema: StripeLoggingWebhookSchema,
            config: {
                rawBody: true
            },
            handler: this.stripeController.loggingWebhook
        });      

        done();
    }
}