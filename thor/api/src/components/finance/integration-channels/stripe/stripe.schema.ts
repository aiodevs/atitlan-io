import { ERROR500 } from "@/util/constants";
import { Static, Type } from "@fastify/type-provider-typebox";
import { FastifySchema } from "fastify";
import { PaymentGatewayType, ExtendedDecryptedPaymentGatewayResultType } from "../../finance.schema.types";

export const StripeCheckoutSessionWebhookSchema: FastifySchema = {
    description: "Stripe webhook for the checkout session. This will be called by stripe when the user completes the payment. Stripe: checkout.session.completed events are the only ones that are supported.",
    tags: ["payment-gateway-integrations"],
    response: {
        200: {
            description: "Successful response",
            type: "object",
            properties: {
                message: { type: "string" },
            },
        },
        500: ERROR500,
    },
};

export const StripeLoggingWebhookSchema: FastifySchema = {
    description: "Stripe webhook for the logging of all stripe events.",
    tags: ["payment-gateway-integrations"],
    response: {
        200: {
            description: "Successful response",
            type: "object",
            properties: {
                message: { type: "string" },
            },
        },
        500: ERROR500,
    },
};

const StripeHostedCheckoutPaymentGatewayBody = Type.Object({
    transactionIntentId: Type.String({ description: 'The ID of the transaction intent' }),
    paymentGatewayId: Type.String({ description: 'The ID of the payment gateway' })
});

export const StripeHostedCheckoutPaymentGatewaySchema = {
    description: 'Confirm that that the user intents to pay with stripe. This will return the stripe url to complete the payment.',
    tags: ['payment-gateway-integrations'],
    body: StripeHostedCheckoutPaymentGatewayBody,
    response: {
        200: Type.Object({
            message: Type.String(),
            stripeUrl: Type.String(),
            publisheableApiKey: Type.String()
        }),
        400: Type.Object({
            message: Type.String(),
        }),
        500: Type.Object({
            message: Type.String(),
        }),
    },
};

export type StripeHostedCheckoutPaymentGatewayBodyType = Static<typeof StripeHostedCheckoutPaymentGatewayBody>;

export const StripeCreatePaymentGatewayBody = Type.Object({
    paymentGatewayName: Type.String(),
    paymentGatewayWalletName: Type.String(),
    secretApiKey: Type.String(),
    publicApiKey: Type.String(),
    endpointSecret: Type.String(),
    organizationId: Type.Optional(Type.String()),
});

export const StripeCreatePaymentGatewaySchema: FastifySchema = {
    description: 'Create a new stripe payment gateway. If you add the org id it will create an org owned wallet instead.',
    tags: ['payment-gateway-integrations'],
    body: StripeCreatePaymentGatewayBody,
    response: {
        200: Type.Object({
            message: Type.String(),
            paymentGateway: PaymentGatewayType
        }),
        400: Type.Object({
            message: Type.String(),
        }),
        500: ERROR500
    },
    security: [{ cookieAuth: [] }]
};

export const StripeUpdatePaymentGatewaySettings: FastifySchema = {
    description: 'Update the settings for the recurrente payment gateway. Requires the PaymentManagement wallet permission.',
    tags: ['payment-gateway-integrations'],
    body: Type.Object({
        paymentGatewayId: Type.String({ format: 'uuid' }),
        paymentGatewayName: Type.String(),
        secretApiKey: Type.String(),
        publicApiKey: Type.String(),
        endpointSecret: Type.String(),
        disabledAt: Type.Optional(Type.String({ format: 'date-time' }))
    }),
    response: {
        200: Type.Object({
            message: Type.String(),
            paymentGateway: Type.Object(ExtendedDecryptedPaymentGatewayResultType)
        }),
        400: Type.Object({
            message: Type.String(),
        }),
        500: ERROR500
    },
    security: [{ cookieAuth: [] }]
};