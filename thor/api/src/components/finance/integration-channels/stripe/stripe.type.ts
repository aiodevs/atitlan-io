
export enum StripeSettings {
    endpointSecret = 'stripe-endpoint-secret',
    publicApiKey = 'stripe-public-api-key',
    secretApiKey = 'stripe-secret-api-key',
}