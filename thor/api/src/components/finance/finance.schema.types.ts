import { PaymentGatewayApiKeyType, PaymentIntegrationChannel } from "@prisma/client";
import { Type } from "@sinclair/typebox";

// Not so finance types
export const UserType = Type.Object({
    id: Type.String(),
    email: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
});

export const RoleType = Type.Object({
    id: Type.String(),
    name: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
});

export const OrganizationType = Type.Object({
    id: Type.String(),
    name: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
});

export const UserSessionType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    lastUsedAt: Type.Optional(Type.String({ format: 'date-time' })),
    userId: Type.String(),
    activeExpires: Type.String(),
    idleExpires: Type.String(),
});

export const KeyType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    lastUsedAt: Type.Optional(Type.String({ format: 'date-time' })),
    hashedPassword: Type.Optional(Type.String()),
    userId: Type.String(),
});

export const UserPermissionLinkType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    revokedAt: Type.Optional(Type.String({ format: 'date-time' })),
    lastUsed: Type.String({ format: 'date-time' }),
    userId: Type.String(),
    permissionId: Type.String(),
});

export const PermissionRoleLinkType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    roleId: Type.String(),
    permissionId: Type.String(),
});

export const PermissionType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    lastUsed: Type.Optional(Type.String({ format: 'date-time' })),
    resource: Type.String(),
    action: Type.String(),
});

export const WalletType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    lastUsed: Type.Optional(Type.String({ format: 'date-time' })),
    name: Type.String(),
});

export const InvoiceType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    amount: Type.String(),
    currency: Type.String(),
    targetWalletId: Type.String(),
    description: Type.String(),
});

export const BalanceType = Type.Record(Type.String(), Type.Object({
    incoming: Type.String(),
    outgoing: Type.String(),
    net: Type.String(),
}));

export const TransactionIntentType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    status: Type.String(),
    invoiceId: Type.String(),
    sourceWalletId: Type.String(),
    requestingUserId: Type.Optional(Type.String()),
    paymentGatewayId: Type.Optional(Type.String()),
    paymentGatewayIdentifier: Type.Optional(Type.String()),
    anonymous: Type.Boolean(),
});

export const PaymentGatewayType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    disabledAt: Type.Optional(Type.String()),
    name: Type.String(),
    paymentIntegrationChannel: Type.Enum(PaymentIntegrationChannel),
    sourceWalletId: Type.String(),
});

export const TransactionType = Type.Object({
    id: Type.String(),
    transactionTime: Type.String({ format: 'date-time' }),
    sourceCurrency: Type.String(),
    sourceAmount: Type.String(),
    targetCurrency: Type.String(),
    targetAmount: Type.String(),
    description: Type.String(),
    sourceWalletId: Type.String(),
    targetWalletId: Type.String()
});

export const ApiKeyType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    name: Type.String(),
    apiKey: Type.String(),
});

export const CompletedTransactionLinkType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    transactionIntentId: Type.String(),
    transactionId: Type.String(),
    invoiceId: Type.String(),
    type: Type.String()
});

export const PaymentGatewaySettingsLinkType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    settingsId: Type.String(),
    paymentGatewayId: Type.String(),
});

export const PaymentGatewayApiLinkType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    apiKeyId: Type.String(),
    paymentGatewayId: Type.String(),
    type: Type.String()
});

export const PaymentGatewaySettingsType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    name: Type.String(),
    value: Type.String(),
    paymentGatewayLink: Type.Optional(PaymentGatewaySettingsLinkType)
});

export const PaymentGatewaySettingsTypeResult = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    name: Type.String(),
    value: Type.String(),
    encrypted: Type.Boolean()
});

export const PaymentGatewayDecryptedApiKeysResult = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    name: Type.String(),
    apiKey: Type.String(),
    type: Type.Enum(PaymentGatewayApiKeyType),
});

export const UserWalletRoleAccessLinkType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    revokedAt: Type.Optional(Type.String({ format: 'date-time' })),
    walletId: Type.String(),
    userId: Type.String(),
    roleId: Type.String(),
    permissions: Type.String(),
});

export const OrganizationWalletRoleAccessLinkType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    revokedAt: Type.Optional(Type.String({ format: 'date-time' })),
    walletId: Type.String(),
    organizationId: Type.String(),
    roleId: Type.String(),
});

export const OrganizationUserWalletRoleAccessLinkType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    revokedAt: Type.Optional(Type.String({ format: 'date-time' })),
    organizationId: Type.String(),
    userId: Type.String(),
    walletId: Type.String(),
    roleId: Type.String(),
});


export const OrganizationUserRoleAccessLinkType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    revokedAt: Type.Optional(Type.String({ format: 'date-time' })),
    userId: Type.String(),
    organizationId: Type.String(),
    roleId: Type.String(),
});

export const OrganizationGeneralInfoSchemaType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    name: Type.String(),
});

// Before here the normal types
// After this the extended types

export const PermissionResultType = Type.Object({
    id: Type.String(),
    resource: Type.String(),
    action: Type.String(),
    isDefault: Type.Boolean(),
});

export const ExtendedRoleWithPermission = Type.Object({
    id: Type.String(),
    name: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    isDefault: Type.Boolean(),
    permissions: Type.Array(PermissionResultType),
});

export const ExtendedCompletedTransactionLinkType = Type.Object({
    id: Type.String(),
    transactionIntentId: Type.String(),
    transactionId: Type.String(),
    invoiceId: Type.String(),
    type: Type.String(),
    transaction: TransactionType,
});

export const ExtendedEncryptedApiKeyType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    name: Type.String(),
    encryptedKey: Type.String(),
    PaymentGatewayApiKeyLink: Type.Optional(PaymentGatewayApiLinkType)
});

export const ExtendedApiKeyType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    name: Type.String(),
    apiKey: Type.String(),
    PaymentGatewayApiKeyLink: Type.Optional(PaymentGatewayApiLinkType)
});

export const ExtendedWalletTransactionType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    name: Type.String(),
    incomingTransactions: Type.Array(TransactionType),
    outgoingTransactions: Type.Array(TransactionType)
});

export const ExtendedWalletGeneralInfoType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    name: Type.String(),
    balance: Type.Optional(BalanceType),
    paymentGateway: Type.Optional(PaymentGatewayType),
    organizationName: Type.Optional(Type.String())
});

export const ExtendedOrganizationWalletGeneralInfoType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    name: Type.String(),
    balance: Type.Optional(BalanceType),
    paymentGateway: Type.Optional(PaymentGatewayType),
    role: ExtendedRoleWithPermission,
});

export const ExtendedAuthorizedOrganizationWalletUsersType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    revokedAt: Type.Optional(Type.String({ format: 'date-time' })),
    organizationId: Type.String(),
    userId: Type.String(),
    walletId: Type.String(),
    roleId: Type.String(),

    organization: OrganizationType,
    user: UserType,
    role: ExtendedRoleWithPermission
});

export const ExtendedTransactionType = Type.Object({
    id: Type.String(),
    transactionTime: Type.String(),
    sourceCurrency: Type.String(),
    sourceAmount: Type.String(),
    targetCurrency: Type.String(),
    targetAmount: Type.String(),
    description: Type.String(),
    sourceWalletId: Type.String(),
    targetWalletId: Type.String(),
    hash: Type.String(),
    previousHash: Type.String(),
});

export const ExtendedTransactionIntentType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    status: Type.String(),
    sourceWalletId: Type.Optional(Type.String()),
    requestingUserId: Type.Optional(Type.String()),
    invoiceId: Type.String(),
    paymentGatewayId: Type.Optional(Type.String()),
    anonymous: Type.Boolean(),
    paymentGatewayIdentifier: Type.Optional(Type.String()),
    invoice: Type.Optional(InvoiceType),
    paymentGateway: Type.Optional(PaymentGatewayType),
    completedTransactions: Type.Array(ExtendedCompletedTransactionLinkType),
});

export const ExtendedInvoiceType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    amount: Type.String(),
    currency: Type.String(),
    description: Type.String(),
    targetWalletId: Type.String(),
    transactionIntent: Type.Array(TransactionIntentType),
    completedTransactions: Type.Array(ExtendedCompletedTransactionLinkType),
});

export const ExtendedUserWalletRoleAccessLinkType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    revokedAt: Type.Optional(Type.String({ format: 'date-time' })),
    walletId: Type.String(),
    userId: Type.String(),
    roleId: Type.String(),
    user: UserType,
    role: ExtendedRoleWithPermission,
});

export const ExtendedRoleOrganizationWalletRoleAccessLinkType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    revokedAt: Type.Optional(Type.String({ format: 'date-time' })),
    walletId: Type.String(),
    organizationId: Type.String(),
    roleId: Type.String(),
    organization: OrganizationType,
    role: ExtendedRoleWithPermission,
});

export const ExtendedOrganizationWalletRoleAccessLinkType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    revokedAt: Type.Optional(Type.String({ format: 'date-time' })),
    walletId: Type.String(),
    organizationId: Type.String(),
    roleId: Type.String(),
    organization: OrganizationType,
    role: ExtendedRoleWithPermission,
});

export const ExtendedWalletType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    name: Type.String(),
    outgoingTransactions: Type.Array(TransactionType),
    incomingTransactions: Type.Array(TransactionType),
    invoices: Type.Array(InvoiceType),
    authorizedUsers: Type.Array(UserWalletRoleAccessLinkType),
    authorizedOrganizations: Type.Array(OrganizationWalletRoleAccessLinkType),
});

export const ExtendedUserOrganizationRoleAccessLinkType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    revokedAt: Type.Optional(Type.String({ format: 'date-time' })),
    userId: Type.String(),
    organizationId: Type.String(),
    roleId: Type.String(),
    user: UserType,
    role: ExtendedRoleWithPermission,
});

export const ExtendedUserType = Type.Object({
    id: Type.String(),
    email: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    authSession: Type.Array(UserSessionType),
    key: Type.Array(KeyType),
    permissions: Type.Array(UserPermissionLinkType),
    organizationRoles: Type.Array(OrganizationUserRoleAccessLinkType),
    walletRoles: Type.Array(UserWalletRoleAccessLinkType),
    organizationWalletRoles: Type.Array(OrganizationUserWalletRoleAccessLinkType),
    transactionIntents: Type.Array(TransactionIntentType),
});

export const ExtendedSessionType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    lastUsedAt: Type.Optional(Type.String({ format: 'date-time' })),
    activeExpires: Type.String(),
    idleExpires: Type.String(),
    user: UserType,
});

export const ExtendedKeyType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    lastUsedAt: Type.Optional(Type.String({ format: 'date-time' })),
    hashedPassword: Type.Optional(Type.String()),
    userId: Type.String(),
    user: UserType,
});

export const ExtendedUserPermissionLinkType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    revokedAt: Type.Optional(Type.String({ format: 'date-time' })),
    userId: Type.String(),
    permissionId: Type.String(),
    user: UserType,
    permission: PermissionType,
});

export const ExtendedPermissionType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    lastUsed: Type.Optional(Type.String({ format: 'date-time' })),
    resource: Type.String(),
    action: Type.String(),
    role: Type.Array(PermissionRoleLinkType),
    user: Type.Array(UserPermissionLinkType),
});

export const ExtendedRoleTemplateWithPermissionsType = Type.Object({
    id: Type.String(),
    organizationId: Type.String(),
    roleId: Type.String(),
    role: ExtendedRoleWithPermission,
});

export const RoleWithPermissionsSchemaType = Type.Object({
    name: Type.String(),
    id: Type.String(),
    isDefault: Type.Boolean(),
    permissions: Type.Array(Type.Object({
        id: Type.String(),
        resource: Type.String(),
        action: Type.String(),
        isDefault: Type.Boolean()
    })),
});

export const ExtendedWalletDetailsType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    name: Type.String(),
    balance: Type.Optional(BalanceType),
    paymentGateway: Type.Optional(PaymentGatewayType),
    incomingTransactions: Type.Array(TransactionType),
    outgoingTransactions: Type.Array(TransactionType),
    invoices: Type.Array(InvoiceType),
    transactionIntents: Type.Array(TransactionIntentType),
    authorizedOrganizationUsers: Type.Array(ExtendedAuthorizedOrganizationWalletUsersType),
    authorizedOrganizations: Type.Array(ExtendedOrganizationWalletRoleAccessLinkType),
    authorizedUsers: Type.Array(ExtendedUserWalletRoleAccessLinkType),
    defaultWalletRoles: Type.Array(RoleWithPermissionsSchemaType),
});

export const ExtendedOrganizationDetailsSchemaType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    name: Type.String(),
    authorizedUsers: Type.Array(ExtendedUserOrganizationRoleAccessLinkType),
    authorizedWalletUsers: Type.Array(ExtendedAuthorizedOrganizationWalletUsersType),
    roleTemplates: Type.Array(ExtendedRoleTemplateWithPermissionsType),
    wallets: Type.Array(ExtendedOrganizationWalletGeneralInfoType),
    defaultUserOrganizationRoles: Type.Array(Type.Object({
        name: Type.String(),
        roleId: Type.String(),
    })),
    defaultUserOrganizationPermissions: Type.Array(Type.Object({
        name: Type.String(),
        permissionId: Type.String(),
    })),
    defaultWalletRoles: Type.Array(RoleWithPermissionsSchemaType),
});

export const ExtendedDecryptedPaymentGatewayResultType = Type.Object({
    id: Type.String(),
    createdAt: Type.String({ format: 'date-time' }),
    updatedAt: Type.String({ format: 'date-time' }),
    disabledAt: Type.String({ format: 'date-time' }),
    name: Type.String(),
    paymentIntegrationChannel: Type.Enum(PaymentIntegrationChannel),
    sourceWalletId: Type.String(),
    settings: Type.Array(PaymentGatewaySettingsTypeResult),
    apiKeys: Type.Array(PaymentGatewayDecryptedApiKeysResult),
});