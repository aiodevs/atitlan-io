import WalletService from "@/components/finance/wallet/wallet.service";
import { getPrisma } from "@/util/prisma";
import { PaymentGateway, PaymentGatewayApiKeyType, PaymentIntegrationChannel } from "@prisma/client";
import AuthorizationService from "@/components/heimdall/authorization/authorization.service";
import { WalletPermissions } from "@/components/heimdall/authorization/authorization.type";
import { BadRequest, Unauthorized } from "@/util/constants";
import { ExtendedPaymentGateway } from "./payment-gateways.type";
import { decrypt } from "@/util/crypto";

export class PaymentGatewayService {
    private walletService: WalletService = new WalletService();
    private authorizationService: AuthorizationService = new AuthorizationService();

    public async create(
        name: string,
        sourceWalletId: string,
        paymentIntegrationChannel: PaymentIntegrationChannel
    ): Promise<PaymentGateway | null> {
        const sourceWallet = await this.walletService.getWalletByIdNoAuthorization(sourceWalletId);
        if (!sourceWallet) {
            console.log(`PaymentGatewayService.create - no wallet found sourceWalletId:${sourceWalletId}`);
            return null;
        }
        // TODO PaymentGatewayService.create - should we do more wallet checks?
        return await getPrisma().paymentGateway.create({
            data: {
                name: name,
                sourceWalletId: sourceWalletId,
                paymentIntegrationChannel: paymentIntegrationChannel
            }
        });
    }

    public async get(
        id: string,
        requestingUserId: string
    ): Promise<PaymentGateway | null> {
        const result = await getPrisma().paymentGateway.findUnique({
            where: {
                id: id
            }
        });
        if (!result) return null;
        const wallet = await this.walletService.getWalletByIdNoAuthorization(result.sourceWalletId);
        if (!wallet) return null;
        const hasPermissionToView = await this.authorizationService
            .userHasAnyWalletPermission(requestingUserId, wallet.id, WalletPermissions.Read);
        if (!hasPermissionToView) {
            console.log(`PaymentGatewayService.get - user does not have permission to view wallet requestingUserId:${requestingUserId} walletId:${wallet.id}`)
            throw new Unauthorized();
        }
        return result;
    }

    public async getNoAuth(
        id: string
    ): Promise<PaymentGateway | null> {
        return await getPrisma().paymentGateway.findUnique({
            where: {
                id: id
            }
        });
    }

    public async validPaymentGateway(
        id: string,
        paymentIntegrationChannel: PaymentIntegrationChannel
    ): Promise<boolean> {
        const result = await getPrisma().paymentGateway.findFirst({
            where: {
                id: id,
                OR: [{ disabledAt: null }, { disabledAt: { gt: new Date() } }],
                paymentIntegrationChannel: paymentIntegrationChannel
            }
        });
        const valid: boolean = !!result;
        if (valid) return true;
        const pg = await this.getNoAuth(id);
        if (!pg) {
            console.log(`PaymentGatewayService.validPaymentGateway - payment gateway not found id:${id}`);
            throw new BadRequest();
        }
        if (!!pg.disabledAt && pg.disabledAt < new Date()) {
            console.log(`PaymentGatewayService.validPaymentGateway - payment gateway is disabled id:${id}`);
            throw new BadRequest();
        }
        if (pg.paymentIntegrationChannel !== paymentIntegrationChannel) {
            console.log(`PaymentGatewayService.validPaymentGateway - payment gateway is not of the correct channel id:${id}`);
            throw new BadRequest();
        }
        return false;
    }

    public async paymentGatewayWithWalletExists(
        walletId: string
    ): Promise<boolean> {
        const result = await getPrisma().paymentGateway.findFirst({
            where: {
                sourceWalletId: walletId,
                OR: [{ disabledAt: null }, { disabledAt: { gt: new Date() } }]
            }
        });
        return !!result;
    }

    public async getDetailsNoAuth(
        id: string
    ): Promise<ExtendedPaymentGateway | null> {
        const result = await getPrisma().paymentGateway.findUnique({
            relationLoadStrategy: 'join',
            where: {
                id: id
            },
            include: {
                settings: {
                    where: {
                        paymentGatewayId: id
                    },
                    include: {
                        setting: true
                    },
                },
                apiKeys: {
                    where: {
                        paymentGatewayId: id
                    },
                    include: {
                        apiKey: true,
                    }
                }
            }
        });
        if (!result) return null;
        const apikeys: {
            id: string;
            createdAt: Date;
            updatedAt: Date;
            name: string;
            apiKey: string;
            type: PaymentGatewayApiKeyType;
        }[] = [];
        for (const apiKey of result.apiKeys) {
            const decrypted = await decrypt(apiKey.apiKey.encryptedKey);
            const remappedResult = {
                id: apiKey.apiKey.id,
                createdAt: apiKey.apiKey.createdAt,
                updatedAt: apiKey.apiKey.updatedAt,
                name: apiKey.apiKey.name,
                apiKey: decrypted,
                type: apiKey.type
            };
            apikeys.push(remappedResult);
        }
        const settings: {
            id: string;
            createdAt: Date;
            updatedAt: Date;
            name: string;
            value: string;
            encrypted: boolean;
        }[] = [];
        for (const setting of result.settings) {
            const decrypted = setting.setting.encrypted ? await decrypt(setting.setting.value) : setting.setting.value;
            settings.push({
                id: setting.setting.id,
                createdAt: setting.setting.createdAt,
                updatedAt: setting.setting.updatedAt,
                name: setting.setting.name,
                value: decrypted,
                encrypted: setting.setting.encrypted
            });
        }
        const remappedResult: ExtendedPaymentGateway = {
            id: result.id,
            createdAt: result.createdAt,
            updatedAt: result.updatedAt,
            disabledAt: result.disabledAt,
            name: result.name,
            sourceWalletId: result.sourceWalletId,
            paymentIntegrationChannel: result.paymentIntegrationChannel,
            settings: settings,
            apiKeys: apikeys
        }
        return remappedResult;
    }

    public async update(
        id: string,
        name: string,
        disabledAt: Date | null
    ): Promise<PaymentGateway> {
        const existing = await getPrisma().paymentGateway.findUnique({
            where: { id: id }
        });
        if (!existing) {
            console.log(`PaymentGatewayService.update - payment gateway not found id:${id}`);
            throw new BadRequest();
        }
        return await getPrisma().paymentGateway.update({
            where: { id: id },
            data: {
                name: name,
                disabledAt: disabledAt
            }
        });
    }
}

