import { ERROR400, ERROR500 } from "@/util/constants";
import { Type } from "@sinclair/typebox";
import { FastifySchema } from "fastify";
import { ExtendedDecryptedPaymentGatewayResultType, PaymentGatewayType } from "../finance.schema.types";

export const UpdatePaymentGatewayRequestBody = Type.Object({
    id: Type.String(),
    name: Type.String(),
    disabledAt: Type.Optional(Type.String({ format: 'date-time' }))
});

export const UpdatePaymentGatewaySchema: FastifySchema = {
    description: 'Update a payment gateway.',
    tags: ['payment-gateway'],
    body: UpdatePaymentGatewayRequestBody,
    response: {
        200: Type.Object({
            message: Type.String(),
            paymentGateway: PaymentGatewayType
        }),
        400: ERROR400,
        500: ERROR500
    },
    security: [{ cookieAuth: [] }]
};

export const GetPaymentGatewayRequestParams = Type.Object({
    id: Type.String(),
});

export const GetPaymentGatewaySchema: FastifySchema = {
    description: 'Retrieve a payment gateway by ID.',
    tags: ['payment-gateway'],
    params: GetPaymentGatewayRequestParams,
    response: {
        200: Type.Object({
            message: Type.String(),
            paymentGateway: PaymentGatewayType
        }),
        400: ERROR400,
        500: ERROR500
    },
    security: [{ cookieAuth: [] }]
};

export const GetDetailsPaymentGatewaySchema: FastifySchema = {
    description: 'Retrieve a payment gateway with all the settings. Requires PaymentGatewayManagement permission. Contains decrypted values!',
    tags: ['payment-gateway'],
    params: GetPaymentGatewayRequestParams,
    response: {
        200: Type.Object({
            message: Type.String(),
            paymentGateway: ExtendedDecryptedPaymentGatewayResultType
        }),
        400: ERROR400,
        500: ERROR500
    },
    security: [{ cookieAuth: [] }]
};