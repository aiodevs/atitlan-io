import { FastifyInstance, RouteOptions } from "fastify";
import PaymentGatewayController from "./payment-gateways.controller";
import { GetDetailsPaymentGatewaySchema, GetPaymentGatewaySchema, UpdatePaymentGatewaySchema } from "./payment-gateways.schema";
import AtitlanApiRoutes from "@/components/shared/routes";

export default class PaymentGatewayRoutes {
    public controller = new PaymentGatewayController();

    public initializeRoutes(fastify: FastifyInstance, opts: RouteOptions, done: () => void) {

        fastify.route({
            method: "GET",
            url: AtitlanApiRoutes.PAYMENT_GATEWAY.GET_URL,
            schema: GetPaymentGatewaySchema,
            preHandler: fastify.authenticateUser,
            handler: this.controller.get
        });

        fastify.route({
            method: "POST",
            url: AtitlanApiRoutes.PAYMENT_GATEWAY.UPDATE,
            schema: UpdatePaymentGatewaySchema,
            preHandler: fastify.authenticateUser,
            handler: this.controller.update
        });

        fastify.route({
            method: "GET",
            url: AtitlanApiRoutes.PAYMENT_GATEWAY.GET_DETAILS_URL,
            schema: GetDetailsPaymentGatewaySchema,
            preHandler: fastify.authenticateUser,
            handler: this.controller.getDetails
        });

        done();
    }
}