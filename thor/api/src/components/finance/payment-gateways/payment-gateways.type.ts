import { PaymentGateway, Settings, ApiKey, PaymentGatewayApiKeyType } from "@prisma/client";
import { PaymentGatewayResult } from "../transaction-intent/transaction-intent.type";

export type ExtendedPaymentGateway = PaymentGateway & {
    settings: Settings[],
    apiKeys: {
        id: string;
        createdAt: Date;
        updatedAt: Date;
        name: string;
        apiKey: string;
        type: PaymentGatewayApiKeyType;
    }[]
};
