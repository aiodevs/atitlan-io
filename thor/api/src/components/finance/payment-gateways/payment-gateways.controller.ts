import { FastifyReply, FastifyRequest } from "fastify";
import { BadRequest, Unauthorized } from "@/util/constants";
import { PaymentGatewayService } from "./payment-gateways.service";
import { UpdatePaymentGatewayRequestBody } from "./payment-gateways.schema"
import AuthorizationService from "@/components/heimdall/authorization/authorization.service";
import { WalletPermissions } from "@/components/heimdall/authorization/authorization.type";

export default class PaymentGatewayController {
    private paymentGatewayService: PaymentGatewayService = new PaymentGatewayService();
    private authorizationService: AuthorizationService = new AuthorizationService();

    public get = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
        if (!req.user) throw new Unauthorized();
        const id = req.params.id;
        const paymentGateway = await this.paymentGatewayService.get(id, req.user.id);
        if (!paymentGateway) {
            throw new BadRequest('Payment gateway not found');
        }
        return reply.send({ message: 'Payment gateway retrieved', paymentGateway });
    };

    public update = async (req: FastifyRequest<{ Body: typeof UpdatePaymentGatewayRequestBody }>, reply: FastifyReply) => {
        const { id, name } = req.body as typeof UpdatePaymentGatewayRequestBody;
        const disabledAt: Date | null = req.body.disabledAt ? new Date(req.body.disabledAt) : null;
        const updatedPaymentGateway = await this.paymentGatewayService.update(id, name, disabledAt);
        return reply.send({ message: 'Payment gateway updated', paymentGateway: updatedPaymentGateway });
    };

    public getDetails = async (req: FastifyRequest<{ Params: { id: string } }>, reply: FastifyReply) => {
        if (!req.user) throw new Unauthorized();
        const id = req.params.id;
        const paymentGateway = await this.paymentGatewayService.getDetailsNoAuth(id);

        const hasPaymentGatewayManagementPermission = await this.authorizationService.userHasAnyWalletPermission(
            req.user.id,
            paymentGateway?.sourceWalletId ?? "",
            WalletPermissions.PaymentGatewayManagement);
        if (!hasPaymentGatewayManagementPermission) {
            console.log(`PaymentGatewayController.getExtendedWithSettings - user does not have permission to view payment gateway requestingUserId:${req.user.id} paymentGatewayId:${id}`);
            throw new Unauthorized();
        }
        if (!paymentGateway) {
            console.log(`PaymentGatewayController.getExtendedWithSettings - payment gateway not found id:${id}`);
            throw new BadRequest();
        }
        console.log(`PaymentGatewayController.getExtendedWithSettings - payment gateway found id:${id} sourceWalletId:${paymentGateway.sourceWalletId} userId:${req.user.id}`);
        return reply.send({ message: 'Payment gateway details retrieved', paymentGateway });
    }
}