import { Type } from "@sinclair/typebox";
import { FastifySchema } from "fastify";
import { ERROR400, ERROR500 } from "@/util/constants";
import { ExtendedInvoiceType, InvoiceType } from "../finance.schema.types";

export const CreateInvoiceRequestBody = Type.Object({
    amount: Type.String(),
    currency: Type.String({ minLength: 3, maxLength: 3, pattern: "^[A-Z]{3}$" }),
    description: Type.String(),
    targetWalletId: Type.String(),
});

export type CreateInvoiceRequestBodyType = typeof CreateInvoiceRequestBody;

export const CreateInvoiceSchema: FastifySchema = {
    description: 'Create a new invoice.',
    tags: ['invoice'],
    body: CreateInvoiceRequestBody,
    response: {
        200: {
            description: 'Successful invoice creation',
            type: 'object',
            properties: {
                message: { type: 'string' },
                invoice: InvoiceType,
            }
        },
        400: ERROR400,
        500: ERROR500
    },
    security: [{ cookieAuth: [] }]
};

export const GetInvoiceRequestParams = Type.Object({
    id: Type.String(),
});

export const GetInvoiceSchema: FastifySchema = {
    description: 'Retrieve an invoice by ID.',
    tags: ['invoice'],
    params: GetInvoiceRequestParams,
    response: {
        200: {
            description: 'Invoice retrieval successful',
            type: 'object',
            properties: {
                message: { type: 'string' },
                invoice: ExtendedInvoiceType,
            }
        },
        400: ERROR400,
        500: ERROR500
    },
    security: [{ cookieAuth: [] }]
};
