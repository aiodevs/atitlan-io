import { FastifyInstance, RouteOptions } from "fastify";
import { Routes } from "@/plugins/initializeRoutes";
import InvoiceController from "./invoices.controller";
import { CreateInvoiceSchema, GetInvoiceSchema } from "./invoices.schema";
import AtitlanApiRoutes from "@/components/shared/routes";

export default class InvoiceRoutes implements Routes {
    public path = "/invoices";
    public invoiceController = new InvoiceController();

    public initializeRoutes(
        fastify: FastifyInstance,
        opts: RouteOptions,
        done: () => void
    ) {
        fastify.route({
            method: "GET",
            url: AtitlanApiRoutes.INVOICE.GET_URL,
            schema: GetInvoiceSchema,
            preHandler: fastify.authenticateUser,
            handler: this.invoiceController.get
        });

        fastify.route({
            method: "POST",
            url: AtitlanApiRoutes.INVOICE.CREATE,
            schema: CreateInvoiceSchema,
            preHandler: fastify.authenticateUser,
            handler: this.invoiceController.create
        });

        done();
    }
}