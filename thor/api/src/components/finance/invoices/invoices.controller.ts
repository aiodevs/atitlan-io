import { FastifyReply, FastifyRequest } from "fastify";
import { BadRequest, NotFound, Unauthorized } from "@/util/constants";
import InvoiceService from "./invoices.service";
import { CreateInvoiceRequestBody, CreateInvoiceRequestBodyType, GetInvoiceRequestParams } from "./invoices.schema";
import Decimal from "decimal.js";

export default class InvoiceController {
    private invoiceService: InvoiceService = new InvoiceService();

    public create = async (req: FastifyRequest<{ Body: typeof CreateInvoiceRequestBody }>, reply: FastifyReply) => {
        const currency: string = req.body.currency;
        const description: string = req.body.description ? req.body.description : "";
        const amount: Decimal = new Decimal(req.body.amount);
        const targetWalletId = req.body.targetWalletId;
        if (!req.user) throw new Unauthorized();
        const invoice = await this.invoiceService.createInvoice(
            amount,
            currency,
            description,
            targetWalletId,
            req.user.id
        );
        if (!invoice) {
            throw new BadRequest('Unable to create invoice');
        }
        return reply.send({
            message: 'Invoice created',
            invoice: invoice
        });
    };

    public get = async (req: FastifyRequest<{ Params: typeof GetInvoiceRequestParams }>, reply: FastifyReply) => {
        const id = req.params.id;
        let invoice;
        try {
            invoice = await this.invoiceService.getInvoice(id);

            if (!invoice) {
                throw new NotFound('Invoice not found');
            }
        } catch (e) {
            throw new NotFound('Invoice not found');
        }
        return reply.send({ message: 'Invoice retrieved', invoice: invoice });
    };
}