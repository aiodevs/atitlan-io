import AuthorizationService from "@/components/heimdall/authorization/authorization.service";
import { WalletPermissions } from "@/components/heimdall/authorization/authorization.type";
import { getPrisma } from "@/util/prisma";
import { Invoice, PaymentGateway, PrismaClient, Transaction, TransactionIntent } from "@prisma/client";
import Decimal from "decimal.js";

export interface ExtendedInvoice extends Invoice {
    transactionIntent?: TransactionIntent[];
    paymentGateway?: PaymentGateway | null;
    completedTransaction?: Transaction | null;
}

export default class InvoiceService {

    private authorizationService: AuthorizationService = new AuthorizationService();

    public async createInvoice(
        amount: Decimal,
        currency: string,
        description: string,
        targetWalletId: string,
        requestUserId: string
    ): Promise<Invoice> {
        const hasPermisions = await this.authorizationService.userHasAnyWalletPermission(
            requestUserId,
            targetWalletId,
            WalletPermissions.Invoice
        );
        return await getPrisma().invoice.create({
            data: {
                amount: amount,
                currency: currency,
                description: description,
                targetWalletId: targetWalletId
                // TODO products
            }
        });
    }

    public async getInvoice(invoiceId: string): Promise<ExtendedInvoice | null> {
        return await getPrisma().invoice.findUnique({
            where: {
                id: invoiceId
            },
            include: {
                completedTransactions: true,
                transactionIntent: true
            }
        });
    }
}