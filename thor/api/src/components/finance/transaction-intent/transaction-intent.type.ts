import { TransactionIntent, Invoice, PaymentGateway, CompletedTransactionLink, Transaction } from "@prisma/client";

export type ExtendedTransactionIntent = TransactionIntent & {
    invoice?: Invoice | null;
    paymentGateway?: PaymentGateway | null;
    completedTransactions: CompletedTransactionsLinkWithTransaction[];
};

export type CompletedTransactionsLinkWithTransaction = CompletedTransactionLink & {
    transaction?: Transaction;
};

export enum PaymentGatewayResult {
    success,
    failed,
    expired
}

export enum TransactionIntentStatus {
    Created = 'created',
    Processing = 'processing',
    Success = 'success',
    Failed = 'failed',
    NoInvoiceFound = "NoInvoiceFound",
    NoPaymentGatewayFound = "NoPaymentGatewayFound",
    NoSourceWalletFound = "NoSourceWalletFound",
    Error = "Error",
}