import { FastifyReply, FastifyRequest } from "fastify";
import { BadRequest, Unauthorized } from "@/util/constants";
import TransactionIntentService from "./transaction-intent.service";
import { CreateAnyonymousTransactionIntentBodyType, CreateTransactionIntentBodyType, UpdateAnonymousIntentWithUserWalletBodyType as UpdateIntentWithUserWalletBodyType } from "./transaction-intent.schema";

export default class TransactionIntentController {
    private transactionIntentService: TransactionIntentService = new TransactionIntentService();

    public getTransactionIntent = async (
        req: FastifyRequest<{ Params: { id: string } }>
    ) => {
        const intentId = req.params.id;
        if (!req.user) {
            throw new Unauthorized();
        }
        const intent = await this.transactionIntentService.getIntentById(intentId, true, true, true);
        return {
            message: "Transaction intent retrieved successfully",
            transactionIntent: intent
        };
    }

    public createTransactionIntent = async (req: FastifyRequest<{
        Body: CreateTransactionIntentBodyType
    }>, reply: FastifyReply) => {
        const { invoiceId, userWalletId } = req.body;
        if (!req.user) {
            throw new Unauthorized();
        }
        const userId = req.user.id;
        const transactionIntent = await this.transactionIntentService.createIntentForUserWallet(invoiceId, userWalletId, userId.toString());
        return reply.code(200).send({
            message: "Transaction intent created successfully",
            transactionIntent
        });
    }

    public createAnonymousTransactionIntent = async (req: FastifyRequest<{
        Body: CreateAnyonymousTransactionIntentBodyType
    }>, reply: FastifyReply) => {
        const { invoiceId } = req.body;
        const transactionIntent = await this.transactionIntentService.createIntentForAnonymousWallet(invoiceId);
        return reply.code(200).send({
            message: "Transaction intent created successfully",
            transactionIntent
        });
    }

    public updateIntentWithUserWallet = async (req: FastifyRequest<{
        Body: UpdateIntentWithUserWalletBodyType
    }>, reply: FastifyReply) => {
        const { transactionIntentId, userWalletId } = req.body;
        if (!req.user) {
            throw new Unauthorized();
        }
        const userId = req.user.id;
        const transactionIntent = await this.transactionIntentService.updateIntentWithUserWallet(transactionIntentId, userWalletId, userId);
        return reply.code(200).send({
            message: "Transaction intent updated successfully",
            transactionIntent
        });
    }
}