import { FastifyInstance, FastifyReply, FastifyRequest, RouteOptions } from "fastify";
import { Routes } from "@/plugins/initializeRoutes";
import TransactionIntentController from "./transaction-intent.controller";
import { getTransactionIntentSchema, CreateTransactionIntentSchema, CreateAnyonymousTransactionIntentSchema, UpdateAnonymousIntentWithUserWalletSchema } from "./transaction-intent.schema";
import AtitlanApiRoutes from "@/components/shared/routes";

export default class TransactionIntentRoutes implements Routes {
    public path = "/transaction-intent";
    public transactionIntentController = new TransactionIntentController();

    public initializeRoutes(
        fastify: FastifyInstance,
        opts: RouteOptions,
        done: () => void
    ) {
        fastify.route({
            method: "GET",
            url: AtitlanApiRoutes.TRANSACTION_INTENT.GETURL,
            schema: getTransactionIntentSchema,
            preHandler: fastify.authenticateUser,
            handler: this.transactionIntentController.getTransactionIntent
        });

        fastify.route({
            method: "POST",
            url: AtitlanApiRoutes.TRANSACTION_INTENT.CREATE,
            schema: CreateTransactionIntentSchema,
            preHandler: fastify.authenticateUser,
            handler: this.transactionIntentController.createTransactionIntent
        });

        fastify.route({
            method: "POST",
            url: AtitlanApiRoutes.TRANSACTION_INTENT.CREATE_ANONYMOUS,
            schema: CreateAnyonymousTransactionIntentSchema,
            handler: this.transactionIntentController.createAnonymousTransactionIntent
        });

        fastify.route({
            method: "POST",
            url: AtitlanApiRoutes.TRANSACTION_INTENT.UPDATE_USER_AND_WALLET,
            schema: UpdateAnonymousIntentWithUserWalletSchema,
            preHandler: fastify.authenticateUser,
            handler: this.transactionIntentController.updateIntentWithUserWallet
        });

        done();
    }
}