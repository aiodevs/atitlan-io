import { getPrisma } from "@/util/prisma";
import { PaymentIntegrationChannel, TransactionIntent, TransactionIntentCompletionType, Wallet } from "@prisma/client";
import TransactionsService from "../transactions/transactions.service";
import { BadRequest, Unauthorized } from "@/util/constants";
import WalletService from "@/components/finance/wallet/wallet.service";
import AuthorizationService from "@/components/heimdall/authorization/authorization.service";
import { PaymentGatewayService } from "../payment-gateways/payment-gateways.service";
import { WalletPermissions } from "@/components/heimdall/authorization/authorization.type";
import { PaymentGatewayResult, ExtendedTransactionIntent, TransactionIntentStatus } from "./transaction-intent.type";

export default class TransactionIntentService {
    private transactionsService: TransactionsService = new TransactionsService();
    private walletService: WalletService = new WalletService();
    private authorizationService: AuthorizationService = new AuthorizationService();
    private paymentGatewayService: PaymentGatewayService = new PaymentGatewayService();

    public async createIntentForUserWallet(
        invoiceId: string,
        sourceWalletId: string,
        userId: string
    ): Promise<TransactionIntent> {
        const hasPermission = await this.authorizationService.userHasAnyWalletPermission(
            userId,
            sourceWalletId,
            WalletPermissions.Pay);
        if (!hasPermission) {
            console.log(`createIntent - userId:${userId} does not have permission to pay from walletId:${sourceWalletId}`);
            throw new Unauthorized();
        }
        const created = await getPrisma().transactionIntent.create({
            data: {
                status: TransactionIntentStatus.Created,
                invoiceId: invoiceId,
                sourceWalletId: sourceWalletId,
                requestingUserId: userId
            }
        });
        console.log(`createIntentForUserWallet - intent created for invoiceId:${invoiceId} sourceWalletId:${sourceWalletId} userId:${userId}`);
        return created;
    }

    public async createIntentForAnonymousWallet(
        invoiceId: string
    ) {
        const created = await getPrisma().transactionIntent.create({
            data: {
                status: TransactionIntentStatus.Created,
                invoiceId: invoiceId,
                sourceWalletId: null,
                requestingUserId: null,
                anonymous: true
            }
        });
        console.log(`createIntentForAnonymousWallet - intent created for invoiceId:${invoiceId}`);
        return created;
    }

    public async updateIntentWithUserWallet(
        intentId: string,
        sourceWalletId: string,
        userId: string
    ): Promise<TransactionIntent> {
        const hasPermission = await this.authorizationService.userHasAnyWalletPermission(
            userId,
            sourceWalletId,
            WalletPermissions.Pay);
        if (!hasPermission) {
            console.log(`updateIntentWithUserWallet - intent:${intentId} userId:${userId} does not have permission to pay from walletId:${sourceWalletId}`);
            throw new Unauthorized();
        }
        const intent = await this.getIntentById(intentId);
        if (!intent) {
            console.log(`updateIntentWithUserWallet - no intent found for intentId:${intentId} for userId:${userId}`);
            throw new BadRequest();
        }
        // Consideration: Right now you could do a hostile takeover of a transactionIntent by knowing the intentId
        const updated = await getPrisma().transactionIntent.update({
            where: { id: intentId },
            data: {
                sourceWalletId: sourceWalletId,
                requestingUserId: userId,
                anonymous: false
            }
        });
        console.log(`updateIntentWithUserWallet - intent updated for intentId:${intentId} sourceWalletId:${sourceWalletId} userId:${userId}`);
        return updated;
    }

    public async confirmStartPaymentGatewayProcessing(
        intentId: string,
        paymentGatewayId: string,
        paymentIntegrationChannel: PaymentIntegrationChannel,
        paymentGatewayIdentifier?: string,
    ): Promise<TransactionIntent> {
        const validPaymentGateway = await this.paymentGatewayService.validPaymentGateway(paymentGatewayId, paymentIntegrationChannel);
        if (!validPaymentGateway) {
            console.log(`confirmStartPaymentGatewayProcessing - paymentGatewayId:${paymentGatewayId} is not a valid payment gateway`);
            throw new BadRequest();
        }
        const intent = await this.getIntentById(intentId);
        if (!intent) {
            console.log(`confirmStartPaymentGatewayProcessing - no intent found for intentId:${intentId}`);
            throw new BadRequest();
        }
        const updated = await getPrisma().transactionIntent.update({
            where: { id: intentId },
            data: {
                status: TransactionIntentStatus.Processing,
                paymentGatewayId,
                paymentGatewayIdentifier: paymentGatewayIdentifier ? paymentGatewayIdentifier : ""
            }
        });
        console.log(`confirmStartPaymentGatewayProcessing - intentId ${intentId} - status set to "processing"`);
        return updated;
    }

    // finalizeIntentAndCreatePaymentGatewayTransactions Documentation
    // Supports two types of paymentGatewayTransactions
    //
    // ## A direct payment to an invoice ##
    // The user pays the paymentGateway
    // - Add-Funds - The paymentGateway pays the user for an equivalent amount (possibly minus fees)
    // - Internal-Transaction - The user pays the invoice target wallet
    //
    // ## A balance top up transaction ##
    // A user wants to up their balance throught a paymentgateway like stripe to pay later
    // the invoice target wallet id is the user wallet id
    // - Add-funds transaction is between the stripe wallet and the user wallet
    // - No additional internal-transaction is needed between the users wallet and the users wallet

    public async finalizeIntentAndCreatePaymentGatewayTransactions(
        intentId: string,
        paymentGatewayResult: PaymentGatewayResult,
        paymentGatewayTransactionDescription: string = "",
        invoiceTransactionDescription: string = ""): Promise<TransactionIntent | null> {

        switch (paymentGatewayResult) {
            case PaymentGatewayResult.success:
                return await this.finalizeIntentSucces(
                    intentId,
                    paymentGatewayTransactionDescription,
                    invoiceTransactionDescription)
            case PaymentGatewayResult.expired:
            default:
            case PaymentGatewayResult.failed:
                console.log(`finalizeIntent finalizeIntent - status set to "failed" due to paymentGatewayResult:${paymentGatewayResult}`);
                return await getPrisma().transactionIntent.update({
                    where: { id: intentId },
                    data: { status: TransactionIntentStatus.Failed }// TODO - consider storing the paymentGatewayResult
                });
        }
    }

    private async finalizeIntentSucces(
        intentId: string,
        paymentGatewayTransactionDescription: string = "",
        invoiceTransactionDescription: string = "")
        : Promise<TransactionIntent | null> {
        const result = await getPrisma().$transaction(async (prisma) => {
            let intent: ExtendedTransactionIntent | null = await this.getIntentById(intentId, true, true, false);
            if (intent && intent.anonymous && !intent.requestingUserId) {
                intent = await this.createAnonymousWalletAndUpdateIntent(intentId);
            }
            if (!intent) {
                console.log(`transaction-intent.service.finalizeIntent - No intent found for intentId:${intentId}`);
                throw new BadRequest();
            }
            console.log("intent: ", JSON.stringify(intent, null, 2))
            if (!intent.invoice) {
                console.log(`finalizeIntent - No invoice found for intentId:${intentId} invoiceId:${intent.invoiceId} setting status to thor-error-301`);
                const errorIntent = await prisma.transactionIntent.update({
                    where: { id: intentId },
                    data: {
                        status: TransactionIntentStatus.NoInvoiceFound,
                    }
                });
                return errorIntent;
            }
            if (!intent.paymentGateway) {
                console.log(`finalizeIntent - No paymentGateway found for intentId:${intentId} paymentGatewayId:${intent.paymentGatewayId} setting status to thor-error-301`);
                const errorIntent = await prisma.transactionIntent.update({
                    where: { id: intentId },
                    data: {
                        status: TransactionIntentStatus.NoPaymentGatewayFound,
                    }
                });
                return errorIntent;
            }
            if (!intent.sourceWalletId) {
                console.log(`finalizeIntent - No sourceWalletId found for intentId:${intentId} paymentGatewayId:${intent.paymentGatewayId} setting status to thor-error-301`);
                const errorIntent = await prisma.transactionIntent.update({
                    where: { id: intentId },
                    data: {
                        status: TransactionIntentStatus.NoSourceWalletFound,
                    }
                });
                return errorIntent;
            }

            const paymentGatewayWallet = await this.walletService.getWalletByIdNoAuthorization(intent.paymentGateway.sourceWalletId);
            const targetWallet = await this.walletService.getWalletByIdNoAuthorization(intent.invoice.targetWalletId);
            const userWallet = await this.walletService.getWalletByIdNoAuthorization(intent.sourceWalletId);

            if (!paymentGatewayWallet) {
                console.log(`Source wallet with id ${intent.paymentGateway.sourceWalletId} not found`);
                throw new BadRequest();
            }
            if (!targetWallet) {
                console.log(`Target wallet with id ${intent.invoice.targetWalletId} not found`);
                throw new BadRequest();
            }
            if (!userWallet) {
                console.log(`User wallet with id ${intent.sourceWalletId} not found`);
                throw new BadRequest();
            }

            const validPaymentGatewayWallet = await this.paymentGatewayService.paymentGatewayWithWalletExists(paymentGatewayWallet.id);
            if (!validPaymentGatewayWallet) {
                console.log(`finalizeIntent - paymentGatewayWallet:${paymentGatewayWallet.id} is not a valid payment gateway wallet`);
                throw new BadRequest();
            }
            try {
                const paymentGatewayTransaction =
                    await this.transactionsService.addFundsTransactionNoAuthorization(
                        paymentGatewayWallet.id,
                        userWallet.id,
                        intent.invoice.amount,
                        intent.invoice.amount,
                        intent.invoice.currency,
                        intent.invoice.currency,
                        paymentGatewayTransactionDescription
                    );
                await prisma.completedTransactionLink.create({
                    data: {
                        transactionIntentId: intent.id,
                        transactionId: paymentGatewayTransaction.id,
                        invoiceId: intent.invoiceId,
                        type: TransactionIntentCompletionType.PaymentGateway
                    },
                });

                const extraTransactionForTargetWalletNotUserWallet = userWallet.id !== targetWallet.id;
                if (extraTransactionForTargetWalletNotUserWallet) {
                    let invoiceTransaction;
                    if (intent.anonymous && !intent.requestingUserId) {
                        invoiceTransaction = await this.transactionsService.createAnonymousInternalTransaction(
                            userWallet.id,
                            targetWallet.id,
                            intent.invoice.amount,
                            intent.invoice.amount,
                            intent.invoice.currency,
                            intent.invoice.currency,
                            invoiceTransactionDescription
                        );
                    } else {
                        if (!intent.requestingUserId) {
                            console.log(`finalizeIntent - No requestingUserId found for intentId:${intentId} invoiceId:${intent.invoiceId} setting status to thor-error-302`);
                            throw new BadRequest();
                        }

                        invoiceTransaction = await this.transactionsService.createInternalTransaction(
                            intent.requestingUserId,
                            userWallet.id,
                            targetWallet.id,
                            intent.invoice.amount,
                            intent.invoice.amount,
                            intent.invoice.currency,
                            intent.invoice.currency,
                            invoiceTransactionDescription
                        );
                    }

                    await prisma.completedTransactionLink.create({
                        data: {
                            transactionIntentId: intent.id,
                            transactionId: invoiceTransaction.id,
                            invoiceId: intent.invoiceId,
                            type: TransactionIntentCompletionType.Internal
                        },
                    });
                }
                console.log(`finalizeIntent - intentId:${intentId} invoiceId:${intent.invoiceId} paymentGatewayTransaction:${paymentGatewayTransaction.id} invoiceTransaction:${intent.invoiceId}`);
                return await prisma.transactionIntent.update({
                    where: { id: intentId },
                    data: {
                        status: TransactionIntentStatus.Success,
                        // TODO finalizeIntent - do we want to store the gatewaymessage? maybe just log it.
                    }
                });
            } catch (error) {
                console.log(`finalizeIntent - No transaction created for intentId:${intentId} invoiceId:${intent.invoiceId} setting status to thor-error-302`);
                console.log(`finalizeIntent - Error`, error);
                const errorIntent = prisma.transactionIntent.update({
                    where: { id: intentId },
                    data: {
                        status: TransactionIntentStatus.Error,
                    }
                });
                return errorIntent;
            }
        });
        return result ? result : null;
    }

    public async createAnonymousWalletAndUpdateIntent(intentId: string)
        : Promise<ExtendedTransactionIntent | null> {
        const intent = await this.getIntentById(intentId);
        if (!intent) {
            console.log(`getIntentAndCreateAnonymousWalletIfAnonymous - No intent found for intentId:${intentId}`);
            throw new BadRequest();
        }
        if (!intent.anonymous) {
            return intent;
        }
        const anonymousWallet: Wallet = await this.walletService.createAnonymousWallet();
        const updatedIntent = await getPrisma().transactionIntent.update({
            where: { id: intentId },
            data: {
                sourceWalletId: anonymousWallet.id,
                anonymous: true
            }
        });
        return this.getIntentById(updatedIntent.id);
    }

    async getIntentById(
        intentId: string,
        includeInvoice: boolean = true,
        includePaymentGateway: boolean = true,
        includeCompletedTransaction: boolean = false
    ): Promise<ExtendedTransactionIntent | null> {
        const result = await getPrisma().transactionIntent.findUnique({
            where: { id: intentId },
            include: {
                invoice: includeInvoice ? true : false,
                paymentGateway: includePaymentGateway ? true : false,
                completedTransactions: includeCompletedTransaction ? {
                    include: {
                        transaction: true,
                    }
                } : false
            }
        });

        return result;
    }

    async getIntentByPaymentGatewayIdentifier(paymentGatewayIdentifier: string): Promise<TransactionIntent | null> {

        const result = await getPrisma().transactionIntent.findUnique({
            where: { paymentGatewayIdentifier: paymentGatewayIdentifier },
        });

        return result;
    }
}
