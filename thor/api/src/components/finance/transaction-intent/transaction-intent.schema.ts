import { ERROR401, ERROR500 } from "@/util/constants";
import { Static, Type } from "@sinclair/typebox";
import { FastifySchema } from "fastify";
import { ExtendedTransactionIntentType, TransactionIntentType } from "../finance.schema.types";

export const getTransactionIntentSchema: FastifySchema = {
    description: 'Get a transaction intent',
    tags: ['transaction-intent'],
    params: Type.Object({
        id: Type.String(),
    }),
    response: {
        200: {
            description: 'Successful response',
            params: Type.Object({
                id: Type.String(),
            }),
            type: 'object',
            properties: {
                message: { type: 'string' },
                transactionIntent: ExtendedTransactionIntentType
            }
        },
        401: ERROR401,  // Unauthorized response
        500: ERROR500   // Internal server error response
    },
    security: [{ cookieAuth: [] }]
};

// Define the request body schema
const CreateTransactionIntentBody = Type.Object({
    invoiceId: Type.String({ description: 'ID of the invoice associated with the transaction intent' }),
    userWalletId: Type.String({ description: 'ID of the user wallet associated with the transaction intent' })
});

// Define the Fastify schema for the route
export const CreateTransactionIntentSchema = {
    description: 'Create a new transaction intent',
    tags: ['transaction-intent'],
    body: CreateTransactionIntentBody,
    response: {
        200: Type.Object({
            message: Type.String(),
            transactionIntent: TransactionIntentType,
        }),
        400: Type.Object({ message: Type.String() }),
        500: Type.Object({ message: Type.String() })
    },
    security: [{ cookieAuth: [] }]
};

export type CreateTransactionIntentBodyType = Static<typeof CreateTransactionIntentBody>;

const CreateAnyonymousTransactionIntentBody = Type.Object({
    invoiceId: Type.String({ description: 'ID of the invoice associated with the transaction intent' }),
});

export const CreateAnyonymousTransactionIntentSchema = {
    description: 'Create a new transaction intent for at which state the user is not known or ever known',
    tags: ['transaction-intent'],
    body: CreateAnyonymousTransactionIntentBody,
    response: {
        200: Type.Object({
            message: Type.String(),
            transactionIntent: TransactionIntentType,
        }),
        400: Type.Object({ message: Type.String() }),
        500: Type.Object({ message: Type.String() })
    }
};

export type CreateAnyonymousTransactionIntentBodyType = Static<typeof CreateAnyonymousTransactionIntentBody>;

const UpdateAnonymousIntentWithUserWalletBody = Type.Object({
    transactionIntentId: Type.String({ description: 'ID of the transaction intent to update' }),
    userWalletId: Type.String({ description: 'ID of the user wallet associated with the transaction intent' })

});

export type UpdateAnonymousIntentWithUserWalletBodyType = Static<typeof UpdateAnonymousIntentWithUserWalletBody>;

export const UpdateAnonymousIntentWithUserWalletSchema = {
    description: 'Update an anonymous transaction intent with a user wallet',
    tags: ['transaction-intent'],
    body: UpdateAnonymousIntentWithUserWalletBody,
    response: {
        200: Type.Object({
            message: Type.String(),
            transactionIntent: TransactionIntentType,
        }),
        400: Type.Object({ message: Type.String() }),
        500: Type.Object({ message: Type.String() })
    }
};
