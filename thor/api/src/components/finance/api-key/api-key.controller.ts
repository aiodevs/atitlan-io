import { FastifyReply, FastifyRequest } from "fastify";
import { PaymentGatewayApiKeyService } from "./api-key.service";
import { DeleteApiKeySchemaBody, UpdateApiKeySchemaBody } from "./api-key.schema";

export default class PaymentGatewayApiKeyController {
    private apiKeyService: PaymentGatewayApiKeyService = new PaymentGatewayApiKeyService();

    public update = async (req: FastifyRequest<{ Body: typeof UpdateApiKeySchemaBody }>, reply: FastifyReply) => {
        const { id, name, apiKey } = req.body;
        const updatedApiKey = await this.apiKeyService.update(id, apiKey, name);
        return {
            message: "API Key updated successfully",
            apiKey: updatedApiKey,
        };
    }

    public delete = async (req: FastifyRequest<{ Body: typeof DeleteApiKeySchemaBody }>, reply: FastifyReply) => {
        const { id } = req.body;
        await this.apiKeyService.delete(id);
        return {
            message: "API Key deleted successfully",
        };
    }
}