import { ERROR401, ERROR500 } from "@/util/constants";
import { Type } from "@sinclair/typebox";
import { FastifySchema } from "fastify";
import { ApiKeyType } from "../finance.schema.types";

export const UpdateApiKeySchemaBody = Type.Object({
    id: Type.String(),
    name: Type.String(),
    apiKey: Type.String(),
});

export const DeleteApiKeySchemaBody = Type.Object({
    id: Type.String(),
});

export const GetApiKeySchemaBody = Type.Object({
    id: Type.String(),
});

export const UpdateApiKeySchema: FastifySchema = {
    description: 'Update an existing API key.',
    tags: ['api-keys'],
    body: UpdateApiKeySchemaBody,
    response: {
        200: {
            description: 'Successful response',
            type: 'object',
            properties: {
                message: { type: 'string' },
                apiKey: ApiKeyType
            },
        },
        401: ERROR401,  // Unauthorized response
        500: ERROR500   // Internal server error response
    },
    security: [{ cookieAuth: [] }]
};

export const DeleteApiKeySchema: FastifySchema = {
    description: 'Delete an API key.',
    tags: ['api-keys'],
    body: DeleteApiKeySchemaBody,
    response: {
        200: {
            description: 'API key deleted successfully',
            type: 'object',
            properties: {
                message: { type: 'string' }
            },
        },
        401: ERROR401,  // Unauthorized response
        500: ERROR500   // Internal server error response
    },
    security: [{ cookieAuth: [] }]
};
