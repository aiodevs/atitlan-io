import { FastifyInstance, RouteOptions } from "fastify";
import PaymentGatewayApiKeyController from "./api-key.controller";
import { DeleteApiKeySchema, UpdateApiKeySchema } from "./api-key.schema";
import AtitlanApiRoutes from "@/components/shared/routes";

export default class ApiKeyRoutes {
    public apiKeyController = new PaymentGatewayApiKeyController();

    // TODO test these endpoints and add test coverage for them
    public initializeRoutes(fastify: FastifyInstance, opts: RouteOptions, done: () => void) {
        fastify.route({
            method: "POST",
            url: AtitlanApiRoutes.API_KEYS.UPDATE,
            schema: UpdateApiKeySchema,
            handler: this.apiKeyController.update
        });

        fastify.route({
            method: "DELETE",
            url: AtitlanApiRoutes.API_KEYS.DELETE,
            schema: DeleteApiKeySchema,
            handler: this.apiKeyController.delete
        });

        done();
    }
}