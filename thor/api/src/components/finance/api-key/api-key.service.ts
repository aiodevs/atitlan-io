import { BadRequest } from "@/util/constants";
import { encrypt } from "@/util/crypto";
import { getPrisma } from "@/util/prisma";
import { ApiKey, PaymentGatewayApiKeyLink, PaymentGatewayApiKeyType } from "@prisma/client";

export type ExtendedApiKey = ApiKey & {
    PaymentGatewayApiKeyLink: PaymentGatewayApiKeyLink | null;
};

export class PaymentGatewayApiKeyService {
    public async createPaymentGatewayApiKey(
        apiKeyName: string,
        apiKeyValue: string,
        paymentGatewayId: string,
        paymentGatewayApiKeyType: PaymentGatewayApiKeyType
    ): Promise<ExtendedApiKey> {
        const encryptedApiKey = await encrypt(apiKeyValue);
        const apiKey = await getPrisma().apiKey.create({
            data: {
                name: apiKeyName,
                encryptedKey: encryptedApiKey,
                PaymentGatewayApiKeyLink: {
                    create: {
                        paymentGatewayId: paymentGatewayId,
                        type: paymentGatewayApiKeyType
                    }
                }
            },
            include: {
                PaymentGatewayApiKeyLink: true
            }
        });
        console.log(`api-key.service.ts - createPaymentGatewayApiKey - created: /n${JSON.stringify(apiKey, null, 2)}`);
        return apiKey;
    }

    public async getAllPaymentGatewayApiKeys(paymentGatewayId: string): Promise<ExtendedApiKey[]> {
        return await getPrisma().apiKey.findMany({
            include: {
                PaymentGatewayApiKeyLink: true
            },
            where: {
                PaymentGatewayApiKeyLink: {
                    paymentGatewayId: paymentGatewayId
                }
            }
        });
    }

    public async update(
        id: string,
        apiKey: string,
        name?: string,
    ): Promise<ApiKey> {
        const existingKey = await getPrisma().apiKey.findUnique({
            where: { id: id },
            include: {
                PaymentGatewayApiKeyLink: true
            }
        });
        if (!existingKey) {
            console.log(`api-key.service.ts - update - no key found with id: ${id}`)
            throw new BadRequest();
        }
        const encryptedApiKey = await encrypt(apiKey);
        const newName = name || name === "" ? name : existingKey.name;
        const updatedApiKey = await getPrisma().apiKey.update({
            where: { id: id },
            data: {
                name: newName,
                encryptedKey: encryptedApiKey
            }
        });
        console.log(`api-key.service.ts - update - updated key: ${id}`);

        return updatedApiKey;
    }

    public async delete(id: string) {
        await getPrisma().apiKey.delete({
            where: { id: id }
        });
    }
}