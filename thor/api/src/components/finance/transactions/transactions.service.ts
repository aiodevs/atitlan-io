import { getPrisma } from '@/util/prisma';
import { PrismaClient, Transaction, Wallet } from '@prisma/client';
import crypto from 'crypto';
import { BadRequest, Unauthorized } from '@/util/constants';
import WalletService from '../wallet/wallet.service';
import Decimal from 'decimal.js';
import { BalanceSheet } from '../wallet/wallet.types';
import AuthorizationService from '@/components/heimdall/authorization/authorization.service';
import { WalletPermissions } from '@/components/heimdall/authorization/authorization.type';

export default class TransactionsService {
    private walletService: WalletService = new WalletService();
    private authorizationService: AuthorizationService = new AuthorizationService();

    public async createInternalTransaction(
        requestingUserId: string,
        sourceWalletId: string,
        targetWalletId: string,
        sourceAmount: Decimal,
        targetAmount: Decimal,
        targetCurrency: string,
        sourceCurrency: string,
        description: string = ""): Promise<Transaction> {

        const hasPayPermission = await this.authorizationService.userHasAnyWalletPermission(
            requestingUserId,
            sourceWalletId,
            WalletPermissions.Pay);
        if (!hasPayPermission) {
            console.log(`createInternalTransaction - User:${requestingUserId} does not have permission to pay from sourceWallet:${sourceWalletId}`);
            throw new Unauthorized();
        }

        const transaction = this.createInternalTransactionNoAuthorizationCheck(
            sourceWalletId,
            targetWalletId,
            sourceAmount,
            targetAmount,
            targetCurrency,
            sourceCurrency,
            description
        );
        console.log(`createInternalTransaction succes for: ${targetAmount} ${targetCurrency} to target wallet:${targetWalletId} from: ${sourceWalletId} of ${sourceAmount} ${sourceCurrency}`);
        return transaction;
    }

    public async createAnonymousInternalTransaction(
        sourceWalletId: string,
        targetWalletId: string,
        sourceAmount: Decimal,
        targetAmount: Decimal,
        targetCurrency: string,
        sourceCurrency: string,
        description: string = ""): Promise<Transaction> {

        const sourceWalletDetails = await this.walletService.getWalletDetailsNoAuthorization(sourceWalletId);
        if (!sourceWalletDetails) {
            console.log(`createAnonymousInternalTransactio - targetWallet:${targetWalletId} not found`);
            throw new BadRequest();
        }
        if (sourceWalletDetails.authorizedOrganizationUsers.length > 0
            || sourceWalletDetails.authorizedUsers.length > 0
            || sourceWalletDetails.authorizedOrganizationUsers.length > 0) {
            console.log(`createAnonymousInternalTransactio - targetWallet:${targetWalletId} has ` +
                ` connections and is therefor not anonymous and safe to let anyone use it.`);
            throw new Unauthorized();
        }
        const transaction = this.createInternalTransactionNoAuthorizationCheck(
            sourceWalletId,
            targetWalletId,
            sourceAmount,
            targetAmount,
            targetCurrency,
            sourceCurrency,
            description
        );
        console.log(`createAnonymousInternalTransaction succes for: ${targetAmount} ${targetCurrency} to target wallet:${targetWalletId} from: ${sourceWalletId} of ${sourceAmount} ${sourceCurrency}`);
        return transaction;
    }


    public async createInternalTransactionNoAuthorizationCheck(
        sourceWalletId: string,
        targetWalletId: string,
        sourceAmount: Decimal,
        targetAmount: Decimal,
        targetCurrency: string,
        sourceCurrency: string,
        description: string = ""): Promise<Transaction> {

        if (sourceWalletId === targetWalletId) {
            console.log(`createInternalTransaction - sourceWallet:${sourceWalletId} and targetWallet:${targetWalletId} are the same`);
            throw new BadRequest();
        }

        const result = await getPrisma().$transaction(async (prisma) => {
            const lastTransaction = await prisma.transaction.findFirst({
                orderBy: {
                    transactionTime: 'desc'
                }
            });
            const previousHash = lastTransaction?.hash ?? '';
            const lastTransactionId = lastTransaction?.id;

            const balance: BalanceSheet = await this.walletService.getWalletBalance(sourceWalletId);

            if (!balance || balance[sourceCurrency] === undefined) {
                console.log(`createInternalTransaction - No balance found for sourceWallet:${sourceWalletId} lastTransactionId:${lastTransactionId} to make the transaction to targetwallet ${targetWalletId}`);
                throw new Unauthorized("Insufficient funds");
            }

            if (balance[sourceCurrency].net < sourceAmount) {
                console.log(`createInternalTransaction - Insufficient funds for sourceWallet:${sourceWalletId} lastTransactionId:${lastTransactionId} to make the transaction to targetwallet ${targetWalletId}`);
                throw new Unauthorized('Insufficient funds');
            }

            const hash = createTransactionHash(
                previousHash,
                sourceCurrency,
                sourceAmount.toString(),
                targetCurrency,
                targetAmount.toString(),
                sourceWalletId.toString(),
                targetWalletId.toString(),
                description);

            const newTransaction = await prisma.transaction.create({
                data: {
                    sourceWalletId: sourceWalletId,
                    targetWalletId: targetWalletId,
                    sourceAmount: sourceAmount,
                    targetAmount: targetAmount,
                    sourceCurrency: sourceCurrency,
                    targetCurrency: targetCurrency,
                    description: description,
                    hash: hash,
                    previousHash: previousHash
                }
            });

            return newTransaction;
        });

        return result;
    }

    public async addFundsTransactionNoAuthorization(
        sourceWalletId: string,
        targetWalletId: string,
        sourceAmount: Decimal,
        targetAmount: Decimal,
        targetCurrency: string,
        sourceCurrency: string,
        description: string): Promise<Transaction> {

        if (sourceWalletId === targetWalletId) {
            console.log(`createInternalTransaction - sourceWallet:${sourceWalletId} and targetWallet:${targetWalletId} are the same`);
            throw new BadRequest();
        }

        const result = await getPrisma().$transaction(async (prisma) => {
            const lastTransaction = await prisma.transaction.findFirst({
                orderBy: {
                    transactionTime: 'desc'
                }
            });
            const previousHash = lastTransaction?.hash ?? '';

            const hash = createTransactionHash(
                previousHash,
                sourceCurrency,
                sourceAmount.toString(),
                targetCurrency,
                targetAmount.toString(),
                sourceWalletId.toString(),
                targetWalletId.toString(),
                description);

            const newTransaction = await prisma.transaction.create({
                data: {
                    sourceWalletId: sourceWalletId,
                    targetWalletId: targetWalletId,
                    sourceAmount: targetAmount,
                    targetAmount: targetAmount,
                    sourceCurrency: targetCurrency,
                    targetCurrency: targetCurrency,
                    description: description,
                    hash: hash,
                    previousHash: previousHash
                }
            });
            console.log(`addFundsTransaction succes for: ${targetAmount} ${targetCurrency} to target wallet:${targetWalletId} from: ${sourceWalletId} of ${sourceAmount} ${sourceCurrency} hash: ${hash}`)

            return newTransaction;
        });

        return result;
    }

    public async getTransaction(transactionId: string): Promise<Transaction | null> {
        console.log(`getTransaction:${transactionId}`);
        const transaction = await getPrisma().transaction.findUnique({
            where: {
                id: transactionId,
            },
        });

        return transaction;
    }

    public async checkTransactionIntegrity(): Promise<boolean> {
        const transactions = await getPrisma().transaction.findMany({
            orderBy: {
                transactionTime: 'asc',
            },
        });

        let lastHash = '';
        let lastTransactionId = '';

        for (const transaction of transactions) {

            if (transaction.previousHash !== lastHash) {
                console.log(`Transaction:${transaction.id} previousHash:${transaction.previousHash} does not match lastHash:${lastHash}.  Previous entry with id ${lastTransactionId} most likely comprimised.  Investigation advised!`);
                return false;
            }

            const hashCheck = createTransactionHash(
                transaction.previousHash,
                transaction.sourceCurrency,
                transaction.sourceAmount.toString(),
                transaction.targetCurrency,
                transaction.targetAmount.toString(),
                transaction.sourceWalletId.toString(),
                transaction.targetWalletId.toString(),
                transaction.description ? transaction.description : ""
            );

            if (hashCheck !== transaction.hash) {
                console.log(
                    `Transaction:${transaction.id} hash:${transaction.hash} does not match calculated hash:${hashCheck}. This entry is most likely comprimised. Investigation advised!`)
                return false;
            }

            lastHash = transaction.hash;
            lastTransactionId = transaction.id.toString();
        }

        return true;
    }
}

export function createTransactionHash(
    previousHash: string,
    sourceCurrency: string,
    sourceAmount: string,
    targetCurrency: string,
    targetAmount: string,
    sourceWalletId: string,
    targetWalletId: string,
    description: string): string {
    const dataToHash = `${previousHash}|${sourceCurrency}|${sourceAmount}|${targetCurrency}|${targetAmount}|${sourceWalletId}|${targetWalletId}|${description}`;
    const hash = crypto.createHash('sha256').update(dataToHash).digest('hex');
    console.log(`createTransactionHash dataToHash ${dataToHash}`);
    console.log(`createTransactionHash hash ${hash}`);
    return hash;
}