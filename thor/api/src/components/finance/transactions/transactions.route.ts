import { FastifyInstance, FastifyReply, FastifyRequest, RouteOptions } from "fastify";
import { Routes } from "@/plugins/initializeRoutes";
import TransactionsController from "./transactions.controller";
import { CheckTransactionIntegritySchema, CreateFundsTransactionBody, CreateInternalTransactionSchema, GetTransactionSchema } from "./transactions.schema";
import AtitlanApiRoutes from "@/components/shared/routes";


export default class TransactionRoutes implements Routes {
    public transactionsController = new TransactionsController();

    public initializeRoutes(
        fastify: FastifyInstance,
        opts: RouteOptions,
        done: () => void
    ) {
        fastify.route({
            method: "GET",
            url: AtitlanApiRoutes.TRANSACTIONS.GET_URL,
            schema: GetTransactionSchema,
            preHandler: fastify.authenticateUser,
            handler: this.transactionsController.getTransaction
        });

        fastify.route({
            method: "post",
            url: AtitlanApiRoutes.TRANSACTIONS.CREATE_INTERNAL,
            schema: CreateInternalTransactionSchema,
            preHandler: fastify.authenticateUser,
            handler: this.transactionsController.createInternalTransaction
        });

        fastify.route({
            method: "post",
            url: AtitlanApiRoutes.TRANSACTIONS.ADD_FUNDS,
            schema: CreateFundsTransactionBody,
            preHandler: fastify.authenticateUser,
            handler: this.transactionsController.addFundsTransaction
        });

        fastify.route({
            method: "GET",
            url: AtitlanApiRoutes.TRANSACTIONS.CHECK_INTEGRITY,
            schema: CheckTransactionIntegritySchema,
            preHandler: fastify.authenticateUser,
            handler: this.transactionsController.checkTransactionIntegrity
        });

        done();
    }
}