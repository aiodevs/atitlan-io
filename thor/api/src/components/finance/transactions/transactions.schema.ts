import { ERROR500, ERROR401, responseProperty } from "@/util/constants";
import { Type } from "@sinclair/typebox";
import { FastifySchema } from "fastify";
import { ExtendedTransactionType, TransactionType } from "../finance.schema.types";

export const CreateInternalTransactionBody = Type.Object({
    sourceWalletId: Type.String(),
    targetWalletId: Type.String(),
    sourceAmount: Type.String({ pattern: "^[0-9]+(\\.[0-9]{1,2})?$" }), // Using string to represent currency amount
    targetAmount: Type.String({ pattern: "^[0-9]+(\\.[0-9]{1,2})?$" }), // Using string to represent currency amount
    sourceCurrency: Type.String({ minLength: 3, maxLength: 3, pattern: "^[A-Z]{3}$" }), // Currency code of length 3 and uppercase
    targetCurrency: Type.String({ minLength: 3, maxLength: 3, pattern: "^[A-Z]{3}$" }), // Currency code of length 3 and uppercase
    description: Type.String()
});

export const CreateInternalTransactionSchema: FastifySchema = {
    description: 'Create an internal transaction between two wallets. Requires the source wallet to have the proper currency balance.',
    tags: ['transactions'],
    body: CreateInternalTransactionBody,
    response: {
        200: {
            description: 'Successful create response',
            type: 'object',
            properties: {
                ...responseProperty,
                transaction: TransactionType
            }
        },
        401: ERROR401,  // Unauthorized response
        500: ERROR500   // Internal server error response
    },
    security: [{ cookieAuth: [] }]
}

export const CreateFundsTransactionBody: FastifySchema = {
    description: 'Add funds to a wallet. Requires finance-mint permission, which allows the creation of negative balances for source wallet.',
    tags: ['transactions'],
    body: CreateInternalTransactionBody,
    response: {
        200: {
            description: 'Successful create response',
            type: 'object',
            properties: {
                ...responseProperty,
                transaction: TransactionType
            }
        },
        401: ERROR401,  // Unauthorized response
        500: ERROR500   // Internal server error response
    },
    security: [{ cookieAuth: [] }]
}

export const CheckTransactionIntegritySchema: FastifySchema = {
    description: 'Check the integrity of all transactions in the database.',
    tags: ['transactions'],
    response: {
        200: {
            description: 'Integrity check result',
            type: 'object',
            properties: {
                message: { type: 'string' },
                isIntegrityMaintained: { type: 'boolean' }
            }
        },
        401: ERROR401,  // Unauthorized response
        500: ERROR500   // Internal server error response
    },
    security: [{ cookieAuth: [] }]
};

export const GetTransactionSchema: FastifySchema = {
    description: 'Retrieve details of a specific transaction by ID.',
    tags: ['transactions'],
    params: Type.Object({
        id: Type.String(),
    }),
    response: {
        200: {
            description: 'Transaction details',
            type: 'object',
            properties: {
                message: { type: 'string' },
                transaction: ExtendedTransactionType
            }
        },
        401: ERROR401,  // Unauthorized response
        500: ERROR500   // Internal server error response
    },
    security: [{ cookieAuth: [] }]
};
