import { FastifyReply, FastifyRequest } from "fastify";
import TransactionsService from "./transactions.service";
import { BadRequest, Unauthorized } from "@/util/constants";
import { CreateInternalTransactionBody } from "./transactions.schema";
import WalletService from "../wallet/wallet.service";
import Decimal from "decimal.js";
import { PaymentGatewayService } from "../payment-gateways/payment-gateways.service";
import AuthorizationService from "@/components/heimdall/authorization/authorization.service";
import { Resources, ThorLockedFeaturePermissions, WalletPermissions, ThorAdminPermissions } from "@/components/heimdall/authorization/authorization.type";

export default class TransactionsController {
  private transactionsService: TransactionsService = new TransactionsService();
  private walletService: WalletService = new WalletService();
  private paymentGatewayService: PaymentGatewayService = new PaymentGatewayService();
  private authorizationService: AuthorizationService = new AuthorizationService();

  public createInternalTransaction = async (
    req: FastifyRequest<{ Body: typeof CreateInternalTransactionBody }>,
  ) => {
    if (!req.user) {
      throw new Unauthorized();
    }

    const sourceWalletId: string = req.body.sourceWalletId;
    const targetWalletId: string = req.body.targetWalletId;
    const sourceWallet = await this.walletService.getWalletByIdNoAuthorization(sourceWalletId);
    const targetWallet = await this.walletService.getWalletByIdNoAuthorization(targetWalletId);

    if (!sourceWallet || !targetWallet) {
      console.log(
        `addFundsTransaction - User ${req.user.id} - Wallet does not exist for ${sourceWalletId} ${targetWalletId} ${sourceWallet} ${targetWallet}`
      );
      throw new BadRequest();
    }

    const sourceWalletIsGateway: boolean =
      await this.paymentGatewayService.paymentGatewayWithWalletExists(sourceWalletId);
    const targetWalletIsGateway: boolean = await this.paymentGatewayService.paymentGatewayWithWalletExists(targetWalletId);
    if (sourceWalletIsGateway) {
      console.log(
        `addFundsTransaction - Transcaction not allowed for payment gateways for sourceWallet: ${sourceWalletId}`
      );
      throw new Unauthorized(
        "Payment gateways are not allowed to send funds to other wallets"
      );
    }
    if (targetWalletIsGateway) {
      console.log(
        `addFundsTransaction - Transcaction not allowed for payment gateways for targetWallet: ${targetWalletId}`
      );
      throw new Unauthorized(
        "Payment gateways are not allowed to receive funds from other wallets"
      );
    }

    const sourceAmount: Decimal = new Decimal(req.body.sourceAmount);
    const targetAmount: Decimal = new Decimal(req.body.targetAmount);
    const description = req.body.description || "";

    const transaction = await this.transactionsService.createInternalTransaction(
      req.user.id,
      sourceWallet.id,
      targetWallet.id,
      sourceAmount,
      targetAmount,
      req.body.sourceCurrency,
      req.body.targetCurrency,
      description
    );

    return {
      message: "Transaction sucessful",
      transaction: transaction,
    };
  };

  public getTransaction = async (
    req: FastifyRequest<{ Params: { id: string } }>,
    reply: FastifyReply
  ) => {
    const transactionId = req.params.id;
    if (!req.user) {
      throw new Unauthorized();
    }

    const transaction = await this.transactionsService.getTransaction(
      transactionId
    );
    return {
      message: "Transaction details retrieved successfully",
      transaction: transaction,
    };
  };

  public addFundsTransaction = async (
    req: FastifyRequest<{ Body: typeof CreateInternalTransactionBody }>,
    reply: FastifyReply
  ) => {
    if (!req.user) {
      throw new Unauthorized();
    }

    const hasAddFundsPermission =
      await this.authorizationService.checkUserPermission(
        req.user.id,
        Resources.ThorLockedFeatures,
        ThorLockedFeaturePermissions.AddFunds
      );
    if (hasAddFundsPermission === false) {
      console.log(`User ${req.user.id} does not have permission to add funds`);
      throw new Unauthorized();
    }

    const sourceWalletId: string = req.body.sourceWalletId;
    const targetWalletId: string = req.body.targetWalletId;
    const sourceWallet = await this.walletService.getWalletByIdNoAuthorization(
      sourceWalletId
    );
    const targetWallet = await this.walletService.getWalletByIdNoAuthorization(
      targetWalletId
    );
    if (!sourceWallet || !targetWallet) {
      console.log(
        `addFundsTransaction - User ${req.user.id} - Wallet does not exist for ${sourceWalletId} ${targetWalletId} ${sourceWallet} ${targetWallet}`
      );
      throw new BadRequest();
    }

    const hasPayPermission =
      await this.authorizationService.userHasAnyWalletPermission(
        req.user.id,
        sourceWalletId,
        WalletPermissions.Pay
      );

    const sourceWalletIsGateway: boolean =
      await this.paymentGatewayService.paymentGatewayWithWalletExists(
        sourceWalletId
      );
    const targetWalletIsGateway: boolean =
      await this.paymentGatewayService.paymentGatewayWithWalletExists(
        targetWalletId
      );
    if (sourceWalletIsGateway) {
      console.log(
        `addFundsTransaction - Transcaction not allowed for payment gateways for sourceWallet: ${sourceWalletId}`
      );
      throw new Unauthorized(
        "Payment gateways are not allowed to send funds to other wallets"
      );
    }
    if (targetWalletIsGateway) {
      console.log(
        `addFundsTransaction - Transcaction not allowed for payment gateways for targetWallet: ${targetWalletId}`
      );
      throw new Unauthorized(
        "Payment gateways are not allowed to receive funds from other wallets"
      );
    }
    const sourceAmount: Decimal = new Decimal(req.body.sourceAmount);
    const targetAmount: Decimal = new Decimal(req.body.targetAmount);
    const description = req.body.description || "";

    const transaction = await this.transactionsService.addFundsTransactionNoAuthorization(
      sourceWallet.id,
      targetWallet.id,
      sourceAmount,
      targetAmount,
      req.body.sourceCurrency,
      req.body.targetCurrency,
      description
    );
    return {
      message: "Transaction created",
      transaction: transaction,
    };
  };

  public checkTransactionIntegrity = async (
    req: FastifyRequest,
    reply: FastifyReply
  ) => {
    if (!req.user) {
      throw new Unauthorized();
    }

    const hasPermissionToCheckIntegrity =
      await this.authorizationService.checkUserPermission(
        req.user.id,
        Resources.ThorAdmin,
        ThorAdminPermissions.Admin
      );
    if (hasPermissionToCheckIntegrity === false) {
      console.log(
        `User ${req.user.id} does not have permission check transaction integrity`
      );
      throw new Unauthorized();
    }

    const isIntegrityMaintained =
      await this.transactionsService.checkTransactionIntegrity();

    return {
      message: "Transaction integrity check completed",
      isIntegrityMaintained: isIntegrityMaintained,
    };
  };
}