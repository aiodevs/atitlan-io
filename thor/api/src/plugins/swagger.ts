import { FastifyInstance } from "fastify";
import fastifySwagger, { FastifyDynamicSwaggerOptions } from "@fastify/swagger";
import fastifySwaggerUi, { FastifySwaggerUiOptions } from "@fastify/swagger-ui";
import { fastifyPlugin } from "fastify-plugin";
import { getVersion } from "../util/util";

export const initSwagger = fastifyPlugin(
  (fastify: FastifyInstance, _: unknown, done: () => void) => {
    const opts: FastifyDynamicSwaggerOptions = {
      swagger: {
        info: {
          title: "Atitlan IO API",
          description: "Api documentation for Atitlan IO",
          version: getVersion(),
        },
        tags: [
          { name: "auth", description: "Endpoints for user authentication, including login, logout, and session management." },
          { name: "user", description: "User profile creation, updates, and retrieval, as well as user-specific actions and data." },
          { name: "wallet", description: "Manage financial wallets, including balance checks, wallet transactions, and history." },
          { name: "organization", description: "Organizations with wallets and authorized users." },
          { name: "invoice", description: "Invoices an excuse to initiate a financial transaction" },
          { name: "transactions", description: "Endpoints for creating, processing, and managing financial transactions." },
          { name: "transaction-intent", description: "User initiated intents to do a financial transactions." },
          { name: "payment-gateway", description: "Integration and management of various payment gateways for transaction processing." },
          { name: "payment-gateway-integrations", description: "Specific operations related to the integration of third-party payment services." },
          { name: "permission", description: "Manage and query user permissions and roles within the system." },
          { name: "api-keys", description: "Provision and management of API keys for payment gateway access and security." },
          { name: "health", description: "Check the health and performance status of the API services." },
        ],
        consumes: ["application/json"],
        produces: ["application/json"],
        securityDefinitions: {
          cookieAuth: {
            type: "apiKey",
            name: "aio-session",
            in: "cookie",
          },
        },
        schemes: ["http"],
        security: [],
      },
    };

    fastify.register(fastifySwagger, opts);

    const uiOpts: FastifySwaggerUiOptions = {
      routePrefix: "/",
      staticCSP: true,
      transformStaticCSP: (header) => header,
      uiConfig: {
        docExpansion: "list",
        deepLinking: false,
      },
    };

    fastify.register(fastifySwaggerUi, uiOpts);
    done();
  }
);
