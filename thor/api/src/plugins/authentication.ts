import { FastifyInstance, FastifyRequest } from "fastify";
import { fastifyPlugin } from "fastify-plugin";
import { getPrisma } from "@/util/prisma";
import { Unauthorized } from "@/util/constants";
import SessionService from "@/components/heimdall/authentication/session.service";

const sessionService = new SessionService();

export const authentication = fastifyPlugin(
  (fastify: FastifyInstance, _: unknown, done: () => void) => {
    const authPreHandler = async (
      request: FastifyRequest) => {
      try {
        const sessionId = sessionService.getSessionIdFromCookie(request);
        if (!sessionId) {
          console.log("authPreHandler - No session found")
          throw Error();
        }
        console.log("authenticationPlugin - checking session for sessionId:", sessionId)
        const session = await sessionService.getSession(sessionId);
        if(!session){
          console.log(`Session not found for sessionId: ${sessionId}`);
          throw Error();
        }
        const user = await getPrisma().user.findUnique({
          where: {
            id: session.userId,
          },
        });
        if (!user) {
          console.log(`User not found for valid session ${sessionId}`);
          throw Error();
        }
        request.user = user;
        sessionService.updateSession(sessionId);
      } catch (error) {
        console.log("Authentication failed", error);
        throw new Unauthorized();
      }
    };
    fastify.decorate("authenticateUser", authPreHandler);
    done();
  }
);
