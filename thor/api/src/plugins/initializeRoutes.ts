import { FastifyPluginCallbackTypebox } from "@fastify/type-provider-typebox";
import { FastifyPluginOptions, FastifyInstance, RouteOptions } from "fastify";
import HealthRoute from "@/components/shared/health/health.route";
import UserRoutes from "@/components/heimdall/user/user.route";
import WalletRoutes from "@/components/finance/wallet/wallet.route";
import TransactionRoutes from "@/components/finance/transactions/transactions.route";
import ApiKeyRoutes from "@/components/finance/api-key/api-key.route";
import PaymentGatewayRoutes from "@/components/finance/payment-gateways/payment-gateways.route";
import TransactionIntentRoutes from "@/components/finance/transaction-intent/transaction-intent.route";
import InvoiceRoutes from "@/components/finance/invoices/invoices.route";
import StripeIntegrationRoutes from "@/components/finance/integration-channels/stripe/stripe.route";
import RecurrenteIntegrationRoutes from "@/components/finance/integration-channels/recurrente/recurrente.route";
import OrganizationRoutes from "@/components/heimdall/organization/organization.route";
import PermissionRoute from "@/components/heimdall/authorization/permission/permission.route";
import AuthenticationRoute from "@/components/heimdall/authentication/authentication.route";

export interface Routes {
  initializeRoutes: (
    fastify: FastifyInstance,
    opts: RouteOptions,
    done: () => void
  ) => void;
}

export const initializeRoutes: FastifyPluginCallbackTypebox<
  FastifyPluginOptions
  > = (server, options, done) => {
  // add the new routes here
  const routes = [
    new AuthenticationRoute(),
    new UserRoutes(),
    new WalletRoutes(),
    new OrganizationRoutes(),
    new InvoiceRoutes(),
    new TransactionRoutes(),
    new TransactionIntentRoutes(),
    new ApiKeyRoutes(),
    new PaymentGatewayRoutes(),
    new StripeIntegrationRoutes(options.stripeFactory),
    new RecurrenteIntegrationRoutes(),
    new PermissionRoute(),
    new HealthRoute(),
  ];

  routes.forEach((route: Routes) => {
    server.register(route.initializeRoutes.bind(route));
  });
  done();
};
