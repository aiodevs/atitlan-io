#!/bin/bash

# This script will wait until the test-db container logs the message 
# indicating it's ready to accept connections or until a timeout is reached.

MAX_WAIT=30 # Maximum wait time in seconds (e.g., 120 seconds or 2 minutes)
WAIT_INTERVAL=5 # How often to check the logs, in seconds
counter=0

echo "Waiting for test-db to be ready..."

while ! docker logs api-test-db-1 --tail=1 | grep -q "database system is ready to accept connections";
do
    sleep $WAIT_INTERVAL
    counter=$((counter + WAIT_INTERVAL))
    
    echo "Waiting for test-db to load... (elapsed: ${counter}s)"

    if [ $counter -ge $MAX_WAIT ]; then
        echo "Timeout reached. test-db is not ready after ${MAX_WAIT}s."
        exit 1
    fi
done

echo "test-db is ready."
