import { defineConfig } from 'astro/config';
import mdx from '@astrojs/mdx';
import sitemap from '@astrojs/sitemap';
import tailwind from '@astrojs/tailwind';
import node from '@astrojs/node';

import vue from '@astrojs/vue';
import paraglide from '@inlang/paraglide-astro';
import ViteSvgLoader from 'vite-svg-loader';
import { THOR_MARKETING_HOST } from './src/lib/config';

export default defineConfig({
  site: `http:/${THOR_MARKETING_HOST}`,
  output: 'server',
  adapter: node({
    mode: 'standalone',
  }),
  i18n: {
    defaultLocale: 'en',
    locales: ['en', 'es'],
  },
  integrations: [
    mdx(),
    sitemap(),
    tailwind({
      applyBaseStyles: false,
    }),
    vue(),
    paraglide({
      project: './project.inlang',
      outdir: './src/translation',
    }),
  ],
  vite: {
    plugins: [ViteSvgLoader()],
  },
});
