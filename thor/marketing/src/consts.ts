// Place any global data in this file.
// You can import this data from anywhere in your site by using the `import` keyword.

export const SITE_TITLE: string = 'Atitlan IO';
export const SITE_DESCRIPTION: string = 'Atitlan IO is a software development company that specializes in building web applications, mobile apps, and websites. We believe in open-source, decentralization, privacy and being in service to the community.';
