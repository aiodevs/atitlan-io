import type { APIRoute } from 'astro';
import { z } from 'zod';
import { getPrisma } from '@/lib/prisma';

const requestSchema = z.object({
    email: z.string().email()
});

export const POST: APIRoute = async ({ request }) => {
    console.log('POST /api/waitlistentry/add');
    try {
        const body = await request.json();
        const validation = requestSchema.safeParse(body);

        if (!validation.success) {
            console.error("Validation failed:", validation.error.format());
            return new Response(JSON.stringify({ error: validation.error.format() }), { status: 400 });
        }

        const { email } = validation.data;

        const existingUser = await getPrisma().waitListUser.findUnique({
            where: { email },
        });
        if(existingUser) {
            return new Response('ok', { status: 201 });
        }

        const newUser = await getPrisma().waitListUser.create({
            data: { email },
        });

        console.log('Added user to waitlist:', newUser);

        return new Response('ok', { status: 201 });
    } catch (error) {
        console.error('Error adding user to waitlist:', error);
        return new Response(JSON.stringify({ error: 'Failed to add user' }), { status: 500 });
    }
};