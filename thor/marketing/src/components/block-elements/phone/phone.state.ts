import { GenericStoreManager, createStoreManager } from '@/lib/store-manager';
import { Atom, atom, WritableAtom } from 'nanostores';

export enum AppScreenTexture {
    dashboard = 'images/app/dashboard.png',
    insightsChart = 'images/app/insights-chart.png',
    insightPie = 'images/app/insights-pie.png',
    invoice = 'images/app/invoice.png',
    login = 'images/app/login.png',
    pos = 'images/app/pos.png',
    profile = 'images/app/profile.png',
    statusScan = 'images/app/status-scan.png',
    statusProgress = 'images/app/status-progress.png',
    succes = 'images/app/succes.png',
    transfer = 'images/app/transfer.png',
}

export const appScreenStoreManager: GenericStoreManager<AppScreenTexture> = createStoreManager<AppScreenTexture>();
