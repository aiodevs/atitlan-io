import { atom, WritableAtom } from 'nanostores';

export class GenericStoreManager<T> {
  private stores: { [key: string]: ReturnType<typeof atom<T>> } = {};

  getStore(key: string, initialValue: T):WritableAtom<T> {
    if (!this.stores[key]) {
      console.log('Creating store', key);
      this.stores[key] = atom(initialValue);
    }
    return this.stores[key];
  }

  deleteStore(key: string) {
    delete this.stores[key];
  }

  setStoreValue(key: string, value: T) {
    const store = this.getStore(key, value);
    store.set(value);
  }
}

export const createStoreManager = <T>() => new GenericStoreManager<T>();