import { PrismaClient } from "@prisma/client";
import { DATABASE_URL, loadConfigFile, setDatabaseUrl, THOR_MARKETING_DB_HOST, THOR_MARKETING_DB_PASSWORD, THOR_MARKETING_DB_PORT, THOR_MARKETING_DB_SCHEMA, THOR_MARKETING_DB_USER } from "./config";

let prisma: PrismaClient | null = null;
let databaseURL: string | undefined = undefined;

export function getPrisma() {
    if (!prisma) {
        databaseURL = getOrSetPrismaDbUrl();
        prisma = new PrismaClient({
            datasources: {
                db: {
                    url: databaseURL,
                },
            },
        });
    }

    return prisma;
}

export function getOrSetPrismaDbUrl(): string {
    if (!databaseURL) {
        databaseURL = DATABASE_URL;
        if (!databaseURL) {
            databaseURL = generateDatabaseURL();
        }
    }
    return databaseURL;
}


export async function setPrismaDbUrl(dbUrl: string) {
    setDatabaseUrl(dbUrl);
    databaseURL = dbUrl;
    if (prisma) {
        await prisma.$disconnect();
    }
    prisma = new PrismaClient({
        datasources: {
            db: {
                url: dbUrl,
            },
        },
    });
}

export async function connectToDb() {
    try {
        await getPrisma().$connect();
        console.log("Database connection successful - connect");
    } catch (error) {
        const url = DATABASE_URL;
        const cleanUrl = url?.split("@")[1];
        console.log(`FAILED - Attempting to connect to DB with URL: ${cleanUrl}`);
    }
}

export function generateDatabaseURL(): string {
    loadConfigFile();
    const POSTGRES_USER = THOR_MARKETING_DB_USER!;
    const POSTGRES_PASSWORD =THOR_MARKETING_DB_PASSWORD!;
    const POSTGRES_DB = THOR_MARKETING_DB_SCHEMA!;
    const POSTGRES_HOST = THOR_MARKETING_DB_HOST!;
    const POSTGRES_PORT = THOR_MARKETING_DB_PORT!;
    return `postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}?schema=public`;
}