import { z } from "zod";
import { config } from "dotenv";

loadConfigFile();

const envSchema = z.object({
    NODE_ENV: z.string(),
    DATABASE_URL: z.string().optional(),
    THOR_MARKETING_DB_USER: z.string().optional(),
    THOR_MARKETING_DB_PASSWORD: z.string().optional(),
    THOR_MARKETING_DB_SCHEMA: z.string().optional(),
    THOR_MARKETING_DB_HOST: z.string().optional(),
    THOR_MARKETING_DB_PORT: z.string().optional(),
    THOR_MARKETING_HOST: z.string().optional(),
});

const parsedEnv = envSchema.safeParse(process.env);
if (!parsedEnv.success) {
    console.error("Invalid environment variables:", parsedEnv.error.format());
    process.exit(1);
}

export const {
    NODE_ENV,
    DATABASE_URL,
    THOR_MARKETING_DB_USER,
    THOR_MARKETING_DB_PASSWORD,
    THOR_MARKETING_DB_SCHEMA,
    THOR_MARKETING_DB_HOST,
    THOR_MARKETING_DB_PORT,
    THOR_MARKETING_HOST,
} = parsedEnv.data;

export function setDatabaseUrl(url: string) {
    process.env.DATABASE_URL = url;
}

export function loadConfigFile(path?: string) {
    const isProduction = process.env.NODE_ENV === "production";
    const isTest = process.env.NODE_ENV === "test";

    if (isProduction || isTest) {
        console.log('In production or test, skipping .env file load.');
        return;
    }

    const result = path ? config({ path }) : config();

    console.log(`Loading config from env file: ${result.parsed}`);

    if (result.error) {
        console.log(`Loading config failed from env file: ${result.error}`);
        throw result.error;
    }
}