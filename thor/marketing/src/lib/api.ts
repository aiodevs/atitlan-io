import { z } from 'zod';

const waitlistAddPath = '/api/waitlistentry/add';

export async function addWaitlistEntry(email: string): Promise<Response> {
    const url = waitlistAddPath;
    const response = await fetch(waitlistAddPath, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email }),
    });
    console.log(response.body);
    return response;
}


export const waitListEntry =
    z.object({
        email: z.string().min(4).max(255).email(),
        terms: z.literal(true, {
            errorMap: () => ({ message: 'You must acknowledge that the product is in early access' }),
          }),
    });