import * as THREE from 'three';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';
import { DRACOLoader } from 'three/addons/loaders/DRACOLoader.js';
import { TextureLoader } from 'three/src/loaders/TextureLoader.js';
export class ThreeLoader {
  private textureLoader: TextureLoader;
  private dracoLoader: DRACOLoader;
  private gltfLoader: GLTFLoader;

  constructor() {
    this.textureLoader = new TextureLoader();
    this.dracoLoader = new DRACOLoader();
    this.gltfLoader = new GLTFLoader();

    this.dracoLoader.setDecoderPath('/draco/');
    this.dracoLoader.setDecoderConfig({ type: 'js' });
    this.gltfLoader.setDRACOLoader(this.dracoLoader);
  }

  public async loadModel(
    path: string,
  ): Promise<THREE.Group> {
    try {
      const gltf = await this.gltfLoader.loadAsync(path);
      return gltf.scene;
    } catch (error) {
      console.error('Error loading model:', error);
      throw error;
    }
  }

  public async loadTexture(
    path: string
  ): Promise<THREE.Texture> {
    try {
      return await this.textureLoader.loadAsync(path);
    } catch (error) {
      console.error('Error loading texture:', error);
      throw error;
    }
  }

  public static setTexture(
    model: THREE.Group,
    material: THREE.material,
    nameFilter: string
  ): void {
    model.traverse(child => {
      if (child instanceof THREE.Mesh && child.name.includes(nameFilter)) {
        child.material = material;
      }
    });
  }
}

export const ThreeLoaderInstance = new ThreeLoader();

export class ThreeScene {
  private scene: THREE.Scene;
  private camera: THREE.PerspectiveCamera;
  private renderer: THREE.WebGLRenderer;
  private canvasWidth: number;
  private canvasHeight: number;

    constructor(
    scene: THREE.Scene,
    camera: THREE.PerspectiveCamera,
    renderer: THREE.WebGLRenderer,
    canvasWidth: number,
    canvasHeight: number
  ) {
    this.scene = scene;
    this.camera = camera;
    this.renderer = renderer;
    this.canvasWidth = canvasWidth;
    this.canvasHeight = canvasHeight;
  }

  public getScene(): THREE.Scene {
    return this.scene;
  }

  public getCamera(): THREE.PerspectiveCamera {
    return this.camera;
  }

  public getRenderer(): THREE.WebGLRenderer {
    return this.renderer;
  }

  public getCanvasWidth(): number {
    return this.canvasWidth;
  }

  public getCanvasHeight(): number {
    return this.canvasHeight;
  }

  public add(model: THREE.Group): void {
    this.scene.add(model);
  }

  public render(): void {
    this.renderer.render(this.scene, this.camera);
  }

  public updateCanvasSize(canvas: HTMLCanvasElement): void {
    this.canvasWidth = canvas.clientWidth;
    this.canvasHeight = canvas.clientHeight;
    this.renderer.setSize(this.canvasWidth, this.canvasHeight);
    this.camera.aspect = this.canvasWidth / this.canvasHeight;
    this.camera.updateProjectionMatrix();
  }

  public static init3DSceneOnCanvas(canvas: HTMLCanvasElement): ThreeScene {
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;
    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);
    const renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    renderer.setSize(width, height);
    const canvast = document.getElementById('threeCanvas');
    canvast?.appendChild(renderer.domElement);

    return new ThreeScene(scene, camera, renderer, width, height);
  }
}
