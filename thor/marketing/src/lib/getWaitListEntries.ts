import { getPrisma } from '@/lib/prisma';


async function logEntries() {
    const entries = await getPrisma().waitListUser.findMany();
    console.log(entries.map(e => e.email));
}

logEntries().catch((e) => {
    console.error(e);
    process.exit(1);
}).finally(async () => {
    await getPrisma().$disconnect();
});
