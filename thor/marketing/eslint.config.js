import globals from "globals";
import pluginJs from "@eslint/js";
import tseslint from "typescript-eslint";
import pluginVue from "eslint-plugin-vue";
import eslintPluginAstro from "eslint-plugin-astro";
import eslintConfigPrettier from "eslint-config-prettier";
import eslintPluginTailwindCSS from "eslint-plugin-tailwindcss";

export default [
  { languageOptions: { globals: globals.browser } },
  pluginJs.configs.recommended,
  ...tseslint.configs.recommended,
  ...pluginVue.configs["flat/recommended"],
  ...eslintPluginAstro.configs.recommended,
  {
    plugins: {
      tailwindcss: eslintPluginTailwindCSS,
    },
    rules: {
      "tailwindcss/no-custom-classname": "error",
    },
  },
  eslintConfigPrettier,
];
