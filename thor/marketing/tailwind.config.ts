import animate from 'tailwindcss-animate';
import tailwindTypography from '@tailwindcss/typography';
import defaultTheme from 'tailwindcss/defaultTheme';

/** @type {import('tailwindcss').Config} */
export default {
  darkMode: ['class'],
  safelist: ['dark'],
  prefix: '',

  content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],

  theme: {
    container: {
      center: true,
      padding: '1rem',
      screens: {
        md: '768px',
        lg: '1024px',
        '2xl': '1200px',
      },
    },
    fontFamily: {
      display: ['urbanist', ...defaultTheme.fontFamily.sans],
      sans: ['inter', ...defaultTheme.fontFamily.sans],
      mono: ['robotmono', ...defaultTheme.fontFamily.mono],
    },
    extend: {
      colors: {
        border: 'hsl(var(--border))',
        input: 'hsl(var(--input))',
        ring: 'hsl(var(--ring))',
        background: 'hsl(var(--background))',
        foreground: 'var(--foreground)',
        alternate: {
          background: 'var(--alternate-background)',
          foreground: 'var(--alternate-foreground)',
        },
        primary: {
          DEFAULT: 'hsl(var(--primary))',
          foreground: 'var(--primary-foreground)',
        },
        secondary: {
          DEFAULT: 'hsl(var(--secondary))',
          foreground: 'var(--secondary-foreground)',
        },
        destructive: {
          DEFAULT: 'var(--destructive)',
          foreground: 'var(--destructive-foreground)',
        },
        muted: {
          DEFAULT: 'var(--muted)',
          foreground: 'var(--muted-foreground)',
          illustration: 'var(--muted-illustration)',
        },
        accent: {
          DEFAULT: 'var(--accent)',
          foreground: 'var(--accent-foreground)',
        },
        popover: {
          DEFAULT: 'var(--popover)',
          foreground: 'var(--popover-foreground)',
        },
        card: {
          DEFAULT: 'hsl(var(--card))',
          foreground: 'var(--card-foreground)',
        },
      },
      screens: {
        xsm: '464px',
      },
      borderRadius: {
        xl: 'calc(var(--radius) + 4px)',
        lg: 'var(--radius)',
        md: 'calc(var(--radius) - 2px)',
        sm: 'calc(var(--radius) - 4px)',
      },
      keyframes: {
        'accordion-down': {
          from: { height: 0 },
          to: { height: 'var(--radix-accordion-content-height)' },
        },
        'accordion-up': {
          from: { height: 'var(--radix-accordion-content-height)' },
          to: { height: 0 },
        },
        'collapsible-down': {
          from: { height: 0 },
          to: { height: 'var(--radix-collapsible-content-height)' },
        },
        'collapsible-up': {
          from: { height: 'var(--radix-collapsible-content-height)' },
          to: { height: 0 },
        },
      },
      animation: {
        'accordion-down': 'accordion-down 0.2s ease-out',
        'accordion-up': 'accordion-up 0.2s ease-out',
        'collapsible-down': 'collapsible-down 0.2s ease-in-out',
        'collapsible-up': 'collapsible-up 0.2s ease-in-out',
        'bounce-x': '',
      },
    },
  },
  plugins: [animate, tailwindTypography],
};
