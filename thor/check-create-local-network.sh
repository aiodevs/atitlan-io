#!/bin/bash
THOR_LOCAL_NETWORK=$1

if [ -n "$THOR_LOCAL_NETWORK" ]; then
  if ! docker network ls | grep -q $THOR_LOCAL_NETWORK; then
    echo "Creating network $THOR_LOCAL_NETWORK"
    docker network create --driver=bridge $THOR_LOCAL_NETWORK
  else
    echo "Network already exists $THOR_LOCAL_NETWORK"
  fi
else
  echo "No local network name provided, skipping creation."
fi
