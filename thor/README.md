# ⚡🪬 THOR 🪬⚡
**Roles:** Authentication, Permissions   
**Apps:** UI, API, DB, Migrator, Seeder   
**Runs in:** *💻terminal💻, 🐋🐳docker compose🐳🐋, 🐋🐳docker stack🐳🐋*  

- **New here? Thor?**  
Run `make setup` in the terminal in the `project_root/thor` to start!  
This will setup everything thor, build images, boot them up and start the testing ui suite.  
***Hint:** Study the Makefile and the package.json to learn cool commands!*  
- **Test driven development** *Develop with a sexy test ui to keep on running tests after every code change via vitest `make test-ui`. Pump those coverage numbers babay!*

**DB Migration and Seeding.**  
The seeder adds an initial admin user with the right permissions to add and remove permissions. Run them via docker `make migrate-docker` or pnpm commands via `make migrate`.  

**How to ⚡🪬start THOR🪬⚡**  
Three easy ways to run, test and develop:  

1. **Docker (in watch mode)**  
`make watch`  
Have containarized apps with hot reload. Good for dev.  
🐋🐋🐋
2. **Terminal**  
`make terminal-up` or `cd api && pnpm dev` && `cd ui && pnpm dev`  
Pnpm in all the terminals. Dev's favourite.  
💻💻
3. **Docker swarm**  
`make stack-up`  
Deploy the stack to a docker swarm to mimic cloudly goodness. Great for testing the stack.  
🐋🐋🐋🐋🐋🐋🐋🐋🐋

## ♾ About the UI ♾

A simple ui for user authentication via thor api.
It is currently the front-facing public secured interface to talk to other privately networked back end services. It also has an api. Astro supports markdown and easy ts api endpoints.

### Tech stack

- Astro - For the server side handling via SSR (server side rendering for extra securities) and routing for now. Also does auth, permissions and easy endpoints.
- Vue - For fancy components
- Tailwind - For that sweet style
- Post css - For sass like syntax

## ♾About the API♾

### First time baby

```
cd thor
make setup-dev
pnpm dev
```

### Auth

Protected end points for pages and services.  
Pages are protected via:
- Session cookies with db sessions for max security.  

Services are protected via **permissions**.
- Permission for resources.  

### Setup the Test Database & Run Tests in the UI
```
cd thor/api
pnpm install
pnpm setup-prisma-env-test
pnpm test-db
pnpm prisma-migrate-test
pnpm test-e2e-ui
```

***♾ Notes on the permission initalization paradox♾**  
You actually require permission to add and remove permissions, so how do you get permission to add and remove the permissions? Well...???? You tell me buddy bro? Eh?~ .. Its inception, oh no the paradox, infinity ♾♾♾♾♾. Beep boop 0111001001110111beepisalive00101001.....*

***How to fix the ♾infinity♾ glitch?**  
Use the initial admin user via seed script to start working with permissions. Drops mic.*

```
{
  "resource": "thor-api",
  "action": "*"
}
```
Example of a wild card free for all access to the thor-api service resource.

### Tech stack:
- Fastify - For express like syntax and speeeeeeed.
- Prisma - For cute schemas ;3
- Bcrypt - For hashing the passwords. Make sure its sufficiently long salt rounds.
- PostgresDB - For storing users, permissions and 

## 💻Setup Development💻

```
make setup-dev
```
This setup includes the copying of env files in the root folder, the ui and api folder that you are free to change. It also runs the migrator and seeds an initial admin user that can set permissions.

## 💻Terminal💻 - Run development in the terminal

You need a postgress db. With the help of the following commands it spins up a database and the first one opens up both the ui and the api in the same terminal window:

```
make terminal-up
```

or seperately in two terminals via:
```
make terminal-api
make terminal-ui
```

## 💻💻💻Notes about resetting thor-api-db

To **reset the database**, delete the thor-api-db container and delete the corresponding volume associated with it. Afterwards run `terminal-up` again. This can be useful if you want to run the migration script again or restart with a fresh database.  

## 🐋🐳🐋 Run in docker watch 🐋🐳🐋

Docker watch is pretty gangster. It can now hot reload files, so you can develop in the a containarized environment like the whale that you are. Yes You! 🐳🐳🐳🐳🐳🐳🐳🐳🐳

```
make watch
```
New builds to force the building of the images:
```
make build
```

## Simulate Production with a docker swarm stack
Create your local swarm to deploy the full stack too.
```
docker swarm init
make stack-up
```
This runs all the docker images for the api, api-db and the ui and sets up the infra too.
You might have to change your env file, dns and host file to add the domains for traefik and the infra.

### Update images
To update your image run:
```
make build
```

## Test Driven Development

Run vitest sweet UI 💻💻💻 

```
make test-db
make test
```

Run tests on docker images or the app directly. Code changes retrigger the tests. 

Currently relies on the test-db.

## Utilities

```
make kill-ports
```
Kill all the processess that use the apps ports.


```
make seed
```
Reseed the initial admin user based on the environment variables (.env for local development). 

```
make migrate
```
Reruns prisma migrations and reloads the prisma client for the pnpm based terminal ran thor api.