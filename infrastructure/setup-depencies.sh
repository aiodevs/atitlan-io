#!/bin/bash

#General dependencies
sudo apt update
sudo apt upgrade -y
sudo apt install -y curl build-essential make chrony npm zsh make grep gettext

#TimeZone
sudo timedatectl set-timezone America/Guatemala

#Docker
curl -fsSL https://get.docker.com -o install-docker.sh
sudo sh install-docker.sh
sudo usermod -aG docker $USER

#Node
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/$(curl -s https://api.github.com/repos/nvm-sh/nvm/releases/latest | grep -oP '"tag_name": "\K(.*)(?=")')/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
echo 'export NVM_DIR="$HOME/.nvm"' >> ~/.bashrc
echo '[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"' >> ~/.bashrc
nvm install 21.1.0
nvm use 21.1.0

#PNPM
npm install -g pnpm
pnpm install dotenv dotenv-cli

# Znap & ZSH
# Backup existing .zshrc if it exists
[ -f ~/.zshrc ] && cp ~/.zshrc ~/.zshrc.backup
# Install Znap for zsh plugins if it isn't already installed
if [ ! -d "$HOME/Repos/znap" ]; then
    git clone --depth 1 https://github.com/marlonrichert/zsh-snap.git ~/Repos/znap
fi
# Setup .zshrc with znap and plugins
cat > ~/.zshrc << 'EOF'
# Load znap
source ~/Repos/znap/znap.zsh

# Znap plugins
znap source zdharma-continuum/fast-syntax-highlighting
znap source marlonrichert/zsh-autocomplete
znap source zsh-users/zsh-autosuggestions

# Powerlevel10k theme
znap source romkatv/powerlevel10k

# NVM and PNPM configurations
export NVM_DIR="$HOME/.nvm"
[[ -s "$NVM_DIR/nvm.sh" ]] && \. "$NVM_DIR/nvm.sh"
export PNPM_HOME="$HOME/.local/share/pnpm"
case ":$PATH:" in
    *":$PNPM_HOME:"*) ;;
    *) export PATH="$PNPM_HOME:$PATH" ;;
esac

# Load Powerlevel10k theme configuration if it exists
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
EOF
# Skip global compinit warning
grep "skip_global_compinit=1" ~/.zshenv || echo "skip_global_compinit=1" >> ~/.zshenv

# Print installed versions
groups
echo "Should have docker group"
echo "Versions installed:"
docker -v
docker ps
node -v
npm -v
pnpm -v
echo "Zsh version: $(zsh --version)"

# Reminder for re-login
echo "Backed up existing .zshrc to .zshrc.backup, recover that if you dont want the new one"
echo "Please switch to the docker group or relog to apply group changes: 'newgrp docker' or restart your terminal."