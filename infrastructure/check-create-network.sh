#!/bin/bash

# Function to create Docker network if it does not exist
create_network() {
  local network_name=$1
  echo "Checking and creating network: $network_name"
  if ! docker network ls | grep -q $network_name; then
    echo "Creating $network_name network"
    docker network create --driver=overlay --attachable $network_name
  else
    echo "$network_name network already exists "
  fi
}

create_network "public-net"
create_network "btc-net"
create_network "agent-net"
create_network "lnbits-net"
create_network "matrix-net"
create_network "tailscale-net"
