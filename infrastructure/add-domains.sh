#!/bin/bash

# Function to add a domain to /etc/hosts if it doesn't exist
add_domain_if_not_exists() {
    local domain=$1
    local ip=$2

    if ! grep -q "$ip $domain" /etc/hosts; then
        echo "Adding $domain to /etc/hosts"
        echo "$ip $domain" | sudo tee -a /etc/hosts
    else
        echo "$domain already exists in /etc/hosts"
    fi
}

add_domain_if_not_exists "portainer.infra.atitlan.io" "127.0.0.1"
add_domain_if_not_exists "traefik.infra.atitlan.io" "127.0.0.1"
add_domain_if_not_exists "headscale.infra.atitlan.io" "127.0.0.1"
add_domain_if_not_exists "headscale-ui.infra.atitlan.io" "127.0.0.1"
add_domain_if_not_exists "app.atitlan.io" "127.0.0.1"
add_domain_if_not_exists "thor.app.atitlan.io" "127.0.0.1"
add_domain_if_not_exists "lnd.btc.atitlan.io" "127.0.0.1"
add_domain_if_not_exists "lnbits.btc.atitlan.io" "127.0.0.1"

# Optional: Flush DNS cache (Uncomment for macOS)
#sudo dscacheutil -flushcache; sudo killall -HUP mDNSResponder

# Restart networking service
sudo systemctl restart networking
echo "Restarted networking service"
# If the above fails or does not apply, restart NetworkManager
if [ $? -ne 0 ]; then
    sudo systemctl restart NetworkManager
    echo "Restarted NetworkManager"
fi

echo "Operation completed"
