services:
  #UI dashboard
  dashboard:
    image: netbirdio/dashboard:$NETBIRD_DASHBOARD_TAG
    environment:
      # Endpoints
      - NETBIRD_MGMT_API_ENDPOINT=$NETBIRD_MGMT_API_ENDPOINT
      - NETBIRD_MGMT_GRPC_API_ENDPOINT=$NETBIRD_MGMT_API_ENDPOINT
      # OIDC
      - AUTH_AUDIENCE=$NETBIRD_DASH_AUTH_AUDIENCE
      - AUTH_CLIENT_ID=$NETBIRD_AUTH_CLIENT_ID
      - AUTH_CLIENT_SECRET=$NETBIRD_AUTH_CLIENT_SECRET
      - AUTH_AUTHORITY=$NETBIRD_AUTH_AUTHORITY
      - USE_AUTH0=$NETBIRD_USE_AUTH0
      - AUTH_SUPPORTED_SCOPES=$NETBIRD_AUTH_SUPPORTED_SCOPES
      - AUTH_REDIRECT_URI=$NETBIRD_AUTH_REDIRECT_URI
      - AUTH_SILENT_REDIRECT_URI=$NETBIRD_AUTH_SILENT_REDIRECT_URI
      - NETBIRD_TOKEN_SOURCE=$NETBIRD_TOKEN_SOURCE
      # SSL
      - NGINX_SSL_PORT=443
      # Letsencrypt
      - NETBIRD_DISABLE_LETSENCRYPT=true
      - LETSENCRYPT_DOMAIN=$NETBIRD_LETSENCRYPT_DOMAIN
      - LETSENCRYPT_EMAIL=$NETBIRD_LETSENCRYPT_EMAIL
    volumes:
      - $LETSENCRYPT_VOLUMENAME:/etc/letsencrypt/
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.role == manager
          - node.labels.netbird-manager == true
      labels:
        - traefik.enable=true
        - traefik.http.routers.netbird-dashboard.rule=Host(`$NETBIRD_DOMAIN`)
        - traefik.http.services.netbird-dashboard.loadbalancer.server.port=80

  # Signal
  signal:
    image: netbirdio/signal:$NETBIRD_SIGNAL_TAG
    restart: unless-stopped
    volumes:
      - $SIGNAL_VOLUMENAME:/var/lib/netbird
    network:
      - public-net
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.role == manager
          - node.labels.netbird == true
      labels:
        - traefik.enable=true
        - traefik.http.routers.netbird-signal.rule=Host(`$NETBIRD_DOMAIN`) && PathPrefix(`/signalexchange.SignalExchange/`)
        - traefik.http.services.netbird-signal.loadbalancer.server.port=80
        - traefik.http.services.netbird-signal.loadbalancer.server.scheme=h2c

  # Management
  management:
    image: netbirdio/management:$NETBIRD_MANAGEMENT_TAG
    restart: unless-stopped
    depends_on:
      - dashboard
    volumes:
      - $MGMT_VOLUMENAME:/var/lib/netbird
      - $LETSENCRYPT_VOLUMENAME:/etc/letsencrypt:ro
      - ./management.json:/etc/netbird/management.json
    command:
      [
        "--port",
        "443",
        "--log-file",
        "console",
        "--disable-anonymous-metrics=$NETBIRD_DISABLE_ANONYMOUS_METRICS",
        "--single-account-mode-domain=$NETBIRD_MGMT_SINGLE_ACCOUNT_MODE_DOMAIN",
        "--dns-domain=$NETBIRD_MGMT_DNS_DOMAIN",
      ]
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.role == manager
          - node.labels.netbird == true
      labels:
        - traefik.enable=true
        - traefik.http.routers.netbird-api.rule=Host(`$NETBIRD_DOMAIN`) && PathPrefix(`/api`)
        - traefik.http.routers.netbird-api.service=netbird-api
        - traefik.http.services.netbird-api.loadbalancer.server.port=443

        - traefik.http.routers.netbird-management.rule=Host(`$NETBIRD_DOMAIN`) && PathPrefix(`/management.ManagementService/`)
        - traefik.http.routers.netbird-management.service=netbird-management
        - traefik.http.services.netbird-management.loadbalancer.server.port=443
        - traefik.http.services.netbird-management.loadbalancer.server.scheme=h2c

  # Coturn
  coturn:
    image: coturn/coturn:$COTURN_TAG
    restart: unless-stopped
    domainname: $TURN_DOMAIN
    volumes:
      - ./turnserver.conf:/etc/turnserver.conf:ro
    #      - ./privkey.pem:/etc/coturn/private/privkey.pem:ro
    #      - ./cert.pem:/etc/coturn/certs/cert.pem:ro
    network_mode: host
    command:
      - -c /etc/turnserver.conf
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.role == manager
          - node.labels.netbird == true

volumes:
  $MGMT_VOLUMENAME:
  $SIGNAL_VOLUMENAME:
  $LETSENCRYPT_VOLUMENAME:
