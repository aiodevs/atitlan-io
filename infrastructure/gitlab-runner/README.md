# Gitlab runner

We are currently running a shell version. To be setup to docker for greater ease, while making it work with the testing suite.

## Shell setup

See gitlab for the most up to date documentation on how to do this exactly. Here are the rough steps:  

1. **Setup Shell Runner instance**  
Install gitlab runner and create a gitlab-runner user. Make sure you select shell as the runners executor.  
A gotcha is the bash_logout possibly and not adding the docker group to the new gitlab-runner user if you need docker access.
2. **Register new runner**  
     Make sure it is set to protected if it is only meant to run on protected branches like main.
3. **Depencies**  
    Check the `setup-depencies.sh` and run it on new machines.  
4. **Check NVM/PNPM is setup properly**  
    Make sure your new gitlab-runner user's .bashrc is setup to work with nvm, npm and pnpm. At least add this to .bashrc of the gitlab-runner:
    ```
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" 
    export PATH=~/.npm-global/bin:$PATH
    ```
    And possibly run this as the gitlab-runner:
    ```
    mkdir -p ~/.npm-global
    npm install -g pnpm
    ```
5. **Customize the config.toml**  
    Locate and change for examle to support more concurrency.
