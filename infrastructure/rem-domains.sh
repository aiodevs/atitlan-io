#!/bin/bash

# Function to remove a domain from /etc/hosts
remove_domain() {
    local domain=$1
    sudo sed -i "/$domain/d" /etc/hosts
    echo "Removed $domain from /etc/hosts"
}

remove_domain "portainer.infra.atitlan.io"
remove_domain "traefik.infra.atitlan.io"
remove_domain "headscale.infra.atitlan.io"
remove_domain "headscale-ui.infra.atitlan.io"
remove_domain "app.atitlan.io"
remove_domain "thor.app.atitlan.io"
remove_domain "lnd.btc.atitlan.io"
remove_domain "lnbits.btc.atitlan.io"

# Optional: Flush DNS cache (Uncomment for macOS)
#sudo dscacheutil -flushcache; sudo killall -HUP mDNSResponder

# Restart networking service
sudo systemctl restart networking
echo "Restarted networking service"
# If the above fails or does not apply, restart NetworkManager
if [ $? -ne 0 ]; then
    sudo systemctl restart NetworkManager
    echo "Restarted NetworkManager"
fi

echo "Operation completed"
