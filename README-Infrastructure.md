# 🚀✨ Atitlan IO Infrastructure ✨🌋

Welcome to the Atitlan IO Infrastructure guide, a robust setup designed for optimal performance, security, and scalability using Docker Swarm. This infrastructure utilizes **Traefik** for load balancing and routing to containers/services, **Headscale** paired with **Tailscale** for managing dynamic IPs, and **Portainer** for container management.

## Services Overview

Here's a brief overview of each primary service and its purpose within our infrastructure:

- **[Traefik](https://traefik.infra.atitlan.io)** - Serves as our edge router and load balancer. Essential for managing incoming requests and providing SSL termination. 🌐 It requires a static IP to function correctly and is deployed on nodes labeled with `traefik=true`.
  
- **[Portainer](https://portainer.infra.atitlan.io)** - Provides a detailed UI for managing Docker environments. It helps us keep an eye on our containers and manage Docker resources more intuitively. 📊 Deployed on more capable nodes labeled with `portainer=true`.

- **[Headscale](https://headscale.infra.atitlan.io)** - An open-source implementation of the Tailscale control server. Perfect for creating a secure and private network layer over our infrastructure. 🔒 It runs on nodes that can handle dynamic IPs but requires a node with a static IP for the initial connection point.

- **[Headscale-UI](https://headscale-ui.infra.atitlan.io)** - A user-friendly interface for managing Headscale. It simplifies the administration of our private networks and user access. 🖥️

### 📝 Labeling Nodes for Service Deployment

To efficiently manage where services are deployed, we use Docker node labels:

**Label nodes for Traefik:**
```bash
docker node update --label-add traefik=true <node-id>
```

**Label nodes for Portainer:**
```bash
docker node update --label-add portainer=true <node-id>
```

**Label nodes for Headscale (for nodes with static IPs):**
```bash
docker node update --label-add headscale=true <node-id>
```

## Traefik Setup

Traefik is our first line of defense and our primary ingress controller. It handles all incoming HTTP(S) traffic and distributes it across our services.

### Deployment Constraints

Ensure Traefik is deployed on nodes with a static IP that is either handled by the loadbalancer connected to the a record in your dns or straight up to the static ip of the vm/node to handle ingress effectively:

```yaml
services:
  traefik:
    deploy:
      placement:
        constraints:
          - node.role == manager
          - node.labels.traefik == true
```

## Portainer Management

Portainer gives us powerful visibility and control over our Docker Swarm environment. It's deployed on nodes designated for management and monitoring tasks.

### Deployment Considerations & constraints

Portainer should be deployed on nodes with sufficient resources:

``` quote
Minimum 100MB RAM, and about .1% of a CPU core for Portainer (and add 10MB of RAM per cluster added), and for the agent, 20MB RAM and .1% CPU core.
```

```yaml
services:
  portainer:
    deploy:
      placement:
        constraints:
          - node.role == manager
          - node.labels.portainer == true
```

## Headscale and Tailscale Integration

Headscale, along with Tailscale clients, allows us to seamlessly connect services and nodes that might be on dynamic IPs without compromising security.  

### Connect to apps from a dev computer

Tailscale is also a way for developers to connect to the networks as if they were part of their local networks. This way a developer could for example connect to `THOR-API` and hit the `swagger` like a pro low level ganster.

### Deployment constraints

Setup a node that has a static ip for the other tailscale users to connect to.

### Setting Up Headscale

``` yaml
services:
  headscale:
    deploy:
      constraints:
        - node.role == manager
        - node.labels.headscale == true
```

To initialize Headscale and generate keys:

1. **Create a user in Headscale:**
   ```bash
   docker exec -it <headscale-container-id> headscale users create --username <username>
   ```

2. **Generate a pre-authenticated key:**
   ```bash
   docker exec -it <headscale-container-id> headscale preauthkeys create --name <key-name> --namespace <namespace>
   ```

### Connecting Services via Tailscale

Use the pre-authenticated key to connect services that require secure, reliable communication:

```bash
docker exec -it <tailscale-container-id> tailscale up --authkey <your-preauth-key>
```

### Tailscale deployment considerations & constraints

Remember, all nodes with the `manager` role connecting through Tailscale need to authenticate to be part of our secure network. After connecting to the tailnet, it is possible to promote that node to a manager.

```yaml
services:
  tailscale:
    deploy:
      placement:
        constraints:
          - node.labels.tailscale == true
```

