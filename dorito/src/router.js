const express = require("express");
const axios = require('axios')
const router = express.Router();
const invoiceController = require("./controllers/invoiceController");
const webhookController = require("./controllers/webhookController");
// const itemController = require("./controllers/itemController");

const path = __dirname + "/dist/";
router.get("/", function (req, res) {
  res.sendFile(path + "index.html");
});

router.post("/checkPrinterStatus", (req, res) => {
  axios.get(`http://atitlan.io:${req.body.thermalPrinterAutoSSHPort}/hello`)
  .then( (result) => {
    if (result.data ==='hello') {
      res.send('hello')
    } else {
      res.send('goodbye')
    }
  })
  .catch( (e) => {
    // console.error(e)
    res.send('goodbye')
  }
  )
})

router.post("/createInvoice", invoiceController.createInvoice);
router.post("/webhooks", webhookController.handleWebhook);

router.get("/serverTime", (req, res) => {
  // https://www.kindacode.com/article/2-ways-to-set-default-time-zone-in-node-js/
  process.env.TZ = "America/Guatemala"; // TODO is this the best place to put this? refer to above link
  let currentHour = Number(Date().split(" ")[4].split(":")[0]);
  let currentMinute = Number(Date().split(" ")[4].split(":")[1]);
  // let currentDay = Date().split(' ')[0]
  // console.log(currentHour, currentDay)
  // if (currentHour < 8 || currentHour >= 21 || currentDay === 'Sun') {
  //     res.send({ status: false, currentHour: currentHour, currentMinute: currentMinute, currentDay: currentDay})
  // } else {
  //     res.send({ status: true})
  // }
  const day = new Date().toLocaleString(
    "default",
    { weekday: "long" },
  );

  res.send({ day: day, hour: currentHour, minute: currentMinute });
});

// router.post('/webhook', invoiceController.dbInsertWebhook)
// TODO maybe break this into two parts?
// router.post('/awaitResolutionAndUpdateMongoDb', invoiceController.awaitResolutionAndUpdateMongoDb)

module.exports = router;
