const axios = require("axios");

const unprintedOrders = [];
const timeToRetry = 30; // seconds

init();

async function retryUnprintedOrders() {
  console.log("Checking for unprinted orders...");
  const unprintedOrdersCopy = unprintedOrders.slice();

  console.log(`There are ${unprintedOrders.length} orders waiting to print`);

  for (const unprintedDoc of unprintedOrdersCopy) {
    try {
      await exports.sendOrderToPrinter(unprintedDoc);
      console.log(`Unprinted order for entity ${unprintedDoc.payload.entityId} successfully printed.`)
      console.log(unprintedDoc.payload)

      const indexToRemove = unprintedOrders.indexOf(unprintedDoc);
      if (indexToRemove !== -1) {
        unprintedOrders.splice(indexToRemove, 1);
      }
    } catch {
      console.error(
        `Could not print order for entity ${unprintedDoc.payload.entityId}, retrying ${timeToRetry} in seconds...`,
      );
    }
  }

}

function init() {
  console.log(
    `*** initializing retryUnprintedOrders interval for every ${timeToRetry} seconds ***`,
  );
  console.log(unprintedOrders);
  setInterval(retryUnprintedOrders, timeToRetry * 1000);
}

function deepEqual(obj1, obj2) {
  return JSON.stringify(obj1) === JSON.stringify(obj2);
}

exports.addToUnprintedQueue = (printPayload) => {
  const exists = unprintedOrders.some( item => deepEqual(item, printPayload) )
  if (exists) {
    console.log("payload already un unprintedOrders queue")
  } else {
    unprintedOrders.push(printPayload);
    console.log(`added ${printPayload} to unprintedOrdersQueue`);
  }
};

exports.sendOrderToPrinter = async (
  {
    autoSshPort = undefined,
    payload = {},
  },
  test = false,
) => {

  const cashPayment = payload.payMethod === "Cash" ? true: false

  if (test) {
    console.log(
      `Amount: ${payload.amount}\nmetadata: ${payload.metadata}\ncashPayment: ${cashPayment}`,
    );
    return null;
  }

  const NodeApiTherm = axios.create({
    baseURL: `http://atitlan.io:${autoSshPort}`,
    timeout: 10000,
  });

  // payload.entityId *should* be a string, however, in the case it's a single element
  // array, force it to a string
  if (
    Array.isArray(payload.entityId) && typeof (payload.entityId[0]) === "string"
  ) {
    // an array with 1 string element should not occur, but we let it pass
    // an array with multiple string elements is an error
    // NOTE: this is semi-redundant, it's because cash payments bypass the
    // check that card and btc see. TODO unify this
    if (!(payload.entityId.length === 1)) {
      throw new Error(
        "exception for an array with 1 string element, however this has multiple",
      );
    }
    console.warn(
      "warning: payload.entityId is an array, however should be passed as string",
    );
    payload.entityId = payload.entityId[0];
  }

  try {
    const response = await NodeApiTherm.post("/printOrder", {
      entityId: payload.entityId || "",
      amount: payload.amount,
      metadata: payload.metadata,
      cashPayment,
    });
    return response;
  } catch (err) {
    console.error("Failed in printerController.js\n", err);
    throw new Error(`Error making POST request: ${err.message}`);
  }
};
