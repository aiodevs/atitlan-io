const Category = require("../models/Store/category");
// const Filter = require('../models/Store/filter')
// const Item = require('../models/Store/item')
const Store = require("../models/Store/store");
// const Subcategory = require('../models/Store/subcategory')

// ############################## Store ##############################
exports.postStore = async (req, res) => {
  const store = new Store({
    ...req.body,
    owner: req.user._id,
  });

  try {
    await store.save();
    res.status(201).send(store);
  } catch (e) {
    res.status(400).send(e);
  }
};

exports.getStore = async (req, res) => {
  try {
    const store = await Store.findOne({ _id: req.params.storeId });

    if (!store) {
      return res.status(404).send();
    }

    res.send(store);
  } catch (e) {
    console.log(e);
    res.status(500).send();
  }
};

exports.getStores = async (req, res) => {
  try {
    await req.user.populate("stores");
    res.send(req.user.stores);
  } catch (e) {
    res.status(500).send();
  }
};

exports.patchStore = async (req, res) => {
  const updates = Object.keys(req.body);
  const allowedUpdates = ["description", "completed", "active", "openHours"];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    return res.status(400).send({ error: "Invalid updates!" });
  }

  try {
    const store = await Store.findOne({
      _id: req.params.id,
      owner: req.user._id,
    });

    if (!store) {
      return res.status(404).send();
    }

    updates.forEach((update) => store[update] = req.body[update]);
    await store.save();
    res.send(store);
  } catch (e) {
    res.status(400).send(e);
  }
};

exports.deleteStore = async (req, res) => {
  try {
    const store = await Store.findOneAndDelete({
      _id: req.params.id,
      owner: req.user._id,
    });

    if (!store) {
      res.status(404).send();
    }

    res.send(store);
  } catch (e) {
    res.status(500).send();
  }
};

exports.getCategories = async (req, res) => {
  try {
    const store = await Store.findOne({ _id: req.params.storeId });
    console.log(store);
    await store.populate("categories");
    res.send(store.categories);
  } catch (e) {
    res.status(500).send();
  }
};

exports.postCategory = async (req, res) => {
  const category = new Category({
    ...req.body,
    store: req.params.storeId,
  });

  try {
    await category.save();
    res.status(201).send(category);
  } catch (e) {
    res.status(400).send(e);
  }
};

exports.getActiveStoreItems = async (req, res) => {
  const storeId = req.params.storeId;

  // maybe can somehow include the auth middleware and if it doesn't authorize, you don't get to see inactive items
  // this catches a route for both .../items and .../itemsActive
  //

  try {
    const store = await Store.findOne({ _id: storeId });
    console.log(store);
    const items = {}; // items means everything here, menus, categories, subcategories and products
    // get active menus
    await store.populate("categories");
    const categories = store.categories.filter((
      item,
    ) => item.active === true);

    let subcategories = undefined;
    for (const category of categories) { // ############### categories ###############
      const tempObj = {};
      await category.populate("subcategories");

      subcategories = category.subcategories.filter((item) =>
        item.active === true
      );

      for (const subcategory of subcategories) { // ############### subcategories ###############
        await subcategory.populate("items");
        const items = subcategory.items.filter((item) => item.active === true);
        for (const item of items) { // ############### items ###############
          await item.populate("addons");
        }
        tempObj[subcategory.name] = items;
      }
      items[category.name] = tempObj;
    }
    res.send(items);
  } catch (e) {
    console.error(e);
    res.status(500).send();
  }
};
