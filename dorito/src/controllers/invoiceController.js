// activate Neopixel LED strip
// const Neopixel = require("../services/Neopixel");

// const { post } = require('../app')
const GreenfieldApi = require("../services/GreenfieldApi");
const RecurrenteApi = require("../services/RecurrenteApi");

// for sending Telegram Messages
const { reduceAndBroadcast } = require(
  "../services/BroadcastOrder.js",
);

const { reduceInventory, restockInventory } = require(
  "./inventoryController.js",
);

const { PendingInvoice } = require("../models/invoice");
// const { PendingInvoice, CompletedInvoice, FailedInvoice } = require(
//   "../models/invoice",
// );

// used in client/src/services/AuthenticationService.js
// ref: https://docs.btcpayserver.org/API/Greenfield/v1/#operation/Invoices_CreateInvoice
//
// ############################## EXAMPLE PAYLOAD from menu-app ##############################
// payload = {
// entityId: req.body.entityId, // this is the mongodb restaurant _id
// payMethod: req.body.payMethod,
// amount: req.body.amount,
// thermalPrinterAutoSSHPort: req.body.thermalPrinterAutoSSHPort, // TODO maybe move this ?
// // this gets stored in posData as well on the server for BTCPay
// metadata: req.body.metadata,
// btcpay_data: req.body.btcpay_data,
// modifyInventory: req.body.modifyInventory
// }
//

exports.createInvoice = async (req, res) => {
  const payload = req.body;
  payload.created_at = (new Date()).toString();

  const modifyInventory = payload.modifyInventory;
  if (modifyInventory) {
    console.log("****** Store Order, inventory to be modified ******* ");
  }
  delete payload.modifyInventory;

  const payMethod = payload.payMethod;
  // ############ BTCPay pendingInvoice ##################
  if (payMethod === "BTC") {
    GreenfieldApi.createInvoice(payload)
      .then((greenfieldResponse) => {
        return preferBTCdata(payload, greenfieldResponse.data); // piggyback gf invoice
      })
      .then(({ btcDocData, gfInvoiceData }) => {
        try {
          const pendingInvoice = new PendingInvoice({
            _id: "btc_" + gfInvoiceData.id,
            ...payload,
            ...btcDocData,
          });
          pendingInvoice.save();

          if (modifyInventory) reduceInventory(payload.metadata.items);

          res.send(gfInvoiceData);
        } catch (err) {
          console.error(err);
          console.log("Failed to make btc invoice document in the database");
          res.sendStatus(500).send();
        }
      });

    // ############ Recurrente pendingInvoice ##################
  } else if (payMethod === "Card") {
    RecurrenteApi.createInvoice(payload)
      .then((recResponse) => {
        const recInvoiceData = recResponse.data;
        try {
          const pendingInvoice = new PendingInvoice({
            _id: "rec_" + recInvoiceData.id,
            ...payload,
            currency: recInvoiceData.prices[0].currency, // maybe remove, this is already included
          });

          pendingInvoice.save();

          if (modifyInventory) {
            reduceInventory(payload.metadata.items);
            setRecurrenteTimer(pendingInvoice, 10); // if the item is still in Pending invoice collection after X minutes, remove it
          }

          res.send(recInvoiceData);
        } catch {
          console.log("Failed to make card invoice document in the database");
          res.sendStatus(500).send();
        }
      })
      .catch((e) => console.log(e));

    // ############ Cash pendingInvoice ##################
    // TODO figure out how to verify these
  } else if (payMethod === "Cash") {
    // add cash to pending
    try {
      const now = new Date()
      const cashId = `${(now).getTime()}_${Math.floor(Math.random() * 999)}`;

      const pendingInvoice = new PendingInvoice({
        _id: `fiat_${cashId}`,
        currency: "GTQ",
        ...payload,
      });
      pendingInvoice.save();
      payload.metadata.orderDesc = payload.metadata.items;

      const thermalPrinterAutoSSHPort = payload.thermalPrinterAutoSSHPort
      const timestamp = now.toLocaleString()
      const payloadToSend = {
        ...payload,
        printId: cashId,
        timestamp,
        cashPayment: true,
      };

      // Print invoice immediately
      reduceAndBroadcast(thermalPrinterAutoSSHPort, payloadToSend)
      // TODO return the response from reduceAndBroadcast for use here
      res.status(200).send();

    } catch (err) {
      console.error(`Failed to make cash invoice document in the database\n${err}`);
      res.status(500).send();
    }
  }
};

function setRecurrenteTimer(pendingInvoice, timeMins) {
  setTimeout(() => {
    const touched_at = (new Date()).toString()
    PendingInvoice.updateOne(
      { _id: pendingInvoice._id }, 
      { 
        $set: { 
          status: "recurrenteTimer_timeout", 
          touched_at  
          }
      },
      (err, result) => {

        if (err) {
              console.error('Error updating the document:', err);
              return;
            }

        // RESTOCK
        // this is not kosher, but this will throw an error for, e.g., 
        // restaurant items that don't have inventory
        // TODO make condition for this
        console.log(pendingInvoice)
        const items = pendingInvoice.metadata.items;
        try {
          // Restore items to inventory
          restockInventory(items);
        } catch (err) {
          console.error(err)
        }
        console.log('Document updated successfully', result);
        }


      );
  }, 1000 * 60 * timeMins);
}

// We try to store the invoice with BTC values and not in fiat when paid in BTC
// TODO create a function that goes through and fixes any invoices that for whatever
// reason couldn't record in BTC
function preferBTCdata(payload, gfInvoiceData) {
  return new Promise((resolve) => {
    GreenfieldApi.getInvoicePaymentMethods(
      payload.btcpay_data.storeId,
      gfInvoiceData.id,
    )
      .then((response) => {
        // function for finding which paymentMethod was used
        // NOTE: this assumes the full amount is paid and there's no partial payment
        const usedPaymentMethods = response.data.filter(
          (method) => method.amount.length > 0,
        );
        // Add up the btc received from each payment method
        let cartTotalBTC = 0;
        usedPaymentMethods.forEach((method) => {
          cartTotalBTC += Number(method.due);
        });

        // generate BTC amount data
        const metadata = payload.metadata;
        const fiatAmount = payload.amount;

        const btcDocData = {
          currency: "BTC",
          amount: Number(cartTotalBTC),
          metadata: {
            ...payload.metadata,
            tipAmount: Number((Number(metadata.tipAmount) / Number(fiatAmount) *
              Number(cartTotalBTC)).toFixed(8)),
            feeAmount: Number((Number(metadata.feeAmount) / Number(fiatAmount) *
              Number(cartTotalBTC)).toFixed(8)),
            // fiat data
            fiatEquivalentCurrency: payload.currency,
            fiatEquivalentAmount: Number(fiatAmount),
            fiatEquivalentTip: metadata.tipAmount
              ? Number(metadata.tipAmount)
              : 0,
            fiatEquivalentFee: metadata.feeAmount
              ? Number(metadata.feeAmount)
              : 0,
          },
        };
        resolve({ btcDocData, gfInvoiceData });
      })
      .catch((e) => {
        console.error(e);
        console.log(
          "Couldn't obtain BTC data for invoice, saving with Fiat values",
        );
        resolve({ btcDocData: {}, gfInvoiceData });
      });
  });
}

// ref: https://docs.recurrente.com/quickstart

// Sample Recurrent api/products response
// {
// "id":"prod_zzysopbk",
// "status":"active",
// "name":"Mi Producto Ejemplo",
// "description": {
//   "id":null,
//   "name":"description",
//   "body":null,
//   "record_type":"Product",
//   "record_id":138120,
//   "created_at":null,
//   "updated_at":null
//   },
// "success_url":null,
// "cancel_url":null,
// "custom_terms_and_conditions": {
//   "id":null,
//   "name":"custom_terms_and_conditions",
//   "body":null,
//   "record_type":"Product",
//   "record_id":138120,
//   "created_at":null,
//   "updated_at":null
//   },
// "phone_requirement":"none",
// "address_requirement":"none",
// "billing_info_requirement":"optional",
// "prices":[
//     {
//     "id":"price_bmvvzfgj",
//     "amount_in_cents":599,
//     "currency":"GTQ",
//     "billing_interval_count":0,
//     "billing_interval":"",
//     "charge_type":"one_time",
//     "periods_before_automatic_cancellation":null,
//     "free_trial_interval_count":0,
//     "free_trial_interval":""
//     },
// ],
// "storefront_link":"https://app.recurrente.com/s/the-emporium/mi-producto-ejemplo-lpfo7o"
// }
//

// exports.thermalPrintOrder = async (req, res) => {
//   // TODO make the invoice types a struct or sth similar (maybe better term)
//   try {
//     if (req.body.type === "InvoicePaymentSettled") {
//       // retrieve invoiceId from payment settled order
//       const invoiceId = req.body.invoiceId;
//       const storeId = req.body.storeId;
//       // fetch invoice from Greenfield API
//       const invoice = await GreenfieldApi.getInvoice(invoiceId, storeId);
//
//       // Print to USB-connected thermal printer
//       ThermalPrinter.printOrder(invoice.data);
//     } else if (req.body.type === "cashPayment") {
//       // req.body is defined in submitPayment() of CartSlideOver.vue
//       // console.log(req.body)
//       // Print to USB-connected thermal printer
//       ThermalPrinter.printOrder(req.body);
//     }
//
//     // Neopixel.turnOn();
//
//     res.send("ok");
//   } catch (error) {
//     console.log("********* triggerPrinterPOST error ********");
//     console.log(error.message);
//     res.send("printer error");
//   }
// };

// POST amount and items to this endpoint to respond with invoice and add to database
// exports.createInvoiceAndInsertInDb = (req, res) => {
//     // const storeId = req.body.storeId
//     const amount = req.body.amount
//     const items = req.body.items
//
//     // post to Greenfield API to create invoice
//     // TODO: make the Api return a promise!
//     GreenfieldApi.createInvoice(amount)
//    .then( (response) => {
//         const invoiceData = response.data
//         invoiceData.items = items
//         // for some reason, the invoice id is stored in id instead of invoiceId
//         // actually, you can't do this yet based on the way you query the db in
//         // the awaitInvoice function
//         // if (invoiceData.id) {
//         // invoiceData.invoiceId = invoiceData.id
//         // }
//
//         // insert invoice into MongoDB
//         _invoice.dbInsert(invoiceData)
//         .then( (dbResponse) => {
//             console.log(dbResponse)
//             res.send(invoiceData)
//         })
//         .catch( err => res.send(err) )
//
//     })
//     .catch( e => console.log(e))
//
// }

// exports.checkForUnresolvedInvoices = async () => {
//     const documents = await pendingCollection.find({}).toArray()
//
//     // Need to fill array with unique IDs since the database may contain multiple documents
//     // with different status but the same id
//     let unresolvedInvoices = []
//
//     if (documents.length > 0) {
//         console.log('Unresolved Invoices found, resolving them now...')
//
//         // note that document is a reserved variable in javascript!
//         for ( let _document of documents) {
//             console.log(_document)
//             // only want the items in the database that have items attached to them
//             if (_document.id) {
//                 // note: using let to define a variable has a local scope
//                 var invoiceId = _document.id
//                 var items = _document.items
//                 unresolvedInvoices.push({ invoiceId : invoiceId, items : items })
//             }
//         }
//
//         for ( let invoice of unresolvedInvoices ) {
//             this.fixUnresolvedInvoices(invoice.invoiceId, invoice.items)
//         }
//
//     } else {
//         console.log('No unresolved invoices!')
//     }
//
// }

// exports.fixUnresolvedInvoices = async (invoiceId, items) => {
//     // for each invoice :
//     // 1) do a greenfield api call to get the invoice and retrieve the expiration time
//     //      a) if not then send it to awaitInvoiceStatus
//     //      b) if it has passed expiration then find the invoice status
//     //      TODO: you still have to see if multiple webhooks are sent for when an invoice is fully paid
//     let response = await GreenfieldApi.getInvoice(invoiceId)
//     const invoice = response.data

//     // convert expiration time from seconds to milliseconds
//     const invoiceExpTimeMS = invoice.expirationTime * 1000

//     // TODO: verify different possibilities for invoice status
//     if ( Date.now() <= invoiceExpTimeMS && ( invoice.status === 'New' || invoice.status === 'Processing') ) {
//         let invoiceStatus = await awaitInvoiceStatus(invoiceId, invoiceExpTimeMS)
//         return updateMongoDb(invoiceId, invoiceStatus, items)
//     } else {
//         // TODO: double check this
//         return updateMongoDb(invoiceId, invoice.status, items)
//     }

// }

/* Sample Invoice Data
    {
      id: '5HkVMQTPHwkutFgThDSuVb',
      storeId: 'AE5cyxSv4zBMadQVRVNJNjAdwYrjDozNFQm95gBE7EXJ',
      amount: '90.0',
      checkoutLink: 'https://btcpay.atitlan.io/i/5HkVMQTPHwkutFgThDSuVb',
      status: 'New',
      additionalStatus: 'None',
      monitoringExpiration: 1633021358,
      expirationTime: 1632934958,
      createdTime: 1632934898,
      type: 'Standard',
      currency: 'GTQ',
      metadata: {},
      checkout: {
        speedPolicy: 'HighSpeed',
        paymentMethods: [ 'BTC-LightningNetwork', 'BTC' ],
        expirationMinutes: 1,
        monitoringMinutes: 1440,
        paymentTolerance: 0,
        redirectURL: 'https://news.ycombinator.com',
        redirectAutomatically: true,
        defaultLanguage: null
      }
    }
 */

// // ****************************** database calls ******************************/

// // https://stackoverflow.com/questions/951021/what-is-the-javascript-version-of-sleep
// function sleep(ms) {return new Promise(resolve => setTimeout(resolve,ms))}

// // async function that waits for resolved invoice to come into database
// async function awaitInvoiceStatus(invoiceId, expirationTimeMS) {

//     // padding time to allow webhook to be sent
//     const padding = 1000 * 60 * 5 // padding time to allow webhook to be sent

//     while (Date.now() <= expirationTimeMS + padding ) {

//         let result = await pendingCollection.findOne({ $and: [{ invoiceId: invoiceId }, { type: { $ne: 'InvoiceCreated'} }] })

//         if (result != null ) {
//             // for a webhook the relevant info is in the type property of the object
//             return result.type
//         }
//         console.log('sleeping for 5 seconds')
//         await sleep(5000)
//     }

//     try {
//         console.log(`invoice ${invoiceId} webhook not received, attempting to fetch using Greenfield API`)
//         let invoice = GreenfieldApi.getInvoice(invoiceId)
//         if (invoice) {
//             console.log(`getInvoice successful; type: ${invoice.type}`)
//             return invoice.status
//         } else {
//             throw 'error fetching invoice from Greenfield API'
//         }

//     } catch (error) {
//         console.log(error)
//     }

// }

// function updateMongoDb(invoiceId, invoiceStatus, items) {

//     // handle each case depending on status of the invoice
//     // note this will only run after expiration time, so the invoice
//     // will either have settled or not
//     if (invoiceStatus.includes('Settled')) {
//         for (let item of items) {
//             console.log(`**placeholder** remove ${item} from database`)
//             // update the inventory for the item
//             // e.g.,
//             // const book = new Book()
//             // book.updateInventory() // maybe function for add or remove
//         }
//         _invoice.dbRemove(invoiceId) // remove all items matching invoiceId from db
//         return `Invoice ${invoiceId} has been successfully paid`
//     }

//     else if (invoiceStatus.includes('Expired')){
//         console.log("Invoice has expired")
//         for (let item of items) {
//             console.log(`item #${item._id} inventory unchanged`)
//         }
//         _invoice.dbRemove(invoiceId) //removes all items from the database
//         return `Invoice ${invoiceId} has expired`
//     }

//     else if (invoiceStatus.includes('Invalid')){
//         return `invoice status invalid, no action taken`
//     }

//     else {
//         return `invoice status ${invoiceStatus} received, no action taken`
//     }

//     // Apparently this is unreachable
//     // console.log("*****updateMongoDb function completed*****")
// }

// // TODO scan database to see if there exist unresolved invoices, e.g., in case of power outage

// // Return a promise using async that will resolve when an invoice is resolved (either paid or fails)
// // If the invoice is settled successfully, remove the items from the database
// // TODO maybe break this into awaitResolve() and updateDatabase() functions
// exports.awaitResolutionAndUpdateMongoDb = async (req, res) => {
//     // req.data is the response from a createInvoice Greenfield API post request
//     // ref: https://docs.btcpayserver.org/API/Greenfield/v1/#operation/Invoices_CreateInvoice
//     // const storeId = req.body.invoice.storeId
//     const invoiceId = req.body.invoice.id
//     const expirationTimeMS = req.body.invoice.expirationTime * 1000
//     const items = req.body.items

//     console.log('looking for a non-invoiceCreated item')

//     // wait for invoice expiration time + desired padding time for invoice to resolve
//     // modify the database accordingly
//     await awaitInvoiceStatus(invoiceId, expirationTimeMS)
//     .then( (invoiceStatus) => {
//        updateMongoDb(invoiceId, invoiceStatus, items)
//        console.log('**** updateMongoDb executed ****')
//        res.send('*** updateMongoDb executed ****')
//     })
//     .catch( async (err) => {
//         console.log(err)
//     })

// }

// exports.dbInsertWebhook = (req,res) => {
//     // TODO maybe modify this only to insert webhooks for settled or expired?
//     _invoice.dbInsert(req.body)
//     .then( (result) => {
//         console.log(req.body) // this returns storeId: ***, amount: ** sent from Vue3 it seems
//         console.log(result)
//         res.send("ok")
//     })
//     .catch( err => {
//         console.log(err)
//         res.status(404) //might need better status code ref : https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
//         res.send('failed')
//     })
// }

// // TODO have the server monitor the database and if it finds a resolved invoice, settle it and
// // move the invoice data to a record
// // create a fork that removes API of Greenfield API from client-side
