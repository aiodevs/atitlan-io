const Category = require("../models/Store/category");
const Subcategory = require("../models/Store/subcategory");
const Item = require("../models/Store/item");
// const Filter = require('../models/Store/filter')
// const Item = require('../models/Store/item')
const Store = require("../models/Store/store");
// const Subcategory = require('../models/Store/subcategory')

// ############################## Store ##############################
exports.postStore = async (req, res) => {
  const store = new Store({
    ...req.body,
    owner: req.user._id,
  });

  try {
    await store.save();
    res.status(201).send(store);
  } catch (e) {
    res.status(400).send(e);
  }
};

exports.getStore = async (req, res) => {
  try {
    const store = await Store.findOne({ _id: req.params.storeId });

    if (!store) {
      return res.status(404).send();
    }

    res.send(store);
  } catch (e) {
    console.log(e);
    res.status(500).send();
  }
};

exports.getStores = async (req, res) => {
  try {
    await req.user.populate("stores");
    res.send(req.user.stores);
  } catch (e) {
    res.status(500).send();
  }
};

exports.patchStore = async (req, res) => {
  const updates = Object.keys(req.body);
  const allowedUpdates = ["description", "completed", "active", "openHours"];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    return res.status(400).send({ error: "Invalid updates!" });
  }

  try {
    const store = await Store.findOne({
      _id: req.params.id,
      owner: req.user._id,
    });

    if (!store) {
      return res.status(404).send();
    }

    updates.forEach((update) => store[update] = req.body[update]);
    await store.save();
    res.send(store);
  } catch (e) {
    res.status(400).send(e);
  }
};

exports.deleteStore = async (req, res) => {
  try {
    const store = await Store.findOneAndDelete({
      _id: req.params.id,
      owner: req.user._id,
    });

    if (!store) {
      res.status(404).send();
    }

    res.send(store);
  } catch (e) {
    res.status(500).send();
  }
};

// this function is specifically for a cart that wants to check to make sure all the items in it
// are still active and have inventory
exports.receiveListOfItems = async (req, res) => {
  try {
    // store items in this list, which will be sent in the response
    const itemReturnList = [];

    console.log(req.body);
    for (const item of req.body.cartItemList) {
      const itemCurrent = await Item.findById(item._id);
      itemReturnList.push(itemCurrent);
    }

    res.send(itemReturnList);
  } catch (e) {
    console.error(e);
    res.status(500).send();
  }
};

exports.getCategories = async (req, res) => {
  try {
    const store = await Store.findOne({ _id: req.params.storeId });
    console.log(store);
    await store.populate("categories");
    res.send(store.categories);
  } catch (e) {
    res.status(500).send();
  }
};

exports.postCategory = async (req, res) => {
  console.log("here");
  const category = new Category({
    ...req.body,
    store: req.params.storeId,
  });

  try {
    await category.save();
    res.status(201).send(category);
  } catch (e) {
    res.status(400).send(e);
  }
};

exports.patchCategory = async (req, res) => {
  const categoryId = req.params.categoryId;
  const updates = Object.keys(req.body);
  const allowedUpdates = ["name", "active"];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    return res.status(400).send({ error: "Invalid updates!" });
  }

  try {
    const category = await Category.findOne({
      _id: categoryId,
    });

    if (!category) {
      return res.status(404).send();
    }

    updates.forEach((update) => category[update] = req.body[update]);
    await category.save();
    res.send(category);
  } catch (e) {
    res.status(400).send(e);
  }
};

exports.deleteCategory = async (req, res) => {
  const categoryId = req.params.categoryId;

  try {
    const category = await Category.findOneAndDelete({
      _id: categoryId,
    });

    if (!category) {
      res.status(404).send();
    }

    res.send(category);
  } catch (e) {
    console.error(e);
    res.status(500).send();
  }
};

exports.getSubcategories = async (req, res) => {
  try {
    const store = await Store.findOne({ _id: req.params.storeId });
    console.log(store);
    await store.populate("categories");
    res.send(store.categories);
  } catch (e) {
    console.error(e);
    res.status(500).send();
  }
};

exports.postSubcategory = async (req, res) => {
  const subcategory = new Subcategory({
    ...req.body,
    store: req.params.storeId,
    category: req.params.categoryId,
  });

  try {
    await subcategory.save();
    res.status(201).send(subcategory);
  } catch (e) {
    res.status(400).send(e);
  }
};

exports.patchSubcategory = async (req, res) => {
  const subcategoryId = req.params.subcategoryId;
  const updates = Object.keys(req.body);
  const allowedUpdates = ["name", "active"];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    return res.status(400).send({ error: "Invalid updates!" });
  }

  try {
    const subcategory = await Subcategory.findOne({
      _id: subcategoryId,
    });

    if (!subcategory) {
      return res.status(404).send();
    }

    updates.forEach((update) => subcategory[update] = req.body[update]);
    await subcategory.save();
    res.send(subcategory);
  } catch (e) {
    res.status(400).send(e);
  }
};

exports.deleteSubcategory = async (req, res) => {
  const subcategoryId = req.params.subcategoryId;

  try {
    const subcategory = await Subcategory.findOneAndDelete({
      _id: subcategoryId,
    });

    if (!subcategory) {
      res.status(404).send();
    }

    res.send(subcategory);
  } catch (e) {
    console.error(e);
    res.status(500).send();
  }
};

exports.getItems = async (req, res) => {
  try {
    const store = await Store.findOne({ _id: req.params.storeId });
    console.log(store);
    await store.populate("categories");
    res.send(store.categories);
  } catch (e) {
    res.status(500).send();
  }
};

exports.getItem = async (req, res) => {
  res.send("not filled out yet");
};

exports.postItem = async (req, res) => {
  const item = new Item({
    ...req.body,
    store: req.params.storeId,
    category: req.params.categoryId,
    subcategory: req.params.subcategoryId,
  });
  console.log("HERE?")

  try {
    await item.save();
    res.status(201).send(item);
  } catch (e) {
    console.error(e)
    res.status(400).send(e);
  }
};

exports.deleteItem = async (req, res) => {
  const itemId = req.params.itemId;

  try {
    const item = await Item.findOneAndDelete({
      _id: itemId,
    });

    if (!item) {
      res.status(404).send();
    }

    res.send(item);
  } catch (e) {
    res.status(500).send();
  }
};

// TODO revise allowedUpdates, I'm not sure that, e.g., the image will ever be patched here...
exports.patchItem = async (req, res) => {
  const itemId = req.params.itemId;
  const updates = Object.keys(req.body);
  console.log(updates);
  const allowedUpdates = [
    "active",
    "name",
    "description",
    "image",
    "price",
    "imgSrc",
    "imgFile",
    "requiredOptions",
    "addons",
    "filters",
    "inventory",
    "quantity",
  ];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    console.log("invalid updates!");
    return res.status(400).send({ error: "Invalid updates!" });
  }

  try {
    const item = await Item.findOne({ _id: itemId });

    if (!item) {
      return res.status(404).send();
    }

    updates.forEach((update) => item[update] = req.body[update]);
    await item.save();
    res.send(item);
  } catch (e) {
    res.status(400).send(e);
  }
};

exports.postItemImage = async (req, res) => {
  try {
    const item = await Item.findById(req.params.itemId);
    item.image = req.buffer;

    await item.save();

    res.send(item.image);
  } catch (e) {
    console.error(e);
  }
};

exports.getItemImage = async (req, res) => {
  try {
    const item = await Item.findById(req.params.itemId);

    if (!item || !item.image) {
      throw new Error(
        "Either the user does not exist, or it does not have an image",
      );
    }

    // note default Content-Type is 'application/json'
    res.set("Content-Type", "image/webp");
    res.setHeader("content-type", "image/webp");
    res.send(item.image);
  } catch (e) {
    res.status(404).send();
  }
};

exports.deleteItemImage = async (req, res) => {
  const item = Item.findById(req.params.itemId);
  item.image = undefined;
  await req.user.save();

  res.send();
};

exports.getActiveItems = async (req, res) => {
  const storeId = req.params.storeId;

  // maybe can somehow include the auth middleware and if it doesn't authorize, you don't get to see inactive items
  // this catches a route for both .../items and .../itemsActive
  //

  try {
    const store = await Store.findOne({ _id: storeId });
    console.log(store);
    const items = {}; // items means everything here, menus, categories, subcategories and products
    // get active menus
    try {
      await store.populate("categories");
      const categories = store.categories.filter((
        item,
      ) => item.active === true);

      let subcategories = undefined;
      for (const category of categories) { // ############### categories ###############
        const tempObj = {};
        await category.populate("subcategories");

        subcategories = category.subcategories.filter((item) =>
          item.active === true
        );

        for (const subcategory of subcategories) { // ############### subcategories ###############
          await subcategory.populate("items");
          const items = subcategory.items.filter((item) =>
            item.active === true
          );
          tempObj[subcategory.name] = items;
        }
        items[category.name] = tempObj;
      }
      res.send(items);
    } catch (e) {
      console.log("I think you need to have at least one leaf created");
      console.error(e);
    }
  } catch (e) {
    console.error(e);
    res.status(500).send();
  }
};
