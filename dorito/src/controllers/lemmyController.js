const { LemmyHttp } = require("lemmy-js-client");

const loginForm = {
  username_or_email: "HelpBot",
  password: "vtv5ben2tnx9rqx-ETQ",
};

const baseUrl = "https://forum.atitlan.io";

const client = new LemmyHttp(baseUrl);

const getJWT = async function (client) {
  const user = await client.login(loginForm);
  return user;
};

// functions

exports.createPost = async (payload) => {
  const user = await getJWT(client, loginForm);
  const jwt = user.jwt.unwrap();

  const response = await client.createPost({
    auth: jwt,
    name: payload.name,
    body: payload.body,
    community_id: payload.community_id,
  });

  return response;
};

exports.getCommunity = async (payload) => {
  const user = await getJWT(client, loginForm);
  const jwt = user.jwt.unwrap();

  const response = await client.getCommunity({ auth: jwt, name: payload.name });

  return response;
};

