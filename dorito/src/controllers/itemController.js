const Item = require("../models/item");
// const Invoice = require('../models/Invoice')

// get inventory populates the items for the customer to see on the front-end
exports.getInventory = (req, res) => {
  let item = new Item();
  item.getInventory(req.query.collectionName)
    .then((result) => res.send(result))
    .catch((e) => {
      res.send("database failure" + e);
      throw e;
    });
};

// async returns a Promise that resolves with the return value of the async function and rejects with the value of a thrown exception)
// req here is the created invoice
// Effectively, we are creating a Promise here that resolves if the invoice is settled and rejects otherwise
// exports.updateDatabase = async (req) => {
//         const invoiceId = req.body.invoiceId
//         const items = req.body.items
//         // const invoiceId = req.invoiceId

//         // Look for resolved invoice in invoiceStatusQueue and update database
//         // invoice.expirationTime is in seconds
//         // ******************
//         // console.log(req.body)
//         // console.log("ITEMS")
//         // console.log(items)

//         // note that padding needs to be long enough for the webhook sent by BTCPay to come through in the case of an expiration
//         const padding = 1000 * 60

//         // check to see whether the invoice has been resolved or rejected
//         // for cases where the webhook isn't sent for whatever reason, maybe have a function
//         // that runs x times a day to look for unresolved invoices instead of having this loop
//         // go forever
//         while ( Date.now() < invoice.expirationTime*1000 + padding  ) {
//             // iterate through webhook data and see if payment has been settled
//             // update database accordingly
//             for (let webhook of invoiceStatusQueue) {
//                 if (webhook.invoiceId === invoice.id && webhook.type === 'InvoiceSettled') {
//                     // Create Book instance to handle database operations
//                     let book = new Book()
//                     console.log('updating ' + items)
//                     for (let item of items) {
//                         book.subtractFromDb(item)
//                         .then(console.log("OK"))
//                         .catch( e => console.log(e))
//                     }
//                     // TODO handle any errors if needed
//                     return 'database updated successfully'
//                     // update the inventory from the database
//                     // remove if it goes to 0
//                     // add to order history collection
//                 } else if ( webhook.invoiceId === invoice.id && webhook.type === 'InvoiceExpired') {
//                     // console.log('invoice expired')
//                     for (let item of items) {
//                         console.log(item._id + " not affected in database")
//                     }
//                     return 'Database not modified'
//                 }
//             }

//             console.log('sleeping for 5 seconds')
//             await sleep(5000)

//         }

//         return 'invoice has already expired!' // throws error? acts as a reject
// }

// obsolete code; keeping for a bit to verify above functionality and compare
// ref https://old.reddit.com/r/learnjavascript/comments/pu36i4/mind_is_a_bit_fried_trying_to_deconstruct_this/

// async function checkArray(invoice, items, resolve, reject) {
//     console.log("inside async function")
//     console.log(invoice)
//     console.log(items)

//     //NOTE : The padding time must be enough for the webhook to be received by BTCPay Server!
//     const padding = 1000 * 60

//     while ( Date.now() < invoice.expirationTime*1000 + padding  ) {
//             // iterate through webhook data and see if payment has been settled
//             // update database accordingly
//             for (let webhook of invoiceStatusQueue) {
//                 if (webhook.invoiceId === invoice.id && webhook.type === 'InvoiceSettled') {
//                     // Create Book instance to handle database operations
//                     console.log("MEOW")
//                     let book = new Book()
//                     console.log('updating ' + items)
//                     for (let item of items) {
//                         book.subtractFromDb(item)
//                         .then(console.log("OK"))
//                         .catch( e => console.log(e))
//                     }
//                     return resolve()
//                     // update the inventory from the database
//                     // remove if it goes to 0
//                     // add to order history collection
//                 } else if ( webhook.invoiceId === invoice.id && webhook.type === 'InvoiceExpired') {
//                     console.log('invoice expired')
//                     for (let item of items) {
//                         console.log(item._id + " not affected in database")
//                     }
//                     return resolve()
//                 }
//             }
//             console.log("sleeping for 5s")
//             await sleep(5000)

//         }
//     return reject("invoice expired")
// }

// // req here is the created invoice
// exports.updateDatabase = (req) => {
//     console.log("INSIDE bookControler")
//     const invoiceStatusQueue = webhookController.invoiceStatusQueue
//     console.log(invoiceStatusQueue)

//     return new Promise((resolve,reject) => {
//         const invoice = req.body.invoice
//         const items = req.body.items

//         checkArray(invoice, items, resolve, reject)

//         reject('invoice has already expired!')
//     })
// }
