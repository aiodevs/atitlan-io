// const Category = require('../models/Restaurant/category')
// const Filter = require('../models/Restaurant/filter')
// const Item = require('../models/Restaurant/item')
const Restaurant = require("../models/Restaurant/restaurant");
// const Subcategory = require('../models/Restaurant/subcategory')

exports.getMenus = async (req, res) => {
  try {
    const restaurant = await Restaurant.findOne({
      _id: req.params.restaurantId,
    });
    console.log(restaurant);
    await restaurant.populate("menus");
    res.send(restaurant.menus);
  } catch (e) {
    res.status(500).send();
  }
};

// ############################## Store ##############################
// exports.postStore = async (req, res) => {
//     const store = new Store({
//         ...req.body,
//         owner: req.user._id
//     })
//
//     try {
//         await store.save()
//         res.status(201).send(store)
//     } catch (e) {
//         res.status(400).send(e)
//     }
// })
//
// exports.getStore =async (req, res) => {
//
//     try {
//         const store = await Store.findOne({ _id: req.params.storeId })
//
//         if (!store) {
//             return res.status(404).send()
//         }
//
//         res.send(store)
//
//     } catch (e) {
// 	console.log(e)
//         res.status(500).send()
//     }
// })
//
// exports.getStores = async (req, res) => {
//     try {
//         await req.user.populate('stores')
//         res.send(req.user.stores)
//     } catch (e) {
//         res.status(500).send()
//     }
// })
//
// exports.patchStore = async (req, res) => {
//     const updates = Object.keys(req.body)
//     const allowedUpdates = ['description', 'completed', 'active', 'openHours']
//     const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
//
//     if (!isValidOperation) {
//         return res.status(400).send({ error: 'Invalid updates!' })
//     }
//
//     try {
//         const store = await Store.findOne({ _id: req.params.id, owner: req.user._id})
//
//         if (!store) {
//             return res.status(404).send()
//         }
//
//         updates.forEach((update) => store[update] = req.body[update])
//         await store.save()
//         res.send(store)
//     } catch (e) {
//         res.status(400).send(e)
//     }
// })
//
// exports.deleteStore = async (req, res) => {
//     try {
//         const store = await Store.findOneAndDelete({ _id: req.params.id, owner: req.user._id })
//
//         if (!store) {
//             res.status(404).send()
//         }
//
//         res.send(store)
//     } catch (e) {
//         res.status(500).send()
//     }
// })
//
//
// exports.getCategories = async (req, res) => {
//     try {
//         const store = await Store.findOne({_id: req.params.storeId})
//         console.log(store)
//         await store.populate('categories')
//         res.send(store.categories)
//     } catch (e) {
//         res.status(500).send()
//     }
// })
//
// exports.postCategories = async (req, res) => {
//     const category = new Category({
//         ...req.body,
//         store: req.params.storeId
//     })
//
//     try {
//         await category.save()
//         res.status(201).send(category)
//     } catch (e) {
//         res.status(400).send(e)
//     }
//
// })
