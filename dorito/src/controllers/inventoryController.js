// Store item to modify inventory
const StoreItem = require("../models/Store/item.js");

exports.reduceInventory = async function (items) {
  for (const item of items) {
    const itemDoc = await StoreItem.findById(item._id);
    // console.log(itemDoc); // NOTE: this will print the whole item image
    itemDoc.inventory = itemDoc.inventory - item.quantity;
    itemDoc.save(); // don't think i need an await here for any reason, leaving it out
  }
};

exports.restockInventory = async function (items) {
  for (const item of items) {
  
    let itemDoc = undefined 

    try {
      // itemDoc = await StoreItem.find(item._id);
      itemDoc = await StoreItem.findOne({ _id: item._id })
    } catch {
      console.error(`no item document found for ${item._id}`)
    }

    if (itemDoc) {
      itemDoc.inventory += item.quantity;
      console.log(`${itemDoc.name} successfully restocked, inventory : ${itemDoc.inventory}`);
      itemDoc.save();
    }
  }
};
