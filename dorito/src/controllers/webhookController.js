const {
  CompletedInvoice,
  PendingInvoice,
} = require("../models/invoice");

const { restockInventory } = require("./inventoryController.js");

const { reduceAndBroadcast } = require("../services/BroadcastOrder.js");

const processPendingToComplete = function (
  { method = "", status = { success: null, label: "" }, body = null },
) {
  console.log(`webhookController:processPendingToComple: this is a ${method} webhook`);

  if (!method || !status || !body) {
    throw new Error(
      "missing information to process pending invoice to complete",
    );
  }

  let mongoId = "";
  if (method === "recurrente") {
    mongoId = "rec_" + body.product.id;
  } else if (method === "btc") {
    mongoId = "btc_" + body.invoiceId;
  }

  // Maybe status is an object with a boolean as well as the string name
  // status = { success: <boolean>, label: "payment_intent.suceeded"}
  if (status.success) {
    // result = await PendingInvoice.findOne({ _id: invoiceId });
    PendingInvoice.findOne(
      { _id: mongoId },
      function (err, result) {

        if (err) {
          console.error("Error finding the document:", err);
          return;
        } 

        if (!result) {
          console.log('Document not found');
          return;
        }

        const resultJson = result.toJSON();

        // The purpose of the following block of code is to be able to receive both a string
        // (i.e., a single restaurant order) as well as an Array (i.e., a group of restaurants in an order)
        console.log(resultJson)
        const _entityId = resultJson.entityId;

        // For Recurrente
        // we receive the checkout ID in the webhook which we swap out the product_id for
        if (method === "recurrente") {
          resultJson._id = `rec_${body.checkout.id}`;
        }

        // handle cases for 1 restaurant
        if (
            typeof (_entityId) === "string" ||
            (Array.isArray(_entityId) && _entityId.length === 1)
          ) {

          if (Array.isArray(_entityId) && _entityId.length === 1) {
            resultJson.entityId = resultJson.entityId[0]
          }

          resultJson.confirmed_at = (new Date()).toString();
          resultJson.status = status.label;
          const autoSshPort = resultJson.thermalPrinterAutoSSHPort;
          delete result.thermalPrinterAutoSSHPort;
          const swap = new CompletedInvoice(resultJson);
          result.remove();

            try {
              swap.save();
              // remove entry from pending invoices
              result.remove();
              console.log(
                `${method} Invoice moved from Pending to Succeeded!`,
              );
              // Send to Thermal printer & Telegram
              reduceAndBroadcast(autoSshPort, resultJson);
            } catch (err) {
              console.error(err);
            }

        } else if (
          Array.isArray(_entityId) &&
          _entityId.every((item) => typeof (item) === "string")
        ) {
          // set confirmed_at timestamp
          resultJson.confirmed_at = (new Date()).toString();

          splitAndBroadcastRestaurantInvoices(
            status.label,
            resultJson,
            method,
            _entityId,
          );

          result.remove();
        } else {
          throw new TypeError(
            `Expecting String or Array of Strings. Received type ${typeof (_entityId)} `,
          );
        } 
      });
  } else if (!status.success) {

    const touched_at = (new Date()).toString()
    const query = { _id: mongoId }
    const update = { $set: { status: status.label, touched_at}}
    const options = {} // { new : true } if you want to returnd modified doc
    PendingInvoice.findOneAndUpdate(
      query, 
      update,
      options,
      (err, result) => {

        if (err) {
              console.error('Error updating the document:', err);
              return;
            } else {

              // RESTOCK
              const items = result.metadata.items;
              try {
                // Restore items to inventory
                restockInventory(items);
              } catch (err) {
                console.error(err);
              }
              console.log('Document updated successfully');
            }

        }


      );
  }
};

const splitCard = function (
  status,
  unsplitInvoice,
  method,
  restaurantIdArray,
) {
  const payMethod = "Card";
  // Get the subtotal for doing calculations in the splitting of invoices
  const subtotal = unsplitInvoice.amount -
    (unsplitInvoice.metadata.tipAmount + unsplitInvoice.metadata.feeAmount);
  // TODO pull this out to put into a splitInvoices() function
  // (remember to pass in `subtotal`)

  const splitInvoices = [];

  for (const entityId of restaurantIdArray) {
    let newInvoice = undefined;

    const items = unsplitInvoice.metadata.items.filter((item) =>
      item.restaurant === entityId
    );

    const indSubTot = items.reduce(
      (accumulator, item) => accumulator + item.price,
      0,
    );

    const tipAmount = Number((indSubTot / subtotal *
      unsplitInvoice.metadata.tipAmount).toFixed(2));
    const feeAmount = Number((indSubTot / subtotal *
      unsplitInvoice.metadata.feeAmount).toFixed(2));

    // TODO NEED TO CHECK ITEMS
    const newId = unsplitInvoice._id + `_${entityId}`;
    newInvoice = {
      ...unsplitInvoice,
      _id: newId,
      entityId,
      payMethod,
      amount: indSubTot + tipAmount + feeAmount,
      metadata: {
        ...unsplitInvoice.metadata,
        items,
        tipAmount,
        feeAmount,
      },
    };

    // note this is not status.label
    newInvoice.status = status;
    const autoSshPort = unsplitInvoice.thermalPrinterAutoSSHPort;
    const swap = new CompletedInvoice(newInvoice);
    swap.save();
    console.log(
      `${method} Invoice moved from Pending to Succeeded!`,
    );
    // Send to Thermal printer & Telegram
    splitInvoices.push({ sshPort: autoSshPort, invoice: newInvoice });
    // Remove result
  }
  return splitInvoices;
};

const splitBtc = function (
  status,
  unsplitInvoice,
  method,
  restaurantIdArray,
) {
  const payMethod = "BTC";
  // Get the subtotal for doing calculations in the splitting of invoices

  const totBtcTip = unsplitInvoice.metadata.tipAmount;
  const totBtcFee = unsplitInvoice.metadata.feeAmount;

  const totInvAmount = unsplitInvoice.metadata.fiatEquivalentAmount;
  const totTipAmount = unsplitInvoice.metadata.fiatEquivalentTip;
  const totFeeAmount = unsplitInvoice.metadata.fiatEquivalentFee;

  const subtotal = totInvAmount - (totTipAmount + totFeeAmount);
  // TODO pull this out to put into a splitInvoices() function
  // (remember to pass in `subtotal`)

  const splitInvoices = [];

  for (const entityId of restaurantIdArray) {
    let newInvoice = undefined;

    const items = unsplitInvoice.metadata.items.filter((item) =>
      item.restaurant === entityId
    );

    const indSubTot = items.reduce(
      (accumulator, item) => accumulator + item.price,
      0,
    );

    // Calc BTC values
    const indBtcAmount = Number(
      (indSubTot / subtotal * unsplitInvoice.amount).toFixed(8),
    );

    const indBtcTip = Number((indSubTot / subtotal *
      totBtcTip).toFixed(8));

    const indBtcFee = Number((indSubTot / subtotal *
      totBtcFee).toFixed(8));

    // Calc Fiat Values

    const indTipAmount = Number((indSubTot / subtotal *
      totTipAmount).toFixed(2));

    const indFeeAmount = Number((indSubTot / subtotal *
      totFeeAmount).toFixed(2));

    // TODO NEED TO CHECK ITEMS
    const newId = unsplitInvoice._id + `_${entityId}`;
    newInvoice = {
      ...unsplitInvoice,
      _id: newId,
      entityId,
      payMethod,
      amount: indBtcAmount,
      metadata: {
        ...unsplitInvoice.metadata,
        items,
        tipAmount: indBtcTip,
        feeAmount: indBtcFee,
        fiatEquivalentAmount: indSubTot + indTipAmount + indFeeAmount,
        fiatEquivalentTip: indTipAmount,
        fiatEquivalentFee: indFeeAmount,
      },
    };

    newInvoice.status = status; //note it's not status.label, maybe clean this up in a refactor
    // TODO get this from the database
    const autoSshPort = unsplitInvoice.thermalPrinterAutoSSHPort;
    const swap = new CompletedInvoice(newInvoice);
    swap.save();
    console.log(
      `${method} Invoice moved from Pending to Succeeded!`,
    );
    // Send to Thermal printer & Telegram
    splitInvoices.push({ sshPort: autoSshPort, invoice: newInvoice });
    // Remove result
  }
  return splitInvoices;
};

const splitAndBroadcastRestaurantInvoices = function (
  status,
  unsplitInvoice,
  method,
  restaurantIdArray,
) {
  let splitInvoices = [];
  if (method === "recurrente") {
    splitInvoices = splitCard(
      status,
      unsplitInvoice,
      method,
      restaurantIdArray,
    );
  } else if (method === "btc") {
    splitInvoices = splitBtc(
      status,
      unsplitInvoice,
      method,
      restaurantIdArray,
    );
  } else if (method === "cash") {
    // payMethod = "Cash";
    console.log("splitting cash is not a thing yet!");
  } else {
    throw new Error(
      "method not recognized in webhookController.splitAndBroadcastRestaurantInvoices.",
    );
  }

  for (const splitInvoice of splitInvoices) {
    reduceAndBroadcast(splitInvoice.sshPort, splitInvoice.invoice);
  }
};

exports.handleWebhook = (req, res) => {
  // the Reccurente webhook has a field called event_name which we will use to identify it
  // TODO
  // example webhook
  // {
  //   "api_version": "2022-08-08",
  //   "checkout": {
  //     "id": "ch_acteguhg85ruj72e"
  //   },
  //   "created_at": "2023-03-22T19:18:42.946-06:00",
  //   "customer_id": "us_wuz6sn7b",
  //   "event_type": "payment_intent.succeeded",
  //   "id": "pa_aflicdgq",
  //   "product": {
  //     "id": "prod_gbifwstm"
  //   }
  // }

  const body = req.body;

  if (
    req.get("User-Agent")?.startsWith("Svix-Webhooks")
  ) {
    // ############################## RECURRENTE ##############################
    // ref: https://public.3.basecamp.com/p/gn3Tw4xcJxe2aNBjwM2WUn87

    // this property name event_type is different if it's recurrente vs btc ("type")
    const status = body.event_type;

    if (status === "payment_intent.succeeded") {
      processPendingToComplete({
        method: "recurrente",
        status: { success: true, label: status },
        body,
      });
    } else if (status === "payment_intent.failed") {
      processPendingToComplete({
        method: "recurrente",
        status: { success: false, label: status },
        body,
      });
    } else {
      console.log("recurrente webhook status not recognized");
      console.log("this is the recurrente webhook: ", body);
    }

    res.send();
  } else if (req.rawHeaders.includes("BTCPay-Sig")) {
    const status = req.body.type;
    if (status === "InvoicePaymentSettled" || status === "InvoiceSettled") {
      processPendingToComplete({
        method: "btc",
        status: { success: true, label: status },
        body,
      });
    } else {
      processPendingToComplete({
        method: "btc",
        status: { success: false, label: status },
        body,
      });
    }
  }
  res.send();
};

// Recurrente example payload
// {
// event_name: "payment_intent.succeeded"
// id: pa_12345,
// api_version: 2022-08-08,
// created_at: fecha,
// customer_id: us_12345,
// product: { id: pr_12345 },
// checkout: { id: ch_12345 },
// }
//

// else {
//     // ############################## CASH ##############################
//     // The purpose of this is simply to move the pending invoice to the successful collection
//     // It does not trigger a printer post like cash and card
//     // Printer post is triggered immediately when the invoice is created, however the staff does not start the order
//     //   until the payment is received
//
//     console.log("this is a (presumably) a cash webhook");
//     const body = req.body;
//     const status = body.type;
//     // match the id with the one in Mongodb
//     const mongoId = "fiat_" + body.invoiceId;
//     // let result = undefined;
//     if (status === "PaymentSettled") {
//       // result = await PendingInvoice.findOne({ _id: invoiceId });
//       PendingInvoice.findOne(
//         { _id: mongoId },
//         function (err, result) {
//           if (err) {
//             console.log(err);
//           } else if (result) {
//             if (result) {
//               const resultJson = result.toJSON();
//               resultJson.confirmed_at = (new Date()).toString();
//               resultJson.status = status;
//               const swap = new CompletedInvoice(resultJson);
//               result.remove();
//               swap.save();
//               // printerController.sendOrderToPrinter(resultJson); // this is done as soon as the invoice arrive in invoiceController
//               console.log(
//                 `CASH Invoice moved from Pending to Succeeded!`,
//               );
//             }
//           } else {
//             console.log(`No invoice for ${mongoId} found`);
//           }
//         },
//       );
//     } else {
//       PendingInvoice.findOne(
//         { _id: mongoId },
//         function (err, result) {
//           if (err) {
//             console.log(err);
//           } else if (result) {
//             const resultJson = result.toJSON();
//             resultJson.confirmed_at = (new Date()).toString();
//             resultJson.status = status;
//             const swap = new FailedInvoice(resultJson);
//             const items = result.metadata.items;
//             result.remove();
//             swap.save();
//             try {
//               restockInventory(items);
//             } catch (e) {
//               console.error(
//                 "this is probably not an inventory item, but double check",
//                 e,
//               );
//               console.log("CASH caught this");
//             }
//             console.log(
//               `CASH Invoice moved from Pending to Failed!`,
//             );
//           } else {
//             console.log(`No invoice for ${mongoId} found`);
//           }
//         },
//       );
//     }
//   }
//
//
