// let var = value creates a variable that can be changed
// whereas const var = value makes a constant variable that can't be modified
require('./db/mongoose')
const fs = require('fs');
const http = require('http');
const https = require('https');
const express = require('express')
const cors = require('cors')
const morgan = require('morgan')
//note body-parser for handling json is now automatically part of express

const app = express()
const router = require('./router')
const userRouter = require('./routers/user')
const restaurantRouter = require('./routers/restaurant')
const menuRouter = require('./routers/menu')
const categoryRouter = require('./routers/category')
const subcategoryRouter = require('./routers/subcategory')
const itemRouter = require('./routers/item')
const filterRouter = require('./routers/filter')
const addonRouter = require('./routers/addon')

// https://medium.com/the-node-js-collection/making-your-node-js-work-everywhere-with-environment-variables-2da8cdf6e786
// https://www.twilio.com/blog/working-with-environment-variables-in-node-js-html
const dotenv = require('dotenv');
dotenv.config();
console.log('Your NODE_ENV is: ' + process.env.NODE_ENV)
console.log(`Your domain is ${process.env.DOMAIN}`)
console.log(`Your mongo connection str is ${process.env.MONGODB_CONNSTR}`); // 8626

// accept the two most common ways of submitting data
// HTML form submit & JSON data
// adds the user submitted data to request object as req.body
app.use(express.urlencoded({extended: false}))
app.use(express.json())

// enable packages to hit back-end and have logs printed out
app.use(morgan('combined')) // combined arg prints logs in a certain way (ref morgan docs)

// TODO fix the origin, presumably emporium.atitlan.io
app.use(cors({
    origin: '*' //'http://localhost:8080'
})) // to allow any host or client to access note **security risk**



// redirect to https
if (process.env.NODE_ENV != "development") {
  console.log(`*Production* all http requests forwarded to https://${process.env.DOMAIN}`)
  app.use(function(req, res, next) {
    if (!req.secure || req.headers.host != process.env.DOMAIN) {
      console.log(`redirecting to https://${process.env.DOMAIN}`);
      res.redirect(301, `https://${process.env.DOMAIN + req.url}`)
    }
    next()
  });
}


// allow access to /.well-known/acme-challenge
// app.use(express.static(__dirname + '/dist/', { dotfiles: 'allow' } ))
// navigate back to /index.html for SPA *TODO* make this actually work
// https://v3.router.vuejs.org/guide/essentials/history-mode.html
// https://github.com/bripkens/connect-history-api-fallback
app.use(itemRouter)
const history = require('connect-history-api-fallback');
const staticFileMiddleware = express.static(__dirname + '/dist/');
app.use(staticFileMiddleware);
app.use(history({ verbose: true }))
app.use(staticFileMiddleware);
// app.use('/',router)
app.use(addonRouter) // note the position here is important so as not to get caught by another route
app.use(userRouter)
app.use(restaurantRouter)
app.use(menuRouter)
app.use(categoryRouter)
app.use(subcategoryRouter)
app.use(filterRouter)

// Certificate
if (process.env.NODE_ENV != "development") {
  const domain = process.env.DOMAIN
  const privateKey = fs.readFileSync(`/etc/letsencrypt/live/${domain}/privkey.pem`, 'utf8');
  const certificate = fs.readFileSync(`/etc/letsencrypt/live/${domain}/cert.pem`, 'utf8');
  const ca = fs.readFileSync(`/etc/letsencrypt/live/${domain}/fullchain.pem`, 'utf8');

  const credentials = {
   key: privateKey,
    cert: certificate,
    ca: ca
  };


  // Starting both http & https servers
  const httpServer = http.createServer(app);
  const httpsServer = https.createServer(credentials, app);


  // mongodb installation for Ubuntu
  // https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/#install-mongodb-community-edition
  //

  // const dotenv = require('dotenv')
  // dotenv.config()



  httpServer.listen(80, () => {
    console.log('HTTP Server running on port 80'); // this gets redirected to https, ref code above
  });

  httpsServer.listen(443, () => {
    console.log('HTTPS Server running on port 443');
  });

      // console.log("Checking for unresolved invoices...")
      // const invoiceController = require('./controllers/invoiceController')
      // invoiceController.checkForUnresolvedInvoices()
      
} else {
    console.log('you are in development')
    console.log(`Your port is ${process.env.PORT}`); 
    app.listen(process.env.PORT || 8081)
    // console.log("Checking for unresolved invoices...")
    // const invoiceController = require('./controllers/invoiceController')
    // invoiceController.checkForUnresolvedInvoices()
}

// ref https://www.twilio.com/blog/working-with-environment-variables-in-node-js-html
// if (process.env.NODE_ENV != 'production') {
//     require('dotenv').config()
// }

// just an example! this would be modified to a model/controller
// app.post('/register', (req, res) => {
//     res.send({
//         // try sending a post request with Postman!
//         // note how it comes out in the browser as json format
//         email: `${req.body.email}`,
//         password: `${req.body.password}` 
//     })
// })
