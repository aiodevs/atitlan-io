const axios = require("axios");

const RecurrenteApi = axios.create({
  baseURL: process.env.RECURRENTE_API_URL,
  timeout: 10000,
  responseType: "json",
  headers: {
    "X-PUBLIC-KEY": process.env.RECURRENTE_PUB_KEY,
    "X-SECRET-KEY": process.env.RECURRENTE_PRIVATE_KEY,
    "Content-type": "application/json",
  },
});

// exports.storeId = storeId

// maybe bring the above into this file...
// ref: https://github.com/axios/axios
exports.createInvoice = ({
  amount = 0,
  metadata = {},
}) => {
  let orderSummary = `Tip Amount: ${metadata.tipAmount}\n`;
  try {
    for (const item of metadata.items) {
      let addOns = "";
      if (item.addedOn && item.addedOn.length > 0) {
        for (let [index, addOn] of item.addedOn.entries()) {
          addOn = addOn.split("(")[0].trim();
          if (index === item.addedOn.length - 1) {
            addOns = addOns + addOn;
          } else {
            addOns = addOns + addOn + ",";
          }
        }
      }
      if (addOns) {
        orderSummary += `${item.quantity} x ${item.name} ` + `[${addOns}]` +
          `(${item.price})\n`;
      } else {
        orderSummary += `${item.quantity} x ${item.name} (${item.price})\n`;
      }
    }
    orderSummary += ` comments: ${metadata.comments}`;
  } catch (e) {
    console.log("Failed to create order summary for posData");
    console.log(e);
  }

  // this payload is to post to /products which makes a product, however we will use because it returns required info
  const payload = {
    // create the id to be a mixture of current time and a random 3 digit number
    product: {
      name: "order_" + (new Date()).getTime() + "_" +
        Math.floor(Math.random() * 999),
      description: orderSummary,
      prices_attributes: [
        {
          amount_as_decimal: amount.toFixed(2), // this needs to be a string, which .toFixed() returns with added security of only 2 decimal places
          currency: "GTQ",
          charge_type: "one_time",
        },
      ],
    },
  };

  return RecurrenteApi.post(`/products`, payload);

  // example resonse to post('/products')
  // {
  //     "id": "prod_i5qazwl8",
  //     "status": "active",
  //     "name": "Mi Producto Ejemplo",
  //     "description": {
  //         "id": null,
  //         "name": "description",
  //         "body": null,
  //         "record_type": "Product",
  //         "record_id": 147326,
  //         "created_at": null,
  //         "updated_at": null
  //     },
  //     "success_url": null,
  //     "cancel_url": null,
  //     "custom_terms_and_conditions": {
  //         "id": null,
  //         "name": "custom_terms_and_conditions",
  //         "body": null,
  //         "record_type": "Product",
  //         "record_id": 147326,
  //         "created_at": null,
  //         "updated_at": null
  //     },
  //     "phone_requirement": "none",
  //     "address_requirement": "none",
  //     "billing_info_requirement": "optional",
  //     "prices": [
  //         {
  //             "id": "price_2jlvvrb4",
  //             "amount_in_cents": 599,
  //             "currency": "GTQ",
  //             "billing_interval_count": 0,
  //             "billing_interval": "",
  //             "charge_type": "one_time",
  //             "periods_before_automatic_cancellation": null,
  //             "free_trial_interval_count": 0,
  //             "free_trial_interval": ""
  //         }
  //     ],
  //     "storefront_link": "https://app.recurrente.com/s/the-emporium/mi-producto-ejemplo-doqe25"
  // }

  // post('/checkouts')
  // This payload posts to /checkouts and makes an *invisible* product (however it does not return important data in the response..)
  // const payload = {
  //   // create the id to be a mixture of current time and a random 3 digit number
  //   id: id,
  //   items: [{
  //     currency: "GTQ",
  //     description: orderSummary,
  //     quantity: 1,
  //     amount_in_cents: Math.round(amount * 100),
  //   }],
  // };
  // DOUBLE CHECK : this returns an object with the checkoutLink
  // return RecurrenteApi.post(`/checkouts`, payload);
};

// REF https://docs.recurrente.com/quickstart
//const response = await axios.post(
//   'https://app.recurrente.com/api/products',
//   // '{"product": { "name": "Mi Producto Ejemplo", "prices_attributes": [{ "amount_as_decimal": "5.99", "currency": "GTQ", "charge_type": "one_time" }] }}',
//   {
//     'product': {
//       'name': 'Mi Producto Ejemplo',
//       'prices_attributes': [
//         {
//           'amount_as_decimal': '5.99',
//           'currency': 'GTQ',
//           'charge_type': 'one_time'
//         }
//       ]
//     }
//   },
//   {
//     headers: {
//       'X-PUBLIC-KEY': 'pk_live_XXXX',
//       'X-SECRET-KEY': 'sk_live_XXXX',
//       'Content-type': 'application/json'
//     }
//   }
// );/
