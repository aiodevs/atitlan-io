#!/usr/bin/python3

# Trinket IO demo
# Welcome to CircuitPython 3.1.1 :)

import sys
import board
from digitalio import DigitalInOut, Direction, Pull
import time
import neopixel

# Digital input with pullup on D15
# For turning OFF lights
button = DigitalInOut(board.D15)
button.direction = Direction.INPUT
button.pull = Pull.DOWN

# NeoPixel strip (of 30 LEDs) connected on D18
num_pixels = 30
neopixels = neopixel.NeoPixel(board.D18, num_pixels, brightness=.5, auto_write=False)


######################### HELPERS ##############################

# Helper to give us a nice color swirl
def wheel(pos):
  # Input a value 0 to 255 to get a color value.
  # The colours are a transition r - g - b - back to r.
  if (pos < 0):
    return (0, 0, 0)
  if (pos > 255):
    return (0, 0, 0)
  if (pos < 85):
    return (int(pos * 3), int(255 - (pos*3)), 0)
  elif (pos < 170):
    pos -= 85
    return (int(255 - pos*3), 0, int(pos*3))
  else:
    pos -= 170
    return (0, int(pos*3), int(255 - pos*3))


# Wheel Cycle Def
def wheel_cycle(wait):
    for p in range(num_pixels):
      idx = int ((p * 256 / num_pixels) + i)
      neopixels[p] = wheel(idx & 255)
    neopixels.show()

# Rainbow Cycle Def
def rainbow_cycle(wait):
    for j in range(255):
        for i in range(num_pixels):
            pixel_index = (i * 256 // num_pixels) + j
            neopixels[i] = wheel(pixel_index & 255)
        neopixels.show()
        time.sleep(wait)
######################### MAIN LOOP ##############################

i = 0
while True:
  
  rainbow_cycle(0.001)
      
  #for p in range(num_pixels):
  #  if (p == 0):
  #    return neopixels[0] = neopixels[255]
  #  neopixels[p] = neopixels[p-1]
  #neopixels.show()

  # kill script when button pressed
  if button.value:
    print("Button pressed!")
    for p in range(num_pixels):
      neopixels[p] = (0,0,0)
    neopixels.show()
    sys.exit()

  i = (i+ 50) % 256  # run from 0 to 255

