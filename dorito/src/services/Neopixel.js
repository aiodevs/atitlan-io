// ref: https://stackabuse.com/executing-shell-commands-with-node-js/
const { spawn } = require("child_process");

// TODO can you add cmd line parameters to specify the number of LED's, GPIO pin for turning off?

// The following script is currently a python script that turns on a Neopixel strip of LED's
// There is also a physical button attached to a GPIO pin of the raspberry pi that allows
// a staff member to terminate the python script.

// TODO make sure that orders still go through even while the Neopixel is running (they should)

let lightsOn = false;

exports.turnOn = (err) => {
  // Print any errors
  if (err) console.log(err);

  // only turn Neopixel on if not already running
  //     the python script kills itself when a button is pressed
  //     this causes th process to close toggling lightsOn back to false
  if (!lightsOn) {
    lightsOn = true;
    console.log(lightsOn);

    const ls = spawn("sudo", [
      "/home/maya/server/src/services/scripts/pyNeopixLoop.sh",
    ]);

    ls.stdout.on("data", (data) => {
      console.log(`stdout: ${data}`);
    });

    ls.stderr.on("data", (data) => {
      console.log(`stderr: ${data}`);
    });

    ls.on("error", (error) => {
      console.log(`error: ${error.message}`);
      lightsOn = false;
    });

    ls.on("close", (code) => {
      console.log(lightsOn);
      console.log(`child process exited with code ${code}`);
      lightsOn = false;
      console.log(lightsOn);
    });
  } else {
    console.log("New order received, lights already running!");
  }
};
