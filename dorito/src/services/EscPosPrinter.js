// https://github.com/escpos/node-escpos
// npm install escpos
// may need `sudo apt-get install build-essential libudev-dev` on linux (ubuntu)

const escpos = require("escpos");
// install escpos-usb adapter module manually
// npm install escpos-usb
// npm install usb@1.9.2 // https://github.com/song940/node-escpos/issues/376#issuecomment-1033600100
escpos.USB = require("escpos-usb");

let count = 0;

exports.printOrder = (payload) => {
  // reset count
  if (count > 999) count = 0;

  // const util = require('util')
  // console.log("Data recieved =\n")
  // console.log(req.body)
  const data = payload;
  // TODO make sure using req.type is smart
  const cashPayment = payload.type === "cashPayment"; // *** did i modify this before sending it to the flask app? ***
  const items = data.metadata.orderDesc;
  const comments = data.metadata.comments;
  const tipAmount = data.metadata.tipAmount;

  const cartTotal = data.amount;
  console.log(items);
  console.log(`Total: ${cartTotal} Q`);
  console.log(`Tip: ${tipAmount} Q`);
  console.log(`Comments: ${comments}`);

  // console.log(`${util.inspect(req.body,false,null)}`)

  // Select the adapter based on your printer type
  // Current setup for Weeius Thermal Printer
  const device = new escpos.USB(0x4b43, 0x3830); // USB(vid,pid); vid(Vendor Id) and pid (Product Id) can be checked with the lsusb command or escpos.USB.findPrinter() method.

  // Other options
  // const device  = new escpos.Network('localhost');
  // const device  = new escpos.Serial('/dev/usb/lp0');

  // const options = { encoding: "GB18030" /* default */ }
  // encoding is optional

  const printer = new escpos.Printer(device); //, options);

  device.open(function (error) {
    if (error) console.log(error);

    printer.font("a").align("ct").style("b").size(2, 2)
      .text(`Order #${count}\n`); // find out how to format count to have 3 digits

    // 3 long notification beeps
    printer.beep(3, 5);

    if (cashPayment) {
      printer.font("b").align("ct").style("b").size(1, 1).text("EFECTIVO\n");
      printer.font("a").align("ct").style("b").text("**NO PAGADO**");
    }

    if (tipAmount) {
      printer.size(1, 1).text(
        `${cartTotal - tipAmount} Q\n+tip: ${tipAmount}\n`,
      );

      if (!cashPayment) {
        printer.text("PAGADO\n");
      }
    } else {
      printer
        .text(`${cartTotal} Q\n`);

      if (!cashPayment) printer.text("PAGADO\n");
    }

    for (let item of items) {
      printer.align("lt").style("normal").size(1, 1)
        .text(`${item.quantity}x ${item.name}\n`);

      for (let addon of item.addedOn) {
        printer.text(`   -${addon.split("(")[0]}\n`);
      }
    }

    if (comments) {
      printer
        .text(`comments: ${comments}\n`.replace(/\p{Emoji}/ug, "")); //replace any emojis
    }
    printer.cut();
    printer.close();
  });

  count++;
};

// https://www.npmjs.com/package/escpos
// device.open(function(error){
// if (error) {
//   console.log(error)
// }
// printer
// .font('a')
// .align('ct')
// .style('bu')
// .size(1, 1)
// .text('The quick brown fox jumps over the lazy dog')
// .text('敏捷的棕色狐狸跳过懒狗')
// .barcode('1234567', 'EAN8')
// .table(["One", "Two", "Three"])
// .tableCustom([
//   { text:"Left", align:"LEFT", width:0.33 },
//   { text:"Center", align:"CENTER", width:0.33},
//   { text:"Right", align:"RIGHT", width:0.33 }
// ])
// .qrimage('https://github.com/song940/node-escpos', function(err){
//   if (err) {
//     console.log(err)
//   }
//   this.cut();
//   this.close();
// });
// });
