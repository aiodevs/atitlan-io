const axios = require("axios");

exports.sendTelegramMessage = (message) => {
  const botToken = "6494033404:AAEBh2Vr83u3DusptAzkl8aeyfQPqMgCRuU";
  const telegramApiUrl = `https://api.telegram.org/bot${botToken}/sendMessage`;
  const chatId = "-4058173890"; // Replace with the chat or user ID where you want to send the message

  // Send a message to the group
  axios
    .post(telegramApiUrl, {
      chat_id: chatId,
      text: message,
      parse_mode: "Markdown",
    })
    .then((response) => {
      console.log("Message sent:", response.data);
    })
    .catch((error) => {
      console.error("Error sending message:", error);
    });
};

function getCurrentDateTime() {
  const now = new Date();

  // Get the components of the date and time
  const day = String(now.getDate()).padStart(2, "0");
  const month = String(now.getMonth() + 1).padStart(2, "0"); // Months are zero-based, so we add 1.
  const year = String(now.getFullYear()).slice(-2); // Get the last 2 digits of the year.
  const hours = String(now.getHours()).padStart(2, "0");
  const minutes = String(now.getMinutes()).padStart(2, "0");

  // Create the formatted date and time string
  const formattedDateTime = `${day}/${month}/${year} ${hours}:${minutes}`;

  return formattedDateTime;
}

exports.createReadableOrder = async (payload) => {
  const Restaurant = require("../models/Restaurant/restaurant.js");
  let result = "";
  let restaurantName = "";
  try {
    const restaurant = await Restaurant.findOne({
      _id: payload.entityId,
    });
    restaurantName = restaurant.name;
  } catch {
    console.log("could not retrieve restaurant from database");
    restaurantName = "UNKNOWN";
  }

  if (!payload.payMethod) {
    if (payload._id && payload._id.startsWith("rec")) {
      payload.payMethod = "Card 💳";
    } else if (payload._id && payload._id.startsWith("btc")) {
      payload.payMethod = "BTC 🟠";
    } else if (payload._id) {
      payload.payMethod = "Cash 💵";
    } else payload.payMethod = "UNKNOWN";
  }

  if (payload.thermalPrintSuccess) {
    result += "🖨✅ Order printed to kitchen\n";
  } else if (payload.thermalPrintSuccess === false) {
    result += "❌ Order *failed* to print to kitchen\n";
  } else {
    result +=
      "❔ No information on whether order printed successfully to the kitchen\n";
  }

  result +=
    `Order Received for *${restaurantName}*\nPayment Method: ${payload.payMethod}\n${getCurrentDateTime()}\n\n`;

  const cashPayment = payload.cashPayment;
  // TODO fiat payments have unnecessary payload.metadata.orderDesc here, whereas card (and btc?) only have payload.metadata.items
  const items = payload.metadata.orderDesc || payload.metadata.items;
  const comments = payload.metadata.comments;
  const tipAmount = payload.metadata.tipAmount || 0;
  const cartTotal = payload.amount;

  if (cashPayment) {
    result += "*YA NO PAGADO*\n\n";
  }

  if (tipAmount) {
    // toFixed(2) added thanks to Jordan the dancer
    result += `Total: ${Number(cartTotal - tipAmount).toFixed(2)} Q\n+tip: ${
      Number(tipAmount).toFixed(2)
    }\n\n`;

    if (!cashPayment) {
      result += "*PAGADO*\n\n";
    }
  } else {
    result += `Total: ${Number(cartTotal).toFixed(2)} Q\n\n`;

    if (!cashPayment) result += "*PAGADO*\n\n";
  }

  // ############# START ORDER SUMMARY #################

  result += "```\n";

  for (const item of items) {
    result += `${item.quantity}x ${item.name}\n`;

    for (const addon of item.addedOn) {
      result += `   +${addon.split("(")[0]}\n`;
    }
  }

  if (comments) {
    result += `\ncomments: ${comments}\n`;
  }

  result += "```";

  // ############# END ORDER SUMMARY #################

  return result;
};
