const axios = require("axios");

const { sendOrderToPrinter, addToUnprintedQueue } = require(
  "../controllers/printerController",
);

const sendTelegramMessage = (chatId, message, test = false) => {
  const botToken = "6494033404:AAEBh2Vr83u3DusptAzkl8aeyfQPqMgCRuU";
  const telegramApiUrl = `https://api.telegram.org/bot${botToken}/sendMessage`;

  if (test) {
    console.log(
      `Bot Token: ${botToken}\nChat Id: ${chatId}\nMessage: ${message}`,
    );
  } else {
    // Send a message to the group
    axios
      .post(telegramApiUrl, {
        chat_id: chatId,
        text: message,
        parse_mode: "Markdown",
      })
      .then((response) => {
        console.log("Message sent:", response.data);
      })
      .catch((error) => {
        console.error("Error sending message:", error);
      });
  }
};

function getCurrentDateTime() {
  const now = new Date();

  // Get the components of the date and time
  const day = String(now.getDate()).padStart(2, "0");
  const month = String(now.getMonth() + 1).padStart(2, "0"); // Months are zero-based, so we add 1.
  const year = String(now.getFullYear()).slice(-2); // Get the last 2 digits of the year.
  const hours = String(now.getHours()).padStart(2, "0");
  const minutes = String(now.getMinutes()).padStart(2, "0");

  // Create the formatted date and time string
  const formattedDateTime = `${day}/${month}/${year} ${hours}:${minutes}`;

  return formattedDateTime;
}

const getOrderSummary = function(payload) {
  let result = ""

  const items = payload.metadata.orderDesc || payload.metadata.items;
  const comments = payload.metadata.comments;
  const tipAmount = payload.metadata.tipAmount || 0;
  const cartTotal = payload.amount;
  const cashPayment = payload.payMethod === "Cash" ? true : false;

  if (tipAmount) {
    // toFixed(2) added thanks to Jordan the dancer
    result += `Total: ${Number(cartTotal - tipAmount).toFixed(2)} Q\n+tip: ${Number(tipAmount).toFixed(2)
      }\n\n`;

    if (!cashPayment) {
      result += "*PAGADO*\n\n";
    }
  } else {
    result += `Total: ${Number(cartTotal).toFixed(2)} Q\n\n`;

    if (!cashPayment) result += "*PAGADO*\n\n";
  }


  // ############# START ORDER SUMMARY #################

  result += "```OrderSummary\n";

  for (const item of items) {
    result += `${item.quantity}x ${item.name}\n`;

    for (const addon of item.addedOn) {
      result += `   +${addon.split("(")[0]}\n`;
    }
  }

  if (comments) {
    result += `\ncomments: ${comments}\n`;
  }

  result += "```";

  // ############# END ORDER SUMMARY #################

  return result
}

const createPrintStatusMsg = async (restaurantName, payload) => {
  let result = "";

  let payMethod = ""
  if (payload.payMethod === "Card") {
    result += "💳"
    payMethod = "Card";
  } else if (payload.payMethod === "BTC") {
    result += "🟠"
    payMethod = "BTC";
  } else if (payload.payMethod === "Cash") {
    result += "💵"
    payMethod = "Cash";
  } else payMethod = "UNKNOWN";

  if (payload.thermalPrintSuccess) {
    result += "🖨✅ Order printed to kitchen\n";
  } else if (payload.thermalPrintSuccess === false) {
    result += "❌ Order *failed* to print to kitchen\n";
  } else {
    result +=
      "❔ No information on whether order printed successfully to the kitchen\n";
  }

  result +=
    `Order Received for *${restaurantName}*\n`;
  result +=
    `Payment Method: ${payMethod}\n`
  result +=
    `${getCurrentDateTime()}\n\n`

  const cashPayment = payload.payMethod === "Cash" ? true : false;
  //
  // TODO fiat payments have unnecessary payload.metadata.orderDesc here, whereas card (and btc?) only have payload.metadata.items
  if (cashPayment) {
    result += "*YA NO PAGADO*\n\n";
  }

  const orderSummary = getOrderSummary(payload)

  result += orderSummary

  return result;
};

exports.reduceAndBroadcast = async function(autoSshPort, doc) {

  const Restaurant = require("../models/Restaurant/restaurant.js");

  let restaurantName = "";
  let telegramChatId = ""
  try {
    const restaurant = await Restaurant.findOne({
      _id: doc.entityId,
    });
    restaurantName = restaurant.name;
    if (restaurant.telegram) telegramChatId = restaurant.telegram.chatId

  } catch {
    console.log("could not retrieve restaurant from database");
    restaurantName = "UNKNOWN";
  }

  let payMethod = "";

  // doc._id && doc._id.startsWith("rec") === doc._id?.startsWith("rec")
  if (doc._id?.startsWith("rec")) {
    payMethod = "Card"
  } else if (doc._id?.startsWith("btc")) {
    payMethod = "BTC"
  } else {
    payMethod = "Cash"
  }

  const metadata = doc.metadata;

  let payload = undefined;

  if ("fiatEquivalentCurrency" in metadata) {
    payload = {
      entityId: doc.entityId || "",
      amount: Number(metadata.fiatEquivalentAmount),
      payMethod, // not used by printer yet
      metadata: {
        orderDesc: metadata.items,
        tipAmount: Number(metadata.fiatEquivalentTip),
        comments: metadata.comments,
      },
    };
  } else {
    payload = {
      entityId: doc.entityId || "",
      amount: Number(doc.amount),
      payMethod,
      metadata: {
        orderDesc: metadata.items,
        tipAmount: Number(metadata.tipAmount),
        comments: metadata.comments,
      },
    };
  }


  try {
    if (
      metadata.factura &&
      Object.values(metadata.factura).some(value => value !== "") &&
      (payload.entityId === "636523d76f649cb57bb69269" || payload.entityId.includes("636523d76f649cb57bb69269"))
    ) {
      let factura = metadata.factura
      let facturaMessage = `🧾 Ixchel Cafe Factura requested for a cash payment\n\n\`\`\`Customer\n Name: ${factura.name}\n NIT: ${factura.nit}\n Address: ${factura.address}\n e-mail: ${factura.email}\n\`\`\`\n `
      sendTelegramMessage("6766813963", facturaMessage + getOrderSummary(doc))
      sendTelegramMessage("785388400", facturaMessage + getOrderSummary(doc))
    }
  } catch (err) {
    console.error(err)
  }

  if (payload) {
    let thermalPrintSuccess = false;

    // send to thermal printer
    const payloadToSend = { autoSshPort, payload };
    try {
      const response = await sendOrderToPrinter(payloadToSend);
      if (response.status === 200) {
        thermalPrintSuccess = true;
      } // else { // TODO want to test this
      //   addToUnprintedQueue(payloadToSend);
      //   console.error("Could not send order to printer\n", err);
      // }
    } catch (err) {
      addToUnprintedQueue(payloadToSend);
      console.error("Could not send order to printer\n", err);
    }
    // send telegram message
    if (telegramChatId) {
      try {
        payload.thermalPrintSuccess = thermalPrintSuccess;
        const message = await createPrintStatusMsg(restaurantName, payload);
        console.log(message)
        sendTelegramMessage(telegramChatId, message);
      } catch (err) {
        console.error("Could not send to telegram\n", err);
      }
    } else {
      console.warn("No Telegram chatId")
    }

  } else {
    console.error(
      "failed to create payload for printerController in reduceAndBroadcast",
    );
    console.log("Here's the doc: ", doc);
  }
};
