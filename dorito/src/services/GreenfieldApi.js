const axios = require("axios");

const GreenfieldApi = axios.create({
  baseURL: process.env.BTCPAY_API_URL,
  timeout: 10000,
  responseType: "json",
  headers: {
    "Authorization": "token " + process.env.GREENFIELD_API_KEY,
    "Content-Type": "application/json",
  },
});

exports.getInvoicePaymentMethods = (storeId, invoiceId) => {
  return GreenfieldApi.get(
    `stores/${storeId}/invoices/${invoiceId}/payment-methods`,
  );
};

// maybe bring the above into this file...
// ref: https://github.com/axios/axios
exports.createInvoice = (
  {
    amount = 0,
    btcpay_data = {},
    metadata = {},
  },
) => {
  const storeId = btcpay_data.storeId;
  // const items = metadata.items;

  // The following structures the items in a format to be thermal printed
  // let orderSummary = "";
  // try {
  //   for (const item of items) {
  //     let addOns = "";
  //     if (item.addedOn && item.addedOn.length > 0) {
  //       for (let [index, addOn] of item.addedOn.entries()) {
  //         addOn = addOn.split("(")[0].trim();
  //         if (index === item.addedOn.length - 1) {
  //           addOns = addOns + addOn;
  //         } else {
  //           addOns = addOns + addOn + ",";
  //         }
  //       }
  //     }
  //     if (addOns) {
  //       orderSummary += `${item.name} ` + `[${addOns}] ` + `(${item.price})\n`;
  //     } else {
  //       orderSummary += `${item.name} (${item.price})\n`;
  //     }
  //   }
  // console.log(orderSummary)
  // } catch (e) {
  //   console.log("Failed to create order summary for posData");
  //   console.log(e);
  // }

  const payload = {
    amount: Number(amount).toFixed(2),
    currency: btcpay_data.currency, // this will probably become a parameter in the future
    checkout: btcpay_data.checkout,
    metadata: { posData: metadata },
  };
  // DOUBLE CHECK : this returns an object with the checkoutLink
  return GreenfieldApi.post(`/stores/${storeId}/invoices`, payload);
};
