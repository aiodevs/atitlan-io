
var connect = require('connect')
var serveStatic = require('serve-static')
var vhost = require('vhost')


const fs = require('fs');
const http = require('http');
const https = require('https');
const express = require('express')
const cors = require('cors')
const morgan = require('morgan')

const app = connect()
app.use(serveStatic(__dirname + '/dist/')

const menuApp = connect()
staticapp.use(serveStatic('/var/www/menu.atitlan.io'))

// add vhost routing to main app for mail
app.use(vhost('menu.atitlan.io', menuApp))

// create main app
var atitlanioApp = connect()
staticapp.use(serveStatic('/var/www/atitlan.io'))
// around max host connections limit on browsers
app.use(vhost('atitlan.com', staticapp))

// redirect to https
if (process.env.NODE_ENV != "development") {
  console.log(`*Production* all http requests forwarded to https://${process.env.DOMAIN}`)
  app.use(function(req, res, next) {
    if (!req.secure || req.headers.host != process.env.DOMAIN) {
      console.log(`redirecting to https://${process.env.DOMAIN}`);
      res.redirect(301, `https://${process.env.DOMAIN + req.url}`)
    }
    next()
  });
}


const history = require('connect-history-api-fallback');
const staticFileMiddleware = express.static(__dirname + '/dist/');
app.use(staticFileMiddleware);
app.use(history({ verbose: true }))
app.use(staticFileMiddleware);

// SSL Certificate
if (process.env.NODE_ENV != "development") {
  const domain = process.env.DOMAIN
  const privateKey = fs.readFileSync(`/etc/letsencrypt/live/${domain}/privkey.pem`, 'utf8');
  const certificate = fs.readFileSync(`/etc/letsencrypt/live/${domain}/cert.pem`, 'utf8');
  const ca = fs.readFileSync(`/etc/letsencrypt/live/${domain}/chain.pem`, 'utf8');

  const credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca
  };

  // Starting both http & https servers
  const httpServer = http.createServer(app);
  const httpsServer = https.createServer(credentials, app);


  httpServer.listen(80, () => {
    console.log('HTTP Server running on port 80'); // this gets redirected to https, ref code above
  });

  httpsServer.listen(443, () => {
    console.log('HTTPS Server running on port 443');
  });

} else {
    console.log('you are in development')
    app.listen(process.env.PORT || 8081)
}
