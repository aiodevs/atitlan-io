// let var = value creates a variable that can be changed
// whereas const var = value makes a constant variable that can't be modified
require("./db/mongoose");
const fs = require("fs");
const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
//note body-parser for handling json is now automatically part of express

const app = express();
const router = require("./router");
const userRouter = require("./routers/user");
const restaurantRouter = require("./routers/restaurant/restaurant");
const storeRouter = require("./routers/store/store");
const menuRouter = require("./routers/restaurant/menu");
const categoryRouter = require("./routers/restaurant/category");
const subcategoryRouter = require("./routers/restaurant/subcategory");
const itemRouter = require("./routers/restaurant/item");
const filterRouter = require("./routers/restaurant/filter");
const addonRouter = require("./routers/restaurant/addon");
const lemmyRouter = require("./routers/lemmy");

// https://medium.com/the-node-js-collection/making-your-node-js-work-everywhere-with-environment-variables-2da8cdf6e786
// https://www.twilio.com/blog/working-with-environment-variables-in-node-js-html
const dotenv = require("dotenv");
dotenv.config();
console.log("Your NODE_ENV is: " + process.env.NODE_ENV);
console.log(`Your domain is ${process.env.DOMAIN}`);
console.log(`Your mongo connection str is ${process.env.MONGODB_CONN_STR}`); // 8626

// accept the two most common ways of submitting data
// HTML form submit & JSON data
// adds the user submitted data to request object as req.body
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// enable packages to hit back-end and have logs printed out
app.use(morgan("combined")); // combined arg prints logs in a certain way (ref morgan docs)

// TODO fix the origin, presumably emporium.atitlan.io
app.use(cors({
  origin: "*", //'http://localhost:8080'
})); // to allow any host or client to access note **security risk**

// allow access to /.well-known/acme-challenge
// app.use(express.static(__dirname + '/dist/', { dotfiles: 'allow' } ))
// navigate back to /index.html for SPA *TODO* make this actually work
// https://v3.router.vuejs.org/guide/essentials/history-mode.html
// https://github.com/bripkens/connect-history-api-fallback
const history = require("connect-history-api-fallback");
const staticFileMiddleware = express.static(__dirname + "/dist/", { dotfiles: 'allow' } );
app.use(itemRouter);
app.use(filterRouter);
app.use(addonRouter); // note the position here is important so as not to get caught by another route
app.use(storeRouter);
app.use(restaurantRouter);
app.use(staticFileMiddleware);
app.use(history({ verbose: true }));
app.use(staticFileMiddleware);
app.use(router);
app.use(userRouter);
app.use(menuRouter);
app.use(categoryRouter);
app.use(subcategoryRouter);
app.use(lemmyRouter);
// app.use('/',router)

// Certificate
if (process.env.NODE_ENV != "development") {
  app.listen(3330);
  console.log(`Your port is 3330`);
} else {
  console.log("you are in development");
  console.log(`Your port is ${process.env.PORT}`);
  app.listen(process.env.PORT || 8081);
  // console.log("Checking for unresolved invoices...")
  // const invoiceController = require('./controllers/invoiceController')
  // invoiceController.checkForUnresolvedInvoices()
}

// ref https://www.twilio.com/blog/working-with-environment-variables-in-node-js-html
// if (process.env.NODE_ENV != 'production') {
//     require('dotenv').config()
// }

// just an example! this would be modified to a model/controller
// app.post('/register', (req, res) => {
//     res.send({
//         // try sending a post request with Postman!
//         // note how it comes out in the browser as json format
//         email: `${req.body.email}`,
//         password: `${req.body.password}`
//     })
// })
