const jwt = require("jsonwebtoken");
const User = require("../models/user");

const dotenv = require("dotenv");
dotenv.config();

const auth = async (req, res, next) => {
  try {
    console.log(req.headers);
    const token = req.header("Authorization").replace("Bearer ", "");
    const decoded = jwt.verify(token, process.env.SECRET);
    const user = await User.findOne({
      _id: decoded._id,
      "tokens.token": token,
    });

    // this is for checking a token in someone's local storage against the username
    if (req.body.username && req.body.username != user.username) { // to change for lower_case
      throw new Error("token is for another user");
    }

    if (!user) {
      throw new Error("Please authenticate");
    }

    // piggy back fields onto the request
    req.token = token;
    req.user = user;

    next();
  } catch (e) {
    console.error(e);
    res.status(401).send({ error: e });
  }
};

module.exports = auth;
