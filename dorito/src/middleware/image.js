// Item image parameters
const multer = require("multer");
const sharp = require("sharp");

exports.multerize = multer({
  limits: {
    fileSize: 5 * 1e6, // max file size of ~ 5mb
  },
  // fileFilter(req, file, cb) {
  //     console.log(file)
  //     if (!file.originalname.endsWith('.webp')) {
  //         return cb(new Error('Image must be a .webp format'))
  //     }
  //     cb(null,true)
  // }
});

exports.resize = async (req, res, next) => {
    const buffer = await sharp(req.file.buffer).resize({
      width: 1024,
      height: 1024,
    }).webp().toBuffer();
  req.buffer = buffer
  next();
}
