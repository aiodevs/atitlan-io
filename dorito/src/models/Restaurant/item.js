const mongoose = require("mongoose");
const { restaurantConn } = require("../../db/mongoose");

const itemSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  restaurant: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Restaurant",
  },
  menu: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Menu",
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Category",
  },
  subcategory: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Subcategory",
  },
  description: {
    type: String,
    required: false,
    trim: true,
  },
  price: {
    type: Number,
    required: true,
    trim: true,
  },
  unit: {
    type: String, // e.g., "kg", "lb", "oz"
    required: false,
    trim: true,
  },
  // amount in stock
  inventory: {
    type: Number,
    required: false,
    trim: true,
  },
  image: {
    type: Buffer,
    contentType: String,
  },
  requiredOptions: {
    type: Array,
    default: [],
    required: false,
  },
  active: {
    type: Boolean,
    required: true,
    default: true,
  },
  filters: [String],
  addons: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Addon",
    unique: true,
  }],
  // note a position is not unique because an item can belong to different subcategories
  position: {
    type: Number,
  },
}); //, { toJSON: { virtuals: true } })

// =-=-=-=-=-=-=-=-=-=- this is obsolete since modifying the toJSON function below
// itemSchema.virtual('imgSrc').get(
//     function () {
//         if (this.image) {
//         return '/'+this.restaurant+'/'+this.menu+'/'+this.category+'/'+this.subcategory+'/'+this._id+'/image'
//         } else return null
//     }
// )

itemSchema.methods.toJSON = function () {
  const item = this;
  const itemObject = item.toObject();

  itemObject.postPath = "restaurants/" + itemObject.restaurant + "/" +
    itemObject.menu + "/" +
    itemObject.category + "/" + itemObject.subcategory + "/" + itemObject._id;

  if (itemObject.image) {
    itemObject.imgSrc = "restaurants/" + itemObject.restaurant + "/" +
      itemObject.menu + "/" +
      itemObject.category + "/" + itemObject.subcategory + "/" +
      itemObject._id + "/image";
  } else {
    itemObject.imgSrc = "";
  }
  delete itemObject.image;
  return itemObject;
};

itemSchema.pre("save", async function (next) {
  if (this.isNew) {
    await this.constructor.find({ subcategory: this.subcategory })
      .then((result) => {
        // create an array of all of the position values to calculate the new documents position
        const temp = [];
        for (const doc of result) {
          const objKeys = Object.keys(doc._doc);
          if (objKeys.includes("position")) {
            temp.push(doc.position); // ref above NOTE, but if there's no order field it's because it's probably the new doc being added
          }
        }
        const start = 0;
        const end = start + result.length;
        for (let i = start; i < end; i++) {
          if (!(temp.includes(i))) {
            this.position = i;
            break;
          } else if (i === result.length - 1) {
            this.position = end;
          }
        }
      })
      .catch((e) => {
        console.error(e);
      })
      .finally(() => {
        console.log(
          `position of item is ${this.position}`,
        );
        next();
      });
  } else { // not a new item
    next();
  }
});

// itemSchema.virtual('addons', {
//     ref: 'Addon',
//     localField: '_id',
//     foreignField: 'item'
// })

const Item = restaurantConn.model("Item", itemSchema);

module.exports = Item;
