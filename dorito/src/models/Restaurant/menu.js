const mongoose = require("mongoose");
const { restaurantConn } = require("../../db/mongoose");

const menuSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  restaurant: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Restaurant",
  },
  active: {
    type: Boolean,
    // default: true,
    required: true,
  },
});

menuSchema.virtual("categories", {
  ref: "Category",
  localField: "_id",
  foreignField: "menu",
});

const Menu = restaurantConn.model("Menu", menuSchema);

module.exports = Menu;
