const mongoose = require("mongoose");
const { restaurantConn } = require("../../db/mongoose");

const categorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  restaurant: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Restaurant",
  },
  menu: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Menu",
  },
  // Note a position is not unique because a category can belong to different menus
  position: {
    type: Number,
  },
  active: {
    type: Boolean,
    default: true,
  },
});

categorySchema.virtual("subcategories", {
  ref: "Subcategory",
  localField: "_id",
  foreignField: "category",
});

categorySchema.pre("save", async function (next) {
  if (this.isNew) {
    await this.constructor.find({ menu: this.menu })
      .then((result) => {
        // create an array of all of the position values to calculate the new documents position
        const temp = [];
        for (const doc of result) {
          const objKeys = Object.keys(doc._doc);
          if (objKeys.includes("position")) {
            temp.push(doc.position); // ref above NOTE, but if there's no order field it's because it's probably the new doc being added
          }
        }
        const start = 0;
        const end = start + result.length;
        for (let i = start; i < end; i++) {
          if (!(temp.includes(i))) {
            this.position = i;
            break;
          } else if (i === result.length - 1) {
            this.position = end;
          }
        }
      })
      .catch((e) => {
        console.error(e);
      })
      .finally(() => {
        console.log(
          `position of category is ${this.position}`,
        );
        next();
      });
  } else { // not a new item
    next();
  }
});

const Category = restaurantConn.model("Category", categorySchema);

module.exports = Category;
