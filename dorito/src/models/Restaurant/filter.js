const mongoose = require("mongoose");
const { restaurantConn } = require("../../db/mongoose");

const filterSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  item: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Item",
  },
  subset: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
  },
});

const Filter = restaurantConn.model("Filter", filterSchema);

module.exports = Filter;
