// this is just an idea.... how would we allow for multiple levels to a certain depth in the model??

// maybe I will just have to make the individual files for now, down to a certain depth
 
const mongoose = require('mongoose')

const max_depth = 10
for (let i = 0; i < 10; i++ ) {

// if i is that last index, then don't include a lowe leve... ?
if ( i === max_depth - 1 ) {
    const `level${iSchema}` = new mongoose.Schema({
        name: {
            type: String,
            required: true,
            trim: true
        },
        i === max_depth - 1  ? 
        restaurant: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'Restaurant',
        }, 
        active: {
            type: Boolean,
            default: true
        }
    })

    // how would this work... 
    // categoryschema.virtual('subcategories', {
    //     ref: 'subcategory',
    //     localfield: '_id',
    //     foreignfield: 'category'
    // })

    // const Category = mongoose.model('Category', categorySchema)
} else {
    const `level${iSchema}` = new mongoose.Schema({
        name: {
            type: String,
            required: true,
            trim: true
        },
        i === max_depth - 1  ? 
        restaurant: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'Restaurant',
        } : `level_i+1`: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: `level${i+1}`,
        }, 
        active: {
            type: Boolean,
            default: true
        }
    })
    // how would this work... 
    // categoryschema.virtual('subcategories', {
    //     ref: 'subcategory',
    //     localfield: '_id',
    //     foreignfield: 'category'
    // })
    // const Category = mongoose.model('Category', categorySchema)
}



module.exports = Category
