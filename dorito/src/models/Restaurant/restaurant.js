const mongoose = require("mongoose");
const { restaurantConn } = require("../../db/mongoose");
const User = require("../user");
const { PendingInvoice, CompletedInvoice, FailedInvoice } = require(
  "../invoice",
);

const restaurantSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true, // TODO needed?
  },
  small_logo: {
    type: Buffer,
    contentType: String,
  },
  banner_logo: {
    type: Buffer,
    contentType: String,
  },
  background_image: {
    type: Buffer,
    contentType: String,
  },
  description: {
    type: String,
    default: "",
  },
  css: {
    type: String,
    default: ""
  },
  social : {
    type: Object,
    default: { facebook: '', tripadvisor: '', instagram: '', happycow: '' }
  },
  url: {
    type: String,
    default: "https://atitlan.io",
  },
  active: {
    type: Boolean,
    required: true,
    default: true,
  },
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "User",
  },
  town: {
    type: String,
    required: true,
  },
  gmapsLink: {
    type: String,
    required: false,
  },
  // TODO maybe an object e.g., { mon : [8,21], tue: [8,18], ...}
  openHours: {
    type: Object,
    // default: {Monday:[],Tuesday:[],Wednesday:[],Thursday:[],Friday:[],Saturday:[],Sunday:[]}, //is this the best data structure? TODO if so make sure arrays only ever have 2 vals; maybe add property for days off
    required: false,
  },
  thermalPrinterAutoSSHPort: {
    type: Number,
    unique: true,
  },
  telegram: {
    type: Object,
    default: {
      chatId: "" 
    }
  },
  recurrente_pub_key: {
    type: String,
    default: "",
  },
  btcpay_data: {
    type: Object,
    default: {
      api_url: "https://btcpay.atitlan.io",
      storeId: "",
      checkout: {
        paymentMethods: ["BTC", "BTC-LightningNetwork"],
        defaultPaymentMethod: "BTC",
        expirationMinutes: 5,
        redirectURL: "https://atitlan.io",
        defaultLanguage: "en",
      },
      "currency": "GTQ",
      "additionalSearchTerms": [],
    },
    required: false,
  },
  // maybe this is held in a separate document for security
  // recurrente_sec_key: {
  //   type: String,
  //   default: "",
  // },
  // Other things to add, logo, background, colors, btcpay API key (or maybe i'll do these manually), kiosk link?
});

restaurantSchema.virtual("menus", {
  ref: "Menu",
  localField: "_id",
  foreignField: "restaurant",
});

restaurantSchema.virtual("addons", {
  ref: "Addon",
  localField: "_id",
  foreignField: "restaurant",
});

// Foreign field for these is "entity" because it could also be a store
restaurantSchema.virtual("pendingInvoices", {
  ref: PendingInvoice,
  localField: "_id",
  foreignField: "entity",
});
restaurantSchema.virtual("failedInvoices", {
  ref: FailedInvoice,
  localField: "_id",
  foreignField: "entity",
});
restaurantSchema.virtual("completedInvoices", {
  ref: CompletedInvoice,
  localField: "_id",
  foreignField: "entity",
});

// remove any keys from being sent
restaurantSchema.methods.toJSON = function () {
  const restaurant = this;
  const restaurantObject = restaurant.toObject();

  delete restaurantObject.btcpay_auth_token;
  delete restaurantObject.recurrente_pub_key;
  delete restaurantObject.recurrente_sec_key;

  return restaurantObject;
};

// auto increment the ssh port starting at 3000
// https://stackoverflow.com/a/67319608
// TODO: may need to revisit this
// I added to the algorithm because if you removed any of the documents that were not the
// end, the length would overlap with already created document values
restaurantSchema.pre("save", async function (next) {
  if (this.isNew) {
    await this.constructor.find({})
      .then((result) => {
        const temp = [];
        for (const doc of result) {
          const objKeys = Object.keys(doc._doc);
          if (objKeys.includes("thermalPrinterAutoSSHPort")) {
            temp.push(doc.thermalPrinterAutoSSHPort); // ref above NOTE, but if there's no order field it's because it's probably the new doc being added
          }
        }
        const start = 3000;
        const end = start + result.length;
        for (let i = start; i < end; i++) {
          if (!(temp.includes(i))) {
            this.thermalPrinterAutoSSHPort = i;
            break;
          } else if (i === result.length - 1) {
            if (end === 30000) {
              console.error("this is overlapping with the store ports!!!"); // this can be fixed by combining restaurants and stores into one thing...
            } else {
              this.thermalPrinterAutoSSHPort = end;
            }
          }
        }
      })
      .catch((e) => {
        console.error(e);
      })
      .finally(() => {
        console.log(
          `ssh port of restaurant is ${this.thermalPrinterAutoSSHPort}`,
        );
        next();
      });
  } else { // not a new item
    next();
  }
});

const Restaurant = restaurantConn.model("Restaurant", restaurantSchema);

module.exports = Restaurant;
