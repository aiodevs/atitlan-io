const mongoose = require("mongoose");
const { restaurantConn } = require("../../db/mongoose");

const addonSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  restaurant: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Restaurant",
  },
  description: {
    type: String,
    required: false,
    trim: true,
  },
  price: {
    type: Number,
    required: true,
    default: 0,
  },
  active: {
    type: Boolean,
    default: true,
  },
});

const Addon = restaurantConn.model("Addon", addonSchema);

module.exports = Addon;
