const mongoose = require("mongoose");
const { storeConn } = require("../../db/mongoose");

const categorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  store: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Store",
  },
  active: {
    type: Boolean,
    default: true,
  },
});

categorySchema.virtual("subcategories", {
  ref: "Subcategory",
  localField: "_id",
  foreignField: "category",
});

const Category = storeConn.model("Category", categorySchema);

module.exports = Category;
