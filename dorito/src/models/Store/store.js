const mongoose = require("mongoose");
const { storeConn } = require("../../db/mongoose");

const storeSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true, // TODO needed?
  },
  description: {
    type: String,
    default: "",
  },
  url: {
    type: String,
    default: "https://atitlan.io",
  },
  active: {
    type: Boolean,
    required: true,
    default: true,
  },
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "User",
  },
  town: {
    type: String,
    required: false,
  },
  gmapsLink: {
    type: String,
    required: false,
  },
  openHours: {
    type: Object,
    // default: {Monday:[],Tuesday:[],Wednesday:[],Thursday:[],Friday:[],Saturday:[],Sunday:[]}, //is this the best data structure? TODO if so make sure arrays only ever have 2 vals; maybe add property for days off
    required: false,
  },
  thermalPrinterAutoSSHPort: {
    type: Number,
    unique: true,
  },
  telegram: {
    type: Object,
    default: {
      chatId: "" 
    }
  },
  recurrente_pub_key: {
    type: String,
    default: "",
  },
  btcpay_data: {
    type: Object,
    default: {
      "api_url": "https://btcpay.atitlan.io",
      "storeId": "",
      "checkout": {
        "paymentMethods": ["BTC", "BTC-LightningNetwork"],
        "defaultPaymentMethod": "BTC",
        "expirationMinutes": 5,
        "redirectURL": "https://atitlan.io",
        "defaultLanguage": "en",
      },
      "currency": "GTQ",
      "additionalSearchTerms": [],
    },
    required: false,
  },
  // maybe this is held in a separate document for security
  // recurrente_sec_key: {
  //   type: String,
  //   default: "",
  // },
  // Other things to add, logo, background, colors, btcpay API key (or maybe i'll do these manually), kiosk link?
});

storeSchema.virtual("categories", {
  ref: "Category",
  localField: "_id",
  foreignField: "store",
});

// auto increment the ssh port starting at 3000
// https://stackoverflow.com/a/67319608
// TODO: may need to revisit this
storeSchema.pre("save", function (next) {
  if (this.isNew) {
    this.constructor.find({}).then((result) => {
      this.thermalPrinterAutoSSHPort = 30000 + result.length + 1;
      next();
    });
  }
});

const Store = storeConn.model("Store", storeSchema);
module.exports = Store;
