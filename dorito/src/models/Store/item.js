const mongoose = require("mongoose");
const { storeConn } = require("../../db/mongoose");

const itemSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  store: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Store",
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Category",
  },
  subcategory: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Subcategory",
  },
  description: {
    type: String,
    required: false,
    trim: true,
  },
  price: {
    type: Object,
    default: {
      // type: 2,
      symbol: "Q",
      value: 999.00,
      formatted: "999.00Q",
    },
    required: true,
  },
  // price: {
  //   type: Number,
  //   required: true,
  // },
  unit: {
    type: String, // e.g., "kg", "lb", "oz"
    required: false,
    trim: true,
  },
  // amount in stock
  inventory: {
    type: Object,
    required: true,
    default: 0,
    trim: true,
  },
  image: {
    type: Buffer,
    contentType: String,
  },
  // requiredOptions: {
  //     type: Array,
  //     default: [],
  //     required: false,
  // },
  active: {
    type: Boolean,
    required: true,
    default: true,
  },
  // filters: [String],
  //    addons : [{
  //        type: mongoose.Schema.Types.ObjectId,
  //        ref: "Addon",
  // unique: true
  //    }]
}); //, { toJSON: { virtuals: true } })

// =-=-=-=-=-=-=-=-=-=- this is obsolete since modifying the toJSON function below
// itemSchema.virtual('imgSrc').get(
//     function () {
//         if (this.image) {
//         return '/'+this.store+'/'+this.category+'/'+this.subcategory+'/'+this._id+'/image'
//         } else return null
//     }
// )

itemSchema.methods.toJSON = function () {
  const item = this;
  const itemObject = item.toObject();

  itemObject.postPath = "stores/" + itemObject.store + "/" +
    itemObject.category + "/" + itemObject.subcategory + "/" + itemObject._id;

  if (itemObject.image) {
    itemObject.imgSrc = "/stores/" + itemObject.store + "/" +
      itemObject.category + "/" + itemObject.subcategory + "/" +
      itemObject._id + "/image";
  } else {
    itemObject.imgSrc = "";
  }
  delete itemObject.image;
  return itemObject;
};

itemSchema.pre("save", function (next) {

  console.log(this)
  if (this.price.value){
    this.price.formatted = Number(this.price.value).toFixed(2);

    if (this.price.symbol === "$") {
      this.price.formatted = this.price.symbol + this.price.formatted;
    } else {
      this.price.formatted += this.price.symbol;
    }
  } else {
    this.price.formatted = Number(this.price).toFixed(2) + "Q";
  }


  // if (this.price.symbol === "$") {
  //   this.price.formatted = this.price.symbol + this.price.formatted;
  // } else {
  //   this.price.formatted += this.price.symbol;
  // }

  next();
});

// itemSchema.virtual('addons', {
//     ref: 'Addon',
//     localField: '_id',
//     foreignField: 'item'
// })

const Item = storeConn.model("Item", itemSchema);

module.exports = Item;
