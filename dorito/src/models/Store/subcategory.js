const mongoose = require("mongoose");
const { storeConn } = require("../../db/mongoose");

const subcategorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  store: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Store",
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Category",
  },
  active: {
    type: Boolean,
    default: true,
  },
});

subcategorySchema.virtual("items", {
  ref: "Item",
  localField: "_id",
  foreignField: "subcategory",
});

const Subcategory = storeConn.model("Subcategory", subcategorySchema);

module.exports = Subcategory;
