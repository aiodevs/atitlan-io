const mongoose = require("mongoose");
const { invoiceConn } = require("../db/mongoose");

const schema = {
  // entityId can be restaurantId, storeId, etc
  _id: {
    type: String,
    required: true,
  },
  // entityId is the store/restaurant mongodb _id
  entityId: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: false,
  },
  created_at: {
    type: Date,
    required: false,
  },
  confirmed_at: {
    type: Date,
    required: false,
  },
  amount: {
    type: Number,
    required: true,
  },
  currency: {
    type: String,
    required: true,
  },
  metadata: {
    type: Object,
  },
  // this is only for the Pending invoice, it will be used to send the order to its respective printer,
  // then removed before being added to successful invoice collection
  thermalPrinterAutoSSHPort: {
    type: Number,
    required: false,
  },
};

const invoiceSchema = new mongoose.Schema(schema);
const pendingInvoiceSchema = new mongoose.Schema({
  ...schema,
  entityId: {
    type: mongoose.Schema.Types.Mixed,
    required: true,
  },
  touched_at: {
    type: Date,
    required: false,
  },
});

invoiceSchema.virtual("subTotal").get(
  function () {
    if (this.metadata && this.metadata.tipAmount && this.metadata.fee) {
      return this.amount / (1 + this.fee / 100) - this.tipAmount;
    } else if (this.metadata && this.metadata.tipAmount) {
      return this.amount - this.tipAmount;
    } else if (this.metadata && this.metadata.feeAmount) {
      return this.amount - this.tipAmount;
    } else {
      return this.amount;
    }
  },
);

// ref : https://docs.mongodb.com/manual/crud/
// webhook info : https://docs.btcpayserver.org/API/Greenfield/v1/#tag/Webhooks/paths/InvoiceCreated/post

// Pending or Failed invoices entityId may be a String or an Array
const PendingInvoice = invoiceConn.model(
  "PendingInvoice",
  pendingInvoiceSchema,
);
const FailedInvoice = invoiceConn.model(
  "FailedInvoice",
  pendingInvoiceSchema,
);

// Completed Invoice entityId will be a String because it gets split
const CompletedInvoice = invoiceConn.model("CompletedInvoice", invoiceSchema);

module.exports = { PendingInvoice, CompletedInvoice, FailedInvoice };

// Invoice.prototype.dbInsert = (invoice) => pendingCollection.insertOne(invoice);
//
// Invoice.prototype.dbRemove = (invoiceId) =>
//   pendingCollection.deleteMany({
//     $or: [{ invoiceId: invoiceId }, { id: invoiceId }],
//   });
//
// // returns a Cursor object : http://mongodb.github.io/node-mongodb-native/3.6/api/Cursor.html
// Invoice.prototype.readInvoiceItems = (invoiceId) =>
//   pendingCollection.find({ $or: [{ invoiceId: invoiceId }, { id: invoiceId }] })
//     .toArray();
