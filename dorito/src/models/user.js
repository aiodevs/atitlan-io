const mongoose = require("mongoose");
const { userConn } = require("../db/mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Restaurant = require("./Restaurant/restaurant");
const Store = require("./Store/store");

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: true,
    trim: true,
  },
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true,
    lowercase: true,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error("Email is invalid");
      }
    },
  },
  nostr: {
    type: String,
    unique: true,
    required: false,
    default: "",
    trim: true,
  },
  telegram: {
    type: String,
    unique: true,
    default: "",
    trim: true,
  },
  password: {
    type: String,
    required: true,
    minlength: 6,
    trim: true,
    validate(value) {
      if (value.toLowerCase().includes("password")) {
        throw new Error('Password cannot contain "password"');
      }
    },
  },
  btcpay_auth_token: {
    type: String,
    default: "",
  },
  // maybe recurrenty_sec_key should be in a database that only alllows posting.. ?
  recurrente_pub_key: {
    type: String,
    default: "",
  },
  tokens: [{
    token: {
      type: String,
      required: true,
    },
  }],
});

userSchema.virtual("restaurants", {
  ref: Restaurant,
  localField: "_id",
  foreignField: "owner",
});

userSchema.virtual("stores", {
  ref: Store,
  localField: "_id",
  foreignField: "owner",
});

// remove password and tokens from being sent as a response (not needed)
userSchema.methods.toJSON = function () {
  const user = this;
  const userObject = user.toObject();

  delete userObject.password;
  delete userObject.tokens;

  return userObject;
};

userSchema.methods.generateAuthToken = async function () {
  const user = this;
  const token = jwt.sign({ _id: user._id.toString() }, process.env.SECRET, {
    expiresIn: "7 days",
  });

  user.tokens = user.tokens.concat({ token });
  await user.save();

  return token;
};

userSchema.statics.findByCredentials = async (username, password) => {
  let user = undefined;
  if (username.includes("@")) {
    user = await User.findOne({ email: username.toLowerCase() });
  } else {
    user = await User.findOne({ username: username.toLowerCase() });
  }

  if (!user) {
    throw new Error("Unable to login");
  }

  const isMatch = await bcrypt.compare(password, user.password);

  if (!isMatch) {
    throw new Error("Unable to login");
  }

  return user;
};

// Hash the plain text password before saving
userSchema.pre("save", async function (next) {
  const user = this;

  if (user.isModified("password")) {
    user.password = await bcrypt.hash(user.password, 8);
  }

  next();
});

// This should presumably cascade
// Delete user restaurants when user is removed
userSchema.pre("remove", async function (next) {
  const user = this;
  await Restaurant.deleteMany({ owner: user._id });
  await Store.deleteMany({ owner: user._id });
  next();
});

const User = userConn.model("User", userSchema);

module.exports = User;
