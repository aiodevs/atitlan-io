// const mongoose = require('mongoose')
//
// const itemSchema = new mongoose.Schema({
//     name: {
//         type: String,
//         required: true,
//         trim: true
//     },
//     restaurant: {
//         type: mongoose.Schema.Types.ObjectId,
//         required: true,
//         ref: 'Restaurant'
//     },
//     menu: {
//         type: mongoose.Schema.Types.ObjectId,
//         required: true,
//         ref: 'Menu'
//     },
//     category: {
//         type: mongoose.Schema.Types.ObjectId,
//         required: true,
//         ref: 'Category'
//     },
//     subcategory: {
//         type: mongoose.Schema.Types.ObjectId,
//         required: true,
//         ref: 'Subcategory'
//     },
//     description: {
//         type: String,
//         required: false,
//         trim: true
//     },
//     price: {
//         type: Number,
//         required: true,
//         trim: true
//     },
//     unit: {
//         type: String,   // e.g., "kg", "lb", "oz"
//         required: false,
//         trim: true
//     },
//     // amount in stock
//     inventory: {
//         type: Number,
//         required: false,
//         trim: true
//     },
//     image: {
//         type: Buffer,
//         contentType: String
//     },
//     requiredOptions: {
//         type: Array,
//         default: [],
//         required: false,
//     },
//     active: {
//         type: Boolean,
//         required: true,
//         default: true
//     },
//     filters: [String],
//     addons : [{
//         type: mongoose.Schema.Types.ObjectId,
//         ref: "Addon",
// 	unique: true
//     }]
// })//, { toJSON: { virtuals: true } })
//
// // =-=-=-=-=-=-=-=-=-=- this is obsolete since modifying the toJSON function below
// // itemSchema.virtual('imgSrc').get(
// //     function () {
// //         if (this.image) {
// //         return '/'+this.restaurant+'/'+this.menu+'/'+this.category+'/'+this.subcategory+'/'+this._id+'/image'
// //         } else return null
// //     }
// // )
//
// itemSchema.methods.toJSON = function () {
//     const item = this
//     const itemObject = item.toObject()
//
//     if (itemObject.image) {
//         itemObject.imgSrc = itemObject.restaurant+'/'+itemObject.menu+'/'+itemObject.category+'/'+itemObject.subcategory+'/'+itemObject._id+'/image'
//     } else {
//         itemObject.imgSrc = ''
//     }
//     delete itemObject.image
//     return itemObject
// }
//
// // itemSchema.virtual('addons', {
// //     ref: 'Addon',
// //     localField: '_id',
// //     foreignField: 'item'
// // })
//
// const Item = mongoose.model('Item', itemSchema)
//
// module.exports = Item
