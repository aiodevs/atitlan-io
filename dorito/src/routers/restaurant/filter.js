const express = require("express");
const Filter = require("../../models/Restaurant/filter");
// const Item = require('../models/Restaurant/item')
// const Menu = require('../models/Restaurant/menu')
// const { default: mongoose } = require('mongoose')
const router = new express.Router();

// TODO can we generalize this to work for menus, categories and subcategories?

router.get("/restaurants/filters", async (req, res) => {
  try {
    const filters = await Filter.find({});

    res.send(filters);
  } catch (e) {
    res.status(500).send(e);
  }
});

module.exports = router;
