const express = require("express");
const Menu = require("../../models/Restaurant/menu");
const Restaurant = require("../../models/Restaurant/restaurant");
const auth = require("../../middleware/auth");
// const { default: mongoose } = require('mongoose')
const router = new express.Router();

// TODO can we generalize this to work for menus, categories and subcategories?

router.get("/restaurants/:restaurantId/menus", auth, async (req, res) => {
  try {
    const restaurant = await Restaurant.findOne({
      _id: req.params.restaurantId,
      owner: req.user._id,
    });
    await restaurant.populate("menus");
    res.send(restaurant.menus);
  } catch (e) {
    res.status(500).send();
  }
});

router.post("/restaurants/:restaurantId/menus", auth, async (req, res) => {
  const menu = new Menu({
    ...req.body,
    restaurant: req.params.restaurantId,
  });

  try {
    await menu.save();
    res.status(201).send(menu);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.get("/restaurants/:restaurantId/:menuId", async (req, res) => {
  const restaurantId = req.params.restaurantId;
  const menuId = req.params.menuId;

  try {
    if (menuId === "items") {
      const items = {};
      const restaurant = await Restaurant.findOne({ _id: restaurantId });
      await restaurant.populate("menus");
      for (let menu of restaurant.menus) {
        await menu.populate("categories");
        const tempObj1 = {};
        for (let category of menu.categories) {
          await category.populate("subcategories");
          const tempObj2 = {};
          for (let subcategory of category.subcategories) {
            await subcategory.populate("items");
            subcategory.items.push({
              name: "Add Item",
              active: true,
              postPath: menu.restaurant + "/" + category.menu + "/" +
                subcategory.category + "/" + subcategory._id + "/items",
              imgSrc: "src/assets/plus.svg",
            });
            tempObj2[subcategory.name] = subcategory.items;
          }
          tempObj1[category.name] = tempObj2;
        }
        items[menu.name] = tempObj1;
      }

      res.send(items);
    } else {
      const menu = await Menu.findOne({
        _id: menuId,
        restaurant: restaurantId,
      });

      if (!menu) {
        return res.status(404).send();
      }

      res.send(menu);
    }
  } catch (e) {
    res.status(500).send();
  }
});

router.patch("/restaurants/:restaurantId/:menuId", auth, async (req, res) => {
  const restaurantId = req.params.restaurantId;
  const menuId = req.params.menuId;
  const updates = Object.keys(req.body);
  const allowedUpdates = ["name", "active"];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    return res.status(400).send({ error: "Invalid updates!" });
  }

  try {
    const menu = await Menu.findOne({ _id: menuId, restaurant: restaurantId });

    if (!menu) {
      return res.status(404).send();
    }

    updates.forEach((update) => menu[update] = req.body[update]);
    await menu.save();
    res.send(menu);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.delete("/restaurants/:restaurantId/:menuId", auth, async (req, res) => {
  const restaurantId = req.params.restaurantId;
  const menuId = req.params.menuId;

  try {
    const menu = await Menu.findOneAndDelete({
      _id: menuId,
      restaurant: restaurantId,
    });

    if (!menu) {
      res.status(404).send();
    }

    res.send(menu);
  } catch (e) {
    res.status(500).send();
  }
});

module.exports = router;
