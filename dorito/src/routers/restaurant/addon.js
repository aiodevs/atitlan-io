const express = require("express");
const Addon = require("../../models/Restaurant/addon");
// const auth = require('../middleware/auth')
const router = new express.Router();

// TODO can we generalize this to work for menus, categories and subcategories?

router.get("/restaurants/:restaurantId/addons", async (req, res) => {
  const restaurantId = req.params.restaurantId;
  try {
    const addons = await Addon.find({ restaurant: restaurantId });

    res.send(addons);
  } catch (e) {
    res.status(500).send(e);
  }
});

// TODO add auth here
// router.post('/addons', auth, async (req, res) => {
router.post("/restaurants/:restaurantId/addons", async (req, res) => {
  const addon = new Addon({
    ...req.body,
    restaurant: req.params.restaurantId,
  });

  try {
    await addon.save();
    res.status(201).send(addon);
  } catch (e) {
    console.log(e);
    res.status(400).send(e);
  }
});

// router.patch('/addons/:addonId', auth, async (req, res) => {
router.patch("/restaurants/:restaurantId/addons/:addonId", async (req, res) => {
  const addonId = req.params.addonId;
  const updates = Object.keys(req.body);
  const allowedUpdates = ["active", "name", "description", "price", "filters"];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    return res.status(400).send({ error: "Invalid updates!" });
  }

  try {
    const addon = await Addon.findOne({ _id: addonId });
    console.log(addonId);

    if (!addon) {
      return res.status(404).send();
    }

    updates.forEach((update) => addon[update] = req.body[update]);
    await addon.save();
    res.send(addon);
  } catch (e) {
    console.log("*********", e);
    res.status(400).send(e);
  }
});

router.delete(
  "/restaurants/:restaurantId/addons/:addonId",
  async (req, res) => {
    const addonId = req.params.addonId;

    try {
      const addon = await Addon.findOneAndDelete({ _id: addonId });

      if (!addon) {
        res.status(404).send();
      }

      res.send(addon);
    } catch (e) {
      res.status(500).send();
    }
  },
);

module.exports = router;
