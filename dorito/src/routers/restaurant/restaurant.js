const express = require("express");
const Restaurant = require("../../models/Restaurant/restaurant");
const Menu = require("../../models/Restaurant/menu");
const auth = require("../../middleware/auth");
// const { default: mongoose } = require('mongoose')
const router = new express.Router();

// TODO can we generalize this to work for menus, categories and subcategories?

router.post("/restaurants", auth, async (req, res) => {
  const restaurant = new Restaurant({
    ...req.body,
    owner: req.user._id,
  });

  try {
    await restaurant.save();
    res.status(201).send(restaurant);
  } catch (e) {
    console.error(e);
    res.status(400).send(e);
  }
});

router.get("/restaurants", auth, async (req, res) => {
  try {
    await req.user.populate("restaurants");
    res.send(req.user.restaurants);
  } catch (e) {
    res.status(500).send();
  }
});

router.get("/restaurants/:restaurantId/isOpen", async (req, res) => {
  const restaurantId = req.params.restaurantId
  // https://www.kindacode.com/article/2-ways-to-set-default-time-zone-in-node-js/
  process.env.TZ = "America/Guatemala"; // TODO is this the best place to put this? refer to above link
  const currentHour = Number(Date().split(" ")[4].split(":")[0]);
  const currentMinute = Number(Date().split(" ")[4].split(":")[1]);
  // let currentDay = Date().split(' ')[0]
  // console.log(currentHour, currentDay)
  // if (currentHour < 8 || currentHour >= 21 || currentDay === 'Sun') {
  //     res.send({ status: false, currentHour: currentHour, currentMinute: currentMinute, currentDay: currentDay})
  // } else {
  //     res.send({ status: true})
  // }
  const day = new Date().toLocaleString(
    "default",
    { weekday: "long" },
  );

  const restaurant = await Restaurant.findById(restaurantId);
  if (!restaurant) {
    res.status(500).send("error")
    return null
  }
  const todayHours = restaurant.openHours[day];

  if (todayHours[0].length === 0) {
    res.send("false");
  } else {
    for (const hourWindow of todayHours) {
      const openHour = Number(hourWindow[0].split(":")[0]);
      const openMinute = Number(hourWindow[0].split(":")[1]);
      const closeHour = Number(hourWindow[1].split(":")[0]);
      const closeMinute = Number(hourWindow[1].split(":")[1]);

      if (currentHour >= openHour && currentHour <= closeHour) {
        if (currentHour === openHour && currentMinute < openMinute) {
          res.send("false");
        } else if (
          currentHour === closeHour && currentMinute >= closeMinute
          ) {
          res.send("false");
        } else {
          res.send("true");
        }
      } else {
        res.send("false")
      }
    }
  }
});

router.get("/restaurants/:restaurantId/getAllActive", async (req, res) => {
  const restaurantId = req.params.restaurantId;

  // maybe can somehow include the auth middleware and if it doesn't authorize, you don't get to see inactive items
  // this catches a route for both .../items and .../itemsActive
  //

  try {
    const restaurant = await Restaurant.findOne({ _id: restaurantId });
    // console.log(restaurant);
    const items = {}; // items means everything here, menus, categories, subcategories and products
    // get active menus
    await restaurant.populate("menus");
    const menus = restaurant.menus.filter((
      item,
    ) => item.active === true);

    let categories = undefined;

    for (const menu of menus) { // ############### menus ###############
      await menu.populate({
        path: "categories",
        options: { sort: { position: "asc" } },
      });
      categories = menu.categories.filter((item) => item.active === true);

      let subcategories = undefined;

      // tempObj to hold all the category subcategories with their items
      const tempObj = {};

      for (const category of categories) { // ############### categories ###############
        const tempObj2 = {};
        await category.populate({
          path: "subcategories",
          options: { sort: { position: "asc" } },
        });

        subcategories = category.subcategories.filter((item) =>
          item.active === true
        );

        for (const subcategory of subcategories) { // ############### subcategories ###############
          await subcategory.populate({
            path: "items",
            options: { sort: { position: "asc" } },
          });
          const items = subcategory.items.filter((item) =>
            item.active === true
          );
          for (const item of items) { // ############### items ###############
            await item.populate("addons");
          }
          tempObj2[subcategory.name] = items;
        }
        tempObj[category.name] = tempObj2;
      }
      items[menu.name] = tempObj;
    }
    res.send(items);
  } catch (e) {
    console.error(e);
    res.status(500).send();
  }
});

router.get("/restaurants/:restaurantId/menus", auth, async (req, res) => {
  try {
    const restaurant = await Restaurant.findOne({
      _id: req.params.restaurantId,
    });
    await restaurant.populate("menus");
    res.send(restaurant.menus);
  } catch (e) {
    res.status(500).send();
  }
});

router.post("/restaurants/:restaurantId/menus", auth, async (req, res) => {
  const menu = new Menu({
    ...req.body,
    restaurant: req.params.restaurantId,
  });

  try {
    await menu.save();
    res.status(201).send(menu);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.get("/restaurants/:restaurantId", async (req, res) => {
  try {
    const restaurant = await Restaurant.findOne({
      _id: req.params.restaurantId,
    });

    if (!restaurant) {
      return res.status(404).send();
    }

    res.send(restaurant);
  } catch (e) {
    res.status(500).send();
  }
});

router.patch("/restaurants/:restaurandtId", auth, async (req, res) => {
  const updates = Object.keys(req.body);
  const allowedUpdates = [
    "name",
    "town",
    "gmapsLink",
    "openHours",
    "active",
  ];

  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    return res.status(400).send({ error: "Invalid updates!" });
  }

  try {
    const restaurant = await Restaurant.findOne({
      _id: req.params.restaurantId,
    });

    if (!restaurant) {
      return res.status(404).send();
    }

    updates.forEach((update) => {
      restaurant[update] = req.body[update];
    });
    await restaurant.save();
    res.send(restaurant);
  } catch (e) {
    res.status(400).send(e);
  }
});

// Upload images
const multer = require("multer");
const uploadImg = multer({
  limits: {
    fileSize: 2 * 1e6, // max file size of ~ 2mb
  },
});
const sharp = require("sharp");

router.post(
  "/restaurants/:restaurantId/small_logo",
  uploadImg.single("small_logo"), // this corresponds to the input name in the HTML e.g., <input type="file" name="small_logo">
  async (req, res) => {
    try {
      const restaurant = await Restaurant.findById(req.params.restaurantId);
      const buffer = await sharp(req.file.buffer).resize({
        width: 256,
        height: 256,
      }).webp().toBuffer();
      restaurant.small_logo = buffer;

      await restaurant.save();

      res.send(restaurant.small_logo);
    } catch (e) {
      console.error(e)
      res.send(e);
    }
  },
);

router.post(
  "/restaurants/:restaurantId/telegram",
  async (req, res) => {
    try {
      const restaurant = await Restaurant.findById(req.params.restaurantId);

      if (!restaurant) {
        throw new Error(
          "This restaurant doesn't exist",
        );
      }
      restaurant.telegram = req.body.telegram;

      await restaurant.save();

      res.send(restaurant.telegram);
    } catch (e) {
      console.error(e);
      res.status(500).send();
    }
  },
);

router.get(
  "/restaurants/:restaurantId/telegram",
  async (req, res) => {
    try {
      const restaurant = await Restaurant.findById(req.params.restaurantId);

      if (!restaurant || !restaurant.telegram) {
        throw new Error(
          "Either the user does not exist, or it does not any css uploaded",
        );
      }

      res.send(restaurant.telegram);
    } catch (e) {
      console.error(e);
      res.status(404).send();
    }
  },
);

router.get("/restaurants/:restaurantId/small_logo", async (req, res) => {
  try {
    const restaurant = await Restaurant.findById(req.params.restaurantId);

    if (!restaurant || !restaurant.small_logo) {
      throw new Error(
        "Either the user does not exist, or it does not have an image",
      );
    }

    // note default Content-Type is 'application/json'
    res.set("Content-Type", "image/webp");
    res.setHeader("content-type", "image/webp");
    res.send(restaurant.small_logo);
  } catch (e) {
    console.error(e);
    res.status(404).send();
  }
});

router.delete(
  "/restaurants/:restaurantId/small_logo",
  auth,
  async (req, res) => {
    const restaurant = Restaurant.findById(req.params.restaurantId);
    restaurant.small_logo = undefined;
    await req.user.save();

    res.send();
  },
);

router.post(
  "/restaurants/:restaurantId/banner_logo",
  uploadImg.single("banner_logo"), // this corresponds to the input name in the HTML e.g., <input type="file" name="banner_logo">
  async (req, res) => {
    try {
      const restaurant = await Restaurant.findById(req.params.restaurantId);
      const buffer = await sharp(req.file.buffer).resize({
        width: 1024,
        height: 256,
      }).webp().toBuffer();
      restaurant.banner_logo = buffer;

      await restaurant.save();

      res.send(restaurant.banner_logo);
    } catch (e) {
      res.send(e);
    }
  },
);

router.get("/restaurants/:restaurantId/banner_logo", async (req, res) => {
  try {
    const restaurant = await Restaurant.findById(req.params.restaurantId);

    if (!restaurant || !restaurant.banner_logo) {
      throw new Error(
        "Either the user does not exist, or it does not have an image",
      );
    }

    // note default Content-Type is 'application/json'
    res.set("Content-Type", "image/webp");
    res.setHeader("content-type", "image/webp");
    res.send(restaurant.banner_logo);
  } catch (e) {
    console.error(e);
    res.status(404).send();
  }
});

router.delete(
  "/restaurants/:restaurantId/banner_logo",
  auth,
  async (req, res) => {
    const restaurant = Restaurant.findById(req.params.restaurantId);
    restaurant.banner_logo = undefined;
    await req.user.save();

    res.send();
  },
);

router.post(
  "/restaurants/:restaurantId/background_image",
  uploadImg.single("background_image"), // this corresponds to the input name in the HTML e.g., <input type="file" name="background_image">
  async (req, res) => {
    try {
      const restaurant = await Restaurant.findById(req.params.restaurantId);
      const buffer = await sharp(req.file.buffer).resize({
        width: 512,
        height: 512,
      }).webp().toBuffer();
      restaurant.background_image = buffer;

      await restaurant.save();

      res.send(restaurant.background_image);
    } catch (e) {
      res.send(e);
    }
  },
);

router.get("/restaurants/:restaurantId/background_image", async (req, res) => {
  try {
    const restaurant = await Restaurant.findById(req.params.restaurantId);

    if (!restaurant || !restaurant.background_image) {
      throw new Error(
        "Either the user does not exist, or it does not have an image",
      );
    }

    // note default Content-Type is 'application/json'
    res.set("Content-Type", "image/webp");
    res.setHeader("content-type", "image/webp");
    res.send(restaurant.background_image);
  } catch (e) {
    console.error(e);
    res.status(404).send();
  }
});

router.delete(
  "/restaurants/:restaurantId/background_image",
  auth,
  async (req, res) => {
    const restaurant = Restaurant.findById(req.params.restaurantId);
    restaurant.background_image = undefined;
    await req.user.save();

    res.send();
  },
);

router.post(
  "/restaurants/:restaurantId/css",
  async (req, res) => {
    try {
      const restaurant = await Restaurant.findById(req.params.restaurantId);

      if (!restaurant) {
        throw new Error(
          "This restaurant doesn't exist",
        );
      }
      restaurant.css = req.body.css;

      await restaurant.save();

      res.send(restaurant.css);
    } catch (e) {
      console.error(e);
      res.status(500).send();
    }
  },
);

router.get("/restaurants/:restaurantId/css", async (req, res) => {
  try {
    const restaurant = await Restaurant.findById(req.params.restaurantId);

    if (!restaurant || !restaurant.css) {
      throw new Error(
        "Either the user does not exist, or it does not any css uploaded",
      );
    }

    res.send(restaurant.css);
  } catch (e) {
    console.error(e);
    res.status(404).send();
  }
});

router.delete(
  "/restaurants/:restaurantId/css",
  auth,
  async (req, res) => {
    const restaurant = Restaurant.findById(req.params.restaurantId);
    restaurant.css = "";
    await req.user.save();

    res.send();
  },
);

module.exports = router;
