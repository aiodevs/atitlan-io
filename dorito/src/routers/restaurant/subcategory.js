const express = require("express");
const Category = require("../../models/Restaurant/category");
const Subcategory = require("../../models/Restaurant/subcategory");
const auth = require("../../middleware/auth");
// const { default: mongoose } = require('mongoose')
const router = new express.Router();

// TODO can we generalize this to work for menus, categories and subcategories?

router.get(
  "/restaurants/:restaurantId/:menuId/:categoryId/subcategories",
  auth,
  async (req, res) => {
    try {
      const category = await Category.findOne({
        _id: req.params.categoryId,
        menu: req.params.menuId,
      });
      await category.populate("subcategories");
      res.send(category.subcategories);
    } catch (e) {
      res.status(500).send();
    }
  },
);

router.post(
  "/restaurants/:restaurantId/:menuId/:categoryId/subcategories",
  auth,
  async (req, res) => {
    const subcategory = new Subcategory({
      ...req.body,
      restaurant: req.params.restaurantId,
      menu: req.params.menuId,
      category: req.params.categoryId,
    });

    try {
      await subcategory.save();
      res.status(201).send(subcategory);
    } catch (e) {
      res.status(400).send(e);
    }
  },
);

router.get(
  "/restaurants/:restaurantId/:menuId/:categoryId/:subcategoryId",
  async (req, res) => {
    const restaurantId = req.params.restaurantId;
    const menuId = req.params.menuId;
    const categoryId = req.params.categoryId;
    const subcategoryId = req.params.subcategoryId;

    try {
      if (subcategoryId === "items") {
        const items = {}; //[]
        const category = await Category.findOne({
          _id: categoryId,
          menu: menuId,
        });
        await category.populate("subcategories");
        for (let subcategory of category.subcategories) {
          await subcategory.populate("items");
          subcategory.items.push({
            name: "Add Item",
            active: true,
            postPath: restaurantId + "/" + menuId + "/" + categoryId + "/" +
              subcategory._id + "/items",
            imgSrc: "src/assets/plus.svg",
          });
          // let tempObj = {}
          items[subcategory.name] = subcategory.items;
        }

        res.send(items);
      } else {
        const subcategory = await Subcategory.findOne({
          _id: subcategoryId,
          category: categoryId,
        });

        if (!subcategory) {
          return res.status(404).send();
        }

        res.send(subcategory);
      }
    } catch (e) {
      res.status(500).send();
    }
  },
);

router.patch(
  "/restaurants/:restaurantId/:menuId/:categoryId/:subcategoryId",
  auth,
  async (req, res) => {
    const categoryId = req.params.categoryId;
    const subcategoryId = req.params.subcategoryId;
    const updates = Object.keys(req.body);
    const allowedUpdates = ["name", "active", "position"];
    const isValidOperation = updates.every((update) =>
      allowedUpdates.includes(update)
    );

    if (!isValidOperation) {
      return res.status(400).send({ error: "Invalid updates!" });
    }

    try {
      const subcategory = await Subcategory.findOne({
        _id: subcategoryId,
        category: categoryId,
      });

      if (!subcategory) {
        return res.status(404).send();
      }

      updates.forEach((update) => subcategory[update] = req.body[update]);
      await subcategory.save();
      res.send(subcategory);
    } catch (e) {
      res.status(400).send(e);
    }
  },
);

router.delete(
  "/restaurants/:restaurantId/:menuId/:categoryId/:subcategoryId",
  auth,
  async (req, res) => {
    const categoryId = req.params.categoryId;
    const subcategoryId = req.params.subcategoryId;

    try {
      const subcategory = await Subcategory.findOneAndDelete({
        _id: subcategoryId,
        category: categoryId,
      });

      if (!subcategory) {
        res.status(404).send();
      }

      res.send(subcategory);
    } catch (e) {
      res.status(500).send();
    }
  },
);

module.exports = router;
