const express = require("express");
const Menu = require("../../models/Restaurant/menu");
const Category = require("../../models/Restaurant/category");
const auth = require("../../middleware/auth");
// const { default: mongoose } = require('mongoose')
const router = new express.Router();

// TODO can we generalize this to work for menus, categories and subcategories?

router.get(
  "/restaurants/:restaurantId/:menuId/categories",
  auth,
  async (req, res) => {
    try {
      const menu = await Menu.findOne({
        _id: req.params.menuId,
        restaurant: req.params.restaurantId,
      });
      await menu.populate("categories");
      res.send(menu.categories);
    } catch (e) {
      res.status(500).send();
    }
  },
);

router.post(
  "/restaurants/:restaurantId/:menuId/categories",
  auth,
  async (req, res) => {
    const category = new Category({
      ...req.body,
      restaurant: req.params.restaurantId,
      menu: req.params.menuId,
    });

    try {
      await category.save();
      res.status(201).send(category);
    } catch (e) {
      console.error(e);
      res.status(400).send();
    }
  },
);

router.get(
  "/restaurants/:restaurantId/:menuId/:categoryId",
  async (req, res) => {
    const restaurantId = req.params.restaurantId;
    const menuId = req.params.menuId;
    const categoryId = req.params.categoryId;

    try {
      // maybe can somehow include the auth middleware and if it doesn't authorize, you don't get to see inactive items
      // this catches a route for both .../items and .../itemsActive
      if (categoryId.startsWith("items")) {
        const items = {};
        const menu = await Menu.findOne({
          _id: menuId,
          restaurant: restaurantId,
        });
        await menu.populate("categories");
        let categories = undefined;
        if (categoryId === "itemsActive") {
          categories = menu.categories.filter((item) => item.active === true);
        } else {
          categories = menu.categories;
        }
        for (const category of categories) {
          await category.populate("subcategories");
          const tempObj = {};
          let subcategories = undefined;
          if (categoryId === "itemsActive") {
            subcategories = category.subcategories.filter((item) =>
              item.active === true
            );
          } else {
            subcategories = category.subcategories;
          }
          for (const subcategory of subcategories) {
            await subcategory.populate("items");
            if (categoryId === "items") {
              subcategory.items.push({
                name: "Add Item",
                active: true,
                postPath: restaurantId + "/" + menuId + "/" +
                  subcategory.category + "/" + subcategory._id + "/items",
                imgSrc: "src/assets/plus.svg",
              });
              tempObj[subcategory.name] = subcategory.items;
            } else {
              //    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ACTIVE ITEMS THAT ARE SENT TO MENU APP -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
              const items = subcategory.items.filter((item) =>
                item.active === true
              );
              for (const item of items) {
                await item.populate("addons");
              }
              tempObj[subcategory.name] = items;
              //    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ACTIVE ITEMS THAT ARE SENT TO MENU APP -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
            }
          }
          items[category.name] = tempObj;
        }

        res.send(items);
      } else {
        const category = await Category.findOne({
          _id: categoryId,
          menu: menuId,
        });

        if (!category) {
          return res.status(404).send();
        }

        res.send(category);
      }
    } catch (e) {
      res.status(500).send();
    }
  },
);

router.patch(
  "/restaurants/:restaurantId/:menuId/:categoryId",
  auth,
  async (req, res) => {
    const menuId = req.params.menuId;
    const categoryId = req.params.categoryId;
    const updates = Object.keys(req.body);
    const allowedUpdates = ["name", "active", "position"];
    const isValidOperation = updates.every((update) =>
      allowedUpdates.includes(update)
    );

    if (!isValidOperation) {
      return res.status(400).send({ error: "Invalid updates!" });
    }

    try {
      const category = await Category.findOne({
        _id: categoryId,
        menu: menuId,
      });

      if (!category) {
        return res.status(404).send();
      }

      updates.forEach((update) => category[update] = req.body[update]);
      await category.save();
      res.send(category);
    } catch (e) {
      res.status(400).send(e);
    }
  },
);

router.delete(
  "/restaurants/:restaurantId/:menuId/:categoryId",
  auth,
  async (req, res) => {
    const menuId = req.params.menuId;
    const categoryId = req.params.categoryId;

    try {
      const category = await Category.findOneAndDelete({
        _id: categoryId,
        menu: menuId,
      });

      if (!category) {
        res.status(404).send();
      }

      res.send(category);
    } catch (e) {
      res.status(500).send();
    }
  },
);

module.exports = router;
