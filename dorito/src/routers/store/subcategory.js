const express = require("express");
const Category = require("../../models/Store/category");
const Subcategory = require("../../models/Store/subcategory");
const auth = require("../../middleware/auth");
// const { default: mongoose } = require('mongoose')
const router = new express.Router();

// TODO can we generalize this to work for menus, categories and subcategories?

router.get(
  "/stores/:storeId/:categoryId/subcategories",
  auth,
  async (req, res) => {
    const storeId = req.params.storeId;
    const categoryId = req.params.categoryId;
    try {
      const category = await Category.findOne({
        _id: categoryId,
        restaurant: req.params.storeId,
      });
      await category.populate("subcategories");
      res.send(category.subcategories);
    } catch (e) {
      res.status(500).send();
    }
  },
);

router.post(
  "/stores/:storeId/:categoryId/subcategories",
  auth,
  async (req, res) => {
    const subcategory = new Subcategory({
      ...req.body,
      restaurant: req.params.storeId,
      category: req.params.categoryId,
    });

    try {
      await subcategory.save();
      res.status(201).send(subcategory);
    } catch (e) {
      res.status(400).send(e);
    }
  },
);

router.get("/stores/:storeId/:categoryId/:subcategoryId", async (req, res) => {
  const storeId = req.params.storeId;
  const categoryId = req.params.categoryId;
  const subcategoryId = req.params.subcategoryId;

  try {
    if (subcategoryId === "items") {
      const items = {}; //[]
      const category = await Category.findOne({
        _id: categoryId,
        restaurant: restaurant,
      });
      await category.populate("subcategories");
      for (let subcategory of category.subcategories) {
        await subcategory.populate("items");
        subcategory.items.push({
          name: "Add Item",
          active: true,
          postPath: storeId + "/" + categoryId + "/" + subcategory._id +
            "/items",
          imgSrc: "src/assets/plus.svg",
        });
        // let tempObj = {}
        items[subcategory.name] = subcategory.items;
      }

      res.send(items);
    } else {
      const subcategory = await Subcategory.findOne({
        _id: subcategoryId,
        category: categoryId,
      });

      if (!subcategory) {
        return res.status(404).send();
      }

      res.send(subcategory);
    }
  } catch (e) {
    res.status(500).send();
  }
});

router.patch(
  "/stores/:storeId/:categoryId/:subcategoryId",
  auth,
  async (req, res) => {
    const categoryId = req.params.categoryId;
    const subcategoryId = req.params.subcategoryId;
    const updates = Object.keys(req.body);
    const allowedUpdates = ["name", "active"];
    const isValidOperation = updates.every((update) =>
      allowedUpdates.includes(update)
    );

    if (!isValidOperation) {
      return res.status(400).send({ error: "Invalid updates!" });
    }

    try {
      const subcategory = await Subcategory.findOne({
        _id: subcategoryId,
        category: categoryId,
      });

      if (!subcategory) {
        return res.status(404).send();
      }

      updates.forEach((update) => subcategory[update] = req.body[update]);
      await subcategory.save();
      res.send(subcategory);
    } catch (e) {
      res.status(400).send(e);
    }
  },
);

router.delete(
  "/stores/:storeId/:categoryId/:subcategoryId",
  auth,
  async (req, res) => {
    const categoryId = req.params.categoryId;
    const subcategoryId = req.params.subcategoryId;

    try {
      const subcategory = await Subcategory.findOneAndDelete({
        _id: subcategoryId,
        category: categoryId,
      });

      if (!subcategory) {
        res.status(404).send();
      }

      res.send(subcategory);
    } catch (e) {
      res.status(500).send();
    }
  },
);

module.exports = router;
