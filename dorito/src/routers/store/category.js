const express = require("express");
const Store = require("../../models/Store/store");
const Category = require("../../models/Store/category");
const auth = require("../../middleware/auth");
// const { default: mongoose } = require('mongoose')
const router = new express.Router();

// TODO can we generalize this to work for menus, categories and subcategories?

router.get("/stores/:storeId/categories", auth, async (req, res) => {
  const storeId = req.params.storeId;
  try {
    const store = await Store.findOne({ _id: req.params.storeId });
    await store.populate("categories");
    res.send(store.categories);
  } catch (e) {
    res.status(500).send();
  }
});

router.post("/stores/:storeId/categories", auth, async (req, res) => {
  const category = new Category({
    ...req.body,
    store: req.params.storeId,
  });

  try {
    await category.save();
    res.status(201).send(category);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.get("/stores/:storeId/:categoryId", async (req, res) => {
  const storeId = req.params.storeId;
  const categoryId = req.params.categoryId;

  try {
    // maybe can somehow include the auth middleware and if it doesn't authorize, you don't get to see inactive items
    if (categoryId.startsWith("items")) {
      const items = {};
      const store = await Store.findOne({ _id: storeId });
      await store.populate("categories");
      for (let category of store.categories) {
        await category.populate("subcategories");
        const tempObj = {};
        for (let subcategory of category.subcategories) {
          await subcategory.populate("items");
          if (categoryId === "items") {
            subcategory.items.push({
              name: "Add Item",
              active: true,
              postPath: storeId + "/" + subcategory.category + "/" +
                subcategory._id + "/items",
              imgSrc: "src/assets/plus.svg",
            });
            tempObj[subcategory.name] = subcategory.items;
          } else {
            //    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ACTIVE ITEMS THAT ARE SENT TO STORE APP -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
            const items = subcategory.items.filter((item) =>
              item.active === true
            );
            for (const item of items) {
              await item.populate("addons");
            }
            tempObj[subcategory.name] = items;
            //    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ACTIVE ITEMS THAT ARE SENT TO STORE APP -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
          }
        }
        items[category.name] = tempObj;
      }

      res.send(items);
    } else {
      const category = await Category.findOne({
        _id: categoryId,
        store: storeId,
      });

      if (!category) {
        return res.status(404).send();
      }

      res.send(category);
    }
  } catch (e) {
    res.status(500).send();
  }
});

router.patch("/stores/:storeId/:categoryId", auth, async (req, res) => {
  const categoryId = req.params.categoryId;
  const updates = Object.keys(req.body);
  const allowedUpdates = ["name", "active"];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    return res.status(400).send({ error: "Invalid updates!" });
  }

  try {
    const category = await Category.findOne({
      _id: categoryId,
      store: storeId,
    });

    if (!category) {
      return res.status(404).send();
    }

    updates.forEach((update) => category[update] = req.body[update]);
    await category.save();
    res.send(category);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.delete("/stores/:storeId/:categoryId", auth, async (req, res) => {
  const categoryId = req.params.categoryId;

  try {
    const category = await Category.findOneAndDelete({
      _id: categoryId,
      store: storeId,
    });

    if (!category) {
      res.status(404).send();
    }

    res.send(category);
  } catch (e) {
    res.status(500).send();
  }
});

module.exports = router;
