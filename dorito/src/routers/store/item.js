const express = require("express");
// const Store = require('../models/Store/store')
const Category = require("../../models/Store/category");
const Subcategory = require("../../models/Store/subcategory");
const Item = require("../../models/Store/item");
const auth = require("../../middleware/auth");
// const { default: mongoose } = require('mongoose')
const router = new express.Router();

// TODO can we generalize this to work for categories and subcategories?

router.get("/stores/:storeId/:categoryId/items", auth, async (req, res) => {
  const categoryId = req.params.categoryId;
  const storeId = req.params.storeId;
  try {
    const items = [];
    const category = await Category.findOne({
      _id: categoryId,
      store: storeId,
    });
    await category.populate("subcategories");
    for (let subcategory of category.subcategories) {
      await subcategory.populate("items");
      let tempObj = {};
      tempObj[subcategory.name] = subcategory.items;
      items.push(tempObj);
    }
    res.send(items);
  } catch (e) {
    res.status(500).send(e);
  }
});

router.get(
  "/stores/:storeId/:categoryId/:subcategoryId/items",
  async (req, res) => {
    try {
      const subcategory = await Subcategory.findOne({
        _id: req.params.subcategoryId,
        category: req.params.categoryId,
      });
      await subcategory.populate("items");
      for (let item of subcategory.items) {
        await item.populate("addons");
        console.log(item);
      }
      subcategory.items.push({
        name: "Add Item",
        active: true,
        postPath: req.params.storeId + "/" + req.params.categoryId + "/" +
          subcategory._id + "/items",
        imgSrc: "src/assets/plus.svg",
      });
      res.send(subcategory.items);
    } catch (e) {
      res.status(500).send();
    }
  },
);

router.post(
  "/stores/:storeId/:categoryId/:subcategoryId/items",
  auth,
  async (req, res) => {
    console.log("HERE?")
    const item = new Item({
      ...req.body,
      store: req.params.storeId,
      category: req.params.categoryId,
      subcategory: req.params.subcategoryId,
    });

    try {
      await item.save();
      res.status(201).send(item);
    } catch (e) {
      console.error(e)
      res.status(400).send(e);
    }
  },
);

router.get(
  "/stores/:storeId/:categoryId/:subcategoryId/:itemId",
  async (req, res) => {
    // const subcategoryId = req.params.subcategoryId
    const itemId = req.params.itemId;

    try {
      const item = await Item.findOne({ _id: itemId });

      if (!item) {
        return res.status(404).send();
      }

      await item.populate("addons");

      res.send(item);
    } catch (e) {
      res.status(500).send();
    }
  },
);

const multer = require("multer");
const uploadImg = multer({
  limits: {
    fileSize: 5 * 1e6, // max file size of ~ 5mb
  },
  // fileFilter(req, file, cb) {
  //     console.log(file)
  //     if (!file.originalname.endsWith('.webp')) {
  //         return cb(new Error('Image must be a .webp format'))
  //     }
  //     cb(null,true)
  // }
});

const sharp = require("sharp");

router.post(
"/stores/:storeId/:categoryId/:subcategoryId/:itemId/image",
  uploadImg.single("itemImage"), // itemImage is the key in the request
  async (req, res) => {
    const item = await Item.findById(req.params.itemId);
    const buffer = await sharp(req.file.buffer).resize({
      width: 1024,
      height: 1024,
    }).webp().toBuffer();
    item.image = buffer;

    await item.save();

    res.send(item.image);
  },
);

router.delete(
  "/stores/:storeId/:categoryId/:subcategoryId/:itemId/image",
  auth,
  async (req, res) => {
    const item = Item.findById(req.params.itemId);
    item.image = undefined;
    await req.user.save();

    res.send();
  },
);

router.get(
  "/stores/:storeId/:categoryId/:subcategoryId/:itemId/image",
  async (req, res) => {
    try {
      const item = await Item.findById(req.params.itemId);

      if (!item || !item.image) {
        throw new Error(
          "Either the user does not exist, or it does not have an image",
        );
      }

      // note default Content-Type is 'application/json'
      res.set("Content-Type", "image/webp");
      res.setHeader("content-type", "image/webp");
      res.send(item.image);
    } catch (e) {
      res.status(404).send();
    }
  },
);

// TODO revise allowedUpdates, I'm not sure that, e.g., the image will ever be patched here...
router.patch(
  "/stores/:storeId/:categoryId/:subcategoryId/:itemId",
  auth,
  async (req, res) => {
    const subcategoryId = req.params.subcategoryId;
    const itemId = req.params.itemId;
    const updates = Object.keys(req.body);
    const allowedUpdates = [
      "active",
      "name",
      "description",
      "image",
      "price",
      "imgSrc",
      "requiredOptions",
      "addons",
      "filters",
    ];
    const isValidOperation = updates.every((update) =>
      allowedUpdates.includes(update)
    );

    if (!isValidOperation) {
      return res.status(400).send({ error: "Invalid updates!" });
    }

    try {
      const item = await Item.findOne({ _id: itemId });
      console.log(itemId, subcategoryId);

      if (!item) {
        return res.status(404).send();
      }

      updates.forEach((update) => item[update] = req.body[update]);
      await item.save();
      res.send(item);
    } catch (e) {
      console.log("*********", e);
      res.status(400).send(e);
    }
  },
);

router.delete(
  "/stores/:storeId/:categoryId/:subcategoryId/:itemId",
  auth,
  async (req, res) => {
    const subcategoryId = req.params.subcategoryId;
    const itemId = req.params.itemId;

    try {
      const item = await Item.findOneAndDelete({
        _id: itemId,
        subcategory: subcategoryId,
      });

      if (!item) {
        res.status(404).send();
      }

      res.send(item);
    } catch (e) {
      res.status(500).send();
    }
  },
);

module.exports = router;
