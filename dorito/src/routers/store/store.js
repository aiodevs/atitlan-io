const express = require("express");
const auth = require("../../middleware/auth");
const image = require("../../middleware/image");
const storeController = require("../../controllers/storeController");
const router = new express.Router();

// TODO can we generalize this to work for categories and subcategories?

router.post("/stores", auth, storeController.postStore);

router.get("/stores", auth, storeController.getStores);

router.get("/stores/:storeId", storeController.getStore);

router.patch("/stores/:storeId", auth, storeController.patchStore);

router.delete("/stores/:storeId", auth, storeController.deleteStore);

router.post(
  "/stores/:storeId/telegram",
  async (req, res) => {
    try {
      const store = await Store.findById(req.params.storeId);

      if (!store) {
        throw new Error(
          "This store doesn't exist",
        );
      }
      store.telegram = req.body.telegram;

      await store.save();

      res.send(store.telegram);
    } catch (e) {
      console.error(e);
      res.status(500).send();
    }
  },
);

router.get(
  "/stores/:storeId/telegram",
  async (req, res) => {
    try {
      const store = await Store.findById(req.params.storeId);

      if (!store || !store.telegram) {
        throw new Error(
          "Either the user does not exist, or it does not any css uploaded",
        );
      }

      res.send(store.telegram);
    } catch (e) {
      console.error(e);
      res.status(404).send();
    }
  },
);

router.get("/stores/:storeId/categories", auth, storeController.getCategories);

router.post("/stores/:storeId/categories", auth, storeController.postCategory);

router.patch(
  "/stores/:storeId/:categoryId",
  auth,
  storeController.patchCategory,
);

router.delete(
  "/stores/:storeId/:categoryId",
  auth,
  storeController.deleteCategory,
);

router.get(
  "/stores/:storeId/:categoryId/subcategories",
  auth,
  storeController.getSubcategories,
);

router.post(
  "/stores/:storeId/:categoryId/subcategories",
  auth,
  storeController.postSubcategory,
);

router.patch(
  "/stores/:storeId/:categoryId/:subcategoryId",
  auth,
  storeController.patchSubcategory,
);

router.delete(
  "/stores/:storeId/:categoryId/:subcategoryId",
  auth,
  storeController.deleteSubcategory,
);

// GET single item
router.get(
  "/stores/:storeId/:categoryId/:subcategoryId/:itemId", // check the routes on this
  auth,
  storeController.getItem,
);

// DELETE single item
router.delete(
  "/stores/:storeId/:categoryId/:subcategoryId/:itemId", // check the routes on this
  auth,
  storeController.deleteItem,
);

// GET all subcategory item
router.get(
  "/stores/:storeId/:categoryId/:subcategoryId/items", // check the routes on this
  auth,
  storeController.getItems,
);
//
// POST list of items, receive current states
router.post(
  "/stores/receiveListOfItems", // we want this to check all stores
  storeController.receiveListOfItems,
);

// POST single item
router.post(
  "/stores/:storeId/:categoryId/:subcategoryId/items",
  auth,
  storeController.postItem,
);

// PATCH single item
router.patch(
  "/stores/:storeId/:categoryId/:subcategoryId/:itemId",
  auth,
  storeController.patchItem,
);

router.post(
  "/stores/:storeId/:categoryId/:subcategoryId/:itemId/image",
  [auth, image.multerize.single("itemImage"), image.resize],
  storeController.postItemImage,
);

router.get(
  "/stores/:storeId/:categoryId/:subcategoryId/:itemId/image",
  storeController.getItemImage,
);

router.delete(
  "/stores/:storeId/:categoryId/:subcategoryId/:itemId/image",
  auth,
  storeController.deleteItemImage,
);

router.get(
  "/stores/:storeId/getActiveItems",
  storeController.getActiveItems,
);

module.exports = router;
