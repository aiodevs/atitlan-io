const express = require("express");
const lemmyController = require("../controllers/lemmyController");
const router = new express.Router();

router.post("/lemmy/createPost", async (req, res) => {
  const payload = req.body.payload;

  try {
    const response = await lemmyController.createPost(payload);
    res.send(response);
  } catch (e) {
    console.log(e);
    res.status(400).send(e);
  }
});

router.post("/lemmy/getCommunity", async (req, res) => {
  const payload = req.body.payload;

  try {
    const response = await lemmyController.getCommunity(payload);
    res.send(response);
  } catch (e) {
    console.log(e);
    res.status(400).send(e);
  }
});

module.exports = router;

