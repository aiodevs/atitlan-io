const express = require("express");
const User = require("../models/user");
const auth = require("../middleware/auth");
const router = new express.Router();

router.post("/users/checkToken", auth, (_req, res) => {
  res.send("Token checks out");
});

router.post("/users", async (req, res) => {
  console.log(req.body);
  const user = new User(req.body);

  try {
    const token = await user.generateAuthToken();
    await user.save();
    res.status(201).send({ user, token });
  } catch (e) {
    console.log(e);
    res.status(400).send(e);
  }
});

router.post("/users/login", async (req, res) => {
  try {
    // findByCredentials is a custom function in the user model
    const user = await User.findByCredentials(
      req.body.username,
      req.body.password,
    );
    const token = await user.generateAuthToken();
    res.send({ user, token });
  } catch (e) {
    console.log(e);
    res.status(400).send();
  }
});

router.post("/users/logout", auth, async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter((token) => {
      return token.token !== req.token;
    });
    await req.user.save();

    res.send();
  } catch (e) {
    res.status(500).send();
  }
});

router.post("/users/logoutAll", auth, async (req, res) => {
  try {
    req.user.tokens = [];
    await req.user.save();
    res.send();
  } catch (e) {
    res.status(500).send();
  }
});

router.get("/users/me", auth, async (req, res) => {
  res.send(req.user);
});

router.patch("/users/me", auth, async (req, res) => {
  const updates = Object.keys(req.body);
  const allowedUpdates = ["username", "email", "password"];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    return res.status(400).send({ error: "Invalid updates!" });
  }

  try {
    updates.forEach((update) => req.user[update] = req.body[update]);
    await req.user.save();
    res.send(req.user);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.get("/users/getTree", auth, async (req, res) => {
  const user = req.user;
  try {
    await user.populate("restaurants");
    await user.populate("stores");
  } catch (err) {
    console.error("error in routers/user.js\n", err);
  }

  const tree = { restaurants: {}, stores: {} };
  const info = { restaurants: {}, stores: {} };
  for (const restaurant of user.restaurants) {
    const restaurantIdentifier = restaurant._id + "-" + restaurant.name;
    const restaurantTree = tree.restaurants;
    const restaurantInfo = info.restaurants;
    restaurantTree[restaurantIdentifier] = {};
    restaurantInfo[restaurantIdentifier] = {
      ...restaurant._doc,
      postPath: "restaurants/" + restaurant._id,
    };
    await restaurant.populate("menus");
    for (const menu of restaurant.menus) {
      const menuIdentifier = menu._id + "-" + menu.name;
      restaurantTree[restaurantIdentifier][menuIdentifier] = {};
      restaurantInfo[menuIdentifier] = {
        ...menu._doc,
        postPath: "restaurants/" + restaurant._id + "/" + menu._id,
      };
      await menu.populate({
        path: "categories",
        options: { sort: { position: "asc" } },
      });
      for (const category of menu.categories) {
        const categoryIdentifier = category._id + "-" + category.name;
        restaurantTree[restaurantIdentifier][menuIdentifier][
          categoryIdentifier
        ] = {};
        restaurantInfo[categoryIdentifier] = {
          ...category._doc,
          postPath: "restaurants/" + restaurant._id + "/" + menu._id + "/" +
            category._id,
        };
        await category.populate({
          path: "subcategories",
          options: { sort: { position: "asc" } },
        });
        for (const subcategory of category.subcategories) {
          const subcategoryIdentifier = subcategory._id + "-" +
            subcategory.name;
          // NOTE: imageSrc and postPath are added, reference models/Restaurant/item.js
          await subcategory.populate({
            path: "items",
            options: { sort: { position: "asc" } },
          });
          restaurantTree[restaurantIdentifier][menuIdentifier][
            categoryIdentifier
          ][
            subcategoryIdentifier
          ] = subcategory.items;
          restaurantInfo[subcategoryIdentifier] = {
            ...subcategory._doc,
            postPath: "restaurants/" + restaurant._id + "/" + menu._id + "/" +
              category._id +
              "/" + subcategory._id,
          };
          // subcategory.items.push({
          //   name: "Add Item",
          //   active: true,
          //   postPath: "restaurants/" + restaurant._id + "/" + menu._id + "/" + category._id +
          //     "/" + subcategory._id + "/items",
          //   imgSrc: "src/assets/plus.svg",
          // });
        }
      }
    }
  }

  for (const store of user.stores) {
    const storeIdentifier = store._id + "-" + store.name;
    const storeTree = tree.stores;
    const storeInfo = info.stores;
    storeTree[storeIdentifier] = {};
    storeInfo[storeIdentifier] = {
      ...store._doc,
      postPath: "stores/" + store._id,
    };
    await store.populate("categories");
    for (const category of store.categories) {
      const categoryIdentifier = category._id + "-" + category.name;
      storeTree[storeIdentifier][categoryIdentifier] = {};
      storeInfo[categoryIdentifier] = {
        ...category._doc,
        postPath: "stores/" + store._id + "/" + category._id,
      };
      await category.populate("subcategories");
      for (const subcategory of category.subcategories) {
        const subcategoryIdentifier = subcategory._id + "-" +
          subcategory.name;
        // NOTE: imageSrc and postPath are added, reference models/Store/item.js
        await subcategory.populate("items");
        storeTree[storeIdentifier][categoryIdentifier][
          subcategoryIdentifier
        ] = subcategory.items;
        storeInfo[subcategoryIdentifier] = {
          ...subcategory._doc,
          postPath: "stores/" + store._id + "/" + category._id +
            "/" + subcategory._id,
        };
        subcategory.items.push({
          name: "Add Item",
          active: true,
          price: {},
          postPath: "stores/" + store._id + "/" + category._id +
            "/" + subcategory._id + "/items",
          imgSrc: "src/assets/plus.svg",
        });
      }
    }
  }

  res.send({ tree, info });
});

router.delete("/users/me", auth, async (req, res) => {
  try {
    await req.user.remove();
    res.send(req.user);
  } catch (e) {
    res.status(500).send();
  }
});

module.exports = router;
