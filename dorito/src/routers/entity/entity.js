const express = require("express");
const auth = require("../../middleware/auth");
const entityController = require("../../controllers/entityController");
const router = new express.Router();

// TODO can we generalize this to work for categories and subcategories?

router.post("/entities", auth, storeController.postStore);

router.get("/entities", auth, storeController.getStores);

router.get("/:entityId/categories", auth, storeController.getCategories);

router.post("/:entityId/categories", auth, storeController.postCategory);

router.get("/entities/:storeId", storeController.getStore);

router.patch("/entities/:id", auth, storeController.patchStore);

router.delete("/entities/:id", auth, storeController.deleteStore);

router.get(
  "/:entityId/getActiveStoreItems",
  entityController.getActiveStoreItems,
);

module.exports = router;
