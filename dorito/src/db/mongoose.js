const mongoose = require("mongoose");
const dotenv = require("dotenv");
dotenv.config();

// https://mongoosejs.com/docs/connections.html#mongos_connections
const userConn = mongoose.createConnection(
  process.env.MONGODB_CONN_STR + "Users",
  {},
);
const restaurantConn = mongoose.createConnection(
  process.env.MONGODB_CONN_STR + "MenuApp",
  {},
);
const storeConn = mongoose.createConnection(
  process.env.MONGODB_CONN_STR + "Stores",
  {},
);
const invoiceConn = mongoose.createConnection(
  process.env.MONGODB_CONN_STR + "Invoices",
  {},
);

module.exports = {
  userConn,
  restaurantConn,
  storeConn,
  invoiceConn,
};
