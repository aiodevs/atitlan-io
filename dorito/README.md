# 🚀✨ Atitlan IO Legacy Server ✨🌋
### Recommended OS : Ubuntu 20.04 Focal

## Point an A-record to your server IP address using your DNS service (gandi, godaddy, etc)
e.g., `A` `your-domain.com` `12.34.56.78`

## Install Mongodb
```
# Import the MongoDB public GPG key
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -

# Create the MongoDB list file for Ubuntu 20.04 (Focal)
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list

# Reload the local package database
sudo apt-get update

# Install MongoDB packages
sudo apt-get install -y mongodb-org

# Start MongoDB
sudo systemctl start mongod

# Enable MongoDB to start on boot
sudo systemctl enable mongod

# Verify MongoDB installation
sudo systemctl status mongod
mongo --eval 'db.runCommand({ connectionStatus: 1 })'
```

## Install NVM
```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
sudo su ${USER} # in order to use nvm
```
## install nodejs latest LTS
```
nvm install latest
```
## **important** allow node to use port 80, 443 without needing to be root
```
sudo apt-get install libcap2-bin 
sudo setcap cap_net_bind_service=+ep `readlink -f \`which node\``

```
## Pull repos
```
cd ~
git clone git@gitlab.com:aiodevs/atitlan-io.git
cd atitlan-io/dorito; npm install
```
## Configure your .env
```
cp .env.example .env # configure variables
```
## You may run the server how you wish, here is an example using pm2
##  Install and run PM2
```
npm install pm2 -g
# note: had to put .well-known folder and also set up certbot (ref below)
pm2 start src/app.js # make sure you run from this directory to get env variables
pm2 save # save configuration
pm2 startup # run on startup
```
## Modify systemd file to only run after mongod
```
sudo vim /etc/systemd/system/pm2-${USER}.service
```
```
[Unit]
Description=PM2 process manager
Documentation=https://pm2.keymetrics.io/
After=network.target
After=mongod.service
```

# =-=-=-=-=-=-=-=-=-=- Certbot =-=-=-=-=-=-=-=-=-=-
### ref: https://eff-certbot.readthedocs.io/en/stable/install.html
### ref: https://certbot.eff.org/instructions?ws=other&os=ubuntufocal
### ref: https://snapcraft.io/docs/installing-snap-on-raspbian

## Install Snapd
```
sudo apt install -y snapd
sudo reboot # needed to do this for the following step, can try without
```

## Ensure that your version of snapd is up to date
```
sudo snap install core; sudo snap refresh core
```

## Install Certbot
```
sudo snap install --classic certbot
```

## Prepare certbot command using symbolic link
```
sudo ln -s /snap/bin/certbot /usr/bin/certbot
```


## ----------------------------
## Set Up Letsencrypt SSL Cert
### create limited group to allow permissions for letsencrypt certs
### ref: https://stackoverflow.com/questions/48078083/lets-encrypt-ssl-couldnt-start-by-error-eacces-permission-denied-open-et

## create group with user and root as members
```
sudo addgroup nodecert
sudo adduser ${USER} nodecert
sudo adduser root nodecert
```
## Make the relevant letsencrypt folders owned by said group.
```
sudo mkdir -p /etc/letsencrypt/live
sudo chgrp -R nodecert /etc/letsencrypt/live
sudo mkdir -p /etc/letsencrypt/archive
sudo chgrp -R nodecert /etc/letsencrypt/archive
```
## Allow group to open relevant folders
```
sudo chmod -R 750 /etc/letsencrypt/live
sudo chmod -R 750 /etc/letsencrypt/archive
```
## Follow instructions to create cert
```
sudo certbot
```


