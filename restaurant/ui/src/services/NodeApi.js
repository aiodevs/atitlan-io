import axios from "axios";

const NodeApi = axios.create({
  baseURL: import.meta.env.VITE_BASE_URL,
  timeout: 10000,
});

export default {
  async cafeOpen() {
    const response = await NodeApi.get("/cafeOpen");
    return response.data.status;
  },

  async checkPrinterStatus(thermalPrinterAutoSSHPort) {
    return NodeApi.post("/checkPrinterStatus", { thermalPrinterAutoSSHPort });
  },

  // items object is attached in the case of unresolved invoices
  // TODO maybe send arguments as an object?
  createInvoice(
    {
      entityId = "",
      payMethod = "",
      currency = "GTQ",
      amount = 0,
      thermalPrinterAutoSSHPort = null,
      metadata = {},
      btcpay_data = {},
      cashPayment = false,
    },
  ) {
    // this returns the checkout page link to redirect the customer
    if (Object.keys(metadata.items).length > 0 && amount > 0) {
      return NodeApi.post("/createInvoice", {
        entityId, // necessary for inserting into the invoice database
        payMethod,
        currency,
        amount,
        metadata,
        btcpay_data,
        cashPayment,
        // recurrente_pub_key,
        thermalPrinterAutoSSHPort,
      });
    }
  },
};
