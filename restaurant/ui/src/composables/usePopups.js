import { ref } from "vue";

const successModal = ref(false);
const quickViewErrorModal = ref(false);
const printerErrorModal = ref(false);
const isLoading = ref(false);
const hoursModal = ref(false);

export function usePopups() {
  const openSuccessModal = function () {
    successModal.value = true;
  };
  const openHoursModal = function () {
    hoursModal.value = true;
  };
  const openQuickViewErrorModal = function () {
    quickViewErrorModal.value = true;
  };
  const closeQuickViewErrorModal = function () {
    quickViewErrorModal.value = false;
  };
  const openPrinterErrorModal = function () {
    printerErrorModal.value = true;
  };
  const closePrinterErrorModal = function () {
    printerErrorModal.value = false;
  };
  const setLoading = function () {
    isLoading.value = true;
  };
  const unsetLoading = function () {
    isLoading.value = false;
  };
  const closeAllModals = function () {
    successModal.value = false;
    errorModal.value = false;
    isLoading.value = false;
  };

  return {
    successModal,
    hoursModal,
    quickViewErrorModal,
    closeQuickViewErrorModal,
    openQuickViewErrorModal,
    printerErrorModal,
    closePrinterErrorModal,
    openPrinterErrorModal,
    isLoading,
    openSuccessModal,
    openHoursModal,
    setLoading,
    unsetLoading,
    closeAllModals,
  };
}
