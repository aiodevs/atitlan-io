import axios from "axios";
import NodeApi from "../services/NodeApi";

import { ref, reactive } from 'vue'

import { usePopups } from './usePopups.js'
const { isLoading } = usePopups()

const lightningNodeHealth = ref(false);
const thermalPrinterOnline = ref(false);
const kioskBool = ref(false)

const restaurants = ref([])

const btcpay_data = ref(null)
const recurrente_data = ref(null)

const restaurantsObject = reactive({})

const selectedRestaurant = ref(restaurants[0]);

const entitiesArray = ref([])
const someRestaurantOpen = ref(false)

const checkRestaurantsOpen = async function(entitiesArray) {
  for (const entity of entitiesArray) {
    if (await isOpen(entity)) {
      restaurantsObject[entity].open = true
      someRestaurantOpen.value = true
    } else {
      restaurantsObject[entity].open = false
    }
  }
}

import payWithLightning from "../assets/payWithLightning.svg";
import payWithCard from "../assets/payWithCard.svg";
import payWithCash from "../assets/payWithCash.svg";
import cash from '../assets/money.png'
import card from '../assets/atm-card.svg'
import btc from '../assets/bitcoin-simple-lighting.svg'

const payMethods = [
  {
    id: 1,
    title: "BTC",
    color: "orange",
    fixedFee: 0,
    percentFee: 0,
    img: payWithLightning,
    symbol: btc,
  },
  {
    id: 2,
    title: "Card",
    color: "fuchsia",
    fixedFee: 2.1,
    percentFee: 7,
    img: payWithCard,
    symbol: card,
  },
  {
    id: 3,
    title: "Cash",
    color: "green",
    fixedFee: 0,
    percentFee: 0,
    img: payWithCash,
    symbol: cash,
  },
];


const itemHolder = ref(null); //holds the item for quickview
const items = ref({});
// This is created for ease of querying items, e.g., for fuse.js
const singleLevelItemArray = ref([]);
//
// Function to clear the properties of a reactive object
function clearReactiveObject(obj) {
  for (const key of Object.keys(obj)) {
    delete obj[key];
  }
}

async function getRestaurants(restaurantsArray) {
  clearReactiveObject(restaurantsObject)
  isLoading.value = true;
  items.value = {};
  singleLevelItemArray.value = [];
  console.log('here')
  for (const restaurantId of restaurantsArray) {
    try {
      const response = await axios.get(`/restaurants/${restaurantId}`)
      const restaurantData = response.data
      restaurantsObject[restaurantId] = {
        ...restaurantData,
        small_logo: `data:image/webp;base64,${restaurantData.small_logo}`,
        banner_logo: `data:image/webp;base64,${restaurantData.banner_logo}`
      };
      restaurants.value.push({ id: restaurantId, name: restaurantsObject[restaurantId].name, small_logo: function() { return `https://atitlan.io/restaurants/${this.id}/small_logo` } })
      console.log(restaurants.value)
      //   id: "65611174d7e61f14f4d5eafa",
      //   name: "Dragon Moon Temple",
      //   online: true,
      //   small_logo: function () {
      //     return `https://atitlan.io/restaurants/${this.id}/small_logo`;
      //   },

      if (restaurantsArray.length === 1) {
        btcpay_data.value = restaurantData.btcpay_data
        console.log(restaurantData.btcpay_data)
      } else {
        console.log(restaurantData.btcpay_data)
        console.error('This is here to remind you that multirestaurants need to agree on one payment addr')
        // throw new Error('This is here to remind you that multirestaurants need to agree on one payment addr')
      }

      await populateMenu(restaurantId)

      if (await isOpen(restaurantId)) {
        restaurantsObject[restaurantId].open = true;
        someRestaurantOpen.value = true
      } else {
        restaurantsObject[restaurantId].open = false;
      }

      console.log(`🍴 restaurant ${restaurantData.name} loaded`)
      // console.log(restaurantsObject[restaurantId])
    } catch (err) {
      console.error("could not get restaurant", err)
    }

  }
  checkPrinterStatus();
  checkLightningNode();
  isLoading.value = false;
}

function getRestaurant(restaurantId) {
  isLoading.value = true;
  return axios.get(`restaurants/${restaurantId}`)
}

function checkPrinterStatus() {
  // return axios.get(import.meta.env.VITE_BASE_URL + restaurant.value.thermalPrinterAutoSSHPort + '/hello')
  return NodeApi.checkPrinterStatus(
    3001
  )
    .then((result) => {
      if (result && result.data === "hello") {
        console.log("🟢 Printer says ", result.data);
        thermalPrinterOnline.value = true;
      } else {
        console.log("🔴 Printer says ", result.data);
        thermalPrinterOnline.value = false;
      }
      return thermalPrinterOnline.value;
    })
    .catch((e) => {
      console.error(e);
      console.log("printer unreachable");
    });
}

function checkLightningNode() {
  return new Promise((resolve, reject) => {
    axios.get("https://btcpay.atitlan.io/api/v1/health",)
      .then((res) => {
        if (res.data.synchronized === true) lightningNodeHealth.value = true;
        console.log("🟢 Lightning Node Synched")
        resolve(lightningNodeHealth.value);
      })
      .catch((e) => {
        console.log("🔴 Lightning Node offline or synching")
        reject(e)
      });
  });
}

function isOpen(restaurantId) {
  return axios.get(`/restaurants/${restaurantId}/isOpen`)
    .then((res) => {
      if (res.data === true) {
        restaurantsObject[restaurantId].open = true
        return true;
      } else if (res.data === false) {
        restaurantsObject[restaurantId].open = false
        return false;
      } else {
        restaurantsObject[restaurantId].open = false
        console.error("Could not determine if restaurant is open");
        return false;
      }
    });
}

async function populateMenu(restaurantId) {
  // send request to server to get children of document
  // server sends back a response with children
  // if the request is for a subcategory, then it populates items

  const response = await axios.get(`/restaurants/${restaurantId}/getAllActive`);
  // this currently only allows for one active menu at a time
  // will update in the future to have multiple menus
  let restaurantItems = {};
  for (const menu of Object.keys(response.data)) {
    restaurantItems = { ...restaurantItems, ...response.data[menu] };
  }
  restaurantsObject[restaurantId].items = restaurantItems;

  flattenItems(restaurantItems)
}

const flattenItems = function(object) {
  if (!(object instanceof Object)) {
    return console.log("This isn't an object!");
  } else if (!(Array.isArray(object))) {
    for (const [key, value] of Object.entries(object)) {
      flattenItems(value);
    }
  } else if (Array.isArray(object)) {
    for (const item of object) {
      singleLevelItemArray.value.push(item);
    }
  }
};

export function useRestaurant() {
  return {
    isLoading,
    getRestaurants,
    restaurantsObject,
    restaurants,
    selectedRestaurant,
    thermalPrinterOnline,
    lightningNodeHealth,
    isOpen,
    entitiesArray,
    someRestaurantOpen,
    checkRestaurantsOpen,
    items,
    singleLevelItemArray,
    itemHolder,
    payMethods,
    kioskBool,
    btcpay_data,
    recurrente_data
  }
}


