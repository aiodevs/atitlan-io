import { ref } from "vue";

const infobarOpen = ref(false);
const cartOpen = ref(false);

export function useSlideovers() {
  const openInfobar = function () {
    infobarOpen.value = true;
  };
  const closeInfobar = function () {
    infobarOpen.value = false;
  };
  const openCart = function () {
    cartOpen.value = true;
  };
  const closeCart = function () {
    cartOpen.value = false;
  };

  return {
    infobarOpen,
    cartOpen,
    openInfobar,
    closeInfobar,
    openCart,
    closeCart,
  };
}
