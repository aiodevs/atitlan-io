import { defineStore } from "pinia";
import { useRestaurant } from "../composables/useRestaurant";
const { payMethods } = useRestaurant();

function updateLocalStorage(itemName, item) {
  localStorage.setItem(itemName, JSON.stringify(item));
}

export const useCartStore = defineStore("cartStore", {
  state: () => ({
    // TODO make cart an object with products, comments, tipPercernt and selectedPayMethod
    cart: [],
    comments: "",
    tipPercent: null,
    stashed: { cart: [], comments: "", tipPercent: 0.0 },
    selectedPayMethod: payMethods[0],
    factura: { name: "", nit: "", address: "", email: "" }
  }),
  // state() {
  //     return {
  //         cart: [],
  //         // this is an alternate way to return the object than above
  //     }
  // },
  getters: {
    // don't need to pass in 'state' if using function syntax, instead use this keyword
    // note if you want to reference other getters, use the function syntax (not arrow fct)
    //    in order to leverage the `this` keyword to access context outside of state
    //only need to pass in state if using an arrow fct (where `this` doesn't work)
    // cartSubTotal: state => state.cart.reduce((a,b)...)
    cartSubTotal() {
      // 1h43m (Tyson London Youtube Shopping Cart)
      return this.cart.reduce((a, b) => a + (b.price * b.quantity), 0);
    },
    cartQty() {
      return this.cart.reduce((a, b) => a + b.quantity, 0);
    },
    // had to use an arrow function for this to work, checkout TODO below
    productQty: (state) => {
      // ref https://pinia.vuejs.org/core-concepts/getters.html#passing-arguments-to-getters
      return (product) => {
        const item = state.cart.find((i) => i._id === product._id); // high order array fct availalbe in javascript
        if (item) return item.quantity;
        else return null;
      };
    },
    // TODO find what's wrong with this: check out https://stackoverflow.com/questions/32782922/what-do-multiple-arrow-functions-mean-in-javascript
    // might have something to do with binding
    // productQty() {
    //     // ref: https://pinia.vuejs.org/core-concepts/getters.html#passing-arguments-to-getters
    //     return (product) => {
    //         const item = this.cart.find( i => i._id === product._id) // high order array fct availalbe in javascript
    //         if (item) return item.quantity
    //         else return null
    //     }
    // },
    // TODO in Vuex I had things like getCart, but don't think it's needed. Double check
  },
  actions: {
    addToCart(product) {
      const item = this.cart.find((i) => i._id === product._id);

      if (item && !item.inventory) {
        item.quantity++;
      } else if (item && item.quantity < item.inventory) {
        item.quantity++;
      } else if (item && item.quantity === item.inventory) {
        console.log("quantity reached maximum");
      } else {
        this.cart.push({ ...product, quantity: 1 }); //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax
      }

      // if someone's returning to their cart, we want to let them pick up where they left off
      // this function is defined at the top
      updateLocalStorage("cart", this.cart);
      updateLocalStorage("comments", this.comments);
      updateLocalStorage("tipPercent", this.tipPercent);
    },
    delFromCart(product) {
      const item = this.cart.find((i) => i._id === product._id); // i is the element of this.cart whereas product is passed from the component?

      if (item) {
        if (item.quantity > 1) { // this is because we don't want to have an item in the array with a quantity of 0, instead we would delete it
          item.quantity--;
        } else {
          this.cart = this.cart.filter((i) => i._id !== product._id); // this removes it from the cart and the array
        }
      }
      updateLocalStorage("cart", this.cart);
      updateLocalStorage("comments", this.comments);
      updateLocalStorage("tipPercent", this.tipPercent);
    },
    delAllItemFromCart(product) {
      // note this is for a *specific item* with a quantity > 1
      this.cart = this.cart.filter((i) => i._id !== product._id);

      updateLocalStorage("cart", this.cart);
      updateLocalStorage("comments", this.comments);
      updateLocalStorage("tipPercent", this.tipPercent);
    },
    stashCart(payload) {
      const cart = localStorage.getItem("cart");

      if (cart) {
        this.stashed.cart = JSON.parse(cart);
        this.stashed.comments = payload.comments;
        this.stashed.tipPercent = payload.tipPercent;
        this.stashed.selectedPayMethod = payload.selectedPayMethod;
        this.stashed.factura = payload.factura;
        updateLocalStorage("stashed", this.stashed);
      }
    },
    emptyCart() {
      this.cart = [];
      this.comments = "";
      this.tipPercent = null;
      this.factura = {};
      this.selectedPayMethod = payMethods[0];
      updateLocalStorage("cart", this.cart);
      updateLocalStorage("comments", this.comments);
      updateLocalStorage("tipPercent", this.tipPercent);
      updateLocalStorage("factura", this.factura);
    },
    popCart() {
      console.log(this.stashed);
      this.cart = this.stashed.cart;
      this.comments = this.stashed.comments;
      this.tipPercent = this.stashed.tipPercent;
      this.selectedPayMethod = this.stashed.selectedPayMethod;
      this.factura = this.stashed.factura;
      updateLocalStorage("cart", this.cart);
      updateLocalStorage("comments", this.comments);
      updateLocalStorage("tipPercent", this.tipPercent);
      updateLocalStorage("factura", this.factura);
    },
    updateCartFromLocalStorage() {
      const cart = localStorage.getItem("cart");
      const stashed = localStorage.getItem("stashed");
      if (cart) {
        this.cart = JSON.parse(cart);
      }
      if (stashed) {
        // remember stashed is an object containing properties cart, comments and tipPercent
        this.stashed = JSON.parse(stashed);
      }
    },
  },
});
