import { createRouter, createWebHistory } from "vue-router";

const routes = [
  // {
  //   path: "/",
  //   name: "Directory",
  //   component: () => import("./views/HomePage.vue"),
  //   props: true,
  // },
  {
    path: "/:catchAll(.*)?",
    name: "RestaurantPage",
    component: () => import("./views/RestaurantPage.vue"),
    props: true,
  },
  // {
  //   path: "/r/:restaurantId?/:catchAll(.*)?",
  //   name: "SingleRestaurantPage",
  //   component: () => import("./views/RestaurantPage.vue"),
  //   props: true,
  // },
  // {
  //   path: "/c/:clusterId",
  //   name: "Directory",
  //   component: () => import("./views/HomePage.vue"),
  //   props: true,
  // },
  // {
  //   path: "/c/:clusterId/:restaurantId",
  //   name: "MultiRestaurantPage",
  //   component: () => import("./views/MultiRestaurantPage.vue"),
  //   props: true,
  // },
  // {
  //   path: "/keypad",
  //   name: "Keypad",
  //   component: () => import("./views/KeypadPage.vue"),
  //   props: true,
  // },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;

