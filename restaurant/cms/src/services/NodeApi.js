import NodeApi from "./NodeApi.js";
import axios from "axios";

export default {
  // * * * * * * * * * * * * * * * * * * * * CALL API FUNCTION * * * * * * * * * * * * * * * * * * * *
  CallAPI(callType = "", postPath = "", payload = {}) {
    if (callType === "delete") {
      if (!postPath) {
        throw new Error(
          "delete still requires a correct path to the item, i.e., 'restaurantId/menuId/CategoryId/...' ",
        );
      }
      eval(`axios.${callType.toLowerCase()}( '${postPath}', {headers})`);
    } else if (!callType || !postPath || Object.keys(payload).length === 0) {
      throw new Error(
        "CallAPI requires a callType (post/patch), the level (e.g., category, item), and a non-empty payload",
      );
    } else {
      eval(
        `axios.${callType.toLowerCase()}( '${postPath}', ${
          JSON.stringify(payload)
        }, {headers})`,
      );
    }

    // refresh the itemTree
    populateNav("refresh");
  },
  // * * * * * * * * * * * * * * * * * * * * CALL API FUNCTION * * * * * * * * * * * * * * * * * * * *
};

// to call this
// object that allows us to call a register method that will hit that register endpoint
// basically, we're, e.g., exporting a n object that has a register method to it
// then we can call it like so:

//AuthenticationService.register({
//	email: 'testing@gmail.com',
//	password: '123456'
//})
