import { createRouter, createWebHistory } from "vue-router";
import Login from "/src/views/Login.vue";
import Register from "/src/views/Register.vue";
import ContentManager from "/src/views/ContentManager.vue";

const routes = [
  {
    path: "/cms",
    name: "ContentManager",
    component: ContentManager,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
