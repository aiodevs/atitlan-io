import axios from "axios";
import { ref } from "vue";
import { useRoute, useRouter } from "vue-router";

const authToken = ref(null);
const headers = ref({});
const username = ref("");
const userid = ref("");
const restaurants = ref([]);
const stores = ref([]);
const userTree = ref({});
const userTreeInfo = ref({});
const small_logo_url = ref("");

const filterList = ref([]);
const addonList = ref({});

function updateLocalStorage(itemName, item) {
  localStorage.setItem(itemName, item); //JSON.stringify(item))
}

export const useUser = function() {
  // this is for logging out
  // for some reason it can't be placed above
  const router = useRouter();

  const clearEverything = function() {
    authToken.value = "";
    headers.value = {};
    username.value = "";
    userid.value = "";
    restaurants.value = [];
    stores.value = [];
    userTree.value = {};
    userTreeInfo.value = {};
    filterList.value = [];
    addonList.value = {};
  };

  const getRestaurants = function() {
    return new Promise((resolve, reject) => {
      axios.get("restaurants", { headers: headers.value })
        .then((res) => {
          restaurants.value = res.data;
          console.log(res.data);
          small_logo_url.value = `https://atitlan.io/restaurants/${res.data[0]._id
            }/small_logo`;
          resolve();
        })
        .catch((err) => reject(console.error(err)));
    });
  };

  const getStores = function() {
    return new Promise((resolve, reject) => {
      axios.get("stores", { headers: headers.value })
        .then((res) => {
          restaurants.value = res.data;
          console.log(res.data);
          small_logo_url.value = `https://atitlan.io/stores/${res.data[0]._id
            }/small_logo`;
          resolve();
        })
        .catch((err) => reject(console.error(err)));
    });
  };

  const getUsername = function() {
    return new Promise((resolve, reject) => {
      axios.get("users/me", { headers: headers.value })
        .then((res) => {
          username.value = res.data.username;
          resolve();
        })
        .catch((err) => reject(console.error(err)));
    });
  };

  const getFilters = function() {
    return new Promise((resolve, reject) => {
      axios.get("restaurants/filters")
        .then((res) => {
          filterList.value = res.data;
          resolve();
        })
        .catch((err) => reject(console.error(err)));
    });
  };

  const getAddons = function() {
    return new Promise((resolve, reject) => {
      for (let restaurant of restaurants.value) {
        axios.get(`restaurants/${restaurant._id}/addons`)
          .then((res) => {
            addonList.value[restaurant._id] = res.data;
            resolve();
          })
          .catch((err) => reject(console.error(err)));
      }
    });
  };

  const getAuthToken = function() {
    if (authToken.value) {
      return authToken.value;
    } else {
      throw new Error("Auth token not set");
      return null;
    }
  };

  const setAuthToken = function(receivedToken) {
    if (typeof (receivedToken) === "string") {
      authToken.value = receivedToken;
      console.log("authToken set");
      updateLocalStorage("authToken", receivedToken);
      headers.value.Authorization = `Bearer ${receivedToken}`;
    } else {
      throw new Error("Auth Token must be a string");
    }
  };

  const logout = function() {
    axios.post("/users/logout", {}, {
      headers: { "Authorization": `Bearer ${authToken.value}` },
    });
    clearEverything();
    router.push("/login");
  };

  const getAndStoreUser = function(loginForm) {
    return new Promise((resolve, reject) => {
      axios.post("users/login", loginForm)
        .then((res) => {
          username.value = res.data.user.username;
          userid.value = res.data.user._id;
          setAuthToken(res.data.token);
          localStorage.setItem("authToken", res.data.token);
          resolve();
        })
        .catch((err) => {
          reject(console.error(err));
        });
    });
  };

  // should i change this to return a promise.. ?
  const getUserTree = async function() {
    const response = await axios.get("users/getTree", {
      headers: headers.value,
    });
    console.log(response.data);
    // How will this look with stores
    // userTree.value.restaurants
    // userTree.value.stores
    // ?
    // or maybe keep as is and either leverage some kind of inheritance or
    userTree.value = response.data.tree.stores;
    userTreeInfo.value = response.data.info.stores;
  };

  return {
    // authToken,
    getAuthToken,
    headers,
    clearEverything,
    small_logo_url,
    logout,
    getUsername,
    username,
    userid,
    userTree,
    userTreeInfo,
    restaurants,
    getUserTree,
    getAndStoreUser,
    getRestaurants,
    getStores,
    getFilters,
    filterList,
    getAddons,
    addonList,
    setAuthToken,
  };
};
