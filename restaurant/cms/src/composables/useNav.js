import { ref, watch } from "vue";

import { useUser } from "./useUser";

const { userTree, userTreeInfo, restaurants } = useUser();

const pointer = ref({});
const pointerKeys = ref([]);

const itemHolder = ref({});

const levelNames = {
  0: "restaurant",
  1: "category",
  2: "subcategory",
  3: "item",
};

const breadcrumbs = ref([]);

export function useNav() {
  const pluralize = function (word) {
    if (typeof (word) === "string") {
      return word[word.length - 1] === "y"
        ? word.replace(word[word.length - 1], "ies")
        : word + "s";
    }
  };

  function getChildren(parent) {
    // the uuid is the unique id of the level being selected, if it is empty, the items will simply refresh
    // TODO refactor this function name to populateItemTree?
    breadcrumbs.value.push(parent);
    pointer.value = pointer.value[parent];
    // const level = levelNames[breadcrumbs.value.length];
    // TODO remove addLevel
    // addLevel.value = { name: `Add ${level}`, icon: PlusIcon, href: '#', current: true, postPath: "restaurants", formType: 'restaurant' }
  }

  function goBack(steps) {
    if (steps === 0) {
      return console.log("0 steps chosen");
    }
    console.log("steps", steps);

    const crumbLength = breadcrumbs.value.length;
    const retrace = crumbLength - steps;

    let temp2 = userTree.value;
    for (let i = 0; i < steps; i++) {
      breadcrumbs.value.pop();
    }
    for (let i = 0; i < retrace; i++) {
      console.log(breadcrumbs.value[i]);
      temp2 = temp2[breadcrumbs.value[i]];
    }
    pointer.value = temp2;
  }

  const refreshAddons = function () {
    return new Promise((resolve, reject) => {
      for (const restaurant of restaurants.value) {
        axios.get(`restaurants/${restaurant._id}/addons`)
          .then((res) => {
            addonList.value[restaurant._id] = res.data;
            resolve();
          })
          .catch((err) => reject(console.error(err)));
      }
    });
  };

  watch(userTree, () => {
    let temp2 = userTree.value;
    for (let i = 0; i < breadcrumbs.value.length; i++) {
      temp2 = temp2[breadcrumbs.value[i]];
    }
    pointer.value = temp2;
  });

  watch(pointer, () => {
    pointerKeys.value = Object.keys(pointer.value);
  });

  return {
    pointer,
    pointerKeys,
    itemHolder,
    breadcrumbs,
    levelNames,
    getChildren,
    goBack,
    refreshAddons,
    pluralize,
  };
}
