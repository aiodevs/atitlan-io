import { ref } from "vue";
import axios from "axios";
import { useUser } from "./useUser";
import { useNav } from "./useNav";

const { itemHolder } = useNav();
const { getAuthToken, getUserTree } = useUser();
const callTypeGlobal = ref("");

export function useApi() {
  // https://stackoverflow.com/questions/65703814/how-to-upload-file-in-vue-js-version-3
  let headers = {}
  try {
    const authToken = getAuthToken();
    headers = { Authorization: `Bearer ${authToken}` };
  } catch {
    console.warn("No Auth Token Set")
  }
  const handleImgUpload = async (postPath, imgFiles) => {
    if (postPath.split("/")[4] !== "items") {
      const form = new FormData();
      // const promise = await getBase64(imgFiles[0]);
      // promise.then(function(result) {
      //     imageReplaced = result
      // });
      form.append("itemImage", imgFiles[0]);
      // axios.post('localhost:8081/' + itemHolder.restaurant)
      let result = false;
      await axios.post(postPath + "/image", form, {
        headers: { ...headers, "Content-Type": "image/webp" },
      })
        .then(() => {
          result = true;
        })
        .catch((e) => {
          console.error(e);
        });
      return result;
    } else {
      console.log("item not created yet");
    }
  };

  const callApi = function (
    {
      callType = callTypeGlobal.value,
      postPath = itemHolder.value.postPath,
      payload = {},
    },
  ) {
    const authToken = getAuthToken();
    const headers = { Authorization: `Bearer ${authToken}` };
    postPath = postPath.replace("restaurant","store")
    // postPath = postPath.replace("categories","subcategories")
    console.log("##########")
    console.log(postPath)
    console.log("##########")

    if (callType === "post") { // ########### POST ############
      return new Promise((resolve, reject) => {
        axios.post(postPath, payload, { headers })
          .then((res) => {
            getUserTree();
            resolve(res);
          })
          .catch((e) => {
            reject(console.error(e));
          });
      });
    } else if (callType === "patch") { // ########### PATCH ############
      if (!postPath) {
        throw new Error(
          "delete still requires a correct path to the item, i.e., 'restaurantId/menuId/CategoryId/...' ",
        );
      }
      return new Promise((resolve, reject) => {
        axios.patch(postPath, payload, { headers })
          .then((res) => {
            // let the CMS just update the item object
            if (!itemHolder.imgSrc) getUserTree();
            resolve(res);
          })
          .catch((e) => {
            reject(console.error(e));
          });
      });
    } else if (callType === "delete") { // ########### DELETE ############
      if (!postPath) {
        throw new Error(
          "delete still requires a correct path to the item, i.e., 'restaurantId/menuId/CategoryId/...' ",
        );
      }
      return new Promise((resolve, reject) => {
        axios.delete(postPath, { headers })
          .then((res) => {
            getUserTree();
            resolve(res);
          })
          .catch((e) => {
            reject(console.error(e));
          });
      });
    } else if (!callType || !postPath || Object.keys(payload).length === 0) {
      throw new Error(
        "CallAPI requires a callType (post/patch), the level (e.g., category, item), and a non-empty payload",
      );
    }
  };

  return { callApi, callTypeGlobal, handleImgUpload };
}

// const CallApi = async function(callType, payload) {
// let postPath = ''
// if (itemHolder._id) {
//     postPath = itemHolder.restaurant + '/' + itemHolder.menu + '/' + itemHolder.category + '/' + itemHolder.subcategory + '/' + itemHolder._id
// } else {
//     postPath = itemHolder.postPath
// }

// // Don't know if I need this...
// if (callType === 'patch' || callType === 'post') {
//     console.log("ACTIVE", active.value)

//     console.log('CallAPI', callType, postPath, payload )
//     emit('CallAPI', callType, postPath, payload )

// } else if ( callType === 'delete' ) {
//     emit('CallAPI', callType, postPath )
// } else { console.log("not a valid call request (needs to be 'patch' or 'delete')")}

// emit('close')

// }
