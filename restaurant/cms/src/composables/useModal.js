import { ref } from "vue";

import { useNav } from "./useNav";
const { itemHolder } = useNav();

import { useApi } from "./useApi";
const { callTypeGlobal } = useApi();

const formTypeGlobal = ref("");
const modalOpen = ref(false);
const alertModalOpen = ref(false);

export function useModal() {
  const toggleModal = function (
    { state = true, formType = "", callType = "", item = undefined },
  ) {
    console.log("modal toggled", state);
    formTypeGlobal.value = formType;
    callTypeGlobal.value = callType;
    itemHolder.value = item;

    if (item) {
      // don't know if i need this formType
      console.log("##############################");
      console.log("formtype", "'formTypeGlobal.value'");
      console.log("item", itemHolder.value);

      console.log("##############################");
    }

    if (state === false) {
      // TODO maybe clear out item and formType?
    }

    // TODO does this need to be reversed?
    modalOpen.value = state ? state : !modalOpen.value;
  };

  return {
    formTypeGlobal,
    callTypeGlobal,
    toggleModal,
    modalOpen,
    alertModalOpen,
  };
}
