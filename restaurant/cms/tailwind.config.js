/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        tan: {
          50: "#fff6d2",
          100: "#fcecc8",
          200: "#f2e2be",
          300: "#e8d8b4",
          400: "#deceaa",
          500: "#d4c4a0",
          600: "#caba96",
          700: "#c0b08c",
          800: "#b6a682",
          900: "#ac9c78",
        },
        brown: {
          50: "#7d5e41",
          100: "#735437",
          200: "#694a2d",
          300: "#5f4023",
          400: "#553619",
          500: "#4b2c0f",
          600: "#412205",
          700: "#371800",
          800: "#2d0e00",
          900: "#230400",
        },
      },
    },
  },
  plugins: [
    // ...
    require("@tailwindcss/forms"),
  ],
};
