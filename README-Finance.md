# Finance API

## The lifecycle of a transaction intent

0. Shop Invoice/User payment request  
A user is going throught the shopping conversion pipeline and eventually decides on what /how much to spend or scans a QR code from a pos. The invoice is created.

1. Payment Gateway Selection Page  
Create intent - Two flavours:  
**1.a** User is new and has no user or wallet - **Anonymous intent Flow**  
**2.b** User is using a browser and is not logged in yet, logs in:  
**3.c** User is logged in. - **Normal Intent Flow**  
**TransactionIntent - status**: created

2. ConfirmProcessing - The user has gone through the paymentGateway options and selects a paymentgateway. Now the user is taken to a hosted checkout page. Adds the paymentGateway to the transactionIntent and update the status.  
**TransactionIntent - status**: processing

3. Finalize via webhook - The result from the payment gateway comes back on our webhook: either failed or succesful. The intent is complete and if the intent is still anonymous, create a wallet after a success is confirmed:  
**3.a Failure**  
**TransactionIntent - status**: failed.  
**Anonymous flow:** no new wallet.  
**3.b Succes to non-self wallet** Target wallet is not the user's wallet: create add-funds (1) & internal transaction (2).  
(1) - The add-funds should be between the payment gateway and the user.  
(2) - The internal-transaction should be between the user and the invoice target wallet.  
**TransactionIntent - status**: succes.  
**Anonymous intent flow:** new anonymous wallet.  
**3.c Success to own wallet** The target wallet is the user's wallet: create only an add-fund transaction.
**TransactionIntent - status**: succes.  
**Anonymous intent flow:** No new wallet - *Note it should not be possible to create an invoice for a non existing wallet*

## About Payment gateways

- Payment gateway wallets should not be used for anything but handling payment gateway transactions. This means that the balance on the account of the paymentgateway should mirror exactly the wallet that we have where with for example stripe:  
stripe account +10  
stripe paymentgateway wallet -10  
- Payment gateways can store their own api keys in the db (encrypted)
Stripe account - managed by stripe - API key - recieves funds when users pay to stripe via paypal, ideal, etc  
