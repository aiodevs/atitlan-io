setup-depencies:
	cd infrastructure && ./setup-depencies.sh

# THOR
thor-setup:
	cd thor && make setup

thor-test:
	cd thor && make test-db
	cd thor && make test

thor-watch:
	cd thor && make watch

thor-build:
	make thor-api-build
	make thor-ui-build

thor-ui-build:
	docker build -f apps/Dockerfile.thor-ui -t thor-ui:latest-dev .

thor-api-buil:
	docker build -f apps/Dockerfile.thor-api -t thor-api:latest-dev .

thor-stack-build-push:
	cd thor && make stack-build-push

thor-stack-up:
	cd thor && make stack-up

thor-stack-down:
	cd thor && make stack-down

thor-terminal-up:
	cd thor && make terminal-up

thor-terminal-down:
	cd thor && make terminal-down

#Infra
infra-up:
	cd infrastructure && ./check-create-network.sh
	cd infrastructure && make traefik-up
	cd infrastructure && make portainer-up

infra-down:
	cd infrastructure && make portainer-down
	cd infrastructure && make traefik-down







