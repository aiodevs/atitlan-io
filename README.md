# 🚀✨Atitlan IO✨🌋

A suite of web applications dedicated to promoting decentralization, autonomy, and privacy, designed to support smaller communities by enhancing their digital infrastructure.

We love Free Open Source Software (FOSS)!

> 🌄 Lokah Samastah Sukinho Bhavantu 🌄

## [⚡🪬 THOR 🪬⚡](./thor/README.md)

**Roles:** Authentication, Authorization, Organizations, Users, Transactions, Wallets  
**Apps:** UI, API, DB, seeder, dbMigrator  
**Runs in:** terminal, docker compose, docker stack  
**Runs on:** [Test](https://thor-test.atitlanio.io) [Dev](https://thor-dev.atitlan.io) [Prod](https://thor.atitlan.io)  
**Depencies**: `make setup-depencies` [.sh](/infrastructure/setup-depencies.sh)  
**Get started**: `make thor-setup`  
*About **make thor-setup:** After some prep starts and builds all thor docker images and starts running tests.*

## [⚡☸ INFRA ☯⚡](./README-Infrastructure.md)

[Swarm setup guide](./README-swarm-node.md)

**Traefik** - [Prod](https://traefik.infra.atitlan.io) -  Loadbalancing, SSL certs  
**portainer** - [Prod](https://portainer.infra.atitlan.io) - Container exploration (logs, health, terminal access)  
**Tailscale** - [Prod](https://tailscale.infra.atitlan.io) - Magic DNS (dynamic -> static ips)

## Contributing

Check out our issues. Be a good neighbour: cleanup, refactor, leave helpful readmes, ci stages and makefile commands.  

For ui: headless ui components only, tailwind styling and follow the design as close as possible.

## 🎱 Version bumping apps 🎱

Major.Feature.Fix

- (x.0.0) - Major - major releases after large epics or breaking changes in an api requiring consumers to change the way it is used
- (0.x.0) - Feature - features added to the project
- (0.0.x) - Fix - Fixes and iterations upon the same feature set

Node projects
Stored in package.json, see api or ui for examples in the swagger file on how to use it

## 🪩 Git branching 🪩

- Start a merge request via an issue and create a branch via gitlab
- Rename to: feature- (0.x.0) or fix- (0.0.x) #issuenumber-title
- Change the version number based on the version bumping instructions
- Ask main contributors for a review.

## 🥽 Depencies 🥽

Run [setup-depencies.sh](./infrastructure/setup-depencies.sh) `./infrastructure/setup-depencies.sh` for taking care of most if not all the depencies of this project (docker, npm, node, nvm, pnpm, chrony, zsh with autocomplete, syntax highlight, etc.).  
**Note:** this will override and reinstall your `zsh` if that is something you are already using. If so, just copy paste the commands you need.

## Inspecting containers

Most of this project is container based. We have [portainer](./infrastructure/portainer/portainer.stack.yml) to inspect the logs of the running containers. Yes you can run it locally too! Start a local swarm and run [add-domains](/infrastructure/add-domains.sh) to fake out your host file to point to the local adresses. That way you can fully spin up a local cluster.  

### Docker desktop - Local

[Install docker desktop for linux](https://docs.docker.com/desktop/install/linux-install/#generic-installation-steps>)
