# 🚀 Swarm Node Configuration Guide

This section provides instructions on setting up and expanding your Docker Swarm environment. It covers the creation of the initial swarm manager, adding worker nodes, and the implications of node types and network security.

## 🛠 Setting Up New Nodes

On new nodes, before joining them to the swarm or creating the initial swarm host, ensure that they are properly configured with necessary software and settings:

- **Run `setup-depencies.sh`:** [This script](./infrastructure/setup-depencies.sh) automates the installation of Docker, a customized Zsh terminal, Node.js, pnpm, and Chrony for precise time syncing. It also sets the appropriate timezone.

   ```bash
   curl -sSL https://gitlab.com/aiodevs/atitlan-io/-/raw/main/infrastructure/setup-depencies.sh | sh
   ```

- **Name the node's hostname**

    `hostnamectl hostname <domain>-<environment>-<gateway>-<Manager/Worker>-<name>`  

    *gateway: the gateway node that is acts as the access point to our private docker networks.  

    **Changing the hostname:**  
    `hostnamectl hostname <name>` to set the hostname and `reboot`.  
    If that doesnt work check out `sudo nano /boot/firmware/user-data`  

    **Example name manager**  
    So for the initial swarm manager node that is also a gateway it could be:  
    `aio-prod-gateway-manager-mysticpulse`  

    **Example name worker**  
    `aio-test-worker-greentara`  

    **Transformative names**  
    For our project you are encouraged to use names that invite positive powerful transformative energy to the project.  

## 🌐 Setting Up the Initial Swarm Manager

The initial swarm host must be configured with a static IP and should have a DNS A record pointing to it. This is essential for reliable and consistent access within your network.

1. **Create the Initial Swarm Host:**

   ```bash
   docker swarm init --advertise-addr <static-ip>
   ```

   Replace `<static-ip>` with the public static IP address or url of the intial swarm host, which is crucial for the node to function correctly as the manager.  

2. **Setup infrastructure**
 
    Deploy at least the following on the initial swarm host:
   1. **Traefik**
   2. **Headscale**  

    Have a look at the [infrastructure guide here for a more detailed instruction](README-Infrastructure.md).

## 🔗 Joining Worker and Manager Nodes

Once your initial swarm manager is set up with traefik and headscale, you can add worker nodes to the swarm using the join token. Similarly, additional managers can be added to enhance fault tolerance and management capability.

1. **Retrieve the Join Token for Workers:**

   ```bash
   docker swarm join-token worker
   ```

   This command will output the full command to run on the worker nodes to join them to the swarm.

2. **Retrieve the Join Token for Managers:**

   ```bash
   docker swarm join-token manager
   ```

   **Requires authenticated tailscale client.**  
   This command provides the necessary command to join additional managers to the swarm.  
   Make sure to add the `--advertise-addr` with the tailscale ip. Again for more detail about headscale and tailscale see the [infrasctructure guide](README-Infrastructure.md).

### Caveats with Dynamic IPs on Manager Nodes

Attempting to join a swarm as a manager with a dynamic IP is generally infeasible due to the need for stable, consistent communication endpoints among managers. We generally avoid exposing direct connections to the open internet:

- **Firewall Security:** To maintain robust security without opening unnecessary ports in your firewall, we utilize Tailscale. This setup avoids poking holes in the firewall, leveraging secure tunnels instead.

- **Use of Tailscale:** With Tailscale, there's no need to modify firewall configurations drastically, as it provides a secure and private mesh network.

- **Joining Nodes with Tailscale:**
   Ensure that the `--advertise-addr` flag is set to the static tailscale ip when joining new managers. This address should be reachable over the Tailscale network.


Remember, managing your swarm effectively requires careful consideration of network settings and node capabilities. Ensure that each node, especially a manager node, has a reliable and secure connection within the swarm. 🌍🔒
